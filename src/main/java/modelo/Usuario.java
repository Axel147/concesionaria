package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.TipoUsuarioDAO;
import persistencia.dao.interfaz.UsuarioDAO;

public class Usuario {
	
	private UsuarioDAO usuario;
	private TipoUsuarioDAO tipoUsuario;
	
	public Usuario(DAOAbstractFactory metodo_persistencia)
	{
		this.usuario = metodo_persistencia.createUsuarioDAO();
		this.tipoUsuario = metodo_persistencia.createTipoUsuarioDAO();
	}
	
	public boolean agregarUsuario(UsuarioDTO nuevausuario)
	{
		return this.usuario.insert(nuevausuario);
	}

	public boolean borrarUsuario(UsuarioDTO usuario_a_eliminar) 
	{
		return this.usuario.delete(usuario_a_eliminar);
	}
	
//	public List<UsuarioDTO> buscarUsuario(int campo, int condicion, String valor) {
//
//		return this.usuario.search(campo,condicion,valor);
//		
//	}
	
	public  List<UsuarioDTO> filtrarUsuarios(List<Triple<Integer,Integer,String>> filtrosBusqueda){
		return this.usuario.search(filtrosBusqueda);
	}
	
	public List<UsuarioDTO> obtenerUsuarios()
	{
		return this.usuario.readAll();		
	}
	
	public boolean editarUsuario(UsuarioDTO usuario_a_editar) {
		
		return this.usuario.update(usuario_a_editar);
	}
	
	public List<TipoUsuarioDTO> obtenerTiposUsuario(){
		
		return this.tipoUsuario.readAll();
		
	}
	
//	public UsuarioDTO buscarPorDocumento(int idTipoDoc, String numDoc) {
//		return this.usuario.searchDoc(idTipoDoc,numDoc);
//	}

	public Triple<Integer, TipoUsuarioDTO, SucursalDTO> logearUsuario(String usuario, char[] password){
		return this.usuario.getUsuarioLoging(usuario, password);
	}
	
	
}
