package modelo;

import java.util.List;

import dto.MedioPagoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.MedioPagoDAO;

public class MedioPago {
	private MedioPagoDAO medioPago;
	
	public MedioPago (DAOAbstractFactory metodo_persistencia)
	{
		this.medioPago = metodo_persistencia.createMedioPagoDAO();
	}
	
	public List<MedioPagoDTO> obtenerMediosPago()
	{
		return this.medioPago.readAll();
	}

}
