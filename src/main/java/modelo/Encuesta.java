package modelo;

import dto.EncuestaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.EncuestaDAO;

public class Encuesta {

	private EncuestaDAO encuesta;
	
	public Encuesta(DAOAbstractFactory metodo_persistencia)
	{
		this.encuesta = metodo_persistencia.createEncuestaDAO();
	}
	
	public boolean guardarEncuesta(EncuestaDTO nuevaEncuesta)
	{
		return this.encuesta.insert(nuevaEncuesta);
	}
	
}
