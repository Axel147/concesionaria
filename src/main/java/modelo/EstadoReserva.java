package modelo;

import java.util.List;

import dto.EstadoReservaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.EstadoReservaDAO;

public class EstadoReserva {
	
	private EstadoReservaDAO estado;
	
	public EstadoReserva (DAOAbstractFactory metodo_persistencia)
	{
		this.estado = metodo_persistencia.createEstadoReservaDAO();
	}
	
	public List<EstadoReservaDTO> obtenerEstados()
	{
		return this.estado.readAllEstados();		
	}
}
