package modelo;

import java.util.List;

import dto.OpcionPagoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.ModeloDAO;
import persistencia.dao.interfaz.OpcionPagoDAO;

public class OpcionPago {
private OpcionPagoDAO opcionPago;
	
	public OpcionPago (DAOAbstractFactory metodo_persistencia)
	{
		this.opcionPago= metodo_persistencia.createOpcionPagoDAO();
	}
	
	public List<OpcionPagoDTO> obtenerOpcionPago(int idMedioPago)
	{
		return this.opcionPago.readAll(idMedioPago);
	}
	
}
