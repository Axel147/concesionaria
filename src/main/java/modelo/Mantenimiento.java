package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.MantenimientoDTO;
import dto.TipoAutoparteDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.MantenimientoDAO;

public class Mantenimiento {
private MantenimientoDAO mantenimiento;
	
	public Mantenimiento (DAOAbstractFactory metodo_persistencia) {
		this.mantenimiento= metodo_persistencia.createMantenimientoDAO();
	}
	
	public boolean agregarMantenimiento(MantenimientoDTO nuevoMantenimiento) {
		return this.mantenimiento.insert(nuevoMantenimiento);		
	}
	
	public List<MantenimientoDTO> obtenerMantenimientos() {
		return this.mantenimiento.readAll();		
	}
	
	public List<Pair<AutoparteDTO, Integer>> obtenerAutopartesUtilizadas(MantenimientoDTO mantenimientoSeleccionado){
		return this.mantenimiento.readAutopartesXmantenimiento(mantenimientoSeleccionado);
	}
	
	public boolean finalizarMantenimiento(MantenimientoDTO mantenimientoSeleccionado){
		return this.mantenimiento.finalizarMantenimiento(mantenimientoSeleccionado);
	}
	
	public boolean agregarAutoparteXmantenimiento(MantenimientoDTO mantenimientoSeleccionado, AutoparteDTO autoparte, Integer cantidad){
		return this.mantenimiento.insertAutoparteXmantenimiento(mantenimientoSeleccionado, autoparte, cantidad);
	}
	
	public boolean eliminarAutoparteXmantenimiento(AutoparteDTO autoparteXmantenimientoAEliminar, int cantidad){
		return this.mantenimiento.deleteAutoparteXmantenimiento(autoparteXmantenimientoAEliminar, cantidad);
	}
	
	public List<Pair<TipoAutoparteDTO, Integer>> obtenerTipoAutopartesEnGarantia(MantenimientoDTO mantenimientoSeleccionado){
		return this.mantenimiento.readTipoAutopartesEnGarantia(mantenimientoSeleccionado);
	}
	
	public boolean modificarManoDeObra(MantenimientoDTO mantenimientoSeleccionado){
		return this.mantenimiento.updateManoDeObra(mantenimientoSeleccionado);
	}

	public List<MantenimientoDTO> filtrarMantenimientos(List<Triple<Integer, Integer, String>> filtrosBusqueda) {
		return this.mantenimiento.search(filtrosBusqueda);
	}
	
	public boolean esDeMantenimiento(int idVenta){
		return this.mantenimiento.esDeMantenimiento(idVenta);
	}
	
}
