package modelo;

import java.util.List;

import dto.GarantiaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.GarantiaDAO;

public class Garantia {
private GarantiaDAO garantia;
	
	public Garantia (DAOAbstractFactory metodo_persistencia) {
		this.garantia= metodo_persistencia.createGarantiaDAO();
	}
	
	public List<GarantiaDTO> obtenerGarantia() {
		return this.garantia.readAll();		
	}
	
}
