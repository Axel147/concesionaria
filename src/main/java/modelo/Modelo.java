package modelo;

import java.util.List;

import dto.ModeloDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.ModeloDAO;

public class Modelo {
private ModeloDAO modelo;
	
	public Modelo (DAOAbstractFactory metodo_persistencia)
	{
		this.modelo= metodo_persistencia.createModeloDAO();
	}
	
	public List<ModeloDTO> obtenerModelo(int idMarca)
	{
		return this.modelo.readAll(idMarca);		
	}
	
	public List<ModeloDTO> buscarModeloPor(String dato, int campo){
		return this.modelo.readAllPor(dato, campo);		
	}
	
	public boolean agregarModelo(ModeloDTO nuevoModelo) 
	{
		return this.modelo.insert(nuevoModelo);
	}
	
	
	public boolean editarModelo(ModeloDTO modeloModificado) {
		return this.modelo.update(modeloModificado);
	}
	
	
	public boolean borrarModelo(ModeloDTO sucursal_a_eliminar) 
	{
		return this.modelo.delete(sucursal_a_eliminar);
	}
	
}
