package modelo;

import java.util.List;

import dto.PaisDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.PaisDAO;

public class Pais {
	
	private PaisDAO pais;
	
	public Pais (DAOAbstractFactory metodo_persistencia)
	{
		this.pais = metodo_persistencia.createPaisDAO();
	}
	
	public List<PaisDTO> obtenerPais()
	{
		return this.pais.readAllPais();		
	}
}
