package modelo;

import java.util.List;

import dto.MarcaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.MarcaDAO;

public class Marca {
	private MarcaDAO marca;
	
	public Marca (DAOAbstractFactory metodo_persistencia) {
		this.marca= metodo_persistencia.createMarcaDAO();
	}
	
	public List<MarcaDTO> obtenerMarca() {
		return this.marca.readAll();		
	}
	
	public boolean agregarMarca(MarcaDTO nuevaMarca) {
		return this.marca.insert(nuevaMarca);
	}
	
	public boolean editarMarca(MarcaDTO marcaModificada) {
		return this.marca.update(marcaModificada);
	}
	
	public boolean borrarMarca(MarcaDTO marca_a_eliminar) {
		return this.marca.delete(marca_a_eliminar);
	}

	public List<MarcaDTO> obtenerMarcasPor(String dato) {
		return this.marca.readAllMatchDatMarca(dato);
	}

}
