package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.ReservaDTO;
import persistencia.dao.interfaz.ReservaDAO;
import persistencia.dao.interfaz.DAOAbstractFactory;

public class Reserva {
	private ReservaDAO reserva;
	
	public Reserva (DAOAbstractFactory metodo_persistencia)
	{
		this.reserva = metodo_persistencia.createReservaDAO();
	}
	
	public List<ReservaDTO> obtenerReservas()
	{
		return this.reserva.readAll();		
	}
	
//	public List<ReservaDTO> obtenerReservasPor(String dato, String campo)
//	{
//		return this.reserva.readAllMatchDatReserve(dato, campo);		
//	}
	
	public List<ReservaDTO> filtrarReservas(List<Triple<Integer,Integer,String>> filtros){
		return this.reserva.search(filtros);
	}
	
	public boolean agregarReserva(ReservaDTO nuevoReserva) {
		return this.reserva.insert(nuevoReserva);
	}
	
	public boolean editarReserva(ReservaDTO reservaModificada) {
		return this.reserva.update(reservaModificada);
	}
	
	public boolean cancelarReserva(ReservaDTO reserva_a_cancelar, String motivo) 
	{
		return this.reserva.cancelar(reserva_a_cancelar, motivo);
	}
	
	public List<ReservaDTO> obtenerReservasParaNotificar(){
		return this.reserva.readAllReservasNoNotificadas();
	}

	public Pair<String, String> dameEmail() {
		return this.reserva.readEmail();
	}

	public boolean finalizarReservasVencidas() {
		return this.reserva.updateVencidas();
	}
}
