package modelo;

import java.util.List;

import dto.SucursalDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.SucursalDAO;

public class Sucursal {
	private SucursalDAO sucursal;
	
	public Sucursal (DAOAbstractFactory metodo_persistencia)
	{
		this.sucursal = metodo_persistencia.createSucursalDAO();
	}
	
	public List<SucursalDTO> obtenerSucursales()
	{
		return this.sucursal.readAll();		
	}
	
	public List<SucursalDTO> obtenerSucursales(int id)
	{
		return this.sucursal.readAll(id);
	}
	
	public List<SucursalDTO> obtenerSucursalPor(String dato, int campo)
	{
		return this.sucursal.readAllMatchDatSucursal(dato, campo);		
	}
	
	public boolean agregarSucursal(SucursalDTO nuevaSucursal) {
		return this.sucursal.insert(nuevaSucursal);
	}
	
	public boolean editarSucursal(SucursalDTO sucursalModificada) {
		return this.sucursal.update(sucursalModificada);
	}
	
	public boolean borrarSucursal(SucursalDTO sucursal_a_eliminar) 
	{
		return this.sucursal.delete(sucursal_a_eliminar);
	}
	
}
