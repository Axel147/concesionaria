package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.TipoAutoparteDTO;
import dto.AutoparteDTO;
//import dto.ModeloDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
//import persistencia.dao.interfaz.ModeloAutoDAO;
import persistencia.dao.interfaz.TipoAutoparteDAO;
import persistencia.dao.interfaz.AutoparteDAO;

public class Autoparte {

	private AutoparteDAO autoparte;
	private TipoAutoparteDAO tipoAutoparte;
//	private ModeloAutoDAO modeloAuto;
	
	public Autoparte(DAOAbstractFactory metodo_persistencia)
	{
		this.autoparte = metodo_persistencia.createAutoparteDAO();
		this.tipoAutoparte = metodo_persistencia.createTipoAutoparteDAO();
//		this.modeloAuto = metodo_persistencia.createModeloAutoDAO();
	}
	
	public boolean agregarAutoparte(AutoparteDTO nuevaautoparte)
	{
		return this.autoparte.insert(nuevaautoparte);
	}

	public boolean borrarAutoparte(AutoparteDTO autoparte_a_eliminar) 
	{
		return this.autoparte.delete(autoparte_a_eliminar);
	}
	
	public List<AutoparteDTO> obtenerAutopartes()
	{
		return this.autoparte.readAll();		
	}
	
	public boolean editarAutoparte(AutoparteDTO autoparte_a_editar) {
		
		return this.autoparte.update(autoparte_a_editar);
	}
	
	public List<TipoAutoparteDTO> obtenerTiposAutoparte(){
		
		return this.tipoAutoparte.readAll();
		
	}
	
//	public List<ModeloAutoDTO> obtenerModelosAuto(){
//		
//		return this.modeloAuto.readAll();
//		
//	}
	
	public List<AutoparteDTO> buscarAutopartes(List<Triple<Integer,Integer,String>> filtrosBusqueda){
		
		return this.autoparte.search(filtrosBusqueda);
		
	}
	
	public List<AutoparteDTO> autopartesFaltaStock(){
		return this.autoparte.readAllFaltaStock();
	}

	public List<AutoparteDTO> obtenerAutopartesParaMantenimiento() {
		return this.autoparte.readAllParaMantenimiento();		
	}

	public List<AutoparteDTO> obtenerAutopartesDeSucursal(int idSucursal) {
		return this.autoparte.readAllDeSucursal(idSucursal);		
	}
	
}
