package modelo;

import java.util.List;

import dto.TipoDocumentoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.TipoDocumentoDAO;

public class  TipoDocumento{
	private TipoDocumentoDAO tipoDocumento;
	
	public TipoDocumento (DAOAbstractFactory metodo_persistencia)
	{
		this.tipoDocumento = metodo_persistencia.createTipoDocumentoDAO();
	}
	
	public List<TipoDocumentoDTO> obtenerTiposDocumento(int paisdocumento)
	{
		return this.tipoDocumento.readAllTipoDocumento(paisdocumento);		
	}
}
