package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.ClienteDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.ClienteDAO;

public class Cliente{
	private ClienteDAO cliente;
	
	public Cliente (DAOAbstractFactory metodo_persistencia)
	{
		this.cliente = metodo_persistencia.createClienteDAO();
	}
	
	public List<ClienteDTO> obtenerClientes()
	{
		return this.cliente.readAll();		
	}
	
	public List<ClienteDTO> obtenerClientesPor(String dato, int campo)
	{
		return this.cliente.readAllMatchDatClient(dato, campo);		
	}
	
	public boolean agregarCliente(ClienteDTO nuevoCliente) {
		return this.cliente.insert(nuevoCliente);
	}
	
	public boolean editarCliente(ClienteDTO clienteModificado) {
		return this.cliente.update(clienteModificado);
	}
	
	public boolean borrarCliente(ClienteDTO cliente_a_eliminar) 
	{
		return this.cliente.delete(cliente_a_eliminar);
	}
	
	public List<ClienteDTO> filtrarClientes(List<Triple<Integer,Integer,String>> filtrosBusqueda){
		return this.cliente.search(filtrosBusqueda);
	}
}
