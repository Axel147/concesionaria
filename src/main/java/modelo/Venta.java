package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.MantenimientoDTO;
import dto.MarcaDTO;
import dto.VentaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.VentaDAO;

public class Venta {
	private VentaDAO venta;
	
	public Venta (DAOAbstractFactory metodo_persistencia)
	{
		this.venta = metodo_persistencia.createVentaDAO();
	}
	
	public boolean agregarVenta(VentaDTO nuevaVenta) {
		return this.venta.insert(nuevaVenta);
	}
	
	public boolean agregarVentaMantenimiento(VentaDTO nuevaVenta, MantenimientoDTO mantenimiento){
		return this.venta.insertVentaMantenimiento(nuevaVenta, mantenimiento);
	}

	public List<VentaDTO> obtenerVentas() {
		return this.venta.readAll();
	}

	public boolean actualizarEstado(VentaDTO venta_a_actualizar) {
		return this.venta.update(venta_a_actualizar);
	}

	public List<VentaDTO> filtrarVentas(List<Triple<Integer, Integer, String>> filtrosBusqueda) {
		return this.venta.search(filtrosBusqueda);
	}
	
	/*
	public List<VentaDTO> obtenerVenta()
	{
		return this.venta.readAll();
	}
	*/
}
