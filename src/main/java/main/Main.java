package main;

import javax.swing.UIManager;

import modelo.Automovil;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.Encuesta;
import modelo.AutomovilTaller;
import modelo.EstadoReserva;
import modelo.Garantia;
import modelo.Mantenimiento;
import modelo.Pais;
import modelo.Reserva;
import modelo.Usuario;
import modelo.Venta;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Marca;
import modelo.MedioPago;
import modelo.Modelo;
import modelo.OpcionPago;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorListaDeVentas;
import presentacion.controlador.ControladorMantenimiento;
import presentacion.controlador.ControladorAlarmaStock;
import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorEncuesta;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReporte;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.ControladorVenta;
import presentacion.controlador.Notificador;

public class Main {

	public static void main(String[] args) 
	{
		try{
			UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
			Cliente cliente = new Cliente(new DAOSQLFactory());
			Usuario usuario = new Usuario(new DAOSQLFactory());
			Sucursal sucursal = new Sucursal(new DAOSQLFactory());
			Autoparte autoparte = new Autoparte(new DAOSQLFactory());
			Pais pais = new Pais(new DAOSQLFactory());
			TipoDocumento tipoDocumento = new TipoDocumento(new DAOSQLFactory());
			Marca marca = new Marca(new DAOSQLFactory());
			Modelo modelo = new Modelo(new DAOSQLFactory());
			Automovil automovil = new Automovil(new DAOSQLFactory());
			Reserva reserva = new Reserva(new DAOSQLFactory());
			EstadoReserva estadoReserva = new EstadoReserva(new DAOSQLFactory());
			AutomovilTaller autoTaller= new AutomovilTaller(new DAOSQLFactory());
			ControladorCliente cc = new ControladorCliente(cliente, pais, tipoDocumento);
			ControladorUsuario cu = new ControladorUsuario(usuario, pais, tipoDocumento,sucursal);
			ControladorSucursal cs = new ControladorSucursal(sucursal, pais);
			ControladorStockAutomoviles csa = new ControladorStockAutomoviles(automovil, marca, modelo, sucursal);
			ControladorReserva cr = new ControladorReserva(reserva, estadoReserva, autoTaller, pais, tipoDocumento, marca, modelo);
			ControladorAutopartes ca = new ControladorAutopartes(autoparte, sucursal, marca, modelo);
			ControladorMarca cmarca = new ControladorMarca(marca,modelo);
			ControladorModelo cmodel = new ControladorModelo(modelo, marca);
			ControladorReporte creporte = new ControladorReporte();
			Notificador notificador = new Notificador(reserva);
			ControladorAlarmaStock cas = new ControladorAlarmaStock(autoparte, automovil);
			//ControladorGeneral controladorGeneral = new ControladorGeneral(cc, cu, cs, cr, csa, ca, cmodel, cmarca, notificador,cas);
			//----------------------------------------------------------------------------------------------------------------------------------
			MedioPago medioPago= new MedioPago(new DAOSQLFactory());
			OpcionPago opcionPago= new OpcionPago(new DAOSQLFactory());
			Venta venta = new Venta(new DAOSQLFactory());
			Garantia garantia = new Garantia(new DAOSQLFactory());
			ControladorVenta cVenta = new ControladorVenta(medioPago, opcionPago, venta, garantia);
			
			Mantenimiento mantenimiento = new Mantenimiento(new DAOSQLFactory());
			ControladorMantenimiento cm = new ControladorMantenimiento(mantenimiento);
			
			Encuesta encuesta = new Encuesta(new DAOSQLFactory());
			ControladorEncuesta ce = new ControladorEncuesta(encuesta);
			ControladorListaDeVentas clv = new ControladorListaDeVentas(venta);

			ControladorGeneral controladorGeneral = new ControladorGeneral(cc, cu, cs, cr, csa, ca, cmodel, cmarca, notificador,cas,cVenta, cm, creporte, ce, clv);

			controladorGeneral.verificarConexion();
			
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
