package persistencia.conexion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

//import presentacion.vista.ListaDeClientes;





public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);
	
	
	private Conexion()
	{
		conectar();
	}



	private void conectar() {
		Properties datosConexion = cargarConfiguracion();
		
		String ip = datosConexion.getProperty("ip");
		String puerto = datosConexion.getProperty("puerto");
		String usuario = datosConexion.getProperty("usuario");
		String password = datosConexion.getProperty("password");
		
		String url = "jdbc:mysql://" + ip + ":" + puerto + "/multicar";
	

				
		try
		{
			
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			this.connection = DriverManager.getConnection(url,usuario,password);
			this.connection.setAutoCommit(false);
			log.info("Conexión exitosa");
		}
		catch(Exception e)
		{
			log.error("Conexión fallida", e);
		}
	}



	private Properties cargarConfiguracion() {
		Properties datosConexion = new Properties();
		try {
			datosConexion.load(new FileInputStream(System.getProperty("user.dir") +"/DatosConexion.txt"));
		} catch (FileNotFoundException e1) {
			log.error("El archivo de configuración no ha sido encontrado",e1);
		} catch (IOException e1) {
			log.error("Falla de E/S",e1);
		}
		return datosConexion;
	}
	

	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void actualizarDatosConexion(String ip, String puerto, String usuario, String password) {
		
		Properties datosConexion = cargarConfiguracion();
		
		datosConexion.setProperty("ip", ip);
		datosConexion.setProperty("puerto", puerto);
		datosConexion.setProperty("usuario", usuario);
		datosConexion.setProperty("password", password);
		
		guardarConfiguracion(datosConexion);
		conectar();
		
	}



	private void guardarConfiguracion(Properties datosConexion) {
		try {
			FileOutputStream archivoDatos = new FileOutputStream(System.getProperty("user.dir") + "/DatosConexion.txt");
			datosConexion.store(archivoDatos, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
}