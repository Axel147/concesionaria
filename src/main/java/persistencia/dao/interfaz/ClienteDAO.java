package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.ClienteDTO;

public interface ClienteDAO 
{
	
	public boolean insert(ClienteDTO cliente);

	public boolean delete(ClienteDTO cliente_a_eliminar);
	
	public boolean update(ClienteDTO clienteModificado);
	
	public List<ClienteDTO> readAll();
	
	public List<ClienteDTO> readAllMatchDatClient(String dato, int campo);
	
	public List<ClienteDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda);
	
}
