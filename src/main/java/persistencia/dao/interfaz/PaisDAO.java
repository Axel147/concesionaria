package persistencia.dao.interfaz;

import java.util.List;


import dto.PaisDTO;


public interface PaisDAO {

	public List<PaisDTO> readAllPais();
	
	public PaisDTO buscarPorId(int id);
	
}
