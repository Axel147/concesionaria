package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.MantenimientoDTO;
import dto.TipoAutoparteDTO;

public interface MantenimientoDAO 
{
	
	public boolean insert(MantenimientoDTO mantenimiento);
	
	public List<MantenimientoDTO> readAll();

	public List<Pair<AutoparteDTO, Integer>> readAutopartesXmantenimiento(MantenimientoDTO mantenimientoSeleccionado);
	
	public boolean finalizarMantenimiento(MantenimientoDTO mantenimientoSeleccionado);
	
	public boolean insertAutoparteXmantenimiento(MantenimientoDTO mantenimientoSeleccionado, AutoparteDTO autoparte, Integer cantidad);

	public boolean deleteAutoparteXmantenimiento(AutoparteDTO autoparteXmantenimientoAEliminar, int cantidad);
	
	public List<Pair<TipoAutoparteDTO, Integer>> readTipoAutopartesEnGarantia(MantenimientoDTO mantenimientoSeleccionado);

	public boolean updateManoDeObra(MantenimientoDTO mantenimientoSeleccionado);

	public List<MantenimientoDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda);

	public boolean esDeMantenimiento(int idVenta);
}
