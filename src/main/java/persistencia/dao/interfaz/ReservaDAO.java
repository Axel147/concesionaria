package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.ReservaDTO;

public interface ReservaDAO {
	public boolean insert(ReservaDTO reserva);

	public boolean cancelar(ReservaDTO reserva_a_cancelar, String motivo);
	
	public boolean update(ReservaDTO reservaModificada);
	
	public List<ReservaDTO> readAll();
	
//	public List<ReservaDTO> readAllMatchDatReserve(String dato, String campo);
	
	public List<ReservaDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda);

	public List<ReservaDTO> readAllReservasNoNotificadas();

	public Pair<String, String> readEmail();

	public boolean updateVencidas();
}
