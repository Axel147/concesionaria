package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;

public interface UsuarioDAO {
	
	public boolean insert(UsuarioDTO usuario);

	public boolean delete(UsuarioDTO usuario_a_eliminar);
	
	public boolean update(UsuarioDTO usuario_a_editar);
	
	public List<UsuarioDTO> readAll();

//	public List<UsuarioDTO> search(int campo, int condicion, String valor);

//	public UsuarioDTO searchDoc(int idTipoDoc, String numDoc);
	
	public Triple<Integer, TipoUsuarioDTO, SucursalDTO> getUsuarioLoging(String usuario, char[]password);

	public List<UsuarioDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda);
}
