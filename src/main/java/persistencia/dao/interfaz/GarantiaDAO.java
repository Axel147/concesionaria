package persistencia.dao.interfaz;

import java.util.List;

import dto.GarantiaDTO;

public interface GarantiaDAO {
	public List<GarantiaDTO> readAll();
}
