package persistencia.dao.interfaz;

import java.util.List;

import dto.AutomovilTallerDTO;

public interface AutomovilTallerDAO {
//	
//	public boolean insert(AutomovilTallerDTO automovil);
//
//	public boolean delete(AutomovilTallerDTO automovil_a_eliminar);
//	
	public AutomovilTallerDTO readAllIfExist(AutomovilTallerDTO automovil);
	
}
