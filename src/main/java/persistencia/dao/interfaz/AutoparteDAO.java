package persistencia.dao.interfaz;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;


public interface AutoparteDAO {

	public boolean insert(AutoparteDTO autoparte);

	public boolean delete(AutoparteDTO autoparte_a_eliminar);
	
	public boolean update(AutoparteDTO autoparte_a_editar);
	
	public List<AutoparteDTO> readAll();

//	public List<AutoparteDTO> search(int campo, String valor);
	
	public List<AutoparteDTO> readAllFaltaStock();

	public List<AutoparteDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda);

	public List<AutoparteDTO> readAllParaMantenimiento();

	public List<AutoparteDTO> readAllDeSucursal(int idSucursal);

	
}
