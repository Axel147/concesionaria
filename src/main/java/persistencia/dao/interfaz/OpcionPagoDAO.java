package persistencia.dao.interfaz;

import java.util.List;

import dto.OpcionPagoDTO;

public interface OpcionPagoDAO {
	
	public List<OpcionPagoDTO> readAll(int idMedioPago);

	
}
