package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoAutoparteDTO;

public interface TipoAutoparteDAO {

	public List<TipoAutoparteDTO> readAll();
	
	public TipoAutoparteDTO buscarPorId(int id);
}
