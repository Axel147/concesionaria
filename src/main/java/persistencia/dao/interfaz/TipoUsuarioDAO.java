package persistencia.dao.interfaz;
import java.util.List;

import dto.TipoUsuarioDTO;


public interface TipoUsuarioDAO {

	public List<TipoUsuarioDTO> readAll();
	
	public TipoUsuarioDTO buscarPorId(int id);
	
	
}
