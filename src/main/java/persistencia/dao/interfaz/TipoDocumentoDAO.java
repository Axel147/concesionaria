package persistencia.dao.interfaz;

import java.util.List;


import dto.TipoDocumentoDTO;

public interface TipoDocumentoDAO {
	
	public List<TipoDocumentoDTO> readAllTipoDocumento(int paisdocumento);
	
	public TipoDocumentoDTO buscarPorId(int id);

	
}
