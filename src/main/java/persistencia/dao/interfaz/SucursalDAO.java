package persistencia.dao.interfaz;

import java.util.List;


import dto.SucursalDTO;

public interface SucursalDAO {
	
	public boolean insert(SucursalDTO sucursal);

	public boolean delete(SucursalDTO sucursal_a_eliminar);
	
	public boolean update(SucursalDTO sucursalModificada);
	
	public List<SucursalDTO> readAll();
	
	public List<SucursalDTO> readAll(int id);
	
	public List<SucursalDTO> readAllMatchDatSucursal(String dato, int campo);
	
	public SucursalDTO buscarPorId(int id);
	
}
