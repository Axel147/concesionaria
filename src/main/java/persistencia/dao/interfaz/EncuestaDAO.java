package persistencia.dao.interfaz;

import dto.EncuestaDTO;

public interface EncuestaDAO {

	boolean insert(EncuestaDTO nuevaEncuesta);

}
