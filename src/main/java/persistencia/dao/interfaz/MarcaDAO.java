package persistencia.dao.interfaz;

import java.util.List;

import dto.MarcaDTO;

public interface MarcaDAO {
	
	public boolean insert(MarcaDTO nuevaMarca);

	public boolean delete(MarcaDTO marca_a_eliminar);
	
	public boolean update(MarcaDTO marcaAModificar);
			
	public List<MarcaDTO> readAll();

	public List<MarcaDTO> readAllMatchDatMarca(String dato);

}
