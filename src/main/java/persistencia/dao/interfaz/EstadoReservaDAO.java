package persistencia.dao.interfaz;

import java.util.List;


import dto.EstadoReservaDTO;

public interface EstadoReservaDAO {

	public List<EstadoReservaDTO> readAllEstados();
	
}
