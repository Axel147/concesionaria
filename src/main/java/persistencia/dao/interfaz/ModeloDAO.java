package persistencia.dao.interfaz;

import java.util.List;

import dto.ModeloDTO;

public interface ModeloDAO {

	public boolean insert(ModeloDTO nuevoModelo);

	public boolean delete(ModeloDTO modelo_a_eliminar);
	
	public boolean update(ModeloDTO marcaAModificar);
	
	public List<ModeloDTO> readAll(int idMarca);

	public List<ModeloDTO> readAllPor(String dato, int campo);

}
