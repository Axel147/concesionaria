package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.MantenimientoDTO;
import dto.VentaDTO;

public interface VentaDAO {
	
	public boolean insert(VentaDTO nuevaVenta);
	
	public boolean insertVentaMantenimiento(VentaDTO nuevaVenta, MantenimientoDTO mantenimiento);

	public List<VentaDTO> readAll();

	public boolean update(VentaDTO venta_a_actualizar);

	public List<VentaDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda);
	
	//public List<VentaDTO> readAll();
}
