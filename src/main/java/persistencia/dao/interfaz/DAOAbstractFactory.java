package persistencia.dao.interfaz;


public interface DAOAbstractFactory 
{
	public ClienteDAO createClienteDAO();

	public UsuarioDAO createUsuarioDAO();

	public TipoUsuarioDAO createTipoUsuarioDAO();
	
	public SucursalDAO createSucursalDAO();
	
	public PaisDAO createPaisDAO();
	
	public TipoDocumentoDAO createTipoDocumentoDAO();

	public ReservaDAO createReservaDAO();
	
	public EstadoReservaDAO createEstadoReservaDAO();
	
	public AutomovilDAO createAutomovilDAO();

	public MarcaDAO createMarcaDAO();
	
	public ModeloDAO createModeloDAO();
	
	public AutomovilTallerDAO createAutomovilTallerDAO();

	public AutoparteDAO createAutoparteDAO();
	
//	public ModeloAutoDAO createModeloAutoDAO();

	public TipoAutoparteDAO createTipoAutoparteDAO();
	
	public MedioPagoDAO createMedioPagoDAO();

	public OpcionPagoDAO createOpcionPagoDAO();

	public VentaDAO createVentaDAO();
	
	public GarantiaDAO createGarantiaDAO();

	public MantenimientoDAO createMantenimientoDAO();
	
	public EncuestaDAO createEncuestaDAO();

}
