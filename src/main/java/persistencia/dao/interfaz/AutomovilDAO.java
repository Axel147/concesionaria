package persistencia.dao.interfaz;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.StockAutomovilDTO;

public interface AutomovilDAO {
	
	public boolean insert(StockAutomovilDTO automovil);

	public boolean delete(StockAutomovilDTO automovil_a_eliminar);
	
	public boolean update(StockAutomovilDTO automovilModificado);
	
//	public List<StockAutomovilDTO> readAll();

	public List<StockAutomovilDTO> readConModelos(int modelo);
	
	public List<StockAutomovilDTO> readAllFaltaStock();

	public List<StockAutomovilDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda);

	public List<StockAutomovilDTO> readConModelosPorSucursal(int modelo, int idSucursal);
	
}
