package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.EstadoReservaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EstadoReservaDAO;

public class EstadoReservaDAOSQL implements EstadoReservaDAO{

	private static final String readallEstados = "SELECT * FROM estadoreserva WHERE estado != 'Finalizada'";	
	
	public List<EstadoReservaDTO> readAllEstados()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<EstadoReservaDTO> estados = new ArrayList<EstadoReservaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallEstados);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				estados.add(getEstadoReservaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return estados;
	}
	
	private EstadoReservaDTO getEstadoReservaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String estado = resultSet.getString("estado");

		return new EstadoReservaDTO(id, estado);
	}
	
}
