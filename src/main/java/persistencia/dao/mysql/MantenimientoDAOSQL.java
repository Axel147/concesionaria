package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutomovilTallerDTO;
import dto.AutoparteDTO;
import dto.ClienteDTO;
import dto.EstadoReservaDTO;
import dto.MantenimientoDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.MotivoDTO;
import dto.PaisDTO;
import dto.ReservaDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import dto.TipoDocumentoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.MantenimientoDAO;

public class MantenimientoDAOSQL implements MantenimientoDAO{

	
private static final String insertMantenimiento = "INSERT INTO mantenimiento(id, reserva, manoDeObra, finalizado) VALUES(?, ?, ?, ?)";
private static final String insertAutoparteXmantenimiento = "INSERT INTO autopartexmantenimiento(id, mantenimiento, autoparte, cantidadConsumida) VALUES(?, ?, ?, ?)";
private static final String readLastMantenimiento = "Select MAX(id) as id FROM mantenimiento";
private static final String readTipoAutopartesGarantizadas = "SELECT id, tipoautopartegarantizadas.tipoautoparte, tipoautoparte.nombre, cantidad FROM tipoautopartegarantizadas join tipoautoparte on tipoautopartegarantizadas.tipoautoparte = tipoautoparte.idtipoautoparte";
private static final String readAutopartesGarantizadas = "SELECT idautoparte, autoparte.tipo as idTipoAutoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autoparte.year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calle, sucursal.altura as altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE autoparte.tipo = ? AND autoparte.modelo = ? AND autoparte.year = ? AND autoparte.sucursal = 1";
private static final String updateStockAutoparte = "UPDATE autoparte SET stock = ? WHERE idautoparte = ?";
private static final String readall = "SELECT mantenimiento.id as idMantenimiento, reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia, reserva.service as service, mantenimiento.manoDeObra, mantenimiento.finalizado, mantenimiento.venta as idVenta FROM mantenimiento join reserva on mantenimiento.reserva = reserva.id join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id";
private static final String readAutopartesXmantenimiento = "SELECT cantidadConsumida, idautoparte, autoparte.tipo as idTipoAutoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autoparte.year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calle, sucursal.altura as altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autopartexmantenimiento join autoparte on autopartexmantenimiento.autoparte = autoparte.idautoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE mantenimiento = ?";
private static final String finalizarMantenimiento = "UPDATE mantenimiento SET finalizado = ? WHERE id = ?";
private static final String deleteAutoparteXmantenimiento = "DELETE FROM autopartexmantenimiento WHERE autoparte = ?  ";
private static final String readAutopartesXmantenimientoUtilizada = "SELECT id, cantidadConsumida FROM autopartexmantenimiento WHERE mantenimiento = ? AND autoparte = ?  ";
private static final String updateAutopartesXmantenimiento = "UPDATE autopartexmantenimiento SET cantidadConsumida = ? WHERE id = ?  ";
private static final String readAutopartePorID = "SELECT idautoparte, autoparte.tipo as idTipoAutoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autoparte.year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calle, sucursal.altura as altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE idautoparte = ?  ";
private static final String updateFinalizarReserva = "UPDATE reserva SET estado = 2 WHERE id = ?"; 
private static final String updateManoDeObra = "UPDATE mantenimiento SET manoDeObra = ? WHERE id = ?";
private static final String search = "SELECT mantenimiento.id as idMantenimiento, reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia, reserva.service as service, mantenimiento.manoDeObra, mantenimiento.finalizado, mantenimiento.venta as idVenta FROM mantenimiento join reserva on mantenimiento.reserva = reserva.id join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE ";
private static final String[] arrayCampos = {"cliente.nombre","cliente.apellido","cliente.numdocumento","cliente.mail","autosentaller.patente"};
private static final String tieneVenta = "SELECT mantenimiento.id FROM mantenimiento WHERE venta = ?";	
public boolean insert(MantenimientoDTO mantenimiento)
	{
		PreparedStatement statementMantenimiento;//sentencia insert mantenimiento
		PreparedStatement statementAutoparteXmantenimiento;//sentencia insert autopartes que vienen de regalo con la garantia
		PreparedStatement statementLastMantenimiento;
		PreparedStatement statementTipoAutopartesGarantizadas;
		PreparedStatement statementAutopartesGarantizadas;
		PreparedStatement statementRestarStock;
		PreparedStatement statementFinalizarReserva;
		
		ResultSet resulsetLastMantenimiento;
		ResultSet resulsetTipoAutopartesGarantizadas;
		ResultSet resulsetAutopartesGarantizadas;
		
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			
			statementMantenimiento = conexion.prepareStatement(insertMantenimiento);
			statementMantenimiento.setInt(1, mantenimiento.getId());
			statementMantenimiento.setInt(2, mantenimiento.getReserva().getIdReserva());
			statementMantenimiento.setDouble(3, mantenimiento.getManoDeObra());
			statementMantenimiento.setBoolean(4, mantenimiento.isFinalizado());
			
			if(statementMantenimiento.executeUpdate() > 0){
				
				if(mantenimiento.getReserva().isTieneGarantia()){
					
					statementTipoAutopartesGarantizadas = conexion.prepareStatement(readTipoAutopartesGarantizadas);
					resulsetTipoAutopartesGarantizadas = statementTipoAutopartesGarantizadas.executeQuery();
					
					ArrayList<Triple<Integer,Integer,Integer>> list_id_idtipoautoparte_cantidad = new ArrayList<Triple<Integer,Integer,Integer>>();
					while(resulsetTipoAutopartesGarantizadas.next()){
						int idTipoAutoparteGarantizadas = resulsetTipoAutopartesGarantizadas.getInt("id");
						int idTipoAutoparte = resulsetTipoAutopartesGarantizadas.getInt("tipoautoparte");
						int cantidad = resulsetTipoAutopartesGarantizadas.getInt("cantidad");
						Triple<Integer,Integer,Integer> id_idtipoautoparte_cantidad = new ImmutableTriple<Integer,Integer,Integer>(idTipoAutoparteGarantizadas, idTipoAutoparte, cantidad);
						list_id_idtipoautoparte_cantidad.add(id_idtipoautoparte_cantidad);
					}
					
					
					ArrayList<AutoparteDTO> autopartesGarantizadas = new ArrayList<AutoparteDTO>();
					
					for(int i=0; i<list_id_idtipoautoparte_cantidad.size();i++){
						statementAutopartesGarantizadas = conexion.prepareStatement(readAutopartesGarantizadas);
						statementAutopartesGarantizadas.setInt(1, list_id_idtipoautoparte_cantidad.get(i).getMiddle());
						statementAutopartesGarantizadas.setInt(2, mantenimiento.getReserva().getAuto().getModelo().getId());
						statementAutopartesGarantizadas.setString(3, mantenimiento.getReserva().getAuto().getYear());
						resulsetAutopartesGarantizadas  = statementAutopartesGarantizadas.executeQuery();
						if(resulsetAutopartesGarantizadas.next()){
							AutoparteDTO autoparte = getAutoparteDTO(resulsetAutopartesGarantizadas);
							autopartesGarantizadas.add(autoparte);
						}
					}
					
					statementLastMantenimiento = conexion.prepareStatement(readLastMantenimiento);
					resulsetLastMantenimiento = statementLastMantenimiento.executeQuery();
					
					if(resulsetLastMantenimiento.next()){
						int idMantenimiento = resulsetLastMantenimiento.getInt("id");
						boolean cargarAutopartesGarantizadas = true;
						for(int z=0; z<autopartesGarantizadas.size();z++){
							if(autopartesGarantizadas.get(z).getStock()>=list_id_idtipoautoparte_cantidad.get(z).getRight()){
								
								//caso en que haya mas stock de la cantidad que necesitan en taller para garantia
								statementAutoparteXmantenimiento = conexion.prepareStatement(insertAutoparteXmantenimiento);
								statementAutoparteXmantenimiento.setInt(1, 0);
								statementAutoparteXmantenimiento.setInt(2, idMantenimiento);		
								statementAutoparteXmantenimiento.setInt(3, autopartesGarantizadas.get(z).getId());
								statementAutoparteXmantenimiento.setInt(4, list_id_idtipoautoparte_cantidad.get(z).getRight());
								if(!(statementAutoparteXmantenimiento.executeUpdate() > 0)){
									conexion.rollback();
									z=autopartesGarantizadas.size();
									cargarAutopartesGarantizadas = false;
								}else{
									statementRestarStock = conexion.prepareStatement(updateStockAutoparte);
									statementRestarStock.setInt(1, autopartesGarantizadas.get(z).getStock()-list_id_idtipoautoparte_cantidad.get(z).getRight());
									statementRestarStock.setInt(2, autopartesGarantizadas.get(z).getId());
									if(!(statementRestarStock.executeUpdate() > 0)){
										conexion.rollback();
										z=autopartesGarantizadas.size();
										cargarAutopartesGarantizadas = false;
									}
								}
							}else if(autopartesGarantizadas.get(z).getStock()>0){
								//caso en que no alcance el stock pero si haya al menos un elemento
								statementAutoparteXmantenimiento = conexion.prepareStatement(insertAutoparteXmantenimiento);
								statementAutoparteXmantenimiento.setInt(1, 0);
								statementAutoparteXmantenimiento.setInt(2, idMantenimiento);		
								statementAutoparteXmantenimiento.setInt(3, autopartesGarantizadas.get(z).getId());
								statementAutoparteXmantenimiento.setInt(4, autopartesGarantizadas.get(z).getStock());
								if(!(statementAutoparteXmantenimiento.executeUpdate() > 0)){
									conexion.rollback();
									z=autopartesGarantizadas.size();
									cargarAutopartesGarantizadas = false;
								}else{
									statementRestarStock = conexion.prepareStatement(updateStockAutoparte);
									statementRestarStock.setInt(1, 0);
									statementRestarStock.setInt(2, autopartesGarantizadas.get(z).getId());
									if(!(statementRestarStock.executeUpdate() > 0)){
										conexion.rollback();
										z=autopartesGarantizadas.size();
										cargarAutopartesGarantizadas = false;
									}
								}
							}
						}
						
						statementFinalizarReserva = conexion.prepareStatement(updateFinalizarReserva);
						statementFinalizarReserva.setInt(1, mantenimiento.getReserva().getIdReserva());
						if(statementFinalizarReserva.executeUpdate() > 0){
							conexion.commit();
							isInsertExitoso = true && cargarAutopartesGarantizadas;
						}else conexion.rollback();
					}else conexion.rollback();
					
				}
				//camino que no tiene garantia
				statementFinalizarReserva = conexion.prepareStatement(updateFinalizarReserva);
				statementFinalizarReserva.setInt(1, mantenimiento.getReserva().getIdReserva());
				if(statementFinalizarReserva.executeUpdate() > 0){
					conexion.commit();
					isInsertExitoso = true;
				}else conexion.rollback();
			}else conexion.rollback();
			
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public List<MantenimientoDTO> readAll(){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<MantenimientoDTO> mantenimientos = new ArrayList<MantenimientoDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				mantenimientos.add(getMantenimientoDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return mantenimientos;
	}
	
	
	public List<Pair<AutoparteDTO, Integer>> readAutopartesXmantenimiento(MantenimientoDTO mantenimientoSeleccionado){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<Pair<AutoparteDTO, Integer>> autopartesUtilizadas = new ArrayList<Pair<AutoparteDTO, Integer>>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readAutopartesXmantenimiento);
			statement.setInt(1, mantenimientoSeleccionado.getId());
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartesUtilizadas.add(getAutoparteUtilizada(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return autopartesUtilizadas;
	}
	
	public boolean finalizarMantenimiento(MantenimientoDTO mantenimientoSeleccionado){
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isfinalizarExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(finalizarMantenimiento);
			statement.setBoolean(1, true);
			statement.setInt(2, mantenimientoSeleccionado.getId());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isfinalizarExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isfinalizarExitoso;
	}
	
	public boolean insertAutoparteXmantenimiento(MantenimientoDTO mantenimientoSeleccionado, AutoparteDTO autoparte, Integer cantidad){
		
		PreparedStatement statementAutoparteXmantenimiento;
		PreparedStatement statementAutoparteXmantenimientoUtilizada;
		PreparedStatement statementRestarStock;
		PreparedStatement statementReadAutoparte;
		
		ResultSet resulsetAutoparteXmantenimientoUtilizada;
		ResultSet resulsetReadAutoparte;
		
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			
			statementAutoparteXmantenimientoUtilizada = conexion.prepareStatement(readAutopartesXmantenimientoUtilizada);
			statementAutoparteXmantenimientoUtilizada.setInt(1, mantenimientoSeleccionado.getId());
			statementAutoparteXmantenimientoUtilizada.setInt(2, autoparte.getId());
			resulsetAutoparteXmantenimientoUtilizada = statementAutoparteXmantenimientoUtilizada.executeQuery();
			
			if(resulsetAutoparteXmantenimientoUtilizada.next()){
				int idAutoparteXmantenimiento = resulsetAutoparteXmantenimientoUtilizada.getInt("id");
				int cantidadConsumida = resulsetAutoparteXmantenimientoUtilizada.getInt("cantidadConsumida");
				statementAutoparteXmantenimiento = conexion.prepareStatement(updateAutopartesXmantenimiento);
				statementAutoparteXmantenimiento.setInt(1, cantidadConsumida + cantidad);
				statementAutoparteXmantenimiento.setInt(2, idAutoparteXmantenimiento);		
				
				if(statementAutoparteXmantenimiento.executeUpdate() > 0){
					
					statementReadAutoparte = conexion.prepareStatement(readAutopartePorID);
					statementReadAutoparte.setInt(1, autoparte.getId());
					resulsetReadAutoparte = statementReadAutoparte.executeQuery();
					
					if(resulsetReadAutoparte.next()){
						AutoparteDTO autoparteEnBase = getAutoparteDTO(resulsetReadAutoparte);
						
						if(autoparteEnBase.getStock()>=cantidad){
							statementRestarStock = conexion.prepareStatement(updateStockAutoparte);
							statementRestarStock.setInt(1, autoparteEnBase.getStock()-cantidad);
							statementRestarStock.setInt(2, autoparteEnBase.getId());
							
							if(statementRestarStock.executeUpdate() > 0){
								conexion.commit();
								isInsertExitoso = true;
							}else conexion.rollback();
						}else conexion.rollback();
					} else conexion.rollback();
				}else conexion.rollback();
				
			}else{
				statementAutoparteXmantenimiento = conexion.prepareStatement(insertAutoparteXmantenimiento);
				statementAutoparteXmantenimiento.setInt(1, 0);
				statementAutoparteXmantenimiento.setInt(2, mantenimientoSeleccionado.getId());		
				statementAutoparteXmantenimiento.setInt(3, autoparte.getId());
				statementAutoparteXmantenimiento.setInt(4, cantidad);
				
				if(statementAutoparteXmantenimiento.executeUpdate() > 0){
					statementReadAutoparte = conexion.prepareStatement(readAutopartePorID);
					statementReadAutoparte.setInt(1, autoparte.getId());
					resulsetReadAutoparte = statementReadAutoparte.executeQuery();
					
					if(resulsetReadAutoparte.next()){
						AutoparteDTO autoparteEnBase = getAutoparteDTO(resulsetReadAutoparte);
						
						if(autoparteEnBase.getStock()>=cantidad){
							statementRestarStock = conexion.prepareStatement(updateStockAutoparte);
							statementRestarStock.setInt(1, autoparteEnBase.getStock()-cantidad);
							statementRestarStock.setInt(2, autoparteEnBase.getId());
							
							if(statementRestarStock.executeUpdate() > 0){
								conexion.commit();
								isInsertExitoso = true;
							}else conexion.rollback();
						}else conexion.rollback();
					} else conexion.rollback();
				}else conexion.rollback();
			}
			
		}catch (SQLException e){
			e.printStackTrace();
		}
		return isInsertExitoso;
	}
	
	public boolean deleteAutoparteXmantenimiento(AutoparteDTO autoparteXmantenimientoAEliminar, int cantidad){
		PreparedStatement statement;
		PreparedStatement statementCantidadStock;
		PreparedStatement statementRegresarStock;
		
		ResultSet resultsetCantidadStock;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(deleteAutoparteXmantenimiento);
			statement.setInt(1, autoparteXmantenimientoAEliminar.getId());
			if(statement.executeUpdate() > 0)
			{
				statementCantidadStock = conexion.prepareStatement(readAutopartePorID);
				statementCantidadStock.setInt(1, autoparteXmantenimientoAEliminar.getId());
				resultsetCantidadStock = statementCantidadStock.executeQuery();
				if(resultsetCantidadStock.next()){
					int stockAutoparte = resultsetCantidadStock.getInt("stock");
					statementRegresarStock = conexion.prepareStatement(updateStockAutoparte);
					statementRegresarStock.setInt(1, stockAutoparte + cantidad);
					statementRegresarStock.setInt(2, autoparteXmantenimientoAEliminar.getId());
					if(statementRegresarStock.executeUpdate() > 0){
						conexion.commit();
						isdeleteExitoso = true;
					}else conexion.rollback();
				}else conexion.rollback();
			}else conexion.rollback();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<Pair<TipoAutoparteDTO, Integer>> readTipoAutopartesEnGarantia(MantenimientoDTO mantenimientoSeleccionado){
		PreparedStatement statementTipoAutopartesGarantizadas;
		
		ResultSet resulsetTipoAutopartesGarantizadas;
		List<Pair<TipoAutoparteDTO, Integer>> tipoAutopartesEnGarantia = new ArrayList<Pair<TipoAutoparteDTO, Integer>>(); 
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try{
			statementTipoAutopartesGarantizadas = conexion.prepareStatement(readTipoAutopartesGarantizadas);
			resulsetTipoAutopartesGarantizadas = statementTipoAutopartesGarantizadas.executeQuery();
			while(resulsetTipoAutopartesGarantizadas.next()){
				int idTipoAutoparte = resulsetTipoAutopartesGarantizadas.getInt("tipoautoparte");
				String nombreTipoAutoparte = resulsetTipoAutopartesGarantizadas.getString("nombre");
				TipoAutoparteDTO tipoAutoparte = new TipoAutoparteDTO(idTipoAutoparte, nombreTipoAutoparte);
				int cantidad = resulsetTipoAutopartesGarantizadas.getInt("cantidad");
				tipoAutopartesEnGarantia.add(new ImmutablePair<TipoAutoparteDTO, Integer>(tipoAutoparte, cantidad));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return tipoAutopartesEnGarantia;
	}
	
	public boolean updateManoDeObra(MantenimientoDTO mantenimientoSeleccionado){
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isupdateExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(updateManoDeObra);
			statement.setDouble(1, mantenimientoSeleccionado.getManoDeObra());
			statement.setInt(2, mantenimientoSeleccionado.getId());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isupdateExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isupdateExitoso;
	}
	
	private AutoparteDTO getAutoparteDTO(ResultSet resultSet) throws SQLException
	{
		
		int id = resultSet.getInt("idautoparte");
		int idTipo = resultSet.getInt("idtipoautoparte");
		String nombreTipo = resultSet.getString("nombreTipoAutoparte");
		boolean tieneColor = false;
		TipoAutoparteDTO tipo = new TipoAutoparteDTO(idTipo, nombreTipo);		
		int idMarca = resultSet.getInt("idMarca");
		String nombreMarca = resultSet.getString("nombreMarca");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		int idModelo = resultSet.getInt("idModelo");
		String nombreModelo = resultSet.getString("nombreModelo");
		ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
		String year = resultSet.getString("year");
		String color = "RED";
		int idSucursal = resultSet.getInt("idSucursal");
		String nombreSucursal = resultSet.getString("nombreSucursal");

		String calleSucursal = resultSet.getString("calle");

		String alturaSucursal = resultSet.getString("altura");
		int idPaisSucursal = resultSet.getInt("idPais");
		String nombrePaisSucursal = resultSet.getString("nombrePais");
		PaisDTO paisSucursal = new PaisDTO(idPaisSucursal, nombrePaisSucursal);
		
		
		SucursalDTO sucursal = new SucursalDTO(idSucursal, nombreSucursal, calleSucursal, alturaSucursal, paisSucursal);//sucursalSQL.buscarPorId(idSucursal);
		int stockMinimo = resultSet.getInt("stockminimo");
		int stock = resultSet.getInt("stock");
		double precio = resultSet.getDouble("precio");
	
				
		return new AutoparteDTO(id,tipo,modelo,year,sucursal,stockMinimo,stock,precio);
	
	}
	
	//arma un objeto mantenimientoDTO
	private MantenimientoDTO getMantenimientoDTO(ResultSet resultSet) throws SQLException{
			int id = resultSet.getInt("idMantenimiento");
			double manoDeObra = resultSet.getInt("manoDeObra");
			boolean finalizado = resultSet.getBoolean("finalizado");
			int idReserva = resultSet.getInt("idReserva");
			String fechaEmitida = resultSet.getString("fechaEmitida");
			String fechaTurno = resultSet.getString("fechaTurno");
			String horario = resultSet.getString("horario");
			int idcliente = resultSet.getInt("cliente");
			String nombreCliente = resultSet.getString("nombre");
			String apellidoCliente = resultSet.getString("apellido");
			int idPaisResidencia = resultSet.getInt("idpais");
			String nombrePaisResidencia = resultSet.getString("nombrePais");
			PaisDTO paisResidencia = new PaisDTO(idPaisResidencia, nombrePaisResidencia);
			int idTipoDoc = resultSet.getInt("idTipoDoc");
			String nombreTipoDoc = resultSet.getString("nombreTipoDoc");
			TipoDocumentoDTO tipoDoc = new TipoDocumentoDTO(idTipoDoc, nombreTipoDoc);
			String numDocCliente =resultSet.getString("numdocumento");
			String mailCliente =resultSet.getString("mail");
			String telefonoCliente =resultSet.getString("telefono");
			ClienteDTO cliente = new ClienteDTO(idcliente, nombreCliente, apellidoCliente, paisResidencia, tipoDoc, numDocCliente, mailCliente, telefonoCliente);
			int idAuto = resultSet.getInt("idAuto");
			int idPaisPatente = resultSet.getInt("idPaisPatente");
			String nombrePaisPatente = resultSet.getString("nombrePaisPatente");
			PaisDTO paisPatente = new PaisDTO(idPaisPatente, nombrePaisPatente);
			String patente = resultSet.getString("patente");
			int idMarca = resultSet.getInt("idMarca");
			String nombreMarca = resultSet.getString("nombreMarca");
			MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
			int idModelo = resultSet.getInt("idModelo");
			String nombreModelo = resultSet.getString("nombreModelo");
			ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
			String year = resultSet.getString("year");
			AutomovilTallerDTO auto = new AutomovilTallerDTO(idAuto, paisPatente, patente, modelo, year);
			boolean notificada =resultSet.getBoolean("notificada");
			int idEstado = resultSet.getInt("idEstado");
			String nombreEstado = resultSet.getString("nombreEstado");
			EstadoReservaDTO estadoReserva = new EstadoReservaDTO(idEstado, nombreEstado);		
			String motivoCancelacion =resultSet.getString("motivoCancelacion");
			MotivoDTO motivo = new MotivoDTO(resultSet.getInt("idMotivo"),resultSet.getBoolean("motivoPintura"), resultSet.getBoolean("motivoMecanica"), resultSet.getBoolean("motivoElectronica_Electricidad"), resultSet.getBoolean("motivoEsteticaYAccesorios"));
			boolean tieneGarantia = resultSet.getBoolean("tieneGarantia");
			String service = resultSet.getString("service");
			ReservaDTO reserva = new ReservaDTO(idReserva, fechaEmitida, fechaTurno, horario, cliente, auto, notificada, estadoReserva, motivoCancelacion, motivo, tieneGarantia, service);
			int idVenta = resultSet.getInt("idVenta");
			
			return new MantenimientoDTO(id, reserva, manoDeObra, finalizado, idVenta);
		}
	
		
		private Pair<AutoparteDTO, Integer> getAutoparteUtilizada(ResultSet resultSet) throws SQLException
		{
			
			int id = resultSet.getInt("idautoparte");
			int idTipo = resultSet.getInt("idtipoautoparte");
			String nombreTipo = resultSet.getString("nombreTipoAutoparte");
			boolean tieneColor = false;
			TipoAutoparteDTO tipo = new TipoAutoparteDTO(idTipo, nombreTipo);		
			int idMarca = resultSet.getInt("idMarca");
			String nombreMarca = resultSet.getString("nombreMarca");
			MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
			int idModelo = resultSet.getInt("idModelo");
			String nombreModelo = resultSet.getString("nombreModelo");
			ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
			String year = resultSet.getString("year");
			String color = "RED";
			int idSucursal = resultSet.getInt("idSucursal");
			String nombreSucursal = resultSet.getString("nombreSucursal");
	
			String calleSucursal = resultSet.getString("calle");
	
			String alturaSucursal = resultSet.getString("altura");
			int idPaisSucursal = resultSet.getInt("idPais");
			String nombrePaisSucursal = resultSet.getString("nombrePais");
			PaisDTO paisSucursal = new PaisDTO(idPaisSucursal, nombrePaisSucursal);
			
			
			SucursalDTO sucursal = new SucursalDTO(idSucursal, nombreSucursal, calleSucursal, alturaSucursal, paisSucursal);//sucursalSQL.buscarPorId(idSucursal);
			int stockMinimo = resultSet.getInt("stockminimo");
			int stock = resultSet.getInt("stock");
			double precio = resultSet.getDouble("precio");
		
			AutoparteDTO autoparte = new AutoparteDTO(id,tipo,modelo,year,sucursal,stockMinimo,stock,precio);
		
			int cantidad = resultSet.getInt("cantidadConsumida");
			
			return new ImmutablePair<AutoparteDTO, Integer>(autoparte,cantidad);
		
		}
		
		
		public boolean esDeMantenimiento(int idVenta){
			PreparedStatement statement;
			ResultSet resulset;
			Connection conexion = Conexion.getConexion().getSQLConexion();
			boolean esDeMantenimiento = false;
			try 
			{
				statement = conexion.prepareStatement(tieneVenta);
				statement.setDouble(1, idVenta);
				resulset = statement.executeQuery();
				
				if(resulset.next())
				{
					esDeMantenimiento = true;
				}
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			return esDeMantenimiento;
		}

		@Override
		public List<MantenimientoDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda) {
			List<MantenimientoDTO> mantenimientos = new ArrayList<MantenimientoDTO>();
			PreparedStatement statement;
			ResultSet resultSet; //Guarda el resultado de la query
			Conexion conexion = Conexion.getConexion();
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(search, arrayCampos, filtrosBusqueda);
			if(queryBusqueda!=null) {
				try {
					statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
				
				for(int i=0;i<filtrosBusqueda.size();i++) {
					statement.setString(i+1, filtrosBusqueda.get(i).getRight());
				}
				resultSet = statement.executeQuery();
				while(resultSet.next()) {
					mantenimientos.add(getMantenimientoDTO(resultSet));
				}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return mantenimientos;

		}
		
}		
