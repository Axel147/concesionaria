package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.MedioPagoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.MedioPagoDAO;

public class MedioPagoDAOSQL implements MedioPagoDAO{
	private static final String readall = "SELECT id, nombre FROM mediospago";
	
	@Override
	public List<MedioPagoDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<MedioPagoDTO> medioPago = new ArrayList<MedioPagoDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				medioPago.add(getMedioPagoDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return medioPago;
	}

	private MedioPagoDTO getMedioPagoDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("nombre");

		return new MedioPagoDTO(id, nombre);
	}
}
