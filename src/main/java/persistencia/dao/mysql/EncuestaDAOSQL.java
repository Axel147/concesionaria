package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dto.EncuestaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EncuestaDAO;

public class EncuestaDAOSQL implements EncuestaDAO{

	private static final String insertEncuesta = "INSERT INTO encuesta(id, puntaje1, puntaje2, puntaje3) VALUES(?, ?, ?, ?)";
	
	
	public boolean insert(EncuestaDTO nuevaEncuesta) {
		PreparedStatement statementEncuesta;//sentencia insert sucursal
		
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			
				statementEncuesta = conexion.prepareStatement(insertEncuesta);
				statementEncuesta.setInt(1, nuevaEncuesta.getId());
				statementEncuesta.setInt(2, nuevaEncuesta.getPuntaje_p1());
				statementEncuesta.setInt(3, nuevaEncuesta.getPuntaje_p2());
				statementEncuesta.setInt(4, nuevaEncuesta.getPuntaje_p3());
			
				if(statementEncuesta.executeUpdate() > 0){
					conexion.commit();
					isInsertExitoso = true;
				}else conexion.rollback();
				
			
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

}
