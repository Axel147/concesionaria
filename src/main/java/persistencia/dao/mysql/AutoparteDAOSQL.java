package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.AutoparteDAO;

public class AutoparteDAOSQL implements AutoparteDAO{

	private static final String readall = "SELECT idautoparte, idtipoautoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle, sucursal.altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id";
	private static final String delete = "DELETE FROM autoparte WHERE idautoparte = ?";
	private static final String insert = "INSERT INTO autoparte VALUES (?,?,?,?,?,?,?,?)";
	private static final String update = "UPDATE autoparte SET tipo = ?, modelo = ?, year = ?, sucursal = ?, stockminimo = ?, stock = ?, precio = ? WHERE idautoparte = ?";
	private static final String search = "SELECT idautoparte, idtipoautoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle, sucursal.altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE ";
	private static final String readallFaltaStock = "SELECT idautoparte, idtipoautoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle, sucursal.altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE stock < stockMinimo";
	private static final String readAutoparteIsExist = "SELECT COUNT(idautoparte) as cantidad FROM autoparte WHERE tipo = ? AND modelo = ? AND year = ? AND sucursal = ?";
	private static final String readAutoparteStockID = "SELECT idautoparte, stock FROM autoparte WHERE tipo = ? AND modelo = ? AND year = ? AND sucursal = ?"; 
	private static final String updateStock = "UPDATE autoparte SET stock = ? WHERE idautoparte = ?";
	private static final String[] arrayCampos = {"autoparte.tipo","modeloauto.marca","autoparte.modelo", "year", "autoparte.sucursal","stock"};
	private static final String readallParaMantenimiento = "SELECT idautoparte, idtipoautoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle, sucursal.altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE autoparte.sucursal = 1";
	private static final String readallDeSucursal = "SELECT idautoparte, idtipoautoparte, tipoautoparte.nombre as nombreTipoAutoparte, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, year, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle, sucursal.altura, pais.id as idPais, pais.pais as nombrePais, stockminimo, stock, precio FROM autoparte join tipoautoparte on autoparte.tipo = tipoautoparte.idtipoautoparte join modeloauto on autoparte.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autoparte.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE autoparte.sucursal = ?";
	
	
	@Override
	public boolean insert(AutoparteDTO autoparte_a_insertar) {
		
		PreparedStatement statement;
		PreparedStatement statementIsExistAutoparte;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isinsertExitoso = false;
		ResultSet resultSetIsExistAutoparte;
		try
		{
			
			statementIsExistAutoparte = conexion.prepareStatement(readAutoparteIsExist);
			statementIsExistAutoparte.setString(1, Integer.toString(autoparte_a_insertar.getTipo().getId()));
			statementIsExistAutoparte.setString(2, Integer.toString(autoparte_a_insertar.getModelo().getId()));
			statementIsExistAutoparte.setString(3, autoparte_a_insertar.getYear());
			statementIsExistAutoparte.setString(4, Integer.toString(autoparte_a_insertar.getSucursal().getIdSucursal()));
			
			resultSetIsExistAutoparte = statementIsExistAutoparte.executeQuery();
			
			if(resultSetIsExistAutoparte.next()) {
				if(!(resultSetIsExistAutoparte.getInt("cantidad")>=1)) {
					statement = conexion.prepareStatement(insert);
					statement.setString(1, Integer.toString(autoparte_a_insertar.getId()));
					statement.setString(2, Integer.toString(autoparte_a_insertar.getTipo().getId()));
					statement.setString(3, Integer.toString(autoparte_a_insertar.getModelo().getId()));
					statement.setString(4, autoparte_a_insertar.getYear());
					statement.setString(5, Integer.toString(autoparte_a_insertar.getSucursal().getIdSucursal()));
					statement.setString(6, Integer.toString(autoparte_a_insertar.getStockMinimo()));
					statement.setString(7, Integer.toString(autoparte_a_insertar.getStock()));
					statement.setString(8, Double.toString(autoparte_a_insertar.getPrecio()));
					
					if(statement.executeUpdate() > 0)
					{
						conexion.commit();
						isinsertExitoso = true;
					}else conexion.rollback();
				} else {
					PreparedStatement statementIDStockAutoparte = conexion.prepareStatement(readAutoparteStockID);
					statementIDStockAutoparte.setString(1, Integer.toString(autoparte_a_insertar.getTipo().getId()));
					statementIDStockAutoparte.setString(2, Integer.toString(autoparte_a_insertar.getModelo().getId()));
					statementIDStockAutoparte.setString(3, autoparte_a_insertar.getYear());
					statementIDStockAutoparte.setString(4, Integer.toString(autoparte_a_insertar.getSucursal().getIdSucursal()));
					ResultSet resultSetIDStockAutoparte = statementIDStockAutoparte.executeQuery();
					
					if(resultSetIDStockAutoparte.next()) {
						int idAutoparte = resultSetIDStockAutoparte.getInt("idautoparte");
						int stockNuevo = autoparte_a_insertar.getStock() + resultSetIDStockAutoparte.getInt("stock");
						PreparedStatement statementUpdateStock = conexion.prepareStatement(updateStock);
						statementUpdateStock.setInt(1, stockNuevo);
						statementUpdateStock.setInt(2, idAutoparte);
						
						if(statementUpdateStock.executeUpdate()>0) {
							conexion.commit();
							isinsertExitoso = true;
						}
					}

					
				}
			}
			
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isinsertExitoso;
		
	}
		
	@Override
	public boolean delete(AutoparteDTO autoparte_a_eliminar) {
	
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(autoparte_a_eliminar.getId()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	

	@Override
	public boolean update(AutoparteDTO autoparte_a_editar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isupdateExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setInt(1, autoparte_a_editar.getTipo().getId());
			statement.setInt(2, autoparte_a_editar.getModelo().getId());
			statement.setString(3, autoparte_a_editar.getYear());
			statement.setInt(4,autoparte_a_editar.getSucursal().getIdSucursal());
			statement.setInt(5, autoparte_a_editar.getStockMinimo());
			statement.setInt(6,autoparte_a_editar.getStock());
			statement.setDouble(7,autoparte_a_editar.getPrecio());
			statement.setInt(8,autoparte_a_editar.getId());

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isupdateExitoso = true;
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return isupdateExitoso;
		
	}
	
	


	@Override
	public List<AutoparteDTO> readAll() {

		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<AutoparteDTO> autopartes = new ArrayList<AutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartes.add(getAutoparteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return autopartes;
		
	}
	
	@Override
	public List<AutoparteDTO> readAllParaMantenimiento() {

		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<AutoparteDTO> autopartes = new ArrayList<AutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallParaMantenimiento);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartes.add(getAutoparteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return autopartes;
		
	}
	
	@Override
	public List<AutoparteDTO> readAllDeSucursal(int idSucursal) {

		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<AutoparteDTO> autopartes = new ArrayList<AutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallDeSucursal);
			statement.setInt(1, idSucursal);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartes.add(getAutoparteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return autopartes;
		
	}
	
	private AutoparteDTO getAutoparteDTO(ResultSet resultSet) throws SQLException
	{
		
		int id = resultSet.getInt("idautoparte");
		int idTipo = resultSet.getInt("idtipoautoparte");
		String nombreTipo = resultSet.getString("nombreTipoAutoparte");
//		TipoAutoparteDAOSQL tipoAutoparteSQL = new TipoAutoparteDAOSQL();
		TipoAutoparteDTO tipo = new TipoAutoparteDTO(idTipo, nombreTipo);//tipoAutoparteSQL.buscarPorId(idTipo);		
		int idMarca = resultSet.getInt("idMarca");
		String nombreMarca = resultSet.getString("nombreMarca");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		int idModelo = resultSet.getInt("idModelo");
		String nombreModelo = resultSet.getString("nombreModelo");
		//ModeloAutoDAOSQL modeloSQL = new ModeloAutoDAOSQL();
//		ModeloAutoDTO modelo = modeloSQL.buscarPorId(idModelo);
		ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
		String year = resultSet.getString("year");
		int idSucursal = resultSet.getInt("idSucursal");
//		SucursalDAOSQL sucursalSQL = new SucursalDAOSQL();
		String nombreSucursal = resultSet.getString("nombreSucursal");

		String calleSucursal = resultSet.getString("calle");

		String alturaSucursal = resultSet.getString("altura");
		int idPaisSucursal = resultSet.getInt("idPais");
		String nombrePaisSucursal = resultSet.getString("nombrePais");
		PaisDTO paisSucursal = new PaisDTO(idPaisSucursal, nombrePaisSucursal);
		
		
		SucursalDTO sucursal = new SucursalDTO(idSucursal, nombreSucursal, calleSucursal, alturaSucursal, paisSucursal);//sucursalSQL.buscarPorId(idSucursal);
		int stockMinimo = resultSet.getInt("stockminimo");
		int stock = resultSet.getInt("stock");
		double precio = resultSet.getDouble("precio");
		
		
		
				
		return new AutoparteDTO(id,tipo,modelo,year,sucursal,stockMinimo,stock,precio);
	
	}

	@Override
	public List<AutoparteDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda) {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<AutoparteDTO> autopartes = new ArrayList<AutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(this.search,this.arrayCampos,filtrosBusqueda);
			if(queryBusqueda!=null) {
				statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
				for(int i=0;i<filtrosBusqueda.size();i++) {
					statement.setString(i+1, filtrosBusqueda.get(i).getRight());
				}
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartes.add(getAutoparteDTO(resultSet));
			}
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return autopartes;
	}
	
	private String generarQueryBusqueda(int campo) {
		String queryBusqueda = search + arrayCampos[campo] + " = ?";
		return queryBusqueda;
	}
	
	@Override
	public List<AutoparteDTO> readAllFaltaStock(){
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<AutoparteDTO> autopartes = new ArrayList<AutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallFaltaStock);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				autopartes.add(getAutoparteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return autopartes;
	}
	


}
