package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.MedioPagoDTO;
import dto.OpcionPagoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.OpcionPagoDAO;

public class OpcionPagoDAOSQL implements OpcionPagoDAO {
	private static final String readall = " SELECT opcionpago.id as idopcionpago, opcion, mediospago.id as idmediopago, mediospago.nombre as nombremediopago From opcionpago JOIN mediospago on opcionpago.medioPago = mediospago.id WHERE mediospago.id = ? ";
	
	@Override
	public List<OpcionPagoDTO> readAll(int idMedioPago) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<OpcionPagoDTO> opcionPago = new ArrayList<OpcionPagoDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1,idMedioPago);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				opcionPago.add(getOpcionPagoDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return opcionPago;
	}
	
	
	 
	
	private OpcionPagoDTO getOpcionPagoDTO(ResultSet resultSet) throws SQLException{
		int id = resultSet.getInt("idopcionpago");
		String opcion = resultSet.getString("opcion");
		int idMedioPago= resultSet.getInt("idmediopago");
		String nombreMedioPago= resultSet.getString("nombremediopago");
		MedioPagoDTO medioPago = new MedioPagoDTO(idMedioPago, nombreMedioPago);
		
		return new OpcionPagoDTO(id, opcion, medioPago);
	}

	
}
