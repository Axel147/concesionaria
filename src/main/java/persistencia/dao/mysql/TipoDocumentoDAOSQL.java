package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoDocumentoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoDocumentoDAO;

public class TipoDocumentoDAOSQL implements TipoDocumentoDAO{
	
	private static final String readallTipoDocumento = "SELECT * FROM tipodocumento WHERE paisdocumento = ? or tipoDocumento = 'PASAPORTE'";
	private static final String buscar_x_id = "SELECT * FROM tipodocumento WHERE id = ?";

	
	public List<TipoDocumentoDTO> readAllTipoDocumento(int paisdocumento)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoDocumentoDTO> tipoDocumento = new ArrayList<TipoDocumentoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readallTipoDocumento);
			statement.setInt(1, paisdocumento);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tipoDocumento.add(getTipoDocumentoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipoDocumento;
	}
	
	@Override
	public TipoDocumentoDTO buscarPorId(int id) {
		
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		TipoDocumentoDTO tipoDoc = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(buscar_x_id);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipoDoc  = this.getTipoDocumentoDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipoDoc;
	}

	
	private TipoDocumentoDTO getTipoDocumentoDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("tipoDocumento");	
		
		return new TipoDocumentoDTO(id, nombre);
	}
	
}
