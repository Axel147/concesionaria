package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;
import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.UsuarioDAO;

public class UsuarioDAOSQL implements UsuarioDAO {
	
	private static final String readall = "SELECT * FROM usuario";
	private static final String delete = "DELETE FROM usuario WHERE id = ?";
	private static final String insert = "INSERT INTO usuario VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String update = "UPDATE usuario SET nombre = ?, apellido = ?, sexo = ?, paisresidencia = ?, tipodoc = ?, numdocumento = ?, nombreusuario = ?, mail = ?, pass = ?, tipousr = ?, sucursal = ? WHERE id = ?";
	private static final String buscar_x_campo = "SELECT * FROM usuario WHERE ";
//	private static final String buscar_x_documento = "SELECT * FROM USUARIO WHERE tipodoc = ? AND numdocumento = ?";
	private static final String[] arrayCampos = {"nombre","apellido","sexo","paisresidencia","tipodoc","numdocumento","nombreusuario","mail","pass","tipousr","sucursal"};
//	private static final String[] arrayCondiciones = {" = ?"," LIKE ? '%'"," LIKE '%' ? '%'"};
	private static final String readUsuarioLoging = "Select * FROM usuario WHERE nombreusuario = ? and pass = ?";
	private static final String search = "SELECT * FROM usuario WHERE ";
	@Override
	public boolean insert(UsuarioDTO usuario_a_insertar) {
		
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isinsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, Integer.toString(usuario_a_insertar.getId()));
			statement.setString(2,usuario_a_insertar.getNombre());
			statement.setString(3, usuario_a_insertar.getApellido());
			statement.setString(4,usuario_a_insertar.getSexo());
			statement.setInt(5,usuario_a_insertar.getPaisResidencia().getIdPais());
			statement.setInt(6, usuario_a_insertar.getTipoDocumento().getIdTipoDocumento());
			statement.setString(7, usuario_a_insertar.getNroDocumento());
			statement.setString(8, usuario_a_insertar.getNombreUsuario());
			statement.setString(9, usuario_a_insertar.getMail());
			statement.setString(10, usuario_a_insertar.getPass());
			statement.setString(11, Integer.toString(usuario_a_insertar.getTipoUsuario().getIdTipo()));
			statement.setInt(12, usuario_a_insertar.getSucursal().getIdSucursal());
			
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isinsertExitoso = true;
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return isinsertExitoso;
		
	}
		
	@Override
	public boolean delete(UsuarioDTO usuario_a_eliminar) {
	
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(usuario_a_eliminar.getId()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	

	@Override
	public boolean update(UsuarioDTO usuario_a_editar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isupdateExitoso = false;
		try
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1,usuario_a_editar.getNombre());
			statement.setString(2, usuario_a_editar.getApellido());
			statement.setString(3, usuario_a_editar.getSexo());
			statement.setString(4, Integer.toString(usuario_a_editar.getPaisResidencia().getIdPais()));
			statement.setString(5, Integer.toString(usuario_a_editar.getTipoDocumento().getIdTipoDocumento()));
			statement.setString(6, usuario_a_editar.getNroDocumento());
			statement.setString(7, usuario_a_editar.getNombreUsuario());
			statement.setString(8, usuario_a_editar.getMail());
			statement.setString(9, usuario_a_editar.getPass());
			statement.setString(10, Integer.toString(usuario_a_editar.getTipoUsuario().getIdTipo()));
			statement.setInt(11, usuario_a_editar.getSucursal().getIdSucursal());
			statement.setString(12, Integer.toString(usuario_a_editar.getId()));

			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isupdateExitoso = true;
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return isupdateExitoso;
		
	}
	
	@Override
	public List<UsuarioDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda) {

		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(this.search,this.arrayCampos,filtrosBusqueda);
			if(queryBusqueda!=null) {
			statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
			for(int i=0;i<filtrosBusqueda.size();i++) {
				statement.setString(i+1, filtrosBusqueda.get(i).getRight());
			}
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				usuarios.add(getUsuarioDTO(resultSet));
			}
		
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return usuarios;
		
	}

	@Override
	public List<UsuarioDTO> readAll() {

		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				usuarios.add(getUsuarioDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		
		return usuarios;
		
	}
	
	private UsuarioDTO getUsuarioDTO(ResultSet resultSet) throws SQLException
	{
		
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("nombre");
		String apellido = resultSet.getString("apellido");
		String sexo = resultSet.getString("sexo");
		
		int idPais = resultSet.getInt("paisresidencia");
		PaisDAOSQL paisSQL = new PaisDAOSQL();
		PaisDTO paisResidencia = paisSQL.buscarPorId(idPais);
		
		int idTipoDoc = resultSet.getInt("tipodoc");
		TipoDocumentoDAOSQL tipoDocSQL = new TipoDocumentoDAOSQL();
		TipoDocumentoDTO tipoDoc = tipoDocSQL.buscarPorId(idTipoDoc);

		String numdocumento = resultSet.getString("numdocumento");
		String nombreusuario = resultSet.getString("nombreusuario");
		String mail = resultSet.getString("mail");
		String pass = resultSet.getString("pass");
		
		int idTipoUsr = resultSet.getInt("tipousr");
		TipoUsuarioDAOSQL tipoUsuarioSQL = new TipoUsuarioDAOSQL();
		TipoUsuarioDTO tipoUsr = tipoUsuarioSQL.buscarPorId(idTipoUsr);
		
		int idSucursal = resultSet.getInt("sucursal");
		SucursalDAOSQL sucursalSQL = new SucursalDAOSQL();
		SucursalDTO sucursal = sucursalSQL.buscarPorId(idSucursal);
				
		return new UsuarioDTO(id,nombre,apellido,sexo,paisResidencia,tipoDoc,numdocumento,nombreusuario,mail,pass,tipoUsr,sucursal);
	
	}
	
	
//	private String generarQueryBusqueda(int campo, int condicion) {
//
//		return buscar_x_campo + arrayCampos[campo] + arrayCondiciones[condicion];
//		
//	}

	@Override
//	public UsuarioDTO searchDoc(int idTipoDoc, String numDoc) {
//		PreparedStatement statement;
//		ResultSet resultSet;
//		Conexion conexion = Conexion.getConexion();
//		UsuarioDTO usuario = null;
//		try 
//		{
//			statement = conexion.getSQLConexion().prepareStatement(buscar_x_documento);
//			statement.setString(1, Integer.toString(idTipoDoc));
//			statement.setString(2, numDoc);
//			resultSet = statement.executeQuery();
//			resultSet.next();
//			usuario  = this.getUsuarioDTO(resultSet);
//			
//		} 
//		catch (SQLException e) 
//		{
//			e.printStackTrace();
//		}
//		return usuario;
//	}
	
	public Triple<Integer, TipoUsuarioDTO, SucursalDTO> getUsuarioLoging(String nombreUsuario, char[] password){
		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
//		UsuarioDTO usuario = null;
		Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal = null;
		String pass = "";
		for(int i=0; i<password.length;i++){
			pass = pass + password[i];
		}
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readUsuarioLoging);
			statement.setString(1, nombreUsuario);
			statement.setString(2, pass);
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				int id = resultSet.getInt("id");
				int idTipoUsr = resultSet.getInt("tipousr");
				TipoUsuarioDAOSQL tipoUsuarioSQL = new TipoUsuarioDAOSQL();
				TipoUsuarioDTO tipoUsr = tipoUsuarioSQL.buscarPorId(idTipoUsr);
				int idSucursal = resultSet.getInt("sucursal");
				SucursalDAOSQL sucursalSQL = new SucursalDAOSQL();
				SucursalDTO sucursal = sucursalSQL.buscarPorId(idSucursal);
				if(id!=0&&tipoUsr!=null&&sucursal!=null){
					id_TipoUser_Sucursal = new ImmutableTriple<Integer, TipoUsuarioDTO, SucursalDTO>(id, tipoUsr, sucursal);
				}
//				usuario  = this.getUsuarioDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return id_TipoUser_Sucursal;
	}


}
