/**
 * 
 */
package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.EncuestaDAO;
import persistencia.dao.interfaz.EstadoReservaDAO;
import persistencia.dao.interfaz.GarantiaDAO;
import persistencia.dao.interfaz.MantenimientoDAO;
import persistencia.dao.interfaz.ReservaDAO;
import persistencia.dao.interfaz.PaisDAO;
import persistencia.dao.interfaz.TipoUsuarioDAO;
import persistencia.dao.interfaz.UsuarioDAO;
import persistencia.dao.interfaz.VentaDAO;
import persistencia.dao.interfaz.SucursalDAO;
import persistencia.dao.interfaz.TipoDocumentoDAO;
import persistencia.dao.interfaz.ClienteDAO;
import persistencia.dao.interfaz.MarcaDAO;
import persistencia.dao.interfaz.MedioPagoDAO;
import persistencia.dao.interfaz.ModeloDAO;
import persistencia.dao.interfaz.OpcionPagoDAO;
import persistencia.dao.interfaz.AutomovilDAO;
//import persistencia.dao.interfaz.MarcaDAO;
import persistencia.dao.interfaz.TipoAutoparteDAO;
import persistencia.dao.interfaz.AutoparteDAO;
import persistencia.dao.interfaz.AutomovilTallerDAO;

public class DAOSQLFactory implements DAOAbstractFactory 
{
	/* (non-Javadoc)
	 * @see persistencia.dao.interfaz.DAOAbstractFactory#createPersonaDAO()
	 */
	public ClienteDAO createClienteDAO() 
	{
		return new ClienteDAOSQL();
	}
	
	public UsuarioDAO createUsuarioDAO() {
			
		return new UsuarioDAOSQL();
	}

	@Override
	public TipoUsuarioDAO createTipoUsuarioDAO() {

		return new TipoUsuarioDAOSQL();
	}

	@Override
	public SucursalDAO createSucursalDAO() {
		
		return new SucursalDAOSQL();
	}
	
	public ReservaDAO createReservaDAO() 
	{
				return new ReservaDAOSQL();
	}

	@Override
	public PaisDAO createPaisDAO(){
		
		return new PaisDAOSQL();
	
	}
	
	@Override
	public TipoDocumentoDAO createTipoDocumentoDAO(){
		
		return new TipoDocumentoDAOSQL();
	
	}

	@Override
	public MarcaDAO createMarcaDAO() {
		
		return new MarcaDAOSQL();
	}

	@Override
	public ModeloDAO createModeloDAO() {
		
		return new ModeloDAOSQL();
	}

	@Override
	public AutomovilDAO createAutomovilDAO() {
		
		return new AutomovilDAOSQL();
	}
	
	@Override
	public EstadoReservaDAO createEstadoReservaDAO(){
		
		return new EstadoReservaDAOSQL();
		
	}
	
	@Override
	public AutomovilTallerDAO createAutomovilTallerDAO() {
		
		return new AutomovilTallerDAOSQL();
	}
	
	@Override
	public AutoparteDAO createAutoparteDAO() {
		// TODO Auto-generated method stub
		return new AutoparteDAOSQL();
	}

//	@Override
//	public ModeloAutoDAO createModeloAutoDAO() {
//		// TODO Auto-generated method stub
//		return new ModeloAutoDAOSQL();
//	}

//	@Override
//	public MarcaDAO createMarcaDAO() {
//		// TODO Auto-generated method stub
//		return new MarcaDAOSQL() ;
//	}

	@Override
	public TipoAutoparteDAO createTipoAutoparteDAO() {
		// TODO Auto-generated method stub
		return new TipoAutoparteDAOSQL() ;
	}
	
	@Override
	public MedioPagoDAO createMedioPagoDAO() {
		return new MedioPagoDAOSQL();
	}

	@Override
	public OpcionPagoDAO createOpcionPagoDAO() {
		return new OpcionPagoDAOSQL();
	}

	@Override
	public VentaDAO createVentaDAO() {
		return new VentaDAOSQL();
	}

	@Override
	public GarantiaDAO createGarantiaDAO() {
		return new GarantiaDAOSQL();
	}
	
	@Override
	public MantenimientoDAO createMantenimientoDAO() {
		return new MantenimientoDAOSQL();
	}
	
	@Override
	public EncuestaDAO createEncuestaDAO() {
		return new EncuestaDAOSQL();
	}
}
