package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.ClienteDTO;
import dto.MarcaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.MarcaDAO;

public class MarcaDAOSQL implements MarcaDAO{
	private static final String insertMarca = "INSERT INTO marcaauto(id, marca) VALUES(?, ?)";
	private static final String delete = "DELETE FROM marcaauto WHERE id = ?";
	private static final String editarMarca = "UPDATE marcaauto SET marca = ? WHERE id = ?";	
	private static final String readall = "SELECT id, marca FROM marcaauto";
	private static final String readMarcaIsExist = "SELECT COUNT(id) as cantidad FROM marcaauto WHERE marca = ?";
	private static final String readallMatchNombre = "SELECT id, marca FROM marcaauto WHERE marca like '%' ? '%'";
	private static final String cantModelosEnMarca = "SELECT COUNT(id) as cantidad FROM modeloauto WHERE marca = ?";

	@Override
	public boolean insert(MarcaDTO nuevaMarca) {
		PreparedStatement statementMarca;//sentencia insert marca
		PreparedStatement statementIsExistMarca;//sentencia consulta si marca existe
		
		ResultSet resultSetIsExistMarca;//resultado de si existe la marca
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			statementIsExistMarca = conexion.prepareStatement(readMarcaIsExist);
			statementIsExistMarca.setString(1, nuevaMarca.getNombre());
			resultSetIsExistMarca = statementIsExistMarca.executeQuery();
					
			if(resultSetIsExistMarca.next()){
				if(!(resultSetIsExistMarca.getInt("cantidad")>=1)){ //controlo si existe la marca, si no existe lo creo
					statementMarca = conexion.prepareStatement(insertMarca);
					statementMarca.setInt(1, nuevaMarca.getId());
					statementMarca.setString(2, nuevaMarca.getNombre());
					if(statementMarca.executeUpdate() > 0){
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}


	
	@Override
	public boolean delete(MarcaDTO marca_a_eliminar) {
		PreparedStatement statement;
		PreparedStatement statementCountUsesIdMarca;
		ResultSet resultSetCountUsesIdMarca;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{		
			statementCountUsesIdMarca = conexion.prepareStatement(cantModelosEnMarca);
			
			statementCountUsesIdMarca.setInt(1, marca_a_eliminar.getId());
			resultSetCountUsesIdMarca = statementCountUsesIdMarca.executeQuery();
			if(resultSetCountUsesIdMarca.next()){
				if(!(resultSetCountUsesIdMarca.getInt("cantidad")>=1)){
					statement = conexion.prepareStatement(delete);
					statement.setInt(1, marca_a_eliminar.getId());
					if(statement.executeUpdate() > 0)
					{
						conexion.commit();
						isdeleteExitoso = true;
					}
				}
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}


	@Override
	public boolean update(MarcaDTO marcaAModificar) {
		PreparedStatement statementEditarMarca;
		PreparedStatement statementIsExistMarca;
		ResultSet resultSetIsExistModelo;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementIsExistMarca = conexion.prepareStatement(readMarcaIsExist);
			statementIsExistMarca.setString(1, marcaAModificar.getNombre());
			resultSetIsExistModelo = statementIsExistMarca.executeQuery();
			
			if(resultSetIsExistModelo.next()){
				if(!(resultSetIsExistModelo.getInt("cantidad")>=1)){
					statementEditarMarca = conexion.prepareStatement(editarMarca);
					statementEditarMarca.setString(1,marcaAModificar.getNombre());
					statementEditarMarca.setInt(2,marcaAModificar.getId());
					if(statementEditarMarca.executeUpdate() > 0)
					{
						conexion.commit();
						isEditarExitoso = true;
					}
					else conexion.rollback();
				}
			}	
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isEditarExitoso;
	
	}
	
	@Override
	public List<MarcaDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<MarcaDTO> marca = new ArrayList<MarcaDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				marca.add(getMarcaDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return marca;
	}

	private MarcaDTO getMarcaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("marca");

		return new MarcaDTO(id, nombre);
	}

	@Override
	public List<MarcaDTO> readAllMatchDatMarca(String dato) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<MarcaDTO> marcas = new ArrayList<MarcaDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallMatchNombre);
			statement.setString(1, dato);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				marcas.add(getMarcaDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return marcas;
	}

}
