package persistencia.dao.mysql;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.SucursalDTO;
import dto.TipoUsuarioDTO;

public class GeneradorBusquedaSQL {
	
	private static final String[] arrayCondiciones = {" = ?"," LIKE ? '%'"," LIKE '%' ? '%'"};
	
	public static String generarQueryBusqueda(String queryInicial, String[] campos, List<Triple<Integer, Integer, String>> listaFiltros) {
		int nroFiltros = listaFiltros.size();
		if(nroFiltros>0) {
		String[] condiciones = new String[nroFiltros];	
		for(int i=0;i<nroFiltros;i++) {
		condiciones[i] = campos[listaFiltros.get(i).getLeft()] + " " + arrayCondiciones[listaFiltros.get(i).getMiddle()];
		}
		String queryFinal = queryInicial + condiciones[0];
		if(nroFiltros>1) {
			for(int j=1;j<nroFiltros;j++) {
				queryFinal = queryFinal + " AND " + condiciones[j];
			}
		}
		return queryFinal;
		}else return null;
	}
	

}