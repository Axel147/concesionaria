package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ModeloDAO;

public class ModeloDAOSQL implements ModeloDAO {
	private static final String insertModelo = "INSERT INTO modeloauto(id, modelo, marca) VALUES(?, ?, ?)";
	private static final String readModeloIsExist = "SELECT COUNT(*) as cantidad FROM modeloauto WHERE modelo = ? and marca = ?";
	private static final String delete = "DELETE FROM modeloauto WHERE id = ?";
	private static final String readall = "SELECT modeloauto.id, modelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca FROM modeloauto join marcaauto on modeloauto.marca = marcaauto.id WHERE modeloauto.marca = ?";
	private static final String updateModelo = "UPDATE modeloauto SET modelo = ?, marca = ? WHERE id = ?";
	private static final String readallMatchNombreModelo = "SELECT modeloauto.id, modelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca FROM modeloauto join marcaauto on modeloauto.marca = marcaauto.id WHERE modeloauto.marca = ? and modelo like '%' ? '%'";
	
	
	public boolean insert(ModeloDTO modelo){
		PreparedStatement statementModelo;//sentencia insert sucursal
		PreparedStatement statementIsExistModelo;//sentencia consulta si sucursal existe
		
		ResultSet resultSetIsExistModelo;//resultado de si existe la sucursal
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statementIsExistModelo = conexion.prepareStatement(readModeloIsExist);
			statementIsExistModelo.setString(1, modelo.getModelo());
			statementIsExistModelo.setInt(2, modelo.getMarca().getId());
			
			resultSetIsExistModelo = statementIsExistModelo.executeQuery();
			
					
			if(resultSetIsExistModelo.next()){
				if(!(resultSetIsExistModelo.getInt("cantidad")>=1)){ //controlo si existe el modelo, si no existe lo creo
					statementModelo = conexion.prepareStatement(insertModelo);
					statementModelo.setInt(1, modelo.getId());
					statementModelo.setString(2, modelo.getModelo());
					statementModelo.setInt(3, modelo.getMarca().getId());
					
					if(statementModelo.executeUpdate() > 0){
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	@Override
	public boolean update(ModeloDTO modeloModificado) {
		PreparedStatement statementEditarModelo;
		PreparedStatement statementIsExistModelo;
		ResultSet resultSetIsExistModelo;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try
		{
			statementIsExistModelo = conexion.prepareStatement(readModeloIsExist);
			statementIsExistModelo.setString(1, modeloModificado.getModelo());
			statementIsExistModelo.setInt(2, modeloModificado.getMarca().getId());
			resultSetIsExistModelo = statementIsExistModelo.executeQuery();
			
			if(resultSetIsExistModelo.next()){
				if(!(resultSetIsExistModelo.getInt("cantidad")>=1)){
					statementEditarModelo = conexion.prepareStatement(updateModelo);
					statementEditarModelo.setString(1,modeloModificado.getModelo());
					statementEditarModelo.setInt(2, modeloModificado.getMarca().getId());
					statementEditarModelo.setInt(3,modeloModificado.getId());
					if(statementEditarModelo.executeUpdate() > 0)
					{
						conexion.commit();
						isEditarExitoso = true;
					}
					else conexion.rollback();
				}
			}	
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isEditarExitoso;
	
	}
 
	@Override
	public boolean delete(ModeloDTO sucursal_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(sucursal_a_eliminar.getId()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	
	@Override
	public List<ModeloDTO> readAll(int idMarca) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ModeloDTO> modelo = new ArrayList<ModeloDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1,idMarca);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				modelo.add(getModeloDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return modelo;
	}
	
	
	 
	
	private ModeloDTO getModeloDTO(ResultSet resultSet) throws SQLException{
		int id = resultSet.getInt("id");
		String modelo= resultSet.getString("modelo");
		int idMarca = resultSet.getInt("idMarca");
		String nombreMarca= resultSet.getString("nombreMarca");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		
		return new ModeloDTO(id, modelo, marca);
	}

	@Override
	public List<ModeloDTO> readAllPor(String nombreModelo, int idMarca) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ModeloDTO> modelo = new ArrayList<ModeloDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallMatchNombreModelo);
			statement.setInt(1, idMarca);
			statement.setString(2, nombreModelo);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				modelo.add(getModeloDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return modelo;
	}
	

}
	
