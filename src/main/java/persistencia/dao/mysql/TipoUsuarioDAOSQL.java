package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoUsuarioDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoUsuarioDAO;

public class TipoUsuarioDAOSQL implements TipoUsuarioDAO {

	private static final String readall = "SELECT * FROM tipoUsuario";
	private static final String buscar_x_id = "SELECT * FROM tipousuario WHERE id = ?";

	
	
	@Override
	public List<TipoUsuarioDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<TipoUsuarioDTO> tiposUsuario = new ArrayList<TipoUsuarioDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
		
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
			tiposUsuario.add(getTipoUsuarioDTO(resultSet));
			
			}
		}	catch (SQLException e){
				e.printStackTrace();
		}
		
		return tiposUsuario;
	}

	@Override
	public TipoUsuarioDTO buscarPorId(int id) {

		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		TipoUsuarioDTO tipo = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(buscar_x_id);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipo  = this.getTipoUsuarioDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipo;
	}
	
	private TipoUsuarioDTO getTipoUsuarioDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("id");
		String nombreTipo = resultSet.getString("nombretipo");
		
		return new TipoUsuarioDTO(id,nombreTipo);
		
	}

	
	
}
