package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoAutoparteDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoAutoparteDAO;

public class TipoAutoparteDAOSQL implements TipoAutoparteDAO {

	private static final String readall = "SELECT * FROM tipoautoparte";
	private static final String buscar_x_id = "SELECT * FROM tipoautoparte WHERE idtipoautoparte = ?";

	
	
	@Override
	public List<TipoAutoparteDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<TipoAutoparteDTO> tiposAutoparte = new ArrayList<TipoAutoparteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
		
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
			tiposAutoparte.add(getTipoAutoparteDTO(resultSet));
			
			}
		}	catch (SQLException e){
				e.printStackTrace();
		}
		
		return tiposAutoparte;
	}

	@Override
	public TipoAutoparteDTO buscarPorId(int id) {

		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		TipoAutoparteDTO tipo = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(buscar_x_id);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			tipo  = this.getTipoAutoparteDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipo;
	}
	
	private TipoAutoparteDTO getTipoAutoparteDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("idtipoautoparte");
		String nombreTipo = resultSet.getString("nombre");
		
		return new TipoAutoparteDTO(id,nombreTipo);
		
	}

}
