package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ClienteDAO;
import dto.ClienteDTO;
import dto.PaisDTO;
import dto.TipoDocumentoDTO;

public class ClienteDAOSQL implements ClienteDAO
{
	private static final String insertCliente = "INSERT INTO cliente(id, nombre, apellido, paisResidencia, tipoDocumento, numdocumento, mail, telefono) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String readClienteIsExist = "SELECT COUNT(id) as cantidad FROM cliente WHERE tipoDocumento = ? and numdocumento = ?";
	private static final String delete = "DELETE FROM cliente WHERE id = ?";
	private static final String readall = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id";
	private static final String readallMatchNombre = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE nombre like '%' ? '%'";
	private static final String readallMatchApellido = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE apellido like '%' ? '%'";
	private static final String readallMatchDocumento = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE numdocumento like '%' ? '%'";
	private static final String readallMatchMail = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE mail like '%' ? '%'";
	private static final String readallMatchTelefono = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE telefono like '%' ? '%'";
	private static final String updateCliente = "UPDATE cliente SET nombre = ?, apellido = ?, paisResidencia = ?, tipoDocumento = ?, numdocumento = ?, mail = ?, telefono = ? WHERE id = ?";
	private static final String search = "SELECT cliente.id, cliente.nombre, cliente.apellido, pais.id as idPaisResidencia, pais.pais as nombrePaisResidencia, tipodocumento.id as idTipoDocumento, tipodocumento.tipoDocumento as nombreTipoDocumento, cliente.numdocumento, cliente.mail, cliente.telefono FROM cliente join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id WHERE ";
	private static final String[] arrayCampos = {"cliente.nombre","cliente.apellido","pais.pais","cliente.numdocumento","cliente.mail","cliente.telefono"};
	
	//crea un cliente si no existe (relativo-no controla si es el mismo cliente con otro numero de documento-)
	public boolean insert(ClienteDTO cliente)
	{
		PreparedStatement statementCliente;//sentencia insert cliente
		PreparedStatement statementIsExistCliente;//sentencia consulta si cliente existe
		
		ResultSet resultSetIsExistCliente;//resultado de si existe el cliente
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			statementIsExistCliente = conexion.prepareStatement(readClienteIsExist);
			statementIsExistCliente.setInt(1, cliente.getTipoDocumento().getIdTipoDocumento());
			statementIsExistCliente.setString(2, cliente.getNumDocumento());
			resultSetIsExistCliente = statementIsExistCliente.executeQuery();
					
			if(resultSetIsExistCliente.next()){
				if(!(resultSetIsExistCliente.getInt("cantidad")>=1)){ //controlo si existe el cliente, si no existe lo creo
					statementCliente = conexion.prepareStatement(insertCliente);
					statementCliente.setInt(1, cliente.getIdCliente());
					statementCliente.setString(2, cliente.getNombre());
					statementCliente.setString(3, cliente.getApellido());
					statementCliente.setInt(4, cliente.getPaisResidencia().getIdPais());
					statementCliente.setInt(5, cliente.getTipoDocumento().getIdTipoDocumento());
					statementCliente.setString(6, cliente.getNumDocumento());
					statementCliente.setString(7, cliente.getMail());
					statementCliente.setString(8, cliente.getTelefono());
					if(statementCliente.executeUpdate() > 0){
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	//borra un cliente si existe (falta tener cuidado con la tabla reserva)
	public boolean delete(ClienteDTO cliente_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(cliente_a_eliminar.getIdCliente()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	//devuelve una lista con todos los clientes
	public List<ClienteDTO> readAll(){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				clientes.add(getClienteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return clientes;
	}
	
	//devuelve una lista con todos los clientes que coincidan con el dato en el campo que fueron pasados como parametros
	public List<ClienteDTO> readAllMatchDatClient(String datOfClient, int campo){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = null;
			if(campo==1){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchNombre);
			}else if(campo==2){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchApellido);
			}else if(campo==3){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchDocumento);
			}else if(campo==4){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchMail);
			}else{
				statement = conexion.getSQLConexion().prepareStatement(readallMatchTelefono);
			}
			statement.setString(1, datOfClient);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				clientes.add(getClienteDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return clientes;
	} 
	
	//arma un objeto clienteDTO
	private ClienteDTO getClienteDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("nombre");
		String apellido = resultSet.getString("apellido");
		int idPaisResidencia = resultSet.getInt("idPaisResidencia");
		String nombrePaisResidencia = resultSet.getString("nombrePaisResidencia");
		PaisDTO paisResidencia = new PaisDTO(idPaisResidencia, nombrePaisResidencia);
		int idTipoDocumento = resultSet.getInt("idTipoDocumento");
		String nombreTipoDocumento = resultSet.getString("nombreTipoDocumento");
		TipoDocumentoDTO tipoDocumento = new TipoDocumentoDTO(idTipoDocumento, nombreTipoDocumento);
		String numDoc =resultSet.getString("numdocumento");
		String mail =resultSet.getString("mail");
		String tel =resultSet.getString("telefono");
		
		return new ClienteDTO(id, nombre, apellido, paisResidencia, tipoDocumento, numDoc, mail, tel);
	}
	
	//modifica un cliente
	public boolean update(ClienteDTO clienteModificado) {
		PreparedStatement statementPersona;//sentencia update cliente
		
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try{		
			
			statementPersona = conexion.prepareStatement(updateCliente);
			statementPersona.setString(1, clienteModificado.getNombre());
			statementPersona.setString(2, clienteModificado.getApellido());
			statementPersona.setInt(3, clienteModificado.getPaisResidencia().getIdPais());
			statementPersona.setInt(4, clienteModificado.getTipoDocumento().getIdTipoDocumento());
			statementPersona.setString(5, clienteModificado.getNumDocumento());
			statementPersona.setString(6, clienteModificado.getMail());
			statementPersona.setString(7, clienteModificado.getTelefono());
			statementPersona.setInt(8, clienteModificado.getIdCliente());
							
			if(statementPersona.executeUpdate() > 0){
				conexion.commit();
				isUpdateExitoso = true;
			}else conexion.rollback();
		}catch(SQLException e) {
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch(SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isUpdateExitoso;
	}
	
	public List<ClienteDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda){
		
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		Conexion conexion = Conexion.getConexion();
		
		try {
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(this.search,this.arrayCampos,filtrosBusqueda);
			if(queryBusqueda!=null) {
				statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
				for(int i=0;i<filtrosBusqueda.size();i++) {
					statement.setString(i+1, filtrosBusqueda.get(i).getRight());
				}
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					clientes.add(getClienteDTO(resultSet));
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return clientes;
	}
	
}
