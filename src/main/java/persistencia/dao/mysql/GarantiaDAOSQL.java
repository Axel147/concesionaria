package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.GarantiaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.GarantiaDAO;

public class GarantiaDAOSQL implements GarantiaDAO{
	private static final String readall = "SELECT id, garantia, precio FROM garantia";


	public List<GarantiaDTO> readAll() {
	PreparedStatement statement;
	ResultSet resultSet; //Guarda el resultado de la query
	ArrayList<GarantiaDTO> garantia = new ArrayList<GarantiaDTO>();
	Conexion conexion = Conexion.getConexion();
	try{
		statement = conexion.getSQLConexion().prepareStatement(readall);
		resultSet = statement.executeQuery();
		while(resultSet.next()){
			garantia.add(getGarantiaDTO(resultSet));
		}
	}catch (SQLException e){
		e.printStackTrace();
	}
	return garantia;
}

	private GarantiaDTO getGarantiaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("id");
		String garantia = resultSet.getString("garantia");
		double precio = resultSet.getDouble("precio");

	
		return new GarantiaDTO(id, garantia, precio);
	}
	
	
}
