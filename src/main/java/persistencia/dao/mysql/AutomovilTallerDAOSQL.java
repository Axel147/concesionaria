package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.AutomovilTallerDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.PaisDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.AutomovilTallerDAO;

public class AutomovilTallerDAOSQL implements AutomovilTallerDAO{
	private static final String insertAutomovil = "INSERT INTO autosentaller(id, paispatente, patente, modelo, year) VALUES(?, ?, ?, ?, ?)";
	private static final String readAutomovilIsExist = "SELECT COUNT(id) as cantidad FROM autosentaller WHERE paispatente = ? and patente = ?";
	private static final String delete = "DELETE FROM autosenstock WHERE id = ?";
	private static final String readAllIfExist = "SELECT autosentaller.id, pais.id as idPaisPatente, pais.pais as nombrePaisPatente, patente, modeloauto.id as idModeloAuto, modeloauto.modelo as nombreModeloAuto, marcaauto.id as idMarcaAuto, marcaauto.marca as nombreMarcaAuto, year FROM autosentaller join pais on autosentaller.paispatente = pais.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id WHERE autosentaller.paispatente = ? and patente = ? and autosentaller.modelo = ? and year = ?";
	

//	public boolean insert(AutomovilTallerDTO automovil) {
//		PreparedStatement statementAutomovil;
//		PreparedStatement statementIsExistAutomovil;
//		
//		ResultSet resultSetIsExistSucursal;
//		Connection conexion = Conexion.getConexion().getSQLConexion();
//		boolean isInsertExitoso = false;
//		try{		
//			statementIsExistAutomovil = conexion.prepareStatement(readAutomovilIsExist);
//			statementIsExistAutomovil.setInt(1,automovil.getPaisPatente().getIdPais());
//			statementIsExistAutomovil.setString(2,automovil.getPatente());
//			resultSetIsExistSucursal = statementIsExistAutomovil.executeQuery();
//			
//			if(resultSetIsExistSucursal.next()){
//				if(!(resultSetIsExistSucursal.getInt("cantidad")>=1)){
//					statementAutomovil = conexion.prepareStatement(insertAutomovil);
//					statementAutomovil.setInt(1, automovil.getId());
//					statementAutomovil.setInt(2, automovil.getModelo());
//					statementAutomovil.setString(3, automovil.getYear());
//					statementAutomovil.setString(4, automovil.getColor());
//					statementAutomovil.setString(5, automovil.getCantidadPuertas());
//					statementAutomovil.setInt(6, automovil.getStock());
//					statementAutomovil.setInt(7, automovil.getIdSucursal());
//					
//					if(statementAutomovil.executeUpdate() > 0){
//						conexion.commit();
//						isInsertExitoso = true;
//					}else conexion.rollback();
//				}
//			}
//		}catch (SQLException e){
//			e.printStackTrace();
//			try{
//				conexion.rollback();
//			}catch (SQLException e1){
//				e1.printStackTrace();
//			}
//		}
//		
//		return isInsertExitoso;
//	}


	public boolean delete(AutomovilTallerDTO automovil_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, automovil_a_eliminar.getId());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;

	}


	
	public AutomovilTallerDTO readAllIfExist(AutomovilTallerDTO auto) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		AutomovilTallerDTO automovil = new AutomovilTallerDTO(0,null,null,null,null); 
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllIfExist);
			statement.setInt(1, auto.getPaisPatente().getIdPais());
			statement.setString(2, auto.getPatente());
			statement.setInt(3, auto.getModelo().getId());
			statement.setString(4, auto.getYear());
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				automovil= getAutomovilDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return automovil;
	}
	
	private AutomovilTallerDTO getAutomovilDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("id");
		int idPaisPatente = resultSet.getInt("idPaisPatente");
		String nombrePaisPatente = resultSet.getString("nombrePaisPatente");
		PaisDTO pais = new PaisDTO(idPaisPatente, nombrePaisPatente);
		String patente = resultSet.getString("patente");
		int idModelo = resultSet.getInt("idModeloAuto");
		String nombreModelo = resultSet.getString("nombreModeloAuto");
		int idMarca = resultSet.getInt("idMarcaAuto");
		String nombreMarca = resultSet.getString("nombreMarcaAuto");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
		String year = resultSet.getString("year");

		return new AutomovilTallerDTO(id, pais, patente, modelo, year);
	}
	
}
