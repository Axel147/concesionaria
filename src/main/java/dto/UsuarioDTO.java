package dto;

public class UsuarioDTO {

	private int id;
	private String nombre; 
	private String apellido;
	private String sexo;
	private PaisDTO paisResidencia;
	private TipoDocumentoDTO tipoDocumento;
	private String nroDocumento;
	private String nombreUsuario;
	private String mail;
	private String pass;
	private TipoUsuarioDTO tipoUsuario;
	private SucursalDTO sucursal;
	
	
	
	public UsuarioDTO(int id, String nombre, String apellido, String sexo, PaisDTO paisResidencia, TipoDocumentoDTO tipoDocumento, String nroDocumento, String nombreUsuario, String mail, String pass, TipoUsuarioDTO tipoUsuario, SucursalDTO sucursal) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.sexo = sexo;
		this.paisResidencia = paisResidencia;
		this.tipoDocumento = tipoDocumento;
		this.nroDocumento = nroDocumento;
		this.nombreUsuario = nombreUsuario;
		this.mail = mail;
		this.pass = pass;
		this.tipoUsuario = tipoUsuario;
		this.sucursal = sucursal;
	}
	
	public int getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getSexo() {
		return sexo;
	}

	public PaisDTO getPaisResidencia() {
		return paisResidencia;
	}

	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public String getPass() {
		return pass;
	}
	public TipoUsuarioDTO getTipoUsuario() {
		return tipoUsuario;
	}
	
	public String getMail() {
		return mail;
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	@Override
	public String toString() {
		return nombre+" "+ apellido;
	}

}
