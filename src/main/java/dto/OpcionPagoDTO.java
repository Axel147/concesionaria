package dto;

public class OpcionPagoDTO {
	int id;
	String opcion;
	MedioPagoDTO medioPago;
	
	public OpcionPagoDTO(int id, String opcion, MedioPagoDTO medioPago) {
		this.id = id;
		this.opcion = opcion;
		this.medioPago = medioPago;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion (String efecto) {
		this.opcion = efecto;
	}
	
	public MedioPagoDTO getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(MedioPagoDTO medioPago) {
		this.medioPago = medioPago;
	}
	
	@Override
	public String toString() {
		return this.opcion;
	}
}
