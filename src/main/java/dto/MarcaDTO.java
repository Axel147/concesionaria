package dto;

public class MarcaDTO {
	private int id;
	private String nombre;

	public MarcaDTO(int id, String nombre)
	{
		this.id = id;
		this.nombre= nombre;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
    public String toString(){
        return this.nombre;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((MarcaDTO)objeto).nombre.equals(this.nombre)&&((MarcaDTO)objeto).id == (this.id)){
            return true;
        }
        return false;
    }
}
