package dto;

public class ProductoDTO {
	
	private int id;
	private ModeloDTO modelo;
	private String year;
	private SucursalDTO sucursal;
	private int stockMinimo;
	private int stock;
	private double precio;
	
	public ProductoDTO(int id, ModeloDTO modelo, String year, SucursalDTO sucursal, int stockMinimo,
			int stock, double precio) {
		//super();
		this.id = id;
		this.modelo = modelo;
		this.year = year;
		this.sucursal = sucursal;
		this.stockMinimo = stockMinimo;
		this.stock = stock;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ModeloDTO getModelo() {
		return modelo;
	}

	public void setModelo(ModeloDTO modelo) {
		this.modelo = modelo;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}


	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	public int getStockMinimo() {
		return stockMinimo;
	}

	public void setStockMinimo(int stockMinimo) {
		this.stockMinimo = stockMinimo;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public String getTipoString() {
		return null;
	}
	
	public String getCantidadPuertas(){
		return null;
	}
	
	
}
