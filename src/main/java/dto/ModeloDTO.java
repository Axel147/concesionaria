package dto;

public class ModeloDTO {
	private int id;
	private String modelo;
	private MarcaDTO marca;
	
	public ModeloDTO(int id, String modelo, MarcaDTO marca) {
		this.id = id;
		this.modelo = modelo;
		this.marca = marca;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public MarcaDTO getMarca() {
		return marca;
	}

	public void setMarca(MarcaDTO marca) {
		this.marca = marca;
	}
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((ModeloDTO)objeto).modelo.equals(this.modelo) && ((ModeloDTO)objeto).marca == (this.marca) && ((ModeloDTO)objeto).id == (this.id)){
            return true;
        }
        return false;
    }
	
    @Override
    public String toString() {
    	return this.modelo;
    }
	
}
