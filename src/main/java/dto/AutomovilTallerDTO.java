package dto;

public class AutomovilTallerDTO {
	private int id;
	private PaisDTO paisPatente;
	private String patente;
	private ModeloDTO modelo;
	private String year;
	
	public AutomovilTallerDTO(int id, PaisDTO paisPatente, String patente, ModeloDTO modelo, String year) {
		this.id = id;
		this.paisPatente = paisPatente;
		this.patente = patente;
		this.modelo = modelo;
		this.year = year;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public PaisDTO getPaisPatente() {
		return paisPatente;
	}

	public void setPaisPatente(PaisDTO paisPatente) {
		this.paisPatente = paisPatente;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public ModeloDTO getModelo() {
		return modelo;
	}

	public void setModelo(ModeloDTO modelo) {
		this.modelo = modelo;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
}