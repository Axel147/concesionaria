package dto;

public class EstadoReservaDTO {
	private int idEstado;
	private String estado;

	public EstadoReservaDTO(int idEstado, String estado)
	{
		this.idEstado = idEstado;
		this.estado = estado;
	}
	
	public int getIdEstado() 
	{
		return this.idEstado;
	}

	public void setIdEstado(int idEstado) 
	{
		this.idEstado = idEstado;
	}

	public String getEstado() 
	{
		return this.estado;
	}

	public void setEstado(String estado) 
	{
		this.estado = estado;
	}
	
	@Override
    public String toString(){
        return this.estado;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((EstadoReservaDTO)objeto).estado.equals(this.estado)&&((EstadoReservaDTO)objeto).idEstado == (this.idEstado)){
            return true;
        }
        return false;
    }
}
