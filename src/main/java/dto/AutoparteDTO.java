package dto;
 

public class AutoparteDTO extends ProductoDTO {
	
	private TipoAutoparteDTO tipo;
	
	
	
	public AutoparteDTO(int id, TipoAutoparteDTO tipoAutoparte, ModeloDTO modelo, String year, SucursalDTO sucursal, int stockMinimo,
			int stock, double precio) {
		super(id, modelo, year, sucursal, stockMinimo, stock, precio);
		this.tipo = tipoAutoparte;
	}
	
	
		
	public TipoAutoparteDTO getTipo() {
		return tipo;
	}

	public void setTipo(TipoAutoparteDTO tipo) {
		this.tipo = tipo;
	}


	@Override
	public String getTipoString() {
		return this.tipo.getNombre();
	}

	@Override
	public String toString() {
		return(tipo.getNombre() + " " + super.getModelo().getMarca().getNombre() + " " + super.getModelo().getModelo());
	}
}
