package dto;

public class TipoUsuarioDTO {
	
	private int idTipo;
	private String nombreTipo;
	
		
	public TipoUsuarioDTO(int idTipo, String nombreTipo) {
		super();
		this.idTipo = idTipo;
		this.nombreTipo = nombreTipo;
	}
	
	public int getIdTipo() {
		return idTipo;
	}
	
	public String getNombreTipo() {
		return nombreTipo;
	}

	@Override
	public String toString() {
		return nombreTipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idTipo;
		result = prime * result + ((nombreTipo == null) ? 0 : nombreTipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoUsuarioDTO other = (TipoUsuarioDTO) obj;
		if (idTipo != other.idTipo)
			return false;
		if (nombreTipo == null) {
			if (other.nombreTipo != null)
				return false;
		} else if (!nombreTipo.equals(other.nombreTipo))
			return false;
		return true;
	}
	
	
}
