package dto;

public class TarjetaDTO {
	private int id;
	private String numero;
	private String codigo;
	private String titular;
	private String vencimiento;
	
	public TarjetaDTO (int id, String numero, String codigo, String titular, String vencimiento) {
		this.id = id;
		this.numero = numero;
		this.codigo = codigo;
		this.titular = titular;
		this.vencimiento = vencimiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getVencimiento() {
		return vencimiento;
	}

	public void setVencimiento(String vencimiento) {
		this.vencimiento = vencimiento;
	}
	
}
