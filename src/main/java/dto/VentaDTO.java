package dto;

import java.util.*;

import org.apache.commons.lang3.tuple.Pair;

public class VentaDTO {
	private int id;
	private String fecha;
	private ClienteDTO cliente;
	private OpcionPagoDTO opcionPago;
	private List <Pair<StockAutomovilDTO, GarantiaDTO>> automoviles;
	private List<AutoparteDTO> autopartes;
	private double monto;
	private TarjetaDTO tarjeta;
	private EstadoVentaDTO estado;
	private SucursalDTO sucursal;
	private int idUsr;
	
	public VentaDTO(int id, String fecha, ClienteDTO cliente, OpcionPagoDTO pago,List<Pair<StockAutomovilDTO, GarantiaDTO>> automoviles, List<AutoparteDTO> autopartes, double monto, TarjetaDTO tarjeta, EstadoVentaDTO estado, SucursalDTO sucursal, int idUsr) {
		this.id = id;
		this.fecha = fecha;
		this.cliente = cliente;
		this.opcionPago = pago;
		this.monto = monto;
		this.automoviles = automoviles;
		this.autopartes = autopartes;
		this.tarjeta = tarjeta;
		this.estado = estado;
		this.sucursal = sucursal;
		this.idUsr = idUsr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public OpcionPagoDTO getOpcionPago() {
		return opcionPago;
	}

	public void setOpcionPago(OpcionPagoDTO opcionPago) {
		this.opcionPago = opcionPago;
	}

	public List<Pair<StockAutomovilDTO, GarantiaDTO>> getAutomoviles() {
		return automoviles;
	}

	public void setAutomoviles(List<Pair<StockAutomovilDTO, GarantiaDTO>> automoviles) {
		this.automoviles = automoviles;
	}

	public List<AutoparteDTO> getAutopartes() {
		return autopartes;
	}

	public void setAutopartes(List<AutoparteDTO> autopartes) {
		this.autopartes = autopartes;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public TarjetaDTO getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(TarjetaDTO tarjeta) {
		this.tarjeta = tarjeta;
	}

	public EstadoVentaDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoVentaDTO estado) {
		this.estado = estado;
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	public int getIdUsr() {
		return idUsr;
	}

	public void setIdUsr(int idUsr) {
		this.idUsr = idUsr;
	}
	
}
