package dto;

public class StockAutomovilDTO extends ProductoDTO {
	private String color;
	private String cantidadPuertas;
	private int kilometraje;
	

	public StockAutomovilDTO(int id, ModeloDTO modelo, String year, String color, String cantidadPuertas,int kilometraje, int stockMinimo, int stock,SucursalDTO sucursal, double precio) {
		super(id, modelo, year, sucursal, stockMinimo, stock, precio);
		this.color = color;
		this.cantidadPuertas = cantidadPuertas;
		this.kilometraje = kilometraje;
	}

	@Override
	public String getCantidadPuertas() {
		return cantidadPuertas;
	}

	public void setCantidadPuertas(String cantidadPuertas) {
		this.cantidadPuertas = cantidadPuertas;
	}

	public int getKilometraje() {
		return this.kilometraje;
	}

	public void setKilometraje(int kilometraje) {
		this.kilometraje = kilometraje;
	}

	@Override
	public String getTipoString() {
		return "Automovil";
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
