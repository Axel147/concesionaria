package dto;

public class EstadoVentaDTO {
	private int idEstado;
	private String estado;

	public EstadoVentaDTO(int idEstado, String estado)
	{
		this.idEstado = idEstado;
		this.estado = estado;
	}
	
	public int getIdEstado() 
	{
		return this.idEstado;
	}

	public void setIdEstado(int idEstado) 
	{
		this.idEstado = idEstado;
	}

	public String getEstado() 
	{
		return this.estado;
	}

	public void setEstado(String estado) 
	{
		this.estado = estado;
	}
	
	@Override
    public String toString(){
        return this.estado;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((EstadoVentaDTO)objeto).estado.equals(this.estado)&&((EstadoVentaDTO)objeto).idEstado == (this.idEstado)){
            return true;
        }
        return false;
    }
}
