package dto;

public class MotivoDTO {
	private int id;
	private boolean pintura;
	private boolean mecanica;
	private boolean electronica_electricidad;
	private boolean esteticaYAccesorios;
	
	public MotivoDTO(int id, boolean pintura, boolean mecanica, boolean electronica_electricidad, boolean esteticaYAccesorios) {
		super();
		this.id = id;
		this.pintura = pintura;
		this.mecanica = mecanica;
		this.electronica_electricidad = electronica_electricidad;
		this.esteticaYAccesorios = esteticaYAccesorios;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPintura() {
		return pintura;
	}

	public void setPintura(boolean pintura) {
		this.pintura = pintura;
	}

	public boolean isMecanica() {
		return mecanica;
	}

	public void setMecanica(boolean mecanica) {
		this.mecanica = mecanica;
	}

	public boolean isElectronica_electricidad() {
		return electronica_electricidad;
	}

	public void setElectronica_electricidad(boolean electronica_electricidad) {
		this.electronica_electricidad = electronica_electricidad;
	}

	public boolean isEsteticaYAccesorios() {
		return esteticaYAccesorios;
	}

	public void setEsteticaYAccesorios(boolean esteticaYAccesorios) {
		this.esteticaYAccesorios = esteticaYAccesorios;
	}
	
	@Override
	public String toString() {
		String texto = " ";
		if(this.isPintura()) {
			texto = texto + "Pintura ";
		}
		if(this.isMecanica()) {
			texto = texto + "Mecanica ";
		}
		if(this.isElectronica_electricidad()) {
			texto = texto + "Electrónica/Electricidad ";
		}
		if(this.isEsteticaYAccesorios()) {
			texto = texto +"Estética y Accesorios ";
		}
		return texto;
	}
	
	
}
