package dto;

public class ReservaDTO {
	
	private int idReserva; //obligatorio
	private String fechaEmitida; //obligatorio
	private String fechaTurno; //obligatorio
	private String horario; //obligatorio
	private ClienteDTO cliente; //obligatorio
	private AutomovilTallerDTO auto; //obligatorio
	private boolean notificada; //obligatorio
	private EstadoReservaDTO estado; //obligatorio
	private String motivoCancelacion; //opcional
	private MotivoDTO motivo;
	private boolean tieneGarantia;
	private String service;
	
	public ReservaDTO(int idReserva, String fechaEmitida, String fechaTurno, String horario, ClienteDTO cliente, AutomovilTallerDTO auto, boolean notificada, EstadoReservaDTO estado, String motivoCancelacion, MotivoDTO motivo, boolean tieneGarantia, String service )
	{
		this.idReserva = idReserva;
		this.fechaEmitida = fechaEmitida;
		this.fechaTurno = fechaTurno;
		this.horario = horario;
		this.cliente = cliente;
		this.auto = auto;
		this.notificada = notificada;
		this.estado = estado;
		this.motivoCancelacion = motivoCancelacion;
		this.motivo = motivo;
		this.tieneGarantia = tieneGarantia;
		this.service = service;
	}

	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public String getFechaEmitida() {
		return fechaEmitida;
	}

	public void setFechaEmitida(String fechaEmitida) {
		this.fechaEmitida = fechaEmitida;
	}

	public String getFechaTurno() {
		return fechaTurno;
	}

	public void setFechaTurno(String fechaTurno) {
		this.fechaTurno = fechaTurno;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	public AutomovilTallerDTO getAuto() {
		return auto;
	}

	public void setAuto(AutomovilTallerDTO auto) {
		this.auto = auto;
	}

	public boolean isNotificada() {
		return notificada;
	}

	public void setNotificada(boolean notificada) {
		this.notificada = notificada;
	}

	public EstadoReservaDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoReservaDTO estado) {
		this.estado = estado;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivo) {
		this.motivoCancelacion = motivo;
	}

	public MotivoDTO getMotivo() {
		return motivo;
	}

	public void setMotivo(MotivoDTO motivo) {
		this.motivo = motivo;
	}

	public boolean isTieneGarantia() {
		return tieneGarantia;
	}

	public void setTieneGarantia(boolean tieneGarantia) {
		this.tieneGarantia = tieneGarantia;
	}

	public String getService() {
		return service;
	}

	public void setTipoGarantia(String tipoGarantia) {
		this.service = tipoGarantia;
	}
}
