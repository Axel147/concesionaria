package dto;

public class MantenimientoDTO {

	private int id;
	private ReservaDTO reserva;
	private double manoDeObra;
	private boolean finalizado;
	private int venta;
	
	public MantenimientoDTO(int id, ReservaDTO reserva, double manoDeObra, boolean finalizado, int venta){
		this.id = id;
		this.reserva = reserva;
		this.manoDeObra = manoDeObra;
		this.finalizado = finalizado;
		this.venta = venta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ReservaDTO getReserva() {
		return reserva;
	}

	public void setReserva(ReservaDTO reserva) {
		this.reserva = reserva;
	}

	public double getManoDeObra() {
		return manoDeObra;
	}

	public void setManoDeObra(double manoDeObra) {
		this.manoDeObra = manoDeObra;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public int getVenta() {
		return venta;
	}

	public void setVenta(int venta) {
		this.venta = venta;
	}
	
	
	
}
