package dto;

public class EncuestaDTO {
	private int id;
	private int puntaje_p1;
	private int puntaje_p2;
	private int puntaje_p3;
	
	public EncuestaDTO(int id, int puntaje_p1, int puntaje_p2, int puntaje_p3) {
		this.id = id;
		this.puntaje_p1 = puntaje_p1;
		this.puntaje_p2 = puntaje_p2;
		this.puntaje_p3 = puntaje_p3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPuntaje_p1() {
		return puntaje_p1;
	}

	public void setPuntaje_p1(int puntaje_p1) {
		this.puntaje_p1 = puntaje_p1;
	}

	public int getPuntaje_p2() {
		return puntaje_p2;
	}

	public void setPuntaje_p2(int puntaje_p2) {
		this.puntaje_p2 = puntaje_p2;
	}

	public int getPuntaje_p3() {
		return puntaje_p3;
	}

	public void setPuntaje_p3(int puntaje_p3) {
		this.puntaje_p3 = puntaje_p3;
	}
	
	
}
