package dto;

public class TipoDocumentoDTO {
	private int idTipoDocumento;
	private String tipoDocumento;

	public TipoDocumentoDTO(int idTipoDocumento, String tipoDocumento){
		this.idTipoDocumento = idTipoDocumento;	
		this.tipoDocumento = tipoDocumento;
	}
	
	public int getIdTipoDocumento() 
	{
		return this.idTipoDocumento;
	}

	public void setIdTipoDocumento(int idTipoDocumento) 
	{
		this.idTipoDocumento = idTipoDocumento;
	}

	public String getTipoDocumento() 
	{
		return this.tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) 
	{
		this.tipoDocumento = tipoDocumento;
	}
	
	@Override
    public String toString(){
        return this.tipoDocumento;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((TipoDocumentoDTO)objeto).tipoDocumento.equals(this.tipoDocumento)&&((TipoDocumentoDTO)objeto).idTipoDocumento == (this.idTipoDocumento)){
            return true;
        }
        return false;
    }
}
