package dto;

public class ClienteDTO
{
	private int idCliente; //obligatorio
	private String nombre; //obligatorio
	private String apellido; //obligatorio
	private PaisDTO paisResidencia; //obligatorio
	private TipoDocumentoDTO tipoDocumento; //obligatorio
	private String numDocumento; //obligatorio
	private String mail; //obligatorio
	private String telefono; //opcional

	public ClienteDTO(int idCliente, String nombre, String apellido, PaisDTO paisResidencia, TipoDocumentoDTO tipoDocumento, String numDocumento, String mail, String telefono)
	{
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.apellido = apellido;
		this.paisResidencia = paisResidencia;
		this.tipoDocumento = tipoDocumento;
		this.numDocumento = numDocumento;
		this.mail = mail;
		this.telefono = telefono;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public PaisDTO getPaisResidencia() {
		return paisResidencia;
	}

	public void setPaisResidencia(PaisDTO paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	
	
}
