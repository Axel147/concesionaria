package presentacion.controlador;

import java.awt.event.ActionEvent;

import presentacion.reportes.FacturaMantenimiento;
import presentacion.reportes.FacturaVenta;
import presentacion.reportes.ReporteClientes;
import presentacion.reportes.ReporteDetalleVentasXMarca;
import presentacion.reportes.ReporteEncuesta;
import presentacion.reportes.ReporteEntregaModeloAuto;
import presentacion.reportes.ReporteEntregasMarca;
import presentacion.reportes.ReporteEntregas_x_Year;
import presentacion.reportes.ReporteEntregasxSucursal;
import presentacion.reportes.ReporteIngresosSucursal;
import presentacion.reportes.ReporteReserva;
import presentacion.reportes.ReporteReservasAnuales;
import presentacion.reportes.ReporteReservasDiarias;
import presentacion.reportes.ReporteStockPorSucursal;
import presentacion.reportes.ReporteVentaSucursalDetalle;
import presentacion.vista.VentanaReporte;

public class ControladorReporte {

	
	private VentanaReporte ventanaReporte;
	
	private ControladorGeneral cc;
	
	public ControladorReporte() {
		// se cargan las clases jframe
		this.ventanaReporte = VentanaReporte.getInstance();
		
		// eventos ventana
		// se crean eventos para los elementos del jframe VentanaReporte
		this.ventanaReporte.getBtnCancelar().addActionListener(cr->cancelarReportes(cr));
		this.ventanaReporte.getBtnGenerarReportes().addActionListener(gr->generarReportes(gr));
	}
	
	public void inicializar(ControladorGeneral cc) {
		this.cc = cc;
		this.ventanaReporte.mostrarVentana();
	}
	
	private void generarReportes(ActionEvent gr) {
		if(this.ventanaReporte.getRdbtnReporteCliente().isSelected()){
			ReporteClientes reporte = new ReporteClientes();
			reporte.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteReserva().isSelected()){
			ReporteReserva reporteReserva = new ReporteReserva();
			reporteReserva.mostrar();	
		}
		if(this.ventanaReporte.getRdbtnReporteReservaYear().isSelected()){
			ReporteReservasDiarias reporteReservaAnuales = new ReporteReservasDiarias();
			reporteReservaAnuales.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteReservaDiarias().isSelected()){
			ReporteReservasAnuales reporteReservaDiarias = new ReporteReservasAnuales();
			reporteReservaDiarias.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteStockPorSucursal().isSelected()){
			ReporteStockPorSucursal reporteStockXSucursal = new ReporteStockPorSucursal();
			reporteStockXSucursal.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteDetalleVentasXMarca().isSelected()){
			ReporteDetalleVentasXMarca reporteDetalleVentasXMarca = new ReporteDetalleVentasXMarca();
			reporteDetalleVentasXMarca.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteEntregaModeloAuto().isSelected()){
			ReporteEntregaModeloAuto reporteEntregaModeloAuto = new ReporteEntregaModeloAuto();
			reporteEntregaModeloAuto.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteEntregasMarca().isSelected()){
			ReporteEntregasMarca reporteEntregasMarca = new ReporteEntregasMarca();
			reporteEntregasMarca.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteEntregasXSucursal().isSelected()){
			ReporteEntregasxSucursal reporteEntregasXSucursal = new ReporteEntregasxSucursal();
			reporteEntregasXSucursal.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteEntregasXYear().isSelected()){
			ReporteEntregas_x_Year reporteEntregasXYear = new ReporteEntregas_x_Year();
			reporteEntregasXYear.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteVentaSucursalDetalle().isSelected()){
			ReporteVentaSucursalDetalle reporteVentaSucursalDetalle = new ReporteVentaSucursalDetalle();
			reporteVentaSucursalDetalle.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteIngresosSucursal().isSelected()){
			ReporteIngresosSucursal reporteIngresosSucursal = new ReporteIngresosSucursal();
			reporteIngresosSucursal.mostrar();
		}
		if(this.ventanaReporte.getRdbtnReporteEncuesta().isSelected()){
			ReporteEncuesta reporteEncuesta = new ReporteEncuesta();
			reporteEncuesta.mostrar();
		}
		this.cc.inicializar();
		this.ventanaReporte.cerrar();
	}
	
	protected void generarFacturaVenta(int idVenta, boolean esDeMantenimiento){
		if(esDeMantenimiento){
			FacturaMantenimiento facturaMantenimiento = new FacturaMantenimiento(idVenta);
			facturaMantenimiento.mostrar();
		}else{
			FacturaVenta facturaVenta = new FacturaVenta(idVenta);
			facturaVenta.mostrar();
		}
	}
	
	private void cancelarReportes(ActionEvent cr) {
		this.cc.inicializar();
		this.ventanaReporte.cerrar();
	}
	
}
