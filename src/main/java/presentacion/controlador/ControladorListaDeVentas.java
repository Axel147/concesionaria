package presentacion.controlador;

import presentacion.vista.ListaDeVentas;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import dto.VentaDTO;
import modelo.Venta;

public class ControladorListaDeVentas {
	private ListaDeVentas ventanaListaDeVentas;
	
	private Venta venta;
	
	private List<VentaDTO> ventasEnTabla;
	
	private ControladorGeneral controladorGeneral;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	private List<Triple<Integer,Integer,String>> filtrosBusqueda;
	
	public ControladorListaDeVentas(Venta venta) {
		super();
		this.venta = venta;
		
		this.ventanaListaDeVentas = ListaDeVentas.getInstance();
		
		this.ventanaListaDeVentas.getBtnCancelar().addActionListener(cp -> cancelarPago(cp));
		this.ventanaListaDeVentas.getBtnNotificarPago().addActionListener(np -> notificarPago(np));
		this.ventanaListaDeVentas.getBtnVolver().addActionListener(vdlv -> volverDesdeListaDeVentas(vdlv));
		this.ventanaListaDeVentas.getBtnGenerarFactura().addActionListener(gf->generarFactura(gf));
		this.ventanaListaDeVentas.getBtnAgregarFiltro().addActionListener(afb-> agregarFiltroBusqueda(afb));
		this.ventanaListaDeVentas.getBtnDeshacerFiltros().addActionListener(dfb -> deshacerFiltrosBusqueda(dfb));
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}
	
	public void inicializar(ControladorGeneral cg) {
		this.controladorGeneral = cg;
		refrescarTablaVentas();
		this.ventanaListaDeVentas.mostrarVentana();
	}

	
	public void refrescarTablaVentas() {
		this.ventasEnTabla = this.venta.obtenerVentas();
		this.ventanaListaDeVentas.llenarTabla(this.ventasEnTabla);
	}

	private void volverDesdeListaDeVentas(ActionEvent vdlv) {
		this.ventanaListaDeVentas.cerrar();
		this.controladorGeneral.inicializar();
	}

	private void notificarPago(ActionEvent np) {
		int filaSeleccionada = this.ventanaListaDeVentas.getTablaVentas().getSelectedRow();
		if(filaSeleccionada!=-1) {
			VentaDTO venta_a_notificar = this.ventasEnTabla.get(filaSeleccionada);
			if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
				if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
					if(venta_a_notificar.getEstado().getIdEstado()==1) {
						venta_a_notificar.getEstado().setIdEstado(2);
						if(this.venta.actualizarEstado(venta_a_notificar)) {
							this.ventanaListaDeVentas.mensajeNotificacionExitosa();
							this.refrescarTablaVentas();
						} else {
							this.ventanaListaDeVentas.errorNotificar();
						}
					} else {
						this.ventanaListaDeVentas.advertenciaEstado();
					}
				}else{
					if(this.id_TipoUser_Sucursal.getRight().getIdSucursal()==venta_a_notificar.getSucursal().getIdSucursal()){
						if(venta_a_notificar.getEstado().getIdEstado()==1) {
							venta_a_notificar.getEstado().setIdEstado(2);
							if(this.venta.actualizarEstado(venta_a_notificar)) {
								this.ventanaListaDeVentas.mensajeNotificacionExitosa();
								this.refrescarTablaVentas();
							} else {
								this.ventanaListaDeVentas.errorNotificar();
							}
						} else {
							this.ventanaListaDeVentas.advertenciaEstado();
						}
					}else{
						this.ventanaListaDeVentas.advertirUsuarioNoPerteneceASucursalNotificarPago();
					}
				}
			}else{
				this.ventanaListaDeVentas.advertirUsuarioNoAutorizadoNotificarPago();
			}
		} else {
			this.ventanaListaDeVentas.errorSeleccion();
		}
	}

	private void cancelarPago(ActionEvent cp) {
		int filaSeleccionada = this.ventanaListaDeVentas.getTablaVentas().getSelectedRow();
		if(filaSeleccionada!=-1) {
			VentaDTO venta_a_cancelar = this.ventasEnTabla.get(filaSeleccionada);
			if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
				if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
					if(venta_a_cancelar.getEstado().getIdEstado()==1) {
						venta_a_cancelar.getEstado().setIdEstado(3);
						if(this.venta.actualizarEstado(venta_a_cancelar)) {
							this.ventanaListaDeVentas.mensajeCancelacionExitosa();
							this.refrescarTablaVentas();
						} else {
							this.ventanaListaDeVentas.errorCancelar();
						}
					} else {
						this.ventanaListaDeVentas.advertenciaEstado();
					}
				}else{
					if(this.id_TipoUser_Sucursal.getRight().getIdSucursal()==venta_a_cancelar.getSucursal().getIdSucursal()){
						if(venta_a_cancelar.getEstado().getIdEstado()==1) {
							venta_a_cancelar.getEstado().setIdEstado(3);
							if(this.venta.actualizarEstado(venta_a_cancelar)) {
								this.ventanaListaDeVentas.mensajeCancelacionExitosa();
								this.refrescarTablaVentas();
							} else {
								this.ventanaListaDeVentas.errorCancelar();
							}
						} else {
							this.ventanaListaDeVentas.advertenciaEstado();
						}
					}else{
						this.ventanaListaDeVentas.advertirUsuarioNoPerteneceASucursalCancelarPago();
					}
				}
			}else{
				this.ventanaListaDeVentas.advertirUsuarioNoAutorizadoCancelarPago();
			}
		} else {
			this.ventanaListaDeVentas.errorSeleccion();
		}	
	}
	
	private void generarFactura(ActionEvent gf) {
		int filaSeleccionada = this.ventanaListaDeVentas.getTablaVentas().getSelectedRow();
		if(filaSeleccionada!=-1) {
			VentaDTO venta_a_facturar = this.ventasEnTabla.get(filaSeleccionada);
			boolean esDeMantenimiento = this.controladorGeneral.esDeMantenimiento(venta_a_facturar.getId());
			if(esDeMantenimiento&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
				this.controladorGeneral.generarFacturaVenta(venta_a_facturar.getId(), esDeMantenimiento);
			}else{ 
				if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
					if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5){
						if(this.id_TipoUser_Sucursal.getRight().getIdSucursal()==venta_a_facturar.getSucursal().getIdSucursal()){
							this.controladorGeneral.generarFacturaVenta(venta_a_facturar.getId(), esDeMantenimiento);
						}else{
							this.ventanaListaDeVentas.advertirUsuarioNoPerteneceASucursalGenerarFactura();
						}
					}else{
						this.controladorGeneral.generarFacturaVenta(venta_a_facturar.getId(), esDeMantenimiento);
					}
				}else{
					this.ventanaListaDeVentas.advertirUsuarioTallerNoPermiteGenerarFactura();
				}
			}
		}else{
			this.ventanaListaDeVentas.errorSeleccion();
		}
	}
	
	private void agregarFiltroBusqueda(ActionEvent afb) {
		String valor = this.ventanaListaDeVentas.getTxtBusqueda().getText();
		if(!ControladorCampos.CampoVacio(valor)) {
			int campo = -1;
			if(this.ventanaListaDeVentas.getRdbtnNombre().isSelected()) {
				campo = 0;
			} else if(this.ventanaListaDeVentas.getRdbtnApellido().isSelected()) {
				campo = 1;
			} else if(this.ventanaListaDeVentas.getRdbtnNroDocumento().isSelected()) {
				campo = 2;
			} else if(this.ventanaListaDeVentas.getRdbtnFecha().isSelected()) {
				campo = 3;
			} else if(this.ventanaListaDeVentas.getRdbtnEstado().isSelected()) {
				campo = 4;
			} else if(this.ventanaListaDeVentas.getRdbtnSucursal().isSelected()) {
				campo = 5;
			}
			if(campo!=-1) {
				this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,2,valor));
				this.ventasEnTabla = this.venta.filtrarVentas(this.filtrosBusqueda);
				this.ventanaListaDeVentas.llenarTabla(ventasEnTabla);
			}
		}
	}
	
	private void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
		this.refrescarTablaVentas();
	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}
	
	
	
}
