package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.ClienteDTO;
import dto.MantenimientoDTO;
import dto.ReservaDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import dto.TipoUsuarioDTO;
import modelo.Mantenimiento;
import presentacion.vista.ListaAutopartesXmantenimiento;
import presentacion.vista.ListaDeMantenimientos;
import presentacion.vista.VentanaVentas;

public class ControladorMantenimiento {
	
	private List<MantenimientoDTO> mantenimientosEnTabla;
	private List<Pair<AutoparteDTO, Integer>> autopartesXmantenimientoEnTabla;
	
	private ListaDeMantenimientos ventanaListaDeMantenimientos;
	private ListaAutopartesXmantenimiento ventanaListaAutopartesXmantenimiento;
	private VentanaVentas ventanaVentas;
	
	private Mantenimiento mantenimiento;
	
	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	private List<Triple<Integer,Integer,String>> filtrosBusqueda;
	
	public ControladorMantenimiento(Mantenimiento mantenimiento){
		this.mantenimiento = mantenimiento;
		
		this.ventanaListaDeMantenimientos = ListaDeMantenimientos.getInstance();
		this.ventanaListaAutopartesXmantenimiento = ListaAutopartesXmantenimiento.getInstance();
		
		this.ventanaListaDeMantenimientos.getBtnVerAutopartesUtilizadas().addActionListener(gau ->gestionarAutopartesUtilizadas(gau));
		this.ventanaListaDeMantenimientos.getBtnFinalizar().addActionListener(fm ->finalizarMantenimiento(fm));
		this.ventanaListaDeMantenimientos.getBtnVolver().addActionListener(vm ->volverDesdeMantenimiento(vm));
		this.ventanaListaDeMantenimientos.getBtnModificarManoDeObra().addActionListener(mmo ->modificarManoDeObra(mmo));
		this.ventanaListaDeMantenimientos.getBtnAgregarFiltro().addActionListener(afb -> aplicarFiltrosBusqueda(afb));
		this.ventanaListaDeMantenimientos.getBtnDeshacerFiltros().addActionListener(dfb -> deshacerFiltrosBusqueda(dfb));
		
		this.ventanaListaAutopartesXmantenimiento.getBtnAgregar().addActionListener(agauxm ->seleccionarAutoparteXmantenimiento(agauxm));
		this.ventanaListaAutopartesXmantenimiento.getBtnEliminar().addActionListener(eauxm ->eliminarAutoparteXmantenimiento(eauxm));
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
		
	}
	
	public void inicializar(ControladorGeneral cg) {
		this.cg = cg;
		this.refrescarTablaMantenimientos();
		this.ventanaListaDeMantenimientos.mostrarVentana();
	}
	
	protected boolean crearMantenimiento(ReservaDTO reserva){
		MantenimientoDTO nuevoMantenimiento = new MantenimientoDTO(0, reserva, 0, false, 0); 
		return this.mantenimiento.agregarMantenimiento(nuevoMantenimiento);
	}
	
	private void gestionarAutopartesUtilizadas(ActionEvent gau){
		int[] filasSeleccionadas = this.ventanaListaDeMantenimientos.getTablaMantenimientos().getSelectedRows();
		if (filasSeleccionadas.length == 1){
			for (int fila : filasSeleccionadas) {
				if(!this.mantenimientosEnTabla.get(fila).isFinalizado()){
					this.ventanaListaAutopartesXmantenimiento.setMantenimiento(this.mantenimientosEnTabla.get(fila));
					this.autopartesXmantenimientoEnTabla = this.mantenimiento.obtenerAutopartesUtilizadas(this.mantenimientosEnTabla.get(fila));
					this.ventanaListaAutopartesXmantenimiento.llenarTabla(this.autopartesXmantenimientoEnTabla);
					this.ventanaListaAutopartesXmantenimiento.setVentanaBloqueada(this.ventanaListaDeMantenimientos);
					this.ventanaListaDeMantenimientos.setEnabled(false);
					this.ventanaListaAutopartesXmantenimiento.mostrarVentana();
				}
			}
		}else this.ventanaListaDeMantenimientos.advertirSeleccionIncorrecta();
	}
	
	private void finalizarMantenimiento(ActionEvent fm){
		int[] filasSeleccionadas = this.ventanaListaDeMantenimientos.getTablaMantenimientos().getSelectedRows();
		if (filasSeleccionadas.length == 1){
			for (int fila : filasSeleccionadas) {
				if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
					if(!this.mantenimientosEnTabla.get(fila).isFinalizado()){
						this.ventanaVentas = this.cg.dameVentanaVentas();
						ClienteDTO cliente = this.mantenimientosEnTabla.get(fila).getReserva().getCliente();
						this.ventanaVentas.setCliente(cliente);
						this.ventanaVentas.setTxtCliente(cliente.getApellido()+" "+cliente.getNombre()+"/"+cliente.getNumDocumento());
						List<Pair<AutoparteDTO,Integer>> autopartesUtilizadas = this.mantenimiento.obtenerAutopartesUtilizadas(this.mantenimientosEnTabla.get(fila));
						List<Pair<TipoAutoparteDTO,Integer>> tipoAutopartesEnGarantia = this.mantenimiento.obtenerTipoAutopartesEnGarantia(this.mantenimientosEnTabla.get(fila)); 
						for(int i=0;i<autopartesUtilizadas.size();i++){
							int cantidadGarantizada = 0;
							for(Pair<TipoAutoparteDTO,Integer> tipoAutoparteEnGarantia : tipoAutopartesEnGarantia){
								if(autopartesUtilizadas.get(i).getLeft().getTipo().getId()==tipoAutoparteEnGarantia.getLeft().getId()
										&& autopartesUtilizadas.get(i).getLeft().getModelo().getId()==this.mantenimientosEnTabla.get(fila).getReserva().getAuto().getModelo().getId()
										&& autopartesUtilizadas.get(i).getLeft().getYear().equals(this.mantenimientosEnTabla.get(fila).getReserva().getAuto().getYear())){
									cantidadGarantizada = tipoAutoparteEnGarantia.getRight();
								}
							}
							for(int z=0;z<autopartesUtilizadas.get(i).getRight();z++){
								
								if(!(cantidadGarantizada>0)){
									this.ventanaVentas.agregarAutoparte(autopartesUtilizadas.get(i).getLeft(), false);
								}else{
									this.ventanaVentas.agregarAutoparte(autopartesUtilizadas.get(i).getLeft(), true);
									cantidadGarantizada--;
								}
							}
						}
						this.ventanaVentas.agregarManoDeObra(this.mantenimientosEnTabla.get(fila).getManoDeObra());
						this.ventanaVentas.setMantenimiento(this.mantenimientosEnTabla.get(fila));
						this.ventanaVentas.setVentaMantenimiento(true);
						this.ventanaVentas.getBtnCargarCliente().setEnabled(false);
						this.ventanaVentas.getBtnCargarProducto().setEnabled(false);
						this.ventanaVentas.getBtnCargarAutomovil().setEnabled(false);
						this.ventanaVentas.getBtnQuitar().setEnabled(false);
						this.ventanaVentas.mostrarVentana();
						this.refrescarTablaMantenimientos();
					}
				}else{
					this.ventanaListaDeMantenimientos.advertirUsuarioNoAutorizadoFinalizarMantenimiento();
				}
			}
		}else this.ventanaListaDeMantenimientos.advertirSeleccionIncorrecta();
	}
	
	private void seleccionarAutoparteXmantenimiento(ActionEvent agauxm){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
			this.cg.seleccionarAutoparteMantenimiento(this.ventanaListaAutopartesXmantenimiento);
		}else this.ventanaListaDeMantenimientos.advertirUsuarioNoAutorizadoAgregarAutoparte();
	}
	
	protected void agregarAutoparteXmantenimiento(Pair<AutoparteDTO, Integer> autoparteXmantenimiento){
		MantenimientoDTO mantenimientoSeleccionado = this.ventanaListaAutopartesXmantenimiento.getMantenimiento();
		this.mantenimiento.agregarAutoparteXmantenimiento(mantenimientoSeleccionado, autoparteXmantenimiento.getLeft(), autoparteXmantenimiento.getRight());
		this.autopartesXmantenimientoEnTabla = this.mantenimiento.obtenerAutopartesUtilizadas(mantenimientoSeleccionado);
		this.ventanaListaAutopartesXmantenimiento.llenarTabla(this.autopartesXmantenimientoEnTabla);
	}
	
	private void eliminarAutoparteXmantenimiento(ActionEvent eauxm){
		int[] filasSeleccionadas = this.ventanaListaAutopartesXmantenimiento.getTablaAutopartesXmantenimiento().getSelectedRows();
		if (filasSeleccionadas.length == 1) {
			for (int fila : filasSeleccionadas) {
				if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
					if (!this.mantenimiento.eliminarAutoparteXmantenimiento(this.autopartesXmantenimientoEnTabla.get(fila).getLeft(), this.autopartesXmantenimientoEnTabla.get(fila).getRight())) {
						this.ventanaListaAutopartesXmantenimiento.advertenciaFallaEliminacion();
					} else {
						MantenimientoDTO mantenimientoSeleccionado = this.ventanaListaAutopartesXmantenimiento.getMantenimiento();
						this.autopartesXmantenimientoEnTabla = this.mantenimiento.obtenerAutopartesUtilizadas(mantenimientoSeleccionado);
						this.ventanaListaAutopartesXmantenimiento.llenarTabla(this.autopartesXmantenimientoEnTabla);
					}
				}else this.ventanaListaDeMantenimientos.advertirUsuarioNoAutorizadoEliminarAutoparte();
			}
		} else {
			this.ventanaListaAutopartesXmantenimiento.advertirSeleccionBorrarIncorrecta();
		}
	}
	
	private void modificarManoDeObra(ActionEvent mmo){
		try{
			int[] filasSeleccionadas = this.ventanaListaDeMantenimientos.getTablaMantenimientos().getSelectedRows();
			if (filasSeleccionadas.length == 1) {
				for (int fila : filasSeleccionadas) {
					if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1){
						if(!this.mantenimientosEnTabla.get(fila).isFinalizado()){
							String manoDeObraString = JOptionPane.showInputDialog("Ingrese un nuevo valor para la mano de obra");
							if(manoDeObraString!=null){
								double manoDeObra = Double.parseDouble(manoDeObraString);
								MantenimientoDTO mantenimientoSeleccionado = this.mantenimientosEnTabla.get(fila);
								mantenimientoSeleccionado.setManoDeObra(manoDeObra);
								
								if (!this.mantenimiento.modificarManoDeObra(mantenimientoSeleccionado)) {
									this.ventanaListaDeMantenimientos.advertirErrorModificarManoDeObra();
								}
							}
						}
					}else{
						this.ventanaListaDeMantenimientos.advertirUsuarioNoAutorizadoModificarManoDeObra();
					}
				}
			} else {
				this.ventanaListaDeMantenimientos.advertirSeleccionIncorrecta();
			}
			this.refrescarTablaMantenimientos();
		}catch(Exception ex){
//			ex.printStackTrace();
			this.ventanaListaDeMantenimientos.advertirErrorModificarManoDeObra();
		}
	}
	
	private void aplicarFiltrosBusqueda(ActionEvent afb) {
		String valor = this.ventanaListaDeMantenimientos.getTxtBusqueda().getText();
		if(!ControladorCampos.CampoVacio(valor)) {
			int campo = -1;
			if(this.ventanaListaDeMantenimientos.getRdbtnNombreCliente().isSelected()) {
				campo = 0;
			} else if(this.ventanaListaDeMantenimientos.getRdbtnApellidoCliente().isSelected()) {
				campo = 1;
			} else if(this.ventanaListaDeMantenimientos.getRdbtnNroDocumentoCliente().isSelected()) {
				campo = 2;
			} else if (this.ventanaListaDeMantenimientos.getRdbtnEmailCliente().isSelected()) {
				campo = 3;
			} else if (this.ventanaListaDeMantenimientos.getRdbtnPatenteAuto().isSelected()) {
				campo = 4;
			}
			this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,2,valor));
			this.mantenimientosEnTabla = this.mantenimiento.filtrarMantenimientos(this.filtrosBusqueda);
			this.ventanaListaDeMantenimientos.llenarTabla(mantenimientosEnTabla);
	}
	}
	
	private void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
		this.refrescarTablaMantenimientos();
	}
	
	private void volverDesdeMantenimiento(ActionEvent vm) {
		if (this.ventanaListaAutopartesXmantenimiento.isVisible()) {
			this.ventanaListaAutopartesXmantenimiento.cerrar();
		}
		this.ventanaListaDeMantenimientos.cerrar();
		this.cg.inicializar();
	}
	
	protected boolean esDeMantenimiento(int idVenta){
		return this.mantenimiento.esDeMantenimiento(idVenta);
	}
	
	protected void refrescarMantenimientos(){
		this.refrescarTablaMantenimientos();
	}
	
	private void refrescarTablaMantenimientos() {
		this.mantenimientosEnTabla = this.mantenimiento.obtenerMantenimientos();
		this.ventanaListaDeMantenimientos.llenarTabla(this.mantenimientosEnTabla);
	}

	protected void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}
	
	

}
