package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.util.List;

import dto.MarcaDTO;
import dto.ModeloDTO;
import modelo.Marca;
import modelo.Modelo;
import presentacion.vista.ListaDeAutomoviles;
import presentacion.vista.ListaDeModelos;
import presentacion.vista.VentanaAutopartes;
import presentacion.vista.VentanaModelo;

public class ControladorModelo {

	private List<MarcaDTO> marcasEnTabla; // lista de marcas
	private List<ModeloDTO> modelosEnTabla; // lista de modelos

	private VentanaModelo ventanaModelo; // ventana para agregar o modificar un modelo

	private ListaDeModelos ventanaListaDeModelos; // ventana con todos los modelos(ABM)
	private ListaDeAutomoviles ventanaListaDeAutomoviles;

	private Marca marca;
	private Modelo modelo;

	private ControladorGeneral cg;
	
	private VentanaAutopartes ventanaAutopartes;

	public ControladorModelo(Modelo modelo, Marca marca) {
		// se cargan las clases de tipo modelo
		this.marca = marca;
		this.modelo = modelo;

		// se cargan las clases jframe
		this.ventanaListaDeModelos = ListaDeModelos.getInstance();
		this.ventanaModelo = VentanaModelo.getInstance();
		this.ventanaListaDeAutomoviles = ListaDeAutomoviles.getInstance();
		this.ventanaAutopartes = VentanaAutopartes.getInstance();

		// eventos ventana

		// se crean eventos para los elementos del jframe ventanaListaDeModelos
		this.ventanaListaDeModelos.getBtnBuscar().addActionListener(buscModelo -> buscarModelo(buscModelo));
		this.ventanaListaDeModelos.getBtnAgregar().addActionListener(aModelo -> ventanaAgregarModelo(aModelo));
		this.ventanaListaDeModelos.getBtnEditar().addActionListener(mModelo -> ventanaEditarModelo(mModelo));
		this.ventanaListaDeModelos.getBtnBorrar().addActionListener(bModelo -> borrarModelo(bModelo));
		this.ventanaListaDeModelos.getComboBoxMarca().addActionListener(am -> actualizarModelo(am));		

		// se crean eventos para los elementos del jframe ventanaModelo
		this.ventanaModelo.getBtnGuardar().addActionListener(gModelo -> guardarModelo(gModelo));
		this.ventanaListaDeModelos.getBtnVolver().addActionListener(vdm->volverDesdeModelo(vdm));
	}

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
		int marca;
		if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
			marca = 0;
		else
			marca = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
		this.ventanaListaDeModelos.getBtnVolver().setVisible(true);
		this.refrescarTablaModelos(marca);
		this.ventanaListaDeModelos.mostrarVentana();
	}

	public void ventanaAgregarModelo(ActionEvent aModelo) {
		this.ventanaModelo.setTipoVentana(1);
		this.ventanaModelo.setTitle("Agregar Modelo");
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.ventanaModelo.llenarMarcas(this.marcasEnTabla);
		this.ventanaModelo.setVentanaModeloLlamadora(this.ventanaListaDeModelos);
		this.ventanaModelo.setLlamadoPorListaDeModelos(true);
		this.ventanaListaDeModelos.setEnabled(false);
		this.ventanaModelo.mostrarVentana();
	}

	private void ventanaEditarModelo(ActionEvent mModelo) {
		int[] filasSeleccionadas = this.ventanaListaDeModelos.getTablaModelos().getSelectedRows();
		if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para editar de la tabla modelos
		{
			for (int fila : filasSeleccionadas) {
				this.ventanaModelo.setTipoVentana(2);
				this.ventanaModelo.setTitle("Editar Modelo");
				this.marcasEnTabla = marca.obtenerMarca();
				this.ventanaModelo.llenarMarcas(this.marcasEnTabla);
				this.ventanaModelo.getListadoMarcas().setEnabled(false);
				this.ventanaModelo.getListadoMarcas()
						.setSelectedItem(((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()));
				this.ventanaModelo.setIdModelo(this.modelosEnTabla.get(fila).getId());
				this.ventanaModelo.setNombreModelo(this.modelosEnTabla.get(fila).getModelo().toString());
				this.ventanaModelo.setVentanaModeloLlamadora(this.ventanaListaDeModelos);
				this.ventanaModelo.setLlamadoPorListaDeModelos(true);
				this.ventanaListaDeModelos.setEnabled(false);
				/*
				 * this.ventanaListaDeAutomoviles.setVentanaVentasLlamadora(this.ventanaVentas);//agregado
		this.ventanaListaDeAutomoviles.setLlamadoPorVentas(true);//agregado
		this.ventanaVentas.setEnabled(false);
		
				 */
				this.ventanaModelo.mostrarVentana();
			}
		} else
			this.ventanaListaDeModelos.advertirSeleccionIncorrecta();
	}

	public void guardarModelo(ActionEvent gModelo) {
		String nombre = this.ventanaModelo.getTxtNombreModelo().getText();
		MarcaDTO marca = ((MarcaDTO) this.ventanaModelo.getListadoMarcas().getSelectedItem());

		if (ControladorCampos.CampoVacio(nombre)) {
			this.ventanaModelo.advertenciaFaltaNombreModelo();
		} else {
			if (!ControladorCampos.NombreModeloValido(nombre)) {
				this.ventanaModelo.advertenciaNombreModeloInvalido();
			} else {
				ModeloDTO nuevoModelo = new ModeloDTO(0, nombre, marca);
				if (this.ventanaModelo.getTipoVentana() == 1) {
					if (!this.modelo.agregarModelo(nuevoModelo)) {
						this.ventanaModelo.advertenciaModeloRepetido();
					} else {
						if(ventanaListaDeModelos.getLlamadoDesde() == 1) {
							this.marcasEnTabla = this.marca.obtenerMarca();
							this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
							int marcas;
							if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
								marcas = 0;
							else
								marcas = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
							this.ventanaListaDeModelos.getBtnVolver().setVisible(true);
							this.refrescarTablaModelos(marcas);
						}
						this.ventanaModelo.advertenciaModeloAgregado();
						refrescarTablaModelos(((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId());
						this.ventanaModelo.cerrar();
					}
				} else {
					nuevoModelo.setId(this.ventanaModelo.getIdModelo());
					if (!this.modelo.editarModelo(nuevoModelo)) {
						this.ventanaModelo.advertenciaModeloRepetido();
					} else {
						if(ventanaListaDeModelos.getLlamadoDesde() == 1) {
							this.marcasEnTabla = this.marca.obtenerMarca();
							this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
							int marcas;
							if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
								marcas = 0;
							else
								marcas = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
							this.ventanaListaDeModelos.getBtnVolver().setVisible(true);
							this.refrescarTablaModelos(marcas);
						}
						this.ventanaModelo.advertenciaModeloAgregado();
						this.refrescarTablaModelos(((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId());
						this.ventanaModelo.cerrar();
					}
				}
			}
		}
	}

	public void borrarModelo(ActionEvent bModelo) {
		int[] filasSeleccionadas = this.ventanaListaDeModelos.getTablaModelos().getSelectedRows();
		for (int fila : filasSeleccionadas) {
			if (!this.modelo.borrarModelo(this.modelosEnTabla.get(fila)))
				this.ventanaListaDeModelos.advertenciaModeloUtilizandose();
			else {
				if(ventanaListaDeModelos.getLlamadoDesde() == 1) {
					this.marcasEnTabla = this.marca.obtenerMarca();
					this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
					int marcas;
					if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
						marcas = 0;
					else
						marcas = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
					this.ventanaListaDeModelos.getBtnVolver().setVisible(true);
					this.refrescarTablaModelos(marcas);
				}
				this.refrescarTablaModelos(
						((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId());
			}
		}
	}

	private void actualizarModelo(ActionEvent am) {
		if (this.ventanaListaDeModelos.getComboBoxMarca().isLoaded()) {
			this.modelosEnTabla = this.modelo.obtenerModelo(
					((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId());
			this.ventanaListaDeModelos.llenarTablaModelos(this.modelosEnTabla);
		}
	}

	private void buscarModelo(ActionEvent buscModelo) {
		String dato = this.ventanaListaDeModelos.getTxtBusqueda().getText();
		this.modelosEnTabla = modelo.buscarModeloPor(dato,
				((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId());
		this.ventanaListaDeModelos.llenarTablaModelos(this.modelosEnTabla);
	}
	
	/*private void volverDesdeModelo(ActionEvent vdc) {
		if (this.ventanaModelo.isVisible()) {
			this.ventanaModelo.cerrar();
		}
		this.ventanaListaDeModelos.dispose();
		this.cg.inicializar();
	}*/

	// refresca tabla de ListaDeModelos
	public void refrescarTablaModelos(int Marca) {
		this.modelosEnTabla = this.modelo.obtenerModelo(Marca);
		this.ventanaListaDeModelos.llenarTablaModelos(this.modelosEnTabla);
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}

	private void volverDesdeModelo(ActionEvent vdm) {
		if (this.ventanaListaDeModelos.isVisible()) {
			this.ventanaListaDeModelos.cerrar();
		}
		this.cg.inicializar();
	}

	public void configurarModelo() {
		this.ventanaListaDeModelos.setLlamadoDesde(1);
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
		int marca;
		if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
			marca = 0;
		else
			marca = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
		this.refrescarTablaModelos(marca);
		this.ventanaListaDeModelos.getBtnVolver().setVisible(false);
		this.ventanaListaDeModelos.setVentanaListaDeAutomoviles(this.ventanaListaDeAutomoviles);
		this.ventanaListaDeAutomoviles.setEnabled(false);
		
		this.ventanaListaDeModelos.mostrarVentana();
	}
	
	public void configurarModeloDesdeAutoparte() {
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.ventanaListaDeModelos.llenarMarca(this.marcasEnTabla);
		int marca;
		if (this.ventanaListaDeModelos.getComboBoxMarca().getItemCount() == 0)
			marca = 0;
		else
			marca = ((MarcaDTO) this.ventanaListaDeModelos.getComboBoxMarca().getSelectedItem()).getId();
		this.refrescarTablaModelos(marca);
		this.ventanaListaDeModelos.getBtnVolver().setVisible(false);
		this.ventanaListaDeModelos.setLlamadoDesde(2);
		this.ventanaListaDeModelos.setVentanaAutopartes(this.ventanaAutopartes);
		this.ventanaAutopartes.setEnabled(false);
		
		this.ventanaListaDeModelos.mostrarVentana();
	}

	public ListaDeAutomoviles getVentanaListaDeAutomoviles() {
		return ventanaListaDeAutomoviles;
	}

	public void setVentanaListaDeAutomoviles(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
	}

	public void setVentanaAutopartes(VentanaAutopartes ventanaAutopartes) {
		this.ventanaAutopartes = ventanaAutopartes;
		
	}
	
	
}
