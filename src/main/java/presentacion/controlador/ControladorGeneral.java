package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventListener;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.ReservaDTO;
import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;
import persistencia.conexion.Conexion;
import presentacion.vista.*;

public class ControladorGeneral implements EventListener {
	private VentanaConexion ventanaConexion;
	private VentanaPrincipal ventanaPrincipal;
	private VentanaLoging ventanaLoging;

	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorSucursal cs;
	private ControladorReserva cr;
	private ControladorStockAutomoviles csa;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private ControladorMarca cmarca;
	private ControladorReporte creporte;
	private Notificador notificador;
	private ControladorVenta controladorVenta;
	private ControladorAlarmaStock cas;
	private ControladorMantenimiento controladorMantenimiento;
	private ControladorEncuesta controladorEncuesta;
	private ControladorListaDeVentas controladorListaDeVentas;
	
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	public ControladorGeneral(ControladorCliente cc, ControladorUsuario cu, ControladorSucursal cs, ControladorReserva cr, ControladorStockAutomoviles csa, ControladorAutopartes ca, ControladorModelo cmodel, ControladorMarca cmarca, Notificador notificador, ControladorAlarmaStock cas, ControladorVenta controladorVenta, ControladorMantenimiento controladorMantenimiento, ControladorReporte creporte, ControladorEncuesta controladorEncuesta, ControladorListaDeVentas controladorListaDeVentas) {
		// se cargan las clases de tipo controlador
		this.cc = cc;
		this.cu = cu;
		this.cs = cs;
		this.cr = cr;
		this.csa = csa;
		this.ca = ca;
		this.cmodel = cmodel;
		this.cmarca = cmarca;
		this.creporte = creporte;
		this.notificador = notificador;
		this.controladorVenta = controladorVenta;
		this.cas = cas;
		this.controladorMantenimiento = controladorMantenimiento;
		this.controladorEncuesta = controladorEncuesta;
		this.controladorListaDeVentas = controladorListaDeVentas;

		// se cargan las clases jframe
		this.ventanaConexion = VentanaConexion.getInstance();
		this.ventanaPrincipal = VentanaPrincipal.getInstance();
		this.ventanaLoging = VentanaLoging.getInstance();

		// eventos ventana

		// evento para ventana conexion
		this.ventanaConexion.getBtnConfirmar().addActionListener(c -> confirmarDatosConexion(c));

		// evento para ventana principal
		this.ventanaPrincipal.getBtnABMClientes().addActionListener(abmc -> abmClientes(abmc));
		this.ventanaPrincipal.getBtnABMUsuarios().addActionListener(abmu -> abmUsuarios(abmu));
		this.ventanaPrincipal.getBtnABMSucursales().addActionListener(abms -> abmSucursales(abms));
		this.ventanaPrincipal.getBtnABMMarcas().addActionListener(abmMarca -> abmMarca(abmMarca));
		this.ventanaPrincipal.getBtnABMModelos().addActionListener(abmModelo -> abmModelo(abmModelo));
		this.ventanaPrincipal.getBtnABMStockAutomoviles().addActionListener(abmSA -> abmStockAutomoviles(abmSA));
		this.ventanaPrincipal.getBtnABMReservas().addActionListener(abmr -> abmReservas(abmr));
		this.ventanaPrincipal.getBtnABMAutopartes().addActionListener(abmap -> abmAutoPartes(abmap));
		this.ventanaPrincipal.getBtnRealizarVenta().addActionListener(ventas-> ventas(ventas));
		this.ventanaPrincipal.getBtnControlStock().addActionListener(cstck ->controlStock(cstck));
		this.ventanaPrincipal.getBtnMantenimientos().addActionListener(cm ->controlMantenimientos(cm));
		this.ventanaPrincipal.getBtnReportes().addActionListener(er -> elegirReportes(er));
		this.ventanaPrincipal.getBtnListaDeVentas().addActionListener(lv -> listaDeVentas(lv));

		this.ventanaLoging.getBtnConfirmar().addActionListener(log ->loging(log));
	}

	private void abmClientes(ActionEvent abmc) {
		this.cc.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmUsuarios(ActionEvent abmu) {
		this.cu.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmSucursales(ActionEvent abms) {
		this.cs.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmMarca(ActionEvent abmMarca) {
		this.cmarca.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmModelo(ActionEvent abmModelo) {
		this.cmodel.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmStockAutomoviles(ActionEvent abmSA) {
		this.csa.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmReservas(ActionEvent abmr) {
		this.cr.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void abmAutoPartes(ActionEvent abmap) {
		this.ca.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}
	
	private void elegirReportes(ActionEvent er) {
		this.creporte.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	private void ventas(ActionEvent ventas) {
		this.controladorVenta.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}
	
	private void controlStock(ActionEvent cstck) {
		this.cas.llenarStockFaltante();
		this.cas.inicializar(this,0);
		this.ventanaPrincipal.cerrar();
	}
	
	private void controlMantenimientos(ActionEvent cm){
		this.controladorMantenimiento.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}
	
	private void listaDeVentas(ActionEvent lv) {
		this.controladorListaDeVentas.inicializar(this);
		this.ventanaPrincipal.cerrar();
	}

	// metodo que comunica controladorReserva con controladorCliente
	// reserva envia una ventana la cual cliente debe cargar con datos del cliente seleccionado
	protected void seleccionarClienteReserva(VentanaReserva ventanaReserva) {
		this.cc.seleccionarClienteReserva(ventanaReserva);
	}

	protected void configurarMarca() {
		this.cmarca.setVentanaListaDeAutomoviles(this.csa.getVentanaListaDeAutomoviles());
		this.cmarca.configurarMarca();
		//this.cmarca.inicializar(this);
	}
	
	protected void configurarMarcaDesdeAutoparte() {
		//this.cmarca.setVentanaListaDeAutopartes(this.ca.getVentanaListaDeAutopartes());
		this.cmarca.setVentanaAutopartes(this.ca.getVentanaAutoparte());
		this.cmarca.configurarMarcaDesdeAutopartes();
	}
	
	protected void configurarModelo() {
		this.cmodel.setVentanaListaDeAutomoviles(this.csa.getVentanaListaDeAutomoviles());
		this.cmodel.configurarModelo();
		//this.cmodel.inicializar(this);
	}
	
	protected void configurarModeloDesdeAutoparte() {
		this.cmodel.setVentanaAutopartes(this.ca.getVentanaAutopartes());
		this.cmodel.configurarModeloDesdeAutoparte();
		//this.cmodel.inicializar(this);
	}
	
	protected void configurarVenta() {
		this.controladorVenta.inicializar(this);
	}
	
	protected void seleccionarGarantiaVenta() {
		this.controladorVenta.seleccionarGarantiaVenta();
	}
	
	protected Object[] dameGarantiasVentas(){
		return this.controladorVenta.dameGarantiasVentas();
	}
	
	protected boolean crearMantenimiento(ReservaDTO reserva){
		return this.controladorMantenimiento.crearMantenimiento(reserva);
	}
	
	// evento que realiza la carga de los datos nuevos de conexion
	private void confirmarDatosConexion(ActionEvent c) {

		String ip = ventanaConexion.getTxtIP().getText();
		String puerto = ventanaConexion.getTxtPuerto().getText();
		String usuario = ventanaConexion.getTxtUsuario().getText();
		String password = new String(ventanaConexion.getTxtPassword().getPassword());

		Conexion.getConexion().actualizarDatosConexion(ip, puerto, usuario, password);
		this.ventanaConexion.cerrar();

		this.verificarConexion();
	}

	public void verificarConexion() {
		if (Conexion.getConexion().getSQLConexion() != null) {
//			this.verificarStock();
			this.logearse();
		} else {
			this.ventanaConexion.errorConexion();
			this.ventanaConexion.inicializar();
			this.ventanaConexion.mostrarVentana();
		}
	}
	
	public void verificarStock(int modo) {
		cas.llenarStockFaltante();
		if(this.cas.faltaStock()) {
			int seleccion = JOptionPane.showConfirmDialog(null, "Se ha detectado que el stock de uno o varios productos se encuentra por debajo del mínimo.\n¿Desea consultar la lista de stock faltante?", "Alarma de Falta de Stock",JOptionPane.YES_NO_OPTION
, JOptionPane.WARNING_MESSAGE);
			if(seleccion==JOptionPane.YES_OPTION) {
				cas.inicializar(this,modo);
			} else {
				if(modo==0) {
				this.inicializar();
				}
			}
			}else {	
				this.inicializar();
			}
	}
	
	private void verificarReservasVencidas() {
		this.cr.finalizarReservasVencidas();
	}
	
	private void loging(ActionEvent log){
		Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal = this.cu.loging(this.ventanaLoging.getTxtUsuario().getText(), this.ventanaLoging.getTxtPassword().getPassword());
		if(id_TipoUser_Sucursal!=null){
			this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
//			this.sucursal = usuario.getSucursal();
//			this.tipoUsuario = usuario.getTipoUsuario();
			this.verificarReservasVencidas();
			this.verificarStock(0);
			this.ventanaLoging.cerrar();
//			this.inicializar();
		}else{
			this.ventanaLoging.errorLoging();
		}
	}
	
	public void stockActualizado(AutoparteDTO autoparte) {
		if(autoparte.getStock()<autoparte.getStockMinimo()) {
		int seleccion = JOptionPane.showConfirmDialog(null, "Se ha detectado que el stock de un producto recientemente actualizado se encuentra por debajo del mínimo.\n¿Desea consultar la lista de stock faltante?", "Alarma de Falta de Stock",JOptionPane.YES_NO_OPTION
			, JOptionPane.WARNING_MESSAGE);
			if(seleccion==JOptionPane.YES_OPTION) {
			this.cas.llenarStockFaltante();
			cas.inicializar(this,1);
			}
		}
	}
	
	public void stockActualizado(StockAutomovilDTO automovil) {
		if(automovil.getStock()<automovil.getStockMinimo()) {
		int seleccion = JOptionPane.showConfirmDialog(null, "Se ha detectado que el stock de un producto recientemente actualizado se encuentra por debajo del mínimo.\n¿Desea consultar la lista de stock faltante?", "Alarma de Falta de Stock",JOptionPane.YES_NO_OPTION
			, JOptionPane.WARNING_MESSAGE);
			if(seleccion==JOptionPane.YES_OPTION) {
			this.cas.llenarStockFaltante();
			cas.inicializar(this,1);
			} 
		}
	}

	public void inicializar() {
		this.csa.setControladorGeneral(this);
		this.ca.setControladorGeneral(this);
		this.cc.setControladorGeneral(this);
		this.cu.setControladorGeneral(this);
		this.cs.setControladorGeneral(this);
		this.cr.setControladorGeneral(this);
		this.cmodel.setControladorGeneral(this);
		this.cmarca.setControladorGeneral(this);
		this.controladorVenta.setControladorGeneral(this);
		this.cas.setControladorGeneral(this);
		
		this.csa.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.ca.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.cc.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.cu.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.cs.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.cr.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.controladorMantenimiento.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.controladorVenta.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.cas.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		//this.ventanaLoging.mostrarVentana();
		this.controladorEncuesta.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		this.controladorListaDeVentas.setId_TipoUser_Sucursal(this.id_TipoUser_Sucursal);
		
		establecerPermisosVentanaPrincipal();
		this.ventanaPrincipal.mostrarVentana();
		this.notificador.notificarReservaClientes();

	}
	
	private void establecerPermisosVentanaPrincipal(){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==2){
			this.ventanaPrincipal.getBtnABMSucursales().setEnabled(false);
			this.ventanaPrincipal.getBtnABMUsuarios().setEnabled(false);
		}else if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3){
			this.ventanaPrincipal.getBtnABMSucursales().setEnabled(false);
			this.ventanaPrincipal.getBtnABMUsuarios().setEnabled(false);
		}else if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			this.ventanaPrincipal.getBtnABMAutopartes().setEnabled(false);
			this.ventanaPrincipal.getBtnABMMarcas().setEnabled(false);
			this.ventanaPrincipal.getBtnABMModelos().setEnabled(false);
			this.ventanaPrincipal.getBtnABMStockAutomoviles().setEnabled(false);
			this.ventanaPrincipal.getBtnABMSucursales().setEnabled(false);
			this.ventanaPrincipal.getBtnABMUsuarios().setEnabled(false);
			this.ventanaPrincipal.getBtnControlStock().setEnabled(false);
			this.ventanaPrincipal.getBtnReportes().setEnabled(false);
			this.ventanaPrincipal.getBtnRealizarVenta().setEnabled(false);
		}else if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5){
			this.ventanaPrincipal.getBtnABMMarcas().setEnabled(false);
			this.ventanaPrincipal.getBtnABMModelos().setEnabled(false);
			this.ventanaPrincipal.getBtnABMReservas().setEnabled(false);
			this.ventanaPrincipal.getBtnABMSucursales().setEnabled(false);
			this.ventanaPrincipal.getBtnABMUsuarios().setEnabled(false);
			this.ventanaPrincipal.getBtnControlStock().setEnabled(false);
			this.ventanaPrincipal.getBtnMantenimientos().setEnabled(false);
			this.ventanaPrincipal.getBtnReportes().setEnabled(false);
		}
	}
	
	public void logearse(){
		this.ventanaLoging.mostrarVentana();
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// metodo que comunica controladorVenta con controladorCliente
	// ventas envia una ventana la cual cliente debe cargar con datos del cliente seleccionado
	protected void seleccionarClienteVenta(VentanaVentas ventanaVenta) {
		this.cc.seleccionarClienteVenta(ventanaVenta);
	}

	public void seleccionarAutomovilVenta(VentanaVentas ventanaVentas) {
		this.csa.seleccionarAutomovilVenta(ventanaVentas);
	}

	public void seleccionarAutoparteVenta(VentanaVentas ventanaVentas) {
		this.ca.seleccionarAutoparteVenta(ventanaVentas);
		
	}
	
	protected void seleccionarAutoparteMantenimiento(ListaAutopartesXmantenimiento ventanaListaAutopartesXmantenimiento){
		this.ca.seleccionarAutoparteMantenimiento(ventanaListaAutopartesXmantenimiento);
	}
	
	protected void agregarAutoparteXmantenimiento(Pair<AutoparteDTO, Integer> autoparteXmantenimiento){
		this.controladorMantenimiento.agregarAutoparteXmantenimiento(autoparteXmantenimiento);
	}
	
	protected VentanaVentas dameVentanaVentas(){
		return this.controladorVenta.dameVentanaVentas();
	}
	
	protected void refrescarMantenimientos(){
		this.controladorMantenimiento.refrescarMantenimientos();
	}
	
	protected void realizarEncuesta(){
		this.controladorEncuesta.inicializar();
	}
	
	protected boolean esDeMantenimiento(int idVenta){
		return this.controladorMantenimiento.esDeMantenimiento(idVenta);
	}
	
	protected void generarFacturaVenta(int idVenta, boolean esDeMantenimiento){
		this.creporte.generarFacturaVenta(idVenta, esDeMantenimiento);
	}

}
