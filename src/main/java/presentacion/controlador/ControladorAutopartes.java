package presentacion.controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import dto.TipoUsuarioDTO;
import modelo.Autoparte;
import modelo.Marca;
import modelo.Modelo;
import modelo.Sucursal;
import persistencia.conexion.Conexion;
import presentacion.vista.ListaAutopartesXmantenimiento;
import presentacion.vista.ListaDeAutopartes;
import presentacion.vista.VentanaAutopartes;
import presentacion.vista.VentanaConexion;
import presentacion.vista.VentanaVentas;

public class ControladorAutopartes {

	private VentanaConexion ventanaConexion;
	
	private List<AutoparteDTO> autopartesEnTabla; 
	private List<TipoAutoparteDTO> tiposAutoparte; 
	private List<MarcaDTO> marcasEnTabla; //lista de marcas
	private List<ModeloDTO> modelosEnTabla; //lista de modelos
	private List<SucursalDTO> sucursales;
	
	private VentanaAutopartes ventanaAutopartes;
	private ListaDeAutopartes ventanaListaDeAutopartes;
	private VentanaVentas ventanaVentas;
	private ListaAutopartesXmantenimiento ventanaListaAutopartesXmantenimiento;
	
	private Autoparte autoparte;
	private Marca marca;
	private Modelo modelo;
	private Sucursal sucursal;
	
	private AutoparteDTO autoparte_a_editar;

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	private List<Triple<Integer,Integer,String>> filtrosBusqueda; 
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	
	
	public ControladorAutopartes(Autoparte autoparte, Sucursal sucursal, Marca marca, Modelo modelo) {
		super();
		this.autoparte = autoparte;
		this.sucursal = sucursal;
		this.marca = marca;
		this.modelo = modelo;
	
		this.ventanaConexion = VentanaConexion.getInstance();
		this.ventanaAutopartes = VentanaAutopartes.getInstance();
		this.ventanaListaDeAutopartes = ListaDeAutopartes.getInstance();
		
		//this.ventanaConexion.getBtnConfirmar().addActionListener(c->confirmarDatosConexion(c));

		this.ventanaListaDeAutopartes.getBtnAgregar().addActionListener(aap->ventanaAgregarAutoparte(aap));
		this.ventanaListaDeAutopartes.getBtnBorrar().addActionListener(bap->borrarAutoparte(bap));
		this.ventanaListaDeAutopartes.getBtnEditar().addActionListener(eap->ventanaEditarAutoparte(eap));
		this.ventanaListaDeAutopartes.getComboCampos().addActionListener(acb-> actualizarCampoBusqueda(acb));
		this.ventanaListaDeAutopartes.getComboValor_1().addActionListener(aml->actualizarModelosLista(aml));
		this.ventanaListaDeAutopartes.getBtnAgregarFiltro().addActionListener(afb->aplicarFiltroBusqueda(afb));
		this.ventanaListaDeAutopartes.getBtnDeshacerFiltros().addActionListener(dfb->deshacerFiltrosBusqueda(dfb));
		this.ventanaListaDeAutopartes.getBtnVolver().addActionListener(vdap -> volverDesdeAutoparte(vdap));
		this.ventanaListaDeAutopartes.getBtnIngresarStockPorCSV().addActionListener(iapc-> ingresarAutopartesPorCSV(iapc));
		
		
		
		this.ventanaAutopartes.getComboMarca().addActionListener(acm->actualizarComboModelos(acm));
		this.ventanaAutopartes.getBtnAutoparte().addActionListener(gap->guardarAutoparte(gap));
		this.ventanaAutopartes.getBtnAbmMarcas().addActionListener(abmmc ->abmMarca(abmmc));
		this.ventanaAutopartes.getBtnAbmModelos().addActionListener(abmmd ->abmModelo(abmmd));
		//this.ventanaBuscarAutopartes.getBtnBuscar().addActionListener(bcap->buscarAutoparte(bcap));
		this.ventanaListaDeAutopartes.getTablaAutopartes().addMouseListener(new MouseListener() {@Override public void mouseClicked(MouseEvent e) {confirmarSeleccionAutoparte(e);}@Override public void mouseEntered(MouseEvent e) {}@Override public void mouseExited(MouseEvent e) {}@Override public void mousePressed(MouseEvent e) {}@Override public void mouseReleased(MouseEvent e) {}});
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
		
	}
	
	private void volverDesdeAutoparte(ActionEvent vdap) {
		if(this.ventanaAutopartes.isVisible()) {
			this.ventanaAutopartes.cerrar();
		}
		this.ventanaListaDeAutopartes.dispose();
		this.cg.inicializar();
	}

	private void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.refrescarTablaAutopartes();
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}

	private void aplicarFiltroBusqueda(ActionEvent afb) {
		int seleccion = this.ventanaListaDeAutopartes.getComboCampos().getSelectedIndex();
		if(seleccion!=-1) {
			int campo = -1;
			String valor = null;
			if(seleccion==0) {
				TipoAutoparteDTO tipoAutoparte = (TipoAutoparteDTO) this.ventanaListaDeAutopartes.getComboValor().getSelectedItem();
				if(tipoAutoparte!=null) {
					campo = 0;
					valor = Integer.toString(tipoAutoparte.getId());
				} 
			} else if(seleccion==1) {
				MarcaDTO marca = (MarcaDTO) this.ventanaListaDeAutopartes.getComboValor_1().getSelectedItem();
				ModeloDTO modelo = (ModeloDTO) this.ventanaListaDeAutopartes.getComboValor_2().getSelectedItem();
				if(marca!=null) {
					if(modelo!=null) {
						campo = 2;
						valor = Integer.toString(modelo.getId());
					} else {
						campo = 1;
						valor = Integer.toString(marca.getId());
					}
				}
			} else if(seleccion==2) {
				String year = (String) this.ventanaListaDeAutopartes.getComboValor().getSelectedItem();
				if(year!= null) {
				campo = 3;
				valor = year;
				}
			} else if(seleccion==3) {
				SucursalDTO sucursal = (SucursalDTO) this.ventanaListaDeAutopartes.getComboValor().getSelectedItem();
				if(sucursal != null) {
					campo = 4;
					valor = Integer.toString(sucursal.getIdSucursal());
				}
			}
		if(campo!=-1 && valor!= null) {
			this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,valor));
			this.autopartesEnTabla = this.autoparte.buscarAutopartes(filtrosBusqueda);
			this.ventanaListaDeAutopartes.llenarTabla(autopartesEnTabla);
		} else {
			this.ventanaListaDeAutopartes.errorFiltrar();
			}
		} else {
			this.ventanaListaDeAutopartes.errorComboFiltro();
		}
	}

	private void actualizarModelosLista(ActionEvent aml) {
		MarcaDTO marcaSeleccionada = (MarcaDTO)this.ventanaListaDeAutopartes.getComboValor_1().getSelectedItem();
		if(marcaSeleccionada!= null) {
		this.modelosEnTabla = this.modelo.obtenerModelo(marcaSeleccionada.getId());
		this.ventanaListaDeAutopartes.cargarModelos(this.modelosEnTabla);
		this.ventanaListaDeAutopartes.getComboValor_2().setSelectedIndex(-1);
		}else {
			this.ventanaListaDeAutopartes.getComboValor_2().removeAllItems();
		}
	}

	private void actualizarCampoBusqueda(ActionEvent acb) {
		
		int seleccion = this.ventanaListaDeAutopartes.getComboCampos().getSelectedIndex();
		CardLayout layoutCampo = (CardLayout)this.ventanaListaDeAutopartes.getPanelBusqueda().getLayout();
		
		if(seleccion!=-1){
			if(seleccion==0) {
				layoutCampo.show(this.ventanaListaDeAutopartes.getPanelBusqueda(), "panelCombo");
				this.ventanaListaDeAutopartes.cargarTiposAutoparte(this.tiposAutoparte);
			} else if (seleccion==1) {
				layoutCampo.show(this.ventanaListaDeAutopartes.getPanelBusqueda(), "panelDobleCombo");
				this.ventanaListaDeAutopartes.cargarMarcas(this.marcasEnTabla);
				} else if(seleccion==2){
					layoutCampo.show(this.ventanaListaDeAutopartes.getPanelBusqueda(), "panelCombo");
					this.ventanaListaDeAutopartes.cargarYear();
					} else if(seleccion==3) {
							layoutCampo.show(this.ventanaListaDeAutopartes.getPanelBusqueda(), "panelCombo");
							this.ventanaListaDeAutopartes.cargarSucursales(this.sucursales);
							}
			this.ventanaListaDeAutopartes.datosBusquedaNulos();
		} else {
			layoutCampo.show(this.ventanaListaDeAutopartes.getPanelBusqueda(), "panelVacio");
		}

	}

	public void actualizarComboModelos(ActionEvent acm) {
		MarcaDTO marcaSeleccionada = (MarcaDTO)this.ventanaAutopartes.getComboMarca().getSelectedItem();
		if(marcaSeleccionada!= null) {
		this.modelosEnTabla = this.modelo.obtenerModelo(marcaSeleccionada.getId());
		this.ventanaAutopartes.cargarModelos(this.modelosEnTabla);
		}else {
			this.ventanaAutopartes.getComboModelos().removeAllItems();
		}
	}
	
	
	
	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.refrescarTablaAutopartes();
		this.tiposAutoparte = this.autoparte.obtenerTiposAutoparte();
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.sucursales = this.sucursal.obtenerSucursales(); 
		
		this.ventanaAutopartes.cargarTiposAutoparte(this.tiposAutoparte);
		this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
		this.ventanaAutopartes.cargarSucursales(this.sucursales);
		
		this.ventanaListaDeAutopartes.getComboCampos().setSelectedIndex(-1);
		this.ventanaListaDeAutopartes.getBtnVolver().setVisible(true);
		this.ventanaListaDeAutopartes.setTitle("Lista de Autopartes");
		
        
		this.ventanaListaDeAutopartes.mostrarVentana();
	}
	


	public void guardarAutoparte(ActionEvent gap) {
		
		this.ventanaAutopartes.ocultarLabelsError();
		if(this.ventanaAutopartes.validarCampos()) {
		TipoAutoparteDTO tipoAutoparte = (TipoAutoparteDTO) this.ventanaAutopartes.getComboTipos().getSelectedItem();
		ModeloDTO modeloAuto = (ModeloDTO) this.ventanaAutopartes.getComboModelos().getSelectedItem();
		String year = (String) this.ventanaAutopartes.getComboYear().getSelectedItem();
		SucursalDTO sucursal = (SucursalDTO) this.ventanaAutopartes.getComboSucursales().getSelectedItem();
		int stockMinimo = Integer.parseInt(this.ventanaAutopartes.getTxtStockMinimno().getText());
		int stock = Integer.parseInt(this.ventanaAutopartes.getTxtStock().getText());
		double precio = Double.parseDouble(this.ventanaAutopartes.getTxtPrecio().getText());
		
		int modo = this.ventanaAutopartes.getModo();
		switch(modo) {
		
			case 0: 
				AutoparteDTO autoparte_a_agregar = new AutoparteDTO(0,tipoAutoparte,modeloAuto,year,sucursal,stockMinimo,stock,precio);
				if(!this.autoparte.agregarAutoparte(autoparte_a_agregar)) {
					this.ventanaAutopartes.errorAgregar();
				} else {
					cg.stockActualizado(autoparte_a_agregar);
				}
				break;
		
			case 1:
				AutoparteDTO autoparte_editada = new AutoparteDTO(this.autoparte_a_editar.getId(),tipoAutoparte,modeloAuto,year,sucursal,stockMinimo,stock,precio);
				if(!this.autoparte.editarAutoparte(autoparte_editada)) {
					this.ventanaAutopartes.errorEditar();
				} else {
					cg.stockActualizado(autoparte_editada);
				}
				break;
		
		}
		
		this.ventanaAutopartes.cerrar();
		this.refrescarTablaAutopartes();
		
	} else {
		this.ventanaAutopartes.errorCargar();
		}
	
		
	}



	public void ventanaEditarAutoparte(ActionEvent eap) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int fila = this.ventanaListaDeAutopartes.getTablaAutopartes().getSelectedRow();
			if(fila!=-1){
				if(this.autopartesEnTabla.get(fila).getSucursal().getIdSucursal()==this.id_TipoUser_Sucursal.getRight().getIdSucursal()||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=3){
					if(((SucursalDTO)this.ventanaAutopartes.getComboSucursales().getSelectedItem()).getIdSucursal()==this.id_TipoUser_Sucursal.getRight().getIdSucursal()){
						this.autoparte_a_editar = this.autopartesEnTabla.get(fila);
						this.ventanaAutopartes.modoEditar();
						this.ventanaAutopartes.mostrarAutoparte(autoparte_a_editar);
						this.ventanaAutopartes.setVentanaBloqueada(this.ventanaListaDeAutopartes);
						this.ventanaListaDeAutopartes.setEnabled(false);
						this.ventanaAutopartes.getComboSucursales().setEnabled(false);
						this.ventanaAutopartes.mostrarVentana();
					}	
				}else{
					this.ventanaListaDeAutopartes.advertirUsuarioNoAutorizadoEditarAutoparteOtraSucursal();
				}
			}else{
				this.ventanaListaDeAutopartes.errorSeleccion();
			}
		}else{
			this.ventanaListaDeAutopartes.advertirUsuarioNoAutorizadoEditarAutoparte();
		}
	}
	
	public void borrarAutoparte(ActionEvent bap) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int fila = this.ventanaListaDeAutopartes.getTablaAutopartes().getSelectedRow();
			if(fila!=-1) {
				AutoparteDTO autoparte_a_eliminar = this.autopartesEnTabla.get(fila);
				if(!this.autoparte.borrarAutoparte(autoparte_a_eliminar)) {
					this.ventanaListaDeAutopartes.errorBorrar();
				}
				this.refrescarTablaAutopartes();
			} else {
				this.ventanaListaDeAutopartes.errorSeleccion();
			}
		}else{
			this.ventanaListaDeAutopartes.advertirUsuarioNoAutorizadoEliminarAutoparte();
		}
	}
	
	private void ingresarAutopartesPorCSV(ActionEvent iapc){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int option = this.ventanaListaDeAutopartes.getBuscadorArchivos().showOpenDialog(this.ventanaListaDeAutopartes);
			
			if(option == this.ventanaListaDeAutopartes.getBuscadorArchivos().APPROVE_OPTION){
				try {
				   this.sucursales = this.sucursal.obtenerSucursales(1);
				   ArrayList<Pair <Integer, AutoparteDTO>> autopartesIngresadas = Lector.leerArchivoCSV(this.ventanaListaDeAutopartes.getBuscadorArchivos().getSelectedFile().getAbsolutePath(), this.marca, this.modelo, this.autoparte, this.id_TipoUser_Sucursal.getRight());
				   ArrayList<String> lineasIngresadasCorrectamente = new ArrayList<String>();
				   for(Pair <Integer, AutoparteDTO> autoparte : autopartesIngresadas){
					   if(this.autoparte.agregarAutoparte(autoparte.getValue())){
						   lineasIngresadasCorrectamente.add(autoparte.getKey().toString());
					   }
				   }
				   this.ventanaListaDeAutopartes.notificarIngresosCorrectos(lineasIngresadasCorrectamente);
				} catch (FileNotFoundException e) {
				   e.printStackTrace();
				}
			   
				this.refrescarTablaAutopartes();
			}
		}else{
			this.ventanaListaDeAutopartes.advertirUsuarioNoAutorizadoIngresarAutoparte();
		}
   }
	
	private void refrescarTablaAutopartes()
	{
		this.autopartesEnTabla = autoparte.obtenerAutopartes();
		this.ventanaListaDeAutopartes.llenarTabla(autopartesEnTabla);
	}
	
	
	public void ventanaAgregarAutoparte(ActionEvent aap)
	{
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			this.ventanaAutopartes.modoAgregar();
			this.ventanaAutopartes.setVentanaBloqueada(this.ventanaListaDeAutopartes);
			this.ventanaListaDeAutopartes.setEnabled(false);
			if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3){
				this.ventanaAutopartes.getComboSucursales().setSelectedItem(this.id_TipoUser_Sucursal.getRight());
				this.ventanaAutopartes.getComboSucursales().setEnabled(false);
			}
			this.ventanaAutopartes.mostrarVentana();
		}else{
			this.ventanaListaDeAutopartes.advertirUsuarioNoAutorizadoIngresarAutoparte();
		}
	}

	public VentanaAutopartes getVentanaAutopartes() {
		return ventanaAutopartes;
	}

	public ListaDeAutopartes getVentanaListaDeAutopartes() {
		return ventanaListaDeAutopartes;
	}

	public Autoparte getAutoparte() {
		return autoparte;
	}

	public void seleccionarAutoparteVenta(VentanaVentas ventanaVentas) {
		this.ventanaVentas = ventanaVentas;
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3)this.refrescarTablaAutopartesDeSucursal(this.id_TipoUser_Sucursal.getRight().getIdSucursal());
		else this.refrescarTablaAutopartes();
		this.ventanaListaDeAutopartes.setModoSeleccion(true);
		this.ventanaListaDeAutopartes.setTitle("Seleccione una autoparte...");
		this.ventanaListaDeAutopartes.setTipoSeleccion(1);
		this.tiposAutoparte = this.autoparte.obtenerTiposAutoparte();
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.sucursales = this.sucursal.obtenerSucursales(); 
		
		this.ventanaAutopartes.cargarTiposAutoparte(this.tiposAutoparte);
		this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
		this.ventanaAutopartes.cargarSucursales(this.sucursales);
		
		this.ventanaListaDeAutopartes.getComboCampos().setSelectedIndex(-1);
		this.ventanaListaDeAutopartes.getBtnVolver().setVisible(false);
		this.ventanaListaDeAutopartes.setVentanaVentasLlamadora(this.ventanaVentas);
		this.ventanaListaDeAutopartes.setLlamadoPorVentas(true);
		this.ventanaVentas.setEnabled(false);
		
		
		/*
		 * this.ventanaListaDeAutomoviles.setVentanaVentasLlamadora(this.ventanaVentas);//agregado
		this.ventanaListaDeAutomoviles.setLlamadoPorVentas(true);//agregado
		this.ventanaVentas.setEnabled(false);
		
		 */
		this.ventanaListaDeAutopartes.mostrarVentana();
		
	}
	
	protected void seleccionarAutoparteMantenimiento(ListaAutopartesXmantenimiento ventanaListaAutopartesXmantenimiento){
		this.ventanaListaAutopartesXmantenimiento = ventanaListaAutopartesXmantenimiento;
		this.refrescarTablaAutopartesParaMantenimiento();
		this.ventanaListaDeAutopartes.setModoSeleccion(true);
		this.ventanaListaDeAutopartes.setTitle("Seleccione una autoparte...");
		this.ventanaListaDeAutopartes.setTipoSeleccion(2);
		
		this.tiposAutoparte = this.autoparte.obtenerTiposAutoparte();
		this.marcasEnTabla = this.marca.obtenerMarca();
		this.sucursales = this.sucursal.obtenerSucursales(); 
		
		this.ventanaAutopartes.cargarTiposAutoparte(this.tiposAutoparte);
		this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
		this.ventanaAutopartes.cargarSucursales(this.sucursales);
		
		this.ventanaListaDeAutopartes.getComboCampos().setSelectedIndex(-1);
		this.ventanaListaDeAutopartes.getBtnVolver().setVisible(false);
		this.ventanaListaDeAutopartes.setVentanaAutopartesXmantenimientoLlamadora(this.ventanaListaAutopartesXmantenimiento);
		this.ventanaListaDeAutopartes.setLlamadoPorMantenimiento(true);
		this.ventanaListaAutopartesXmantenimiento.setEnabled(false);
		this.ventanaListaDeAutopartes.mostrarVentana();
		
	}
	
	public void confirmarSeleccionAutoparte(MouseEvent e){
		if(this.ventanaListaDeAutopartes.isModoSeleccion()==true){
			if(e.getClickCount() == 2){
				int fila = this.ventanaListaDeAutopartes.getTablaAutopartes().getSelectedRow();
				if(this.ventanaListaDeAutopartes.getTipoSeleccion()==1) {
					if(this.autopartesEnTabla.get(fila).getStock() > 0){
						this.ventanaVentas.agregarAutoparte(this.autopartesEnTabla.get(fila),false);
						this.ventanaListaDeAutopartes.cerrar();
					}else{
						this.ventanaVentas.advertirFaltaDeStockDeAutoparte();
					}
				}else if(this.ventanaListaDeAutopartes.getTipoSeleccion()==2){
					if(this.autopartesEnTabla.get(fila).getStock() > 0){
						try{
							Object s = JOptionPane.showInputDialog(null,null,
									"Seleccione Una Cantidad", 2, null,
									  this.dameStockAutoparte(this.autopartesEnTabla.get(fila)),"Seleccione");
							int cantidad = Integer.parseInt((String)s);
							Pair<AutoparteDTO, Integer> autoparteXmantenimiento = new ImmutablePair<AutoparteDTO, Integer>(this.autopartesEnTabla.get(fila),cantidad);
							this.cg.agregarAutoparteXmantenimiento(autoparteXmantenimiento);
							this.ventanaListaDeAutopartes.cerrar();
						}catch(Exception ex){
							ex.printStackTrace();
							this.ventanaListaAutopartesXmantenimiento.errorAlSeleccionarAutoparte();
						}
					}else{
						this.ventanaListaAutopartesXmantenimiento.advertirFaltaDeStockDeAutoparte();
					}
				}
			}
		}
	}
	
	private void abmMarca(ActionEvent abmmc) {
		this.cg.configurarMarcaDesdeAutoparte();
	}
	
	private void abmModelo(ActionEvent abmmd) {
		this.cg.configurarModeloDesdeAutoparte();
	}
	
	private String[] dameStockAutoparte(AutoparteDTO autoparteSeleccionada){
		String[] stock = new String[autoparteSeleccionada.getStock()];
		for(int i=0; i<stock.length;i++){
			stock[i] = String.valueOf(i+1);
		}
		return stock;
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursalUsuario = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

	public VentanaAutopartes getVentanaAutoparte() {
		return ventanaAutopartes;
	}

	private void refrescarTablaAutopartesParaMantenimiento()
	{
		this.autopartesEnTabla = autoparte.obtenerAutopartesParaMantenimiento();
		this.ventanaListaDeAutopartes.llenarTabla(autopartesEnTabla);
	}
	
	private void refrescarTablaAutopartesDeSucursal(int idSucursal)
	{
		this.autopartesEnTabla = autoparte.obtenerAutopartesDeSucursal(idSucursal);
		this.ventanaListaDeAutopartes.llenarTabla(autopartesEnTabla);
	}
	
	
//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
	
}
