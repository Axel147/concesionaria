package presentacion.controlador;

import java.util.Calendar;
import java.util.regex.Pattern;

public class ControladorCampos {

	//recibe un string y verifica si cumple las condiciones para ser conciderado como un campo vacio
		public static boolean CampoVacio(String stringCampo){
			boolean campoVacio;
			String regexp = "^$||\\s+$";
			campoVacio = Pattern.matches(regexp, stringCampo);
			return campoVacio;
		}
	
	//recibe un string y verifica si cumple las condiciones para ser aceptado como email valido
	public static boolean EmailValido(String email){
		boolean emailValido;
		String regexp = "[a-zA-Z0-9]+[\\.\\_\\-]*[a-zA-Z0-9]*[a-zA-Z0-9\\.\\_\\-]*+@[a-zA-Z0-9]{3,}\\.{1}[a-zA-Z0-9]{2,}[a-zA-Z0-9\\.\\_\\-]*[a-zA-Z0-9]+";
		emailValido = Pattern.matches(regexp, email);
		return emailValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como nombre de persona valido
	public static boolean NombreValido(String nombre){
		boolean nombreValido;
		String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}";
		nombreValido= Pattern.matches(regexp, nombre);
		return nombreValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como apellido valido
	public static boolean ApellidoValido(String apellido){
		boolean apellidoValido;
		String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}";
		apellidoValido = Pattern.matches(regexp, apellido);
		return apellidoValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como numero de documento valido
	public static boolean NumeroDocumentoValido(String numDoc, String tipoDocumento){
		boolean numeroDocumentoValido = false;
		if(tipoDocumento.equals("DNI")){
			String regexp = "[0-9]{7,9}";
			numeroDocumentoValido = Pattern.matches(regexp, numDoc);
		}else if(tipoDocumento.equals("CU")){
			numDoc = numDoc.replaceAll("\\D", "");
			numeroDocumentoValido = esCUValida(numDoc);
		}else if(tipoDocumento.equals("CP")){
			String regexp = "[0-9]{7,9}";
			numeroDocumentoValido = Pattern.matches(regexp, numDoc);
		}else if(tipoDocumento.equals("RUT")){
			String regexp = "^[0-9]+-[0-9kK]{1}$";
			if (!Pattern.matches(regexp, numDoc)) return false;
			String[] stringRut = numDoc.split("-");
			numeroDocumentoValido = stringRut[1].toLowerCase().equals(ValidadorDigitoChileno(stringRut[0]));
		}else{
			String regexp = "[0-9]{7,9}";
			numeroDocumentoValido = Pattern.matches(regexp, numDoc);
		}
		return numeroDocumentoValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como telefono valido
	public static boolean TelefonoValido(String telefono){
		boolean telefonoValido;
		String regexp = "[0-9]{7,13}";
		telefonoValido = Pattern.matches(regexp, telefono);
		return telefonoValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como nombre sucursal valido
	public static boolean SucursalValida(String sucursal){
		boolean sucursalValida;
		String regexp = "[a-zA-Zñáéíóúü]{3,23}[a-zA-Z0-9ñáéíóúü\\s]+|[a-zA-Zñáéíóúü]{3,45}";
		sucursalValida = Pattern.matches(regexp, sucursal);
		return sucursalValida;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como calle valida
	public static boolean CalleValida(String calle){
		boolean calleValida;
		//String regexpParaNombre = "[0-9]{1,10}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{2,3}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}\\s{1}[0-9]{1,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{2,23}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{1,23}\\s[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-Zñ'ÁáÉéÍíÓóÚúÜü]{2,45}";
		String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}";
		calleValida = Pattern.matches(regexp, calle); 
		return calleValida;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como altura de calle valida
	public static boolean AlturaValida(String altura){
		boolean alturaValida;
		String regexp = "[0-9]{3,5}";
		alturaValida = Pattern.matches(regexp, altura);
		return alturaValida;
	}
	
	public static boolean anioValido(String txtanio)
	{
		boolean esValido = false;
		String regexp = "[0-9]{4}";
		if(Pattern.matches(regexp, txtanio)) {
			int anio = Integer.parseInt(txtanio);
			int anioActual = Calendar.getInstance().get(Calendar.YEAR);
			if(anio>=2000 && anio<=anioActual) {
				esValido = true;
			}
		}
		return esValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como usuario valido
	public static boolean UsuarioValido(String usuario){
		boolean usuarioValido;
		String regexp = "[a-zA-Z0-9]+[\\.\\_\\-]*[a-zA-Z0-9]+";
		usuarioValido = Pattern.matches(regexp, usuario);
		return usuarioValido;
	}

	//recibe un string y verifica si cumple las condiciones para ser aceptado como password valida
	public static boolean PasswordValida(String password){
		boolean passwordValida;
		String regexp = "[a-zA-Z0-9]{8,20}";
		passwordValida = Pattern.matches(regexp, password);
		return passwordValida;
	}

	public static boolean NombreSucursalValido(String nombre) {
		 boolean nombreSucursalValido;
	        String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}";
	        nombreSucursalValido = Pattern.matches(regexp, nombre); 
	        return nombreSucursalValido;
	}

	public static boolean CalleSucursalValida(String calle) {
		boolean calleValida;
		//String regexpParaNombre = "[0-9]{1,10}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{2,3}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}\\s{1}[0-9]{1,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-ZñÁáÉéÍíÓóÚúÜü]{2,23}\\s{1}[a-zA-ZñÁáÉéÍíÓóÚúÜü]{1,23}\\s[a-zA-ZñÁáÉéÍíÓóÚúÜü]{3,23}|[a-zA-Zñ'ÁáÉéÍíÓóÚúÜü]{2,45}";
		String regexp = "[a-zA-Z\\s]{4,25}";
		calleValida = Pattern.matches(regexp, calle); 
		return calleValida;
	}
	
	public static boolean esCUValida(String CU) {
		boolean cuValida = false;
		//controla que la cedula uruguaya tenga 7 o 8 digitos
		if(CU.length() != 7 && CU.length() != 8){
			return false;
		}else{
			try{
				Integer.parseInt(CU);
			}catch (NumberFormatException e){
				return false;
			}
		}
		
		//obtiene el digito verificador
		int digVerificador = Integer.parseInt((CU.charAt(CU.length() - 1)) + "" ) ;
		//array de factores(son unos numeros que se utilizan para comprobar la autenticidad de la cedula)
		int[] factores;
		// si CU es vieja
		if(CU.length() == 7){ 
			factores = new int[]{9, 8, 7, 6, 3, 4};
		}else{ //si CU es nueva
			factores = new int[]{2, 9, 8, 7, 6, 3, 4};
		}
		
		int suma = 0;
		for(int i=0; i<CU.length() - 1;i++){ 
				int digito = Integer.parseInt(CU.charAt(i) + "" ) ;
				suma += digito * factores[ i ];
		}
		int resto = suma % 10;
		int checkdigit = 10 - resto;

		if(checkdigit == 10){
			cuValida = (digVerificador == 0);
		}else {
			cuValida = (checkdigit == digVerificador) ;
		}
		return cuValida;
	}
	
	//valida el digito verificador chileno (rut)
	private static String ValidadorDigitoChileno (String rut) {
		Integer M=0,S=1,T=Integer.parseInt(rut);
		for (;T!=0;T=(int) Math.floor(T/=10))
			S=(S+T%10*(9-M++%6))%11;
		return ( S > 0 ) ? String.valueOf(S-1) : "k";		
	}
	
	public static boolean MarcaValida(String marca){
		boolean calleValida;
		String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}";
		calleValida = Pattern.matches(regexp, marca); 
		return calleValida;
	}

	//--------------------------------------------------------------------------------------------------
	public static boolean NombreModeloValido(String nombre){
		boolean nombreModeloValido;
		//String regexp = "[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]{3,23}|[a-zA-Zñáéíóúü]{3,45}|[0-9]{1,6}|[a-zA-Zñáéíóúü]{3,23}\\s{1,1}[a-zA-Zñáéíóúü]\\s[0-9]{1,6}|[a-zA-Zñáéíóúü]{3,23}[a-zA-Zñáéíóúü]{3,23}[0-9]{1,6}";
		String regexp = "[a-zA-Z\\s(0-9)]{2,25}";
		nombreModeloValido= Pattern.matches(regexp, nombre);
		return nombreModeloValido;
	}

	public static boolean validarCantidadPuertas(String cantPuertas)
	{
		String regexp = "[1-5]{1}";
		return Pattern.matches(regexp, cantPuertas);
	}

	public static boolean validarStock(String stock) {
		String regexp = "[0-9]{1,2}";
		return Pattern.matches(regexp, stock);
	}
	
	public static boolean validarPrecio(String precio) {
		String regexp = "[0-9]{1,7}(.[0-9]{1,2})?";
		return Pattern.matches(regexp, precio);
	}
	
	public static boolean validarNumeroTarjeta(String nroTarjeta) {
		String regexp = "[0-9]{16}";
		return Pattern.matches(regexp, nroTarjeta);
	}
	
	public static boolean validarVencimiento(String vencimiento) {
		String regexp = "((0[1-9])|(1[0-2]))/2[1-9]";
		return Pattern.matches(regexp, vencimiento);
	}
	
	public static boolean validarCodigoTarjeta(String codigoTarjeta) {
		String regexp = "[0-9]{3}";
		return Pattern.matches(regexp,codigoTarjeta);
	}
		
	//recibe un string y verifica si cumple las condiciones para ser aceptado como patente valida
	public static boolean PatenteValida(String patente, String paisPatente){
		boolean patenteValida = false;
		if(paisPatente.equals("ARGENTINA")){
			String regexp = "[A-Z]{3}[0-9]{3}|[A-Z]{2}[0-9]{3}[A-Z]{2}";
			patenteValida = Pattern.matches(regexp, patente);
		}else if(paisPatente.equals("URUGUAY")){
			String regexp = "[A-Z]{3}[0-9]{4}";
			patenteValida = Pattern.matches(regexp, patente);
		}else if(paisPatente.equals("PARAGUAY")){
			String regexp = "[A-Z]{3}[0-9]{3}|[A-Z]{4}[0-9]{3}";
			patenteValida = Pattern.matches(regexp, patente);
		}else if(paisPatente.equals("CHILE")){
			String regexp = "[A-Z]{2}[0-9]{4}|[A-Z]{4}[0-9]{2}";
			patenteValida = Pattern.matches(regexp, patente);
		}
		return patenteValida;
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static boolean validarKilometros(String kilometros) {
		String regexp = "[0-9]{1,6}";
		return Pattern.matches(regexp,kilometros);
		//return true;
	}
	
	public static boolean colorValido(String color){
		boolean colorValido;
		String regexp = "[a-zA-Zñáéíóúü]{3,23}";
		colorValido= Pattern.matches(regexp, color);
		return colorValido;
	}

	public static boolean anioAutoValido(String anio)
	{
		String regexp ="[1]{1}[9]{1}[5-9]{1}[0-9]{1}|[2]{1}[0]{1}[4]{1}[0-9]{1}";
		return Pattern.matches(regexp, anio);
	}
}
