package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.util.List;

import dto.MarcaDTO;
import dto.ModeloDTO;
import modelo.Marca;
import modelo.Modelo;
import presentacion.vista.ListaDeAutomoviles;
import presentacion.vista.ListaDeAutopartes;
import presentacion.vista.ListaDeMarcas;
import presentacion.vista.VentanaAutopartes;
import presentacion.vista.VentanaMarca;

public class ControladorMarca {

	private List<MarcaDTO> marcasEnTabla; // lista de marcas
	private List<ModeloDTO>modelosEnTabla;
	private VentanaMarca ventanaMarca; // ventana para agregar o modificar una marca

	private ListaDeMarcas ventanaListaDeMarcas; // ventana con todos las marcas(ABM)
	private ListaDeAutomoviles ventanaListaDeAutomoviles;

	private Marca marca;
	private Modelo modelo;

	private ControladorGeneral cg;
	private ListaDeAutopartes ventanaListaDeAutopartes;
	private VentanaAutopartes ventanaAutopartes;

	public ControladorMarca(Marca marca, Modelo modelo) {
		// se cargan las clases de tipo modelo
		this.marca = marca;
		this.modelo = modelo;

		// se cargan las clases jframe
		this.ventanaListaDeMarcas = ListaDeMarcas.getInstance();
		this.ventanaMarca = VentanaMarca.getInstance();
		this.ventanaListaDeAutomoviles = ListaDeAutomoviles.getInstance();
		this.ventanaListaDeAutopartes = ListaDeAutopartes.getInstance();
		this.ventanaAutopartes = VentanaAutopartes.getInstance();

		// eventos ventana

		// se crean eventos para los elementos del jframe ventanaListaDeMarcas
		this.ventanaListaDeMarcas.getBtnBuscar().addActionListener(buscMarca -> buscarMarca(buscMarca));
		this.ventanaListaDeMarcas.getBtnAgregar().addActionListener(aMarca -> ventanaAgregarMarca(aMarca));
		this.ventanaListaDeMarcas.getBtnEditar().addActionListener(mMarca -> ventanaEditarMarca(mMarca));
		this.ventanaListaDeMarcas.getBtnBorrar().addActionListener(bMarca -> borrarMarca(bMarca));
		this.ventanaMarca.getBtnGuardar().addActionListener(gMarca -> guardarMarca(gMarca));
		this.ventanaListaDeMarcas.getBtnVolver().addActionListener(vdm-> volverDesdeMarca(vdm));
//		this.ventanaListaDeMarcas.getBtnVolver().addActionListener(vdc -> volverDesdeMarca(vdc));		
	}

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.refrescarTablaMarcas();
		this.ventanaListaDeMarcas.getBtnVolver().setVisible(true);
		//this.ventanaListaDeMarcas.setLlamadoDesdeStock();
		this.ventanaListaDeMarcas.mostrarVentana();
	}

	// evento que abre el jframe 'ventanaMarca' para agregar una marca
	public void ventanaAgregarMarca(ActionEvent aMarca) {
		this.ventanaMarca.setTipoVentana(1);
		this.ventanaMarca.setTitle("Agregar Marca");
		this.ventanaMarca.setVentanaMarcaLlamadora(this.ventanaListaDeMarcas);
		this.ventanaMarca.setLlamadoPorListaDeMarca(true);
		this.ventanaListaDeMarcas.setEnabled(false);
		this.ventanaMarca.mostrarVentana();
	}

	public void ventanaEditarMarca(ActionEvent mMarca) {
		int[] filasSeleccionadas = this.ventanaListaDeMarcas.getTablaMarcas().getSelectedRows();
		if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para editar de la tabla marcas
		{
			for (int fila : filasSeleccionadas) {
				this.ventanaMarca.setTipoVentana(2); // setea tipo ventana editar
				this.ventanaMarca.setTitle("Editar Marca");

				// seteamos datos de la sucursal a editar
				this.ventanaMarca.setId(this.marcasEnTabla.get(fila).getId());
				this.ventanaMarca.setTxtNombre(this.marcasEnTabla.get(fila).getNombre());
				this.ventanaMarca.setVentanaMarcaLlamadora(this.ventanaListaDeMarcas);
				this.ventanaMarca.setLlamadoPorListaDeMarca(true);
				this.ventanaListaDeMarcas.setEnabled(false);
				// mostramos la ventana
				this.ventanaMarca.mostrarVentana();
			}
		} else
			this.ventanaListaDeMarcas.advertirSeleccionIncorrecta();
		this.refrescarTablaMarcas();

	}

	public void guardarMarca(ActionEvent gMarca) {
		String nombre = this.ventanaMarca.getTxtNombre().getText();

		if (ControladorCampos.CampoVacio(nombre)) {
			ventanaMarca.advertirFaltaNombreMarca();
		} else {
			if (!ControladorCampos.MarcaValida(nombre)) {
				ventanaMarca.advertirNombreMarcaInvalido();
			} else {
				MarcaDTO nuevaMarca = new MarcaDTO(0, nombre);
				if (this.ventanaMarca.getTipoVentana() == 1) {
					if (!this.marca.agregarMarca(nuevaMarca)) {
						this.ventanaMarca.advertenciaMarcaRepetidaAgregar();
					} else {
						if(ventanaListaDeMarcas.isLlamadoDesde() == 1) {
							this.marcasEnTabla = marca.obtenerMarca();
							this.ventanaListaDeAutomoviles.llenarMarca(this.marcasEnTabla);
							int marca;
							if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getItemCount() == 0)marca = 0;
							else marca = ((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId();
							this.modelosEnTabla = this.modelo.obtenerModelo(marca);
							this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
							this.ventanaListaDeAutomoviles.llenarModelo(modelosEnTabla);
						}else if(ventanaListaDeMarcas.isLlamadoDesde() == 2) {
							this.marcasEnTabla = this.marca.obtenerMarca();
							this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
							this.ventanaAutopartes.getComboMarca().setSelectedIndex(-1);
						}
						this.refrescarTablaMarcas();
						this.ventanaMarca.cerrar();
					}
				} else {
					nuevaMarca.setId(ventanaMarca.getId());
					if (!this.marca.editarMarca(nuevaMarca)) {
						this.ventanaMarca.advertenciaMarcaRepetidaModificar();
					} else {
						if(ventanaListaDeMarcas.isLlamadoDesde() == 1) {
							this.marcasEnTabla = marca.obtenerMarca();
							this.ventanaListaDeAutomoviles.llenarMarca(this.marcasEnTabla);
							int marca;
							if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getItemCount() == 0)marca = 0;
							else marca = ((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId();
							this.modelosEnTabla = this.modelo.obtenerModelo(marca);
							this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
							this.ventanaListaDeAutomoviles.llenarModelo(modelosEnTabla);
						}else if(ventanaListaDeMarcas.isLlamadoDesde() == 2) {
							this.marcasEnTabla = this.marca.obtenerMarca();
							this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
							this.ventanaAutopartes.getComboMarca().setSelectedIndex(-1);
						}
						this.refrescarTablaMarcas();
						this.ventanaMarca.cerrar();

					}
				}
			}
		}
	}

	public void borrarMarca(ActionEvent bMarca) {
		int[] filasSeleccionadas = this.ventanaListaDeMarcas.getTablaMarcas().getSelectedRows();
		if (filasSeleccionadas.length == 1) {
			for (int fila : filasSeleccionadas) {
				if (!this.marca.borrarMarca(this.marcasEnTabla.get(fila))) {
					this.ventanaListaDeMarcas.advertenciaMarcaUtilizandose();
				} else {
					if(ventanaListaDeMarcas.isLlamadoDesde() == 1) {
						this.marcasEnTabla = marca.obtenerMarca();
						this.ventanaListaDeAutomoviles.llenarMarca(this.marcasEnTabla);
						int marca;
						if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getItemCount() == 0)marca = 0;
						else marca = ((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId();
						this.modelosEnTabla = this.modelo.obtenerModelo(marca);
						this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
						this.ventanaListaDeAutomoviles.llenarModelo(modelosEnTabla);
					}else if(ventanaListaDeMarcas.isLlamadoDesde() == 2) {
						this.marcasEnTabla = this.marca.obtenerMarca();
						this.ventanaAutopartes.cargarMarcas(this.marcasEnTabla);
						this.ventanaAutopartes.getComboMarca().setSelectedIndex(-1);
					}
					this.refrescarTablaMarcas();
				}
			}
		} else {
			this.ventanaListaDeMarcas.advertirSeleccionBorrarIncorrecta();
		}
	}

	public void buscarMarca(ActionEvent buscMarca) {
		String dato = this.ventanaListaDeMarcas.getTxtBusqueda().getText();
		this.marcasEnTabla = marca.obtenerMarcasPor(dato);
		this.ventanaListaDeMarcas.llenarTabla(this.marcasEnTabla);
	}
	
	
	private void volverDesdeMarca(ActionEvent vdc) {
		if (this.ventanaListaDeMarcas.isVisible()) {
			this.ventanaListaDeMarcas.cerrar();
		}
		this.cg.inicializar();
	}

	// refresca tabla de ListaDeMarcas
	public void refrescarTablaMarcas() {
		this.marcasEnTabla = marca.obtenerMarca();
		this.ventanaListaDeMarcas.llenarTabla(this.marcasEnTabla);
	}

	// geter y seter de testing -_-////

	public VentanaMarca getVentanaMarca() {
		return ventanaMarca;
	}

	public Marca getMarca() {
		return marca;
	}

	public ListaDeMarcas getVentanaListaDeMarcas() {
		return ventanaListaDeMarcas;
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}

	public ListaDeAutomoviles getVentanaListaDeAutomoviles() {
		return ventanaListaDeAutomoviles;
	}

	public void setVentanaListaDeAutomoviles(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
	}

	public void configurarMarca() {
		this.refrescarTablaMarcas();
		this.ventanaListaDeMarcas.getBtnVolver().setVisible(false);
		this.ventanaListaDeMarcas.setVentanaListaDeAutomoviles(this.ventanaListaDeAutomoviles);
		this.ventanaListaDeMarcas.setLlamadoDesde(1);
		this.ventanaListaDeAutomoviles.setEnabled(false);
		this.ventanaListaDeMarcas.mostrarVentana();
		
	}
	
	public void configurarMarcaDesdeAutopartes() {
		this.refrescarTablaMarcas();
		this.ventanaListaDeMarcas.getBtnVolver().setVisible(false);
		this.ventanaListaDeMarcas.setVentanaAutopartes(this.ventanaAutopartes);
		this.ventanaListaDeMarcas.setLlamadoDesde(2);
		this.ventanaAutopartes.setEnabled(false);
		//this.ventanaListaDeMarcas.setLlamadoDesdeStock(true);
		this.ventanaListaDeMarcas.mostrarVentana();
		
	}

	public void setVentanaAutopartes(VentanaAutopartes ventanaAutoparte) {
		this.ventanaAutopartes = ventanaAutoparte;
		
	}

}
