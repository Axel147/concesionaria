package presentacion.controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.OpcionPagoDTO;
import dto.PaisDTO;
import dto.ProductoDTO;
import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.TarjetaDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import dto.VentaDTO;
import dto.AutoparteDTO;
import dto.ClienteDTO;
import dto.EstadoVentaDTO;
import dto.GarantiaDTO;
import dto.MedioPagoDTO;
import modelo.Garantia;
import modelo.MedioPago;
import modelo.OpcionPago;
import modelo.Venta;
import presentacion.vista.ListaDeClientes;
import presentacion.vista.VentanaEncuesta;
import presentacion.vista.VentanaGarantia;
import presentacion.vista.VentanaVentas;

public class ControladorVenta {
	private VentanaVentas ventanaVentas;
	private VentanaGarantia ventanaGarantia;
	private ListaDeClientes ventanaListaDeClientes;
	private List<MedioPagoDTO> mediosPagoEnTabla;
	private List<OpcionPagoDTO> opcionesPagoEnTabla;
	private MedioPago medioPago;
	private OpcionPago opcionPago;
	private Venta venta;
	private Garantia garantia;
	
	private ControladorGeneral controladorGeneral;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;
	
	public ControladorVenta(MedioPago medioPago, OpcionPago opcionPago, Venta venta, Garantia garantia) {
		this.medioPago = medioPago;
		this.opcionPago = opcionPago;
		this.venta = venta;
		this.garantia = garantia;
		this.ventanaListaDeClientes = ListaDeClientes.getInstance();
		this.ventanaVentas = VentanaVentas.getInstance();
		this.ventanaGarantia = VentanaGarantia.getInstance();
		
		this.ventanaVentas.getBtnCargarCliente().addActionListener(cargarCliente -> cargarCliente(cargarCliente));
		this.ventanaVentas.getBtnCargarAutomovil().addActionListener(cargarAutomovil->cargarAutomovil(cargarAutomovil));
		this.ventanaVentas.getBtnCargarProducto().addActionListener(cargarAutoparte->cargarAutoparte(cargarAutoparte));
		this.ventanaVentas.getCmbModoPago().addActionListener(aop->actualizarOpcionPago(aop));
		this.ventanaVentas.getBtnAceptar().addActionListener(rv->realizarVenta(rv));
		this.ventanaVentas.getBtnQuitar().addActionListener(qp->quitarProducto(qp));
		this.ventanaVentas.getBtnVolver().addActionListener(vv->volverDesdeVenta(vv));
		
		this.ventanaVentas.getCmbModoPago().addActionListener(cpp->cargarPanelPagos(cpp));
		//this.ventanaGarantia.getBtnSeleccionar().addActionListener(eg->elegirGarantia(eg));
	
	}
	

	private void quitarProducto(ActionEvent qp) {
		int fila = this.ventanaVentas.getTablaVentas().getSelectedRow();
		if(fila!=-1) {
		double precio = Double.parseDouble(this.ventanaVentas.getTxtTotalPagar().getText().substring(4));
		Triple<Integer, Pair<StockAutomovilDTO,GarantiaDTO>, AutoparteDTO> productoEnFila = this.ventanaVentas.getCarritoCompra().get(fila);
		if(productoEnFila.getMiddle()!=null&&productoEnFila.getRight()==null){
			precio = (precio - productoEnFila.getMiddle().getLeft().getPrecio())-productoEnFila.getMiddle().getRight().getPrecio();
		}else if(productoEnFila.getMiddle()==null&&productoEnFila.getRight()!=null){
			precio = precio - productoEnFila.getRight().getPrecio();
		}
		this.ventanaVentas.getCarritoCompra().remove(fila);
//		this.ventanaVentas.getCarro().remove(fila);
		DefaultTableModel modelotabla = ((DefaultTableModel) this.ventanaVentas.getTablaVentas().getModel());
		modelotabla.removeRow(fila);
		this.ventanaVentas.getTxtTotalPagar().setText("US$ " + Double.toString(precio));
		} else {
			this.ventanaVentas.errorSeleccionQuitar();
		}
	}


	private void cargarPanelPagos(ActionEvent cpp) {
		
		MedioPagoDTO medioPago = (MedioPagoDTO) this.ventanaVentas.getCmbModoPago().getSelectedItem();
		CardLayout layoutPago = (CardLayout) this.ventanaVentas.getPanelPagos().getLayout();
		if(medioPago!=null){
			if(medioPago.getNombre().equals("Efectivo" ) || medioPago.getNombre().equals("MercadoPago")) {
				layoutPago.show(this.ventanaVentas.getPanelPagos(), "panelVacio");
			} else if (medioPago.getNombre().equals("Credito") || medioPago.getNombre().equals("Debito")) {
				layoutPago.show(this.ventanaVentas.getPanelPagos(), "panelTarjetas");
			}
		}
	}


	public void inicializar(ControladorGeneral cg) {
		this.controladorGeneral = cg;
		this.ventanaVentas.setTitle("Ventas");
		this.ventanaVentas.setVentaMantenimiento(false);
		this.ventanaVentas.getBtnCargarCliente().setEnabled(true);
		this.ventanaVentas.getBtnCargarProducto().setEnabled(true);
		this.ventanaVentas.getBtnCargarAutomovil().setEnabled(true);
		this.ventanaVentas.getBtnQuitar().setEnabled(true);
		this.mediosPagoEnTabla = medioPago.obtenerMediosPago();
		this.ventanaVentas.llenarMediosDePago(mediosPagoEnTabla);
		int medioPago;
		if(this.ventanaVentas.getCmbModoPago().getItemCount() == 0)medioPago = 0;
		else medioPago = ((MedioPagoDTO)this.ventanaVentas.getCmbModoPago().getSelectedItem()).getId();
		this.opcionesPagoEnTabla = this.opcionPago.obtenerOpcionPago(medioPago);
		this.ventanaVentas.llenarOpcionesPago(this.opcionesPagoEnTabla);
		this.ventanaVentas.mostrarVentana();
		}
	
	private void actualizarOpcionPago(ActionEvent aop){
		if(this.ventanaVentas.getCmbModoPago().isLoaded()){
			this.opcionesPagoEnTabla = this.opcionPago.obtenerOpcionPago(((MedioPagoDTO)this.ventanaVentas.getCmbModoPago().getSelectedItem()).getId());
			this.ventanaVentas.llenarOpcionesPago(this.opcionesPagoEnTabla);
		}
	}

	private void cargarCliente(ActionEvent cargarCliente) {
		this.controladorGeneral.seleccionarClienteVenta(this.ventanaVentas);
	}
	
	private void cargarAutomovil(ActionEvent cargarAutomovil) {
		this.controladorGeneral.seleccionarAutomovilVenta(this.ventanaVentas);
	}
	
	private void cargarAutoparte(ActionEvent cargarAutoparte) {
		this.controladorGeneral.seleccionarAutoparteVenta(this.ventanaVentas);
	}
	
	private void realizarVenta(ActionEvent rv) {
		try{
			if(!this.ventanaVentas.isVentaMantenimiento()){
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		        String fecha = format.format(new Date());
				ClienteDTO cliente =  this.ventanaVentas.getCliente();
				if(cliente !=null){
					OpcionPagoDTO opcionPago = ((OpcionPagoDTO)this.ventanaVentas.getCmbAccion().getSelectedItem());
					List<Pair<StockAutomovilDTO, GarantiaDTO>> autosAcomprar = new ArrayList<Pair<StockAutomovilDTO, GarantiaDTO>>();
					List<AutoparteDTO> autopartesAcomprar = new ArrayList<AutoparteDTO>();
			
					for(Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO> producto : this.ventanaVentas.getCarritoCompra()){
						if(producto.getMiddle()!=null&&producto.getRight()==null){
							autosAcomprar.add(producto.getMiddle());
						}else if(producto.getMiddle()==null&&producto.getRight()!=null){
							autopartesAcomprar.add(producto.getRight());
						}else{
							
						}
					}
					if(autosAcomprar.size()>0||autopartesAcomprar.size()>0){
						if(this.id_TipoUser_Sucursal.getRight()!=null){
							if(this.id_TipoUser_Sucursal.getLeft()!=null){
								double monto = 0;
								monto = Double.parseDouble(this.ventanaVentas.getTxtTotalPagar().getText().substring(4));
						
								VentaDTO venta = null;
								EstadoVentaDTO estado = new EstadoVentaDTO(1,"Realizada");
								if(opcionPago.getMedioPago().getNombre().equals("Credito") || opcionPago.getMedioPago().getNombre().equals("Debito")) {
									//System.out.println(opcionPago.getMedioPago().getNombre());
									String numeroTarjeta = this.ventanaVentas.getTxtNumeroTarjeta().getText();
									if(!ControladorCampos.CampoVacio(numeroTarjeta)) {
										if(ControladorCampos.validarNumeroTarjeta(numeroTarjeta)) {
											String codigoTarjeta = this.ventanaVentas.getTxtCodigoTarjeta().getText();
											if(!ControladorCampos.CampoVacio(codigoTarjeta)) {
												if(ControladorCampos.validarCodigoTarjeta(codigoTarjeta)) {
													String titularTarjeta = this.ventanaVentas.getTxtNombreTarjeta().getText();
													if(!ControladorCampos.CampoVacio(titularTarjeta)) {
														String vencimiento = this.ventanaVentas.getTxtVencimiento().getText();
														if(!ControladorCampos.CampoVacio(vencimiento)) {
															if(ControladorCampos.validarVencimiento(vencimiento)) {
																TarjetaDTO tarjeta = new TarjetaDTO(0,numeroTarjeta, codigoTarjeta, titularTarjeta, vencimiento);
																venta = new VentaDTO(0, fecha, cliente, opcionPago, autosAcomprar,autopartesAcomprar, monto, tarjeta, estado, this.id_TipoUser_Sucursal.getRight(), this.id_TipoUser_Sucursal.getLeft());
															}else this.ventanaVentas.advertirFechaVencimientoInvalida();
														}else this.ventanaVentas.advertirFaltaFechaVencimiento();	
													}else this.ventanaVentas.advertirFaltaTitularTarjeta();	
												}else this.ventanaVentas.advertirCodigoTarjetaInvalido();	
											}else this.ventanaVentas.advertirFaltaCodigoTarjeta();
										} else this.ventanaVentas.advertirNumeroTarjetaInvalido();;
									}else this.ventanaVentas.advertirFaltaNumeroTarjeta();
								}else {
									venta = new VentaDTO(0, fecha, cliente, opcionPago, autosAcomprar,autopartesAcomprar, monto, null, estado, this.id_TipoUser_Sucursal.getRight(),this.id_TipoUser_Sucursal.getLeft());	
								}
								if(this.venta.agregarVenta(venta)) {
									this.ventanaVentas.vaciarCampos();
									this.ventanaVentas.notificarVentaExitosa();
									this.controladorGeneral.verificarStock(1);
								} else this.ventanaVentas.errorVentaFallida();
							}else{
								this.ventanaVentas.advertirFaltaUsuario();
							}
						}else{
							this.ventanaVentas.advertirFaltaSucursal();
						}
					}else{
						this.ventanaVentas.advertirFaltaSeleccionProducto();
					}
				}else{
					this.ventanaVentas.advertirFaltaSeleccionCliente();
				}
			}else{
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		        String fecha = format.format(new Date());
				ClienteDTO cliente =  this.ventanaVentas.getCliente();
				OpcionPagoDTO opcionPago = ((OpcionPagoDTO)this.ventanaVentas.getCmbAccion().getSelectedItem());
				List<Pair<StockAutomovilDTO, GarantiaDTO>> autosAcomprar = new ArrayList<Pair<StockAutomovilDTO, GarantiaDTO>>();
				List<AutoparteDTO> autopartesAcomprar = new ArrayList<AutoparteDTO>();
				for(Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO> producto : this.ventanaVentas.getCarritoCompra()){
					if(producto.getMiddle()==null&&producto.getRight()!=null){
						autopartesAcomprar.add(producto.getRight());
					}
				}
				if(this.id_TipoUser_Sucursal.getLeft()!=null){
					double monto = 0;
					monto = Double.parseDouble(this.ventanaVentas.getTxtTotalPagar().getText().substring(4));
			
					VentaDTO venta = null;
					EstadoVentaDTO estado = new EstadoVentaDTO(1,"Realizada");
					if(opcionPago.getMedioPago().getNombre().equals("Credito") || opcionPago.getMedioPago().getNombre().equals("Debito")) {
						//System.out.println(opcionPago.getMedioPago().getNombre());
						String numeroTarjeta = this.ventanaVentas.getTxtNumeroTarjeta().getText();
						if(!ControladorCampos.CampoVacio(numeroTarjeta)) {
							if(ControladorCampos.validarNumeroTarjeta(numeroTarjeta)) {
								String codigoTarjeta = this.ventanaVentas.getTxtCodigoTarjeta().getText();
								if(!ControladorCampos.CampoVacio(codigoTarjeta)) {
									if(ControladorCampos.validarCodigoTarjeta(codigoTarjeta)) {
										String titularTarjeta = this.ventanaVentas.getTxtNombreTarjeta().getText();
										if(!ControladorCampos.CampoVacio(titularTarjeta)) {
											String vencimiento = this.ventanaVentas.getTxtVencimiento().getText();
											if(!ControladorCampos.CampoVacio(vencimiento)) {
												if(ControladorCampos.validarVencimiento(vencimiento)) {
													TarjetaDTO tarjeta = new TarjetaDTO(0,numeroTarjeta, codigoTarjeta, titularTarjeta, vencimiento);
													venta = new VentaDTO(0, fecha, cliente, opcionPago, autosAcomprar,autopartesAcomprar, monto, tarjeta, estado, this.id_TipoUser_Sucursal.getRight(), this.id_TipoUser_Sucursal.getLeft());
												}else this.ventanaVentas.advertirFechaVencimientoInvalida();
											}else this.ventanaVentas.advertirFaltaFechaVencimiento();	
										}else this.ventanaVentas.advertirFaltaTitularTarjeta();	
									}else this.ventanaVentas.advertirCodigoTarjetaInvalido();	
								}else this.ventanaVentas.advertirFaltaCodigoTarjeta();
							} else this.ventanaVentas.advertirNumeroTarjetaInvalido();;
						}else this.ventanaVentas.advertirFaltaNumeroTarjeta();
					}else {
						venta = new VentaDTO(0, fecha, cliente, opcionPago, autosAcomprar,autopartesAcomprar, monto, null, estado, this.id_TipoUser_Sucursal.getRight(),this.id_TipoUser_Sucursal.getLeft());	
					}
					if(this.venta.agregarVentaMantenimiento(venta, this.ventanaVentas.getMantenimiento())) {
						this.controladorGeneral.refrescarMantenimientos();
						this.ventanaVentas.vaciarCampos();
						this.ventanaVentas.notificarVentaExitosa();
						this.controladorGeneral.realizarEncuesta();
					}else this.ventanaVentas.errorVentaFallida();
				}else{
					this.ventanaVentas.advertirFaltaUsuario();
				}
			}
		}catch(Exception ex){
			//fururo mensaje de erro al ingresar una venta
		}
	}
	
	private void volverDesdeVenta(ActionEvent vdr) {
		if (this.ventanaVentas.isVisible()) {
			this.ventanaVentas.cerrar();
		}
		if(this.ventanaVentas.isVentaMantenimiento()){
			this.ventanaVentas.cerrar();
		}else{
			this.controladorGeneral.inicializar();
		}
	}
	
	protected void seleccionarGarantiaVenta() {
		this.ventanaGarantia.llenarGarantias(this.garantia.obtenerGarantia());
		this.ventanaGarantia.mostrarVentana();
	}
	
	protected Object[] dameGarantiasVentas(){
		List<GarantiaDTO> garantias = this.garantia.obtenerGarantia();
		String[] NombreGarantias = new String[garantias.size()];
		for(GarantiaDTO garantia : garantias){
			
		}
		return this.garantia.obtenerGarantia().toArray();
	}
	
	
	protected VentanaVentas dameVentanaVentas(){
		this.ventanaVentas.setTitle("Venta Mantenimiento");
		this.mediosPagoEnTabla = medioPago.obtenerMediosPago();
		this.ventanaVentas.llenarMediosDePago(mediosPagoEnTabla);
		int medioPago;
		if(this.ventanaVentas.getCmbModoPago().getItemCount() == 0)medioPago = 0;
		else medioPago = ((MedioPagoDTO)this.ventanaVentas.getCmbModoPago().getSelectedItem()).getId();
		this.opcionesPagoEnTabla = this.opcionPago.obtenerOpcionPago(medioPago);
		this.ventanaVentas.llenarOpcionesPago(this.opcionesPagoEnTabla);
		return this.ventanaVentas;
	}
	/*private void elegirGarantia(ActionEvent eg) {
		GarantiaDTO garantiaVenta = ((GarantiaDTO)this.ventanaGarantia.getCmbGarantia().getSelectedItem());
		
	}
	*/
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.controladorGeneral = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursal = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
}
