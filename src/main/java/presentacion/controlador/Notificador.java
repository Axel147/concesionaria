package presentacion.controlador;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.tuple.Pair;

import dto.ReservaDTO;
import modelo.Reserva;


public class Notificador {
	
	private Reserva reserva;
	
	public Notificador(Reserva reserva){
		this.reserva = reserva;
	}
	
	private boolean enviarMail(String gmailEmisor, String claveGmailEmisor, String destinatario, List<String> destinatarios, String asunto, String cuerpo, boolean multipleMails) {
	    // Esto es lo que va delante de @gmail.com en la cuenta de correo. Es el remitente también.
	    String remitente = gmailEmisor;  //Para la dirección gmailEmpresa@gmail.com

	    Properties props = System.getProperties();
	    props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
	    props.put("mail.smtp.user", remitente);
//	    props.put("mail.smtp.clave", claveGmailEmisor);    //La clave de la cuenta
	    props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
	    props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
	    props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google
	    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
	    Session session = Session.getDefaultInstance(props);
	    MimeMessage message = new MimeMessage(session);

	    try {
	        message.setFrom(new InternetAddress(remitente));
	        if(multipleMails){
		        for(int i=0;i<destinatarios.size();i++){
		        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatarios.get(i)));   //Se podrían añadir varios de la misma manera
		        }
	        }else{
	        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
	        }
	        message.setSubject(asunto);
	        message.setText(cuerpo);
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com", remitente, claveGmailEmisor);
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	        return true;
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();   //Si se produce un error
	        return false;
	    }
	}
	
	public void notificarReservaClientes(){
		List<ReservaDTO> reservasSinNotificar = this.reserva.obtenerReservasParaNotificar();
		Pair<String, String> Email = this.reserva.dameEmail();
		if(Email!=null){
			for(int i=0;i<reservasSinNotificar.size();i++){
				String mailDestinatario = reservasSinNotificar.get(i).getCliente().getMail();
			boolean mailEnviado = this.enviarMail(Email.getLeft(), Email.getRight(), mailDestinatario, null, "Recordatorio Reserva Turno Taller", "Estimado "+reservasSinNotificar.get(i).getCliente().getNombre()+", le recordamos que usted tiene una reservar para el taller de la consecionaria multicar para el dia "+reservasSinNotificar.get(i).getFechaTurno()+". De no poder asistir porfavor notificar la cancelación de la reserva y el motivo.\nDesde ya muchas gracias por contar con nosotros esperamos verlo pronto.",false);
				if(mailEnviado){
					reservasSinNotificar.get(i).setNotificada(true);
					this.reserva.editarReserva(reservasSinNotificar.get(i));
				}
			}
		}
		
	}
	
}
