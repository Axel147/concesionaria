package presentacion.controlador;

import java.awt.event.ActionEvent;

import org.apache.commons.lang3.tuple.Triple;

import dto.EncuestaDTO;
import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import modelo.Encuesta;
import presentacion.vista.VentanaEncuesta;

public class ControladorEncuesta {


	private VentanaEncuesta ventanaEncuesta;
	
	private Encuesta encuesta;
	

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	public ControladorEncuesta(Encuesta encuesta) {
		// se cargan las clases de tipo modelo
		this.encuesta = encuesta;

		// se cargan las clases jframe
		this.ventanaEncuesta = VentanaEncuesta.getInstance();

		// eventos ventana
		this.ventanaEncuesta.getBtnGuardar().addActionListener(ge->guardarEncuesta(ge));
	}
	
	
	public void inicializar() {
		this.ventanaEncuesta.mostrarVentana();
	}

	public void guardarEncuesta(ActionEvent ge) {
		int puntaje_p1, puntaje_p2, puntaje_p3;
		if(this.ventanaEncuesta.getRdb1EstOp1().isSelected()){
			puntaje_p1 = 1;
		}else if(this.ventanaEncuesta.getRdb2EstOp1().isSelected()){
			puntaje_p1 = 2;
		}else if(this.ventanaEncuesta.getRdb3EstOp1().isSelected()){
			puntaje_p1 = 3;
		}else if(this.ventanaEncuesta.getRdb4EstOp1().isSelected()){
			puntaje_p1 = 4;
		}else{
			puntaje_p1 = 5;
		}
		if(this.ventanaEncuesta.getRdb1EstOp2().isSelected()){
			puntaje_p2 = 1;
		}else if(this.ventanaEncuesta.getRdb2EstOp2().isSelected()){
			puntaje_p2 = 2;
		}else if(this.ventanaEncuesta.getRdb3EstOp2().isSelected()){
			puntaje_p2 = 3;
		}else if(this.ventanaEncuesta.getRdb4EstOp2().isSelected()){
			puntaje_p2 = 4;
		}else{
			puntaje_p2 = 5;
		}
		if(this.ventanaEncuesta.getRdb1EstOp3().isSelected()){
			puntaje_p3 = 1;
		}else if(this.ventanaEncuesta.getRdb2EstOp3().isSelected()){
			puntaje_p3 = 2;
		}else if(this.ventanaEncuesta.getRdb3EstOp3().isSelected()){
			puntaje_p3 = 3;
		}else if(this.ventanaEncuesta.getRdb4EstOp3().isSelected()){
			puntaje_p3 = 4;
		}else{
			puntaje_p3 = 5;
		}
		EncuestaDTO nuevaEncuesta = new EncuestaDTO(0, puntaje_p1, puntaje_p2, puntaje_p3);
		this.encuesta.guardarEncuesta(nuevaEncuesta);
		this.ventanaEncuesta.cerrar();
	}
	
	
	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}
}
