package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import modelo.Pais;
import modelo.Sucursal;
import presentacion.vista.ListaDeSucursales;
import presentacion.vista.VentanaSucursal;

public class ControladorSucursal {

	private List<SucursalDTO> sucursalesEnTabla; // lista de sucursales

	private VentanaSucursal ventanaSucursal; // ventana para agregar o modificar una sucursal
	private List<PaisDTO> paisesEnTabla;

	private ListaDeSucursales ventanaListaDeSucursales; // ventana con todos las sucursales(ABM)

	private Sucursal sucursal;
	private Pais pais;

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	public ControladorSucursal(Sucursal sucursal, Pais pais) {
		// se cargan las clases de tipo modelo
		this.sucursal = sucursal;
		this.pais = pais;

		// se cargan las clases jframe
		this.ventanaListaDeSucursales = ListaDeSucursales.getInstance();
		this.ventanaSucursal = VentanaSucursal.getInstance();

		// eventos ventana

		// se crean eventos para los elementos del jframe
		// ventanaListaDeSucursales
		this.ventanaListaDeSucursales.getBtnBuscar().addActionListener(buscsuc -> buscarSucursal(buscsuc));
		this.ventanaListaDeSucursales.getBtnAgregar().addActionListener(as -> ventanaAgregarSucursal(as));
		this.ventanaListaDeSucursales.getBtnEditar().addActionListener(ms -> ventanaEditarSucursal(ms));
		this.ventanaListaDeSucursales.getBtnBorrar().addActionListener(bs -> borrarSucursal(bs));
		this.ventanaListaDeSucursales.getBtnVolver().addActionListener(vds -> volverDesdeSucursal(vds));

		// se crean eventos para los elementos del jframe ventanaSucursal
		this.ventanaSucursal.getBtnGuardar().addActionListener(gs -> guardarSucursal(gs));
		this.ventanaSucursal.getTxtAltura().addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				revisarEscrituraAlturaSucursal(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
		});
	}

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.refrescarTablaSucursales();
		this.ventanaListaDeSucursales.mostrarVentana();
	}

	private void ventanaAgregarSucursal(ActionEvent as) {
		this.ventanaSucursal.setTipoVentana(1);
		this.ventanaSucursal.setTitle("Agregar Sucursal");
		this.paisesEnTabla = this.pais.obtenerPais();
		this.ventanaSucursal.llenarPaises(this.paisesEnTabla);
		this.ventanaSucursal.getListadoPaises().setEnabled(true);
		this.ventanaSucursal.setVentanaBloqueada(this.ventanaListaDeSucursales);
		this.ventanaListaDeSucursales.setEnabled(false);
		this.ventanaSucursal.mostrarVentana();
	}

	public void ventanaEditarSucursal(ActionEvent ms) {
		int[] filasSeleccionadas = this.ventanaListaDeSucursales.getTablaSucursales().getSelectedRows();
		if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para editar de la tabla sucursales
		{
			if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para editar de la tabla modelos
			{
				for (int fila : filasSeleccionadas) {
					this.ventanaSucursal.setTipoVentana(2); // setea tipo ventana editar
					this.ventanaSucursal.setTitle("Editar Sucursal");
					// this.paisesEnTabla = pais.obtenerPais();
					// this.ventanaSucursal.llenarPaises(this.paisesEnTabla);
					this.ventanaSucursal.getListadoPaises().setEnabled(false);
					// seteamos datos de la sucursal a editar
					this.ventanaSucursal.setIdSucursal(this.sucursalesEnTabla.get(fila).getIdSucursal());
					this.ventanaSucursal.setNombre(this.sucursalesEnTabla.get(fila).getNombre());
					this.ventanaSucursal.setCalle(this.sucursalesEnTabla.get(fila).getCalle());
					this.ventanaSucursal.setAltura(this.sucursalesEnTabla.get(fila).getAltura());
					this.ventanaSucursal.setVentanaBloqueada(this.ventanaListaDeSucursales);
					this.ventanaListaDeSucursales.setEnabled(false);
					
					// mostramos la ventana
					this.ventanaSucursal.mostrarVentana();
				}
			}
		} else
			this.ventanaListaDeSucursales.advertirSeleccionIncorrecta();
		this.refrescarTablaSucursales();

	}

	public void guardarSucursal(ActionEvent gs) {
		String nombre = this.ventanaSucursal.getTxtNombre().getText();
		String calle = this.ventanaSucursal.getTxtCalle().getText();
		String altura = this.ventanaSucursal.getTxtAltura().getText();
		PaisDTO pais = (PaisDTO) this.ventanaSucursal.getListadoPaises().getSelectedItem();

		if (ControladorCampos.CampoVacio(nombre)) {
			ventanaSucursal.advertirFaltaNombreSucursal();
		} else {
			if (!ControladorCampos.NombreSucursalValido(nombre)) {
				ventanaSucursal.advertirNombreSucursalInvalido();
			} else {
				if (ControladorCampos.CampoVacio(calle)) {
					ventanaSucursal.advertirFaltaCalleSucursal();
				} else {
					if (!ControladorCampos.CalleSucursalValida(calle)) {
						ventanaSucursal.advertirCalleSucursalInvalida();
					} else {
						if (ControladorCampos.CampoVacio(altura)) {
							ventanaSucursal.advertirFaltaAlturaSucursal();
						} else {
							if (!ControladorCampos.AlturaValida(altura)) {
								ventanaSucursal.advertirAlturaSucursalInvalida();
							} else {
								SucursalDTO nuevaSucursal = new SucursalDTO(0, nombre, calle, altura, pais);
								if (this.ventanaSucursal.getTipoVentana() == 1) {
									if (!this.sucursal.agregarSucursal(nuevaSucursal)) {
										this.ventanaSucursal.advertenciaSucursalRepetidaAgregar();
									} else {
										this.refrescarTablaSucursales();
										this.ventanaSucursal.cerrar();
									}
								} else {
									nuevaSucursal.setIdSucursal(ventanaSucursal.getIdSucursal());

									if (!this.sucursal.editarSucursal(nuevaSucursal)) {
										this.ventanaSucursal.advertenciaSucursalRepetidaModificar();
									} else {
										this.refrescarTablaSucursales();
										this.ventanaSucursal.cerrar();
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void borrarSucursal(ActionEvent bs) {
		int[] filasSeleccionadas = this.ventanaListaDeSucursales.getTablaSucursales().getSelectedRows();
		if (filasSeleccionadas.length == 1) {
			for (int fila : filasSeleccionadas) {
				if (!this.sucursal.borrarSucursal(this.sucursalesEnTabla.get(fila))) {
					this.ventanaListaDeSucursales.advertenciaSucursalUtilizandose();
				} else {
					this.refrescarTablaSucursales();
				}
			}
		} else {
			this.ventanaListaDeSucursales.advertirSeleccionIncorrecta();
		}
	}

	public void buscarSucursal(ActionEvent buscsuc) {
		int indiceCampo;
		if (this.ventanaListaDeSucursales.getRdbtnNombre().isSelected())
			indiceCampo = 1;
		else if (this.ventanaListaDeSucursales.getRdbtnCalle().isSelected())
			indiceCampo = 2;
		else if (this.ventanaListaDeSucursales.getRdbtnAltura().isSelected())
			indiceCampo = 3;
		else
			indiceCampo = 4;
		String dato = this.ventanaListaDeSucursales.getTxtBusqueda().getText();
		this.sucursalesEnTabla = sucursal.obtenerSucursalPor(dato, indiceCampo);
		this.ventanaListaDeSucursales.llenarTabla(this.sucursalesEnTabla);
	}

	private void volverDesdeSucursal(ActionEvent vds) {
		if (this.ventanaSucursal.isVisible()) {
			this.ventanaSucursal.cerrar();
		}
		this.ventanaListaDeSucursales.dispose();
		this.cg.inicializar();
	}

	private void revisarEscrituraAlturaSucursal(KeyEvent ctel) {
		if (!Character.isDigit(ctel.getKeyChar()) && ctel.getKeyChar() != KeyEvent.VK_BACK_SPACE) {
			ctel.consume();
			this.ventanaSucursal.advertirEscrituraAltura();
		}
	}

	// refresca tabla de ListaDeSucursales
	private void refrescarTablaSucursales() {
		this.sucursalesEnTabla = this.sucursal.obtenerSucursales();
		this.ventanaListaDeSucursales.llenarTabla(this.sucursalesEnTabla);
	}

	public ListaDeSucursales getVentanaListaDeSucursales() {
		return ventanaListaDeSucursales;
	}

	public VentanaSucursal getVentanaSucursal() {
		return ventanaSucursal;
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursalUsuario = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}

}
