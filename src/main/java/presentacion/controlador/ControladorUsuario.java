package presentacion.controlador;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;
import modelo.Pais;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import presentacion.vista.ListaDeUsuarios;
import presentacion.vista.VentanaUsuario;

public class ControladorUsuario {

	private List<UsuarioDTO> usuariosEnTabla; // lista de usuarios
	private List<TipoUsuarioDTO> tiposUsuario; //lista de tipos de usuarios
	private List<PaisDTO> paises;
	private List<TipoDocumentoDTO> tiposDocumento;
	private List<SucursalDTO> sucursales;

	private UsuarioDTO usuario_a_editar;

	private VentanaUsuario ventanaUsuario; // ventana para agregar o modificar un usuario

	private ListaDeUsuarios ventanaListaDeUsuarios; // ventana con todos los usuario(ABM)
	
	private List<Triple<Integer,Integer,String>> filtrosBusqueda;

	private Usuario usuario;
	private Pais pais;
	private TipoDocumento tipoDocumento;
	private Sucursal sucursal;

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	public ControladorUsuario(Usuario usuario, Pais pais, TipoDocumento tipoDocumento, Sucursal sucursal) {
		// se cargan las clases de tipo modelo
		this.usuario = usuario;
		this.pais = pais;
		this.tipoDocumento = tipoDocumento;
		this.sucursal = sucursal;

		// se cargan las clases jframe
		this.ventanaListaDeUsuarios = ListaDeUsuarios.getInstance();
		this.ventanaUsuario = VentanaUsuario.getInstance();

		// eventos ventana

		// eventos para lista de usuarios
		this.ventanaListaDeUsuarios.getBtnAgregar().addActionListener(au -> ventanaAgregarUsuario(au));
		this.ventanaListaDeUsuarios.getBtnBorrar().addActionListener(bu -> borrarUsuario(bu));
		this.ventanaListaDeUsuarios.getBtnEditar().addActionListener(eu -> ventanaEditarUsuario(eu));
		this.ventanaListaDeUsuarios.getComboCampos().addActionListener(acb-> actualizarCampoBusqueda(acb));
		this.ventanaListaDeUsuarios.getComboPaises().addActionListener(atdl->actualizarTiposDocumentoLista(atdl));
		this.ventanaListaDeUsuarios.getBtnAgregarFiltro().addActionListener(afb->aplicarFiltroBusqueda(afb));
		this.ventanaListaDeUsuarios.getBtnDeshacerFiltros().addActionListener(dfb->deshacerFiltrosBusqueda(dfb));
		this.ventanaListaDeUsuarios.getBtnVolver().addActionListener(vdu -> volverDesdeUsuario(vdu));

		// se crean eventos para los elementos del jframe ventanaUsuario
		this.ventanaUsuario.getComboPaises().addActionListener(atdu->actualizarTipoDocumentoUsuario(atdu));
		this.ventanaUsuario.getBtnUsuario().addActionListener(gu -> guardarUsuario(gu));
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}

	

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.tiposUsuario = this.usuario.obtenerTiposUsuario();
		this.paises = this.pais.obtenerPais();
		this.sucursales = this.sucursal.obtenerSucursales();
		this.refrescarTablaUsuarios();
		this.ventanaUsuario.cargarPaises(paises);
		this.ventanaUsuario.cargarTiposUsuario(tiposUsuario);
		this.ventanaUsuario.cargarSucursales(sucursales);
		this.ventanaListaDeUsuarios.getComboCampos().setSelectedIndex(-1);
		this.ventanaListaDeUsuarios.datosBusquedaNulos();
		this.ventanaListaDeUsuarios.mostrarVentana();
	}
	
	private void actualizarTipoDocumentoUsuario(ActionEvent atdu) {
		PaisDTO paisSeleccionado = (PaisDTO)this.ventanaUsuario.getComboPaises().getSelectedItem();
		if(paisSeleccionado!= null) {
		this.tiposDocumento = this.tipoDocumento.obtenerTiposDocumento(paisSeleccionado.getIdPais());
		this.ventanaUsuario.cargarTiposDocumento(this.tiposDocumento);
		}else {
			this.ventanaUsuario.getComboTiposDoc().removeAllItems();
		}
	}

	private void ventanaAgregarUsuario(ActionEvent au) {
		this.ventanaUsuario.modoAgregar();
		this.ventanaUsuario.setVentanaBloqueada(this.ventanaListaDeUsuarios);
		this.ventanaListaDeUsuarios.setEnabled(false);
		this.ventanaUsuario.mostrarVentana();
	}

	private void ventanaEditarUsuario(ActionEvent eu) {
		int fila = this.ventanaListaDeUsuarios.getTablaUsuarios().getSelectedRow();
		if (fila != -1) {
			this.usuario_a_editar = this.usuariosEnTabla.get(fila);
			this.ventanaUsuario.modoEditar();
			this.ventanaUsuario.setVentanaBloqueada(this.ventanaListaDeUsuarios);
			this.ventanaUsuario.mostrarUsuario(usuario_a_editar);
			this.ventanaListaDeUsuarios.setEnabled(false);
			this.ventanaUsuario.mostrarVentana();
		} else {
			this.ventanaListaDeUsuarios.errorSeleccion();
		}
	}

	public void guardarUsuario(ActionEvent gu) {
		ventanaUsuario.ocultarLabelsError();
		if (this.ventanaUsuario.validarCampos()) {
			String nombre = this.ventanaUsuario.getTxtNombre().getText();
			String apellido = this.ventanaUsuario.getTxtApellido().getText();
			String sexo = this.ventanaUsuario.getSexo();
			PaisDTO paisResidencia = (PaisDTO) this.ventanaUsuario.getComboPaises().getSelectedItem();
			TipoDocumentoDTO tipoDoc = (TipoDocumentoDTO) this.ventanaUsuario.getComboTiposDoc().getSelectedItem();
			String documento = this.ventanaUsuario.getTxtDocumento().getText();
			String nombreUsuario = this.ventanaUsuario.getTxtUsuario().getText();
			String mail = this.ventanaUsuario.getTxtMail().getText();
			String password = this.ventanaUsuario.getTxtPassword().getText();
			TipoUsuarioDTO tipoUsr = (TipoUsuarioDTO) this.ventanaUsuario.getComboTiposUsr().getSelectedItem();
			SucursalDTO sucursal = (SucursalDTO) this.ventanaUsuario.getComboSucursales().getSelectedItem();

			int modo = this.ventanaUsuario.getModo();
			switch (modo) {

			case 0:
				UsuarioDTO usuario_a_agregar = new UsuarioDTO(0,nombre,apellido,sexo,paisResidencia,tipoDoc,documento,nombreUsuario,mail,password,tipoUsr,sucursal);
				if (!this.usuario.agregarUsuario(usuario_a_agregar)) {
					this.ventanaUsuario.errorAgregar();
				}
				break;

			case 1:
				UsuarioDTO usuario_editado = new UsuarioDTO(this.usuario_a_editar.getId(),nombre,apellido,sexo,paisResidencia,tipoDoc,documento,nombreUsuario,mail,password,tipoUsr,sucursal);
				if (!this.usuario.editarUsuario(usuario_editado)) {
					this.ventanaUsuario.errorEditar();
				}
				break;

			}

			this.ventanaUsuario.cerrar();
			this.refrescarTablaUsuarios();

		} else {
			this.ventanaUsuario.errorCargar();
		}

	}

	public void borrarUsuario(ActionEvent bu) {
		int fila = this.ventanaListaDeUsuarios.getTablaUsuarios().getSelectedRow();
		if (fila != -1) {
			UsuarioDTO usuario_a_eliminar = this.usuariosEnTabla.get(fila);
			if (!this.usuario.borrarUsuario(usuario_a_eliminar)) {
				this.ventanaListaDeUsuarios.errorBorrar();
			}
			this.refrescarTablaUsuarios();
		} else {
			this.ventanaListaDeUsuarios.errorSeleccion();
		}
	}
	
	public void actualizarCampoBusqueda(ActionEvent acb) {
		int seleccion = this.ventanaListaDeUsuarios.getComboCampos().getSelectedIndex();
		CardLayout layoutCampo = (CardLayout)this.ventanaListaDeUsuarios.getPanelBusqueda().getLayout();
		if (seleccion != -1){
			if(seleccion==0||seleccion==1||seleccion==5||seleccion==6||seleccion==7) {
				layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(), "panelTxt");
			} else if(seleccion==2) {
				this.ventanaListaDeUsuarios.cargarComboSexo();
				layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(),"panelCombo");
				} else if (seleccion == 3) {
				this.ventanaListaDeUsuarios.cargarComboPais(this.paises);
				layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(),"panelCombo");
					} else if(seleccion==4) {
						this.ventanaListaDeUsuarios.cargarCombosDoc(this.paises);
						layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(), "panelComboDoc");
						} else if(seleccion==8) {
							this.ventanaListaDeUsuarios.cargarTiposUsuario(this.tiposUsuario);
							layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(), "panelCombo");
							} else if(seleccion == 9) {
								this.ventanaListaDeUsuarios.cargarSucursales(sucursales);
								layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(), "panelCombo");
							}
	   this.ventanaListaDeUsuarios.datosBusquedaNulos();

	
	} else {
		layoutCampo.show(this.ventanaListaDeUsuarios.getPanelBusqueda(), "panelVacio");
	}
	}
	
	public void actualizarTiposDocumentoLista(ActionEvent atdl) {
		PaisDTO paisSeleccionado = (PaisDTO) this.ventanaListaDeUsuarios.getComboPaises().getSelectedItem();
		if(paisSeleccionado!= null) {
		this.tiposDocumento = this.tipoDocumento.obtenerTiposDocumento(paisSeleccionado.getIdPais());
		this.ventanaListaDeUsuarios.cargarTiposDoc(this.tiposDocumento);
		}else {
			this.ventanaListaDeUsuarios.getComboTipoDoc().removeAllItems();
		}
	}
	
	public void aplicarFiltroBusqueda(ActionEvent afb) {
		int seleccion = this.ventanaListaDeUsuarios.getComboCampos().getSelectedIndex();
		int condicion = -1;
		String valor = null;
		if(seleccion != -1) {
			int campo = this.ventanaListaDeUsuarios.getNroscampo()[seleccion];
			if(seleccion==0||seleccion==1||seleccion==5||seleccion==6||seleccion==7) {
				condicion = this.ventanaListaDeUsuarios.getComboCondicion().getSelectedIndex();
				valor = this.ventanaListaDeUsuarios.getTxtValor().getText();
				if (condicion!=-1 && !ControladorCampos.CampoVacio(valor)) {
					this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,condicion,valor));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
				}
			} else if(seleccion==2) {
				String sexo = (String) this.ventanaListaDeUsuarios.getComboValor().getSelectedItem();
				if(sexo != null) {
				this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,sexo));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
				}
			} else if(seleccion==3) {
				PaisDTO pais = (PaisDTO) this.ventanaListaDeUsuarios.getComboValor().getSelectedItem();
				if(pais!= null) {
				String idPais = Integer.toString(pais.getIdPais());
				this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,idPais));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
				}
			} else if(seleccion==4) {
				TipoDocumentoDTO tipoDoc = (TipoDocumentoDTO) this.ventanaListaDeUsuarios.getComboTipoDoc().getSelectedItem();
				String numDoc = this.ventanaListaDeUsuarios.getTxtValor_2().getText();
				if(tipoDoc != null && !ControladorCampos.CampoVacio(numDoc)) {
					int idTipoDoc = tipoDoc.getIdTipoDocumento();
					this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(4,0,Integer.toString(idTipoDoc)));
					this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,numDoc));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
				}
			} else if(seleccion==8) {
				TipoUsuarioDTO tipoUsr = (TipoUsuarioDTO) this.ventanaListaDeUsuarios.getComboValor().getSelectedItem();
				if(tipoUsr!= null) {
				String idTipoUsr = Integer.toString(tipoUsr.getIdTipo());
				this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,idTipoUsr));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
				}
			} else if(seleccion==9){
				SucursalDTO sucursal = (SucursalDTO) this.ventanaListaDeUsuarios.getComboValor().getSelectedItem();
				if(sucursal!=null) {
				String idSucursal = Integer.toString(sucursal.getIdSucursal());
				this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,0,idSucursal));
				} else {
					this.ventanaListaDeUsuarios.errorFiltrar();
					}
			} else {
				this.ventanaListaDeUsuarios.errorFiltrar();
			}
			this.usuariosEnTabla = this.usuario.filtrarUsuarios(filtrosBusqueda);
			this.ventanaListaDeUsuarios.llenarTabla(usuariosEnTabla);
		} else this.ventanaListaDeUsuarios.errorComboFiltro();
	}
	
	private void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.refrescarTablaUsuarios();
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}





	private void volverDesdeUsuario(ActionEvent vdu) {

		if (this.ventanaUsuario.isVisible()) {
			this.ventanaUsuario.cerrar();
		}
		this.ventanaListaDeUsuarios.dispose();
		this.cg.inicializar();
	}
	
	protected Triple<Integer, TipoUsuarioDTO, SucursalDTO> loging(String usuario, char[] password){
		return this.usuario.logearUsuario(usuario, password);
	}

	// refresca tabla de ListaDeUsuarios
	private void refrescarTablaUsuarios() {
		this.usuariosEnTabla = usuario.obtenerUsuarios();
		this.ventanaListaDeUsuarios.llenarTabla(usuariosEnTabla);
	}

	// geter y seter de testing -_-////

	public ListaDeUsuarios getVentanaListaDeUsuarios() {
		return ventanaListaDeUsuarios;
	}

	public VentanaUsuario getVentanaUsuario() {
		return ventanaUsuario;
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursalUsuario = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}

}
