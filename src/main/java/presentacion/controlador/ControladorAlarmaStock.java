package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
import modelo.Automovil;
import modelo.Autoparte;
import presentacion.reportes.ReporteFaltaStockAutomovil;
import presentacion.reportes.ReporteFaltaStockAutoparte;
import presentacion.vista.VentanaAlarmaStock;

public class ControladorAlarmaStock {
	
	private VentanaAlarmaStock ventanaAlarmaStock;
	
	private Autoparte autoparte;
	private Automovil automovil;
	
	private List<AutoparteDTO> autopartesFaltaStock;
	private List<StockAutomovilDTO> autosFaltaStock;
	
	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;
	
	public ControladorAlarmaStock(Autoparte autoparte, Automovil automovil) {
		super();
		this.autoparte = autoparte;
		this.automovil = automovil;
		
		this.ventanaAlarmaStock = VentanaAlarmaStock.getInstance();
		
		this.ventanaAlarmaStock.getBtnReporteAutos().addActionListener(gra->generarReporteAutos(gra));
		this.ventanaAlarmaStock.getBtnReporteAutopartes().addActionListener(grap->generarReporteAutopartes(grap));
		this.ventanaAlarmaStock.getBtnVolver().addActionListener(vdas->volverDesdeAlarmaStock(vdas));
	}
	
	private void generarReporteAutopartes(ActionEvent grap) {
		ReporteFaltaStockAutoparte reporte = new ReporteFaltaStockAutoparte();
		reporte.mostrar();
	}

	private void generarReporteAutos(ActionEvent gra) {
		ReporteFaltaStockAutomovil reporte = new ReporteFaltaStockAutomovil();
		reporte.mostrar();
	}

	public void llenarStockFaltante() {
		this.autopartesFaltaStock = this.autoparte.autopartesFaltaStock();
		this.autosFaltaStock = this.automovil.automovilesFaltaStock();
	}
	
	public boolean faltaStock() {
		return (this.autopartesFaltaStock.size()+this.autosFaltaStock.size()>=1);
	}

	public void inicializar(ControladorGeneral cc, int modo) {
		this.cg = cc;
		this.ventanaAlarmaStock.llenarAutos(this.autosFaltaStock, this.autopartesFaltaStock);
		this.ventanaAlarmaStock.mostrarVentana();
		this.ventanaAlarmaStock.setModo(modo);
	}
	
	public void volverDesdeAlarmaStock(ActionEvent vdas) {
		this.ventanaAlarmaStock.dispose();
		if(this.ventanaAlarmaStock.getModo()==0) {
		this.cg.inicializar();
		}
	}

	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}

//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursal = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
	
}
