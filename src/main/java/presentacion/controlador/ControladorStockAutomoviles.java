package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import dto.StockAutomovilDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;
import dto.TipoUsuarioDTO;
//import javafx.util.Pair;
import modelo.Automovil;
import modelo.Marca;
import modelo.Modelo;
import modelo.Sucursal;
import presentacion.vista.ListaDeAutomoviles;
import presentacion.vista.ListaDeModelos;
import presentacion.vista.VentanaAutomovil;
import presentacion.vista.VentanaVentas;

public class ControladorStockAutomoviles {

	private List<StockAutomovilDTO> automovilesEnTabla; //lista de automoviles en stock
	private List<MarcaDTO> marcasEnTabla; //lista de marcas
	private List<ModeloDTO> modelosEnTabla; //lista de modelos
	private List<SucursalDTO> sucursalesEnTabla; //lista de sucursales
	
	private VentanaAutomovil ventanaAutomovil;
	
	private ListaDeAutomoviles ventanaListaDeAutomoviles;
	
	private Automovil automovil;
	private Marca marca;
	private Modelo modelo;
	private Sucursal sucursal;
	
	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	
	public ControladorStockAutomoviles(Automovil automovil, Marca marca, Modelo modelo,  Sucursal sucursal){
		//se cargan las clases de tipo modelo
		this.automovil = automovil;
		this.marca = marca;
		this.modelo = modelo;
		this.sucursal = sucursal;

		//se cargan las clases jframe	
		this.ventanaListaDeAutomoviles = ListaDeAutomoviles.getInstance();
		this.ventanaAutomovil = VentanaAutomovil.getInstance();
		
		//eventos ventana
			
		//se crean eventos para los elementos del jframe ventanaListaDeAutomoviles
		this.ventanaListaDeAutomoviles.getBtnAgregar().addActionListener(aAutomovil->ventanaAgregarAutomovil(aAutomovil));
		this.ventanaListaDeAutomoviles.getBtnEditar().addActionListener(eA->ventanaEditarAuto(eA));
		this.ventanaListaDeAutomoviles.getBtnBorrar().addActionListener(bAutomovil->borrarAutomovil(bAutomovil));
		this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().addActionListener(amas->actualizarModeloAutosStock(amas));
		this.ventanaListaDeAutomoviles.getComboBoxListaModelos().addActionListener(acA -> actualizarTablaAuto(acA));
		this.ventanaListaDeAutomoviles.getBtnConfigurarMarcas().addActionListener(cmarca->configurarMarcas(cmarca));
		this.ventanaListaDeAutomoviles.getBtnConfigurarModelos().addActionListener(cmodel->configurarModelos(cmodel));
		this.ventanaListaDeAutomoviles.getBtnIngresarStockPorCSV().addActionListener(ivpc-> ingresarVehiculosPorCSV(ivpc));
		this.ventanaListaDeAutomoviles.getComboBoxListaSucursales().addActionListener(acA -> actualizarTablaAuto(acA));
		this.ventanaListaDeAutomoviles.getComboBoxListaYear().addActionListener(acA -> actualizarTablaAuto(acA));
		this.ventanaListaDeAutomoviles.getChckbx0Km().addActionListener(acA -> actualizarTablaAuto(acA));
		this.ventanaListaDeAutomoviles.getBtnVolver().addActionListener(vdc -> volverDesdeStockAutomovil(vdc));
		
		
		//se crean eventos para los elementos del jframe ventanaAutomovil
		this.ventanaAutomovil.getBtnGuardar().addActionListener(gAutomovil->guardarAutomovil(gAutomovil));
		this.ventanaAutomovil.getCmbMarca().addActionListener(amva->actualizarModeloVentanaAutomovil(amva));
		//--------------------------------------------------------------------------------------------------------------------------------------------
		this.ventanaListaDeAutomoviles.getTablaAutomoviles().addMouseListener(new MouseListener() {@Override public void mouseClicked(MouseEvent venta) {confirmarSeleccionAutomovil(venta);}@Override public void mouseEntered(MouseEvent venta) {}@Override public void mouseExited(MouseEvent venta) {}@Override public void mousePressed(MouseEvent venta) {}@Override public void mouseReleased(MouseEvent venta) {}});
		this.ventanaListaDeAutomoviles.getBtnVolver().addActionListener(vdsa->volverDesdeStockAutomoviles(vdsa));
		
	}
	
	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.ventanaListaDeAutomoviles.setModoSeleccion(false);
		this.ventanaListaDeAutomoviles.setTitle("Lista De Automoviles");
		this.marcasEnTabla = marca.obtenerMarca();
		this.ventanaListaDeAutomoviles.llenarMarca(this.marcasEnTabla);
		this.sucursalesEnTabla = this.sucursal.obtenerSucursales();
		this.ventanaListaDeAutomoviles.llenarSucursales(this.sucursalesEnTabla);
		int marca;
		if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getItemCount() == 0)marca = 0;
		else marca = ((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId();
		this.modelosEnTabla = this.modelo.obtenerModelo(marca);
		this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
		int modelo;
		if(this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getItemCount() == 0)modelo = 0;
		else modelo = ((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId();
		this.refrescarTablaAutos(modelo);
		this.ventanaListaDeAutomoviles.getBtnVolver().setVisible(true);
		this.ventanaListaDeAutomoviles.mostrarVentana();
	}
	
	public void ventanaAgregarAutomovil(ActionEvent aAutomovil) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			this.ventanaAutomovil.setTipoVentana(1);
			this.ventanaAutomovil.setTitle("Agregar Automovil");
			this.marcasEnTabla = marca.obtenerMarca();
			this.ventanaAutomovil.llenarMarca(marcasEnTabla);
			
			this.modelosEnTabla = modelo.obtenerModelo(((MarcaDTO)this.ventanaAutomovil.getCmbMarca().getSelectedItem()).getId());
			this.ventanaAutomovil.llenarModelo(this.modelosEnTabla);
				
			this.sucursalesEnTabla = sucursal.obtenerSucursales();
			this.ventanaAutomovil.llenarSucursales(sucursalesEnTabla);
			if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3){
				this.ventanaAutomovil.getCmbSucursal().setSelectedItem(this.id_TipoUser_Sucursal.getRight());
				this.ventanaAutomovil.getCmbSucursal().setEnabled(false);
			}
			
			this.ventanaAutomovil.getCmbMarca().setSelectedItem(((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()));
			this.ventanaAutomovil.getCmbMarca().setEnabled(true);
			this.ventanaAutomovil.getCmbModelo().setSelectedItem((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem());
			this.ventanaAutomovil.getCmbModelo().setEnabled(true);
			this.ventanaAutomovil.setVentanaBloqueada(this.ventanaListaDeAutomoviles);
			this.ventanaListaDeAutomoviles.setEnabled(false);
			
			this.ventanaAutomovil.mostrarVentana();
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoIngresarAutoparte();
		}
	}
	
	public void ventanaEditarAuto(ActionEvent eA)
	{
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int[] filasSeleccionadas = this.ventanaListaDeAutomoviles.getTablaAutomoviles().getSelectedRows();
			if(filasSeleccionadas.length == 1)//verificamos que seleccionara solo un valor para editar de la tabla localidades
			{
				for (int fila : filasSeleccionadas)
				{
					if(this.automovilesEnTabla.get(fila).getSucursal().getIdSucursal()==this.id_TipoUser_Sucursal.getRight().getIdSucursal()||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=3){
						this.ventanaAutomovil.setTipoVentana(2);
						this.ventanaAutomovil.setTitle("Editar Automovil");
						
						this.marcasEnTabla = marca.obtenerMarca();
						this.ventanaAutomovil.llenarMarca(marcasEnTabla);
							
						this.modelosEnTabla = modelo.obtenerModelo(((MarcaDTO)this.ventanaAutomovil.getCmbMarca().getSelectedItem()).getId());
						this.ventanaAutomovil.llenarModelo(this.modelosEnTabla);
							
						this.sucursalesEnTabla = sucursal.obtenerSucursales();
						this.ventanaAutomovil.llenarSucursales(sucursalesEnTabla);
//						this.ventanaAutomovil.getCmbSucursal().setEnabled(false);
						
						this.ventanaAutomovil.setTxtColor(this.automovilesEnTabla.get(fila).getColor());
						this.ventanaAutomovil.setTxtCantidadPuertas(this.automovilesEnTabla.get(fila).getCantidadPuertas());
						this.ventanaAutomovil.setTxtKilometros(this.automovilesEnTabla.get(fila).getKilometraje() + "");
						this.ventanaAutomovil.setTxtStock(Integer.toString(this.automovilesEnTabla.get(fila).getStock()));
						this.ventanaAutomovil.setTxtYear(this.automovilesEnTabla.get(fila).getYear());
						this.ventanaAutomovil.setTextStockMinimo(Integer.toString(this.automovilesEnTabla.get(fila).getStockMinimo()));
						this.ventanaAutomovil.setTextPrecio(Double.toString(this.automovilesEnTabla.get(fila).getPrecio()));
						this.ventanaAutomovil.setIdAutomovil(this.ventanaListaDeAutomoviles.getAutosACargar().get(fila).getId());
						
						this.ventanaAutomovil.getCmbMarca().setSelectedItem(((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()));
						this.ventanaAutomovil.getCmbMarca().setEnabled(true);
						this.ventanaAutomovil.getCmbModelo().setSelectedItem((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem());
						this.ventanaAutomovil.getCmbModelo().setEnabled(true);
						this.ventanaAutomovil.setVentanaBloqueada(this.ventanaListaDeAutomoviles);
						this.ventanaListaDeAutomoviles.setEnabled(false);
						this.ventanaAutomovil.mostrarVentana();
					}else{
						this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoEditarAutomovilOtraSucursal();
					}
				}
			}else this.ventanaListaDeAutomoviles.advertirSeleccionIncorrecta();
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoEditarAutomovil();
		}
	}
	
	public void guardarAutomovil(ActionEvent gA){
		
		ModeloDTO modelo = (ModeloDTO)this.ventanaAutomovil.getCmbModelo().getSelectedItem();
		String year = this.ventanaAutomovil.getTxtYear().getText();
		String color = this.ventanaAutomovil.getTxtColor().getText();
		String cantPuertas = this.ventanaAutomovil.getTxtCantidadPuertas().getText();
		String txtKilometros = this.ventanaAutomovil.getTxtKilometros().getText();
		String txtStockMinimo = this.ventanaAutomovil.getTextStockMinimo().getText();
		String txtStock = this.ventanaAutomovil.getTxtStock().getText();
		SucursalDTO sucursal = (SucursalDTO)this.ventanaAutomovil.getCmbSucursal().getSelectedItem();
		String txtPrecio = this.ventanaAutomovil.getTextPrecio().getText();
		
		if(ControladorCampos.CampoVacio(year)) {
			this.ventanaAutomovil.advertirFaltaAnioAutomovil();
		}else{
			if(!ControladorCampos.anioValido(year)) {
				this.ventanaAutomovil.advertenciaAnioAutomovilInvalido();
			}else{
				if(ControladorCampos.CampoVacio(color)){
					this.ventanaAutomovil.advertirFaltaColorAutomovil();
				}else {
					if(!ControladorCampos.colorValido(color)) {
						this.ventanaAutomovil.advertenciaColorAutomovilInvalido();
					}else {
						if(ControladorCampos.CampoVacio(cantPuertas)) {
							this.ventanaAutomovil.advertirFaltaCantidadPuertasAutomovil();
						}else {
							if(!ControladorCampos.validarCantidadPuertas(cantPuertas)) {
								this.ventanaAutomovil.advertenciaCantidadPuertasAutoInvalido();
							}else {								
								if(ControladorCampos.CampoVacio(txtStockMinimo)) {
									this.ventanaAutomovil.advertirFaltaStockMinimo();
								} else {
									if(!ControladorCampos.validarStock(txtStockMinimo)) {
										this.ventanaAutomovil.advertenciaStockAutomovilInvalido();
									} else {
										if(ControladorCampos.CampoVacio(txtStock)) {
											this.ventanaAutomovil.advertirFaltaStockAutomovil();
										}else {
											if(!ControladorCampos.validarStock(txtStock)) {
												this.ventanaAutomovil.advertenciaStockAutomovilInvalido();
											}else {			
												if(ControladorCampos.CampoVacio(txtPrecio)) {
													this.ventanaAutomovil.advertirFaltaPrecio();
												} else {
													if(!ControladorCampos.validarPrecio(txtPrecio)) {
														this.ventanaAutomovil.advertenciaPrecioInvalido();
													} else {
														if(ControladorCampos.CampoVacio(txtKilometros)) {
															this.ventanaAutomovil.advertirKilometrajeVacio();
														}else {
															if(!ControladorCampos.validarKilometros(txtKilometros)) {
																this.ventanaAutomovil.advertenciaKilometrajeInvalido();
															}else{
																int stock = Integer.parseInt(txtStock);
																int stockMinimo = Integer.parseInt(txtStockMinimo);
																int kilometros = Integer.parseInt(txtKilometros);
																double precio = Double.parseDouble(txtPrecio);
																StockAutomovilDTO nuevoAuto = new StockAutomovilDTO(0, modelo, year, color, cantPuertas, kilometros, stockMinimo, stock, sucursal, precio);
																if(this.ventanaAutomovil.getTipoVentana()==1){
																	if(!this.automovil.agregarAutomovil(nuevoAuto)){
																		this.ventanaAutomovil.advertenciaAutomovilRepetidoAgregar();
																	}else{
																		this.ventanaAutomovil.advertenciaAutomovilAgregadoCorrectamente();
																		this.refrescarTablaAutos(((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId());
																		this.ventanaAutomovil.cerrar();
																		this.cg.stockActualizado(nuevoAuto);
																	}
																}else{
																	nuevoAuto.setId(ventanaAutomovil.getIdAutomovil());
																	if(!this.automovil.editarAutomovil(nuevoAuto)){
																		this.ventanaAutomovil.advertenciaAutomovilRepetidoModificar();
																	}else{
																		this.ventanaAutomovil.advertenciaAutomovilAgregadoCorrectamente();
																		this.refrescarTablaAutos(((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId());
																		this.ventanaAutomovil.cerrar();
																		this.cg.stockActualizado(nuevoAuto);
																	}
																}
															}														
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void borrarAutomovil(ActionEvent bAutomovil) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int[] filasSeleccionadas = this.ventanaListaDeAutomoviles.getTablaAutomoviles().getSelectedRows();
			if(filasSeleccionadas.length == 1){
				for (int fila : filasSeleccionadas){
					if(!this.automovil.borrarAutomovil(this.automovilesEnTabla.get(fila))) {
						this.ventanaListaDeAutomoviles.advertenciaAutomovilesUtilizandose();
					}else {
						this.refrescarTablaAutos(((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId());
					}
				}
			}else {
				this.ventanaListaDeAutomoviles.advertirSeleccionIncorrecta();
			}
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoEliminarAutomovil();
		}
	}
	
	private void actualizarModeloAutosStock(ActionEvent amas){
		if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().isLoaded()){
			this.modelosEnTabla = this.modelo.obtenerModelo(((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId());
			this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
		}
	}
	
	private void actualizarTablaAuto(ActionEvent am){
		if(this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getItemCount() == 0)this.automovilesEnTabla = this.automovil.obtenerAutomoviles(0);
		else {
//			this.refrescarTablaAutos(((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId());
			this.automovilesEnTabla = this.automovil.filtrarAutomoviles(this.obtenerFiltrosBusqueda());
			this.ventanaListaDeAutomoviles.llenarTablaAutomovil(automovilesEnTabla);
		}
	}
	
	
	private void actualizarModeloVentanaAutomovil(ActionEvent amva){
		if(this.ventanaAutomovil.getCmbMarca().isLoaded()){
			this.modelosEnTabla = this.modelo.obtenerModelo(((MarcaDTO)this.ventanaAutomovil.getCmbMarca().getSelectedItem()).getId());
			this.ventanaAutomovil.llenarModelo(this.modelosEnTabla);
		}
	}	
	
	private void configurarMarcas(ActionEvent cmarca){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			this.cg.configurarMarca();
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoConfigurarMarcas();
		}
	}
	
	private void configurarModelos(ActionEvent cmarca){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			this.cg.configurarModelo();
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoConfigurarModelos();
		}
	}

	private void ingresarVehiculosPorCSV(ActionEvent ivpc){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=5&&this.id_TipoUser_Sucursal.getMiddle().getIdTipo()!=4){
			int option = this.ventanaListaDeAutomoviles.getBuscadorArchivos().showOpenDialog(this.ventanaListaDeAutomoviles);
			
			if(option == this.ventanaListaDeAutomoviles.getBuscadorArchivos().APPROVE_OPTION){
				try {
				   this.sucursalesEnTabla = this.sucursal.obtenerSucursales(1);
				   ArrayList<Pair <Integer, StockAutomovilDTO>> autosIngresados = Lector.leerArchivoCSV(this.ventanaListaDeAutomoviles.getBuscadorArchivos().getSelectedFile().getAbsolutePath(), this.marca, this.modelo, this.id_TipoUser_Sucursal.getRight());
				   ArrayList<String> lineasIngresadasCorrectamente = new ArrayList<String>();
				   for(Pair <Integer, StockAutomovilDTO> auto : autosIngresados){
					   if(this.automovil.agregarAutomovil(auto.getValue())){
						   lineasIngresadasCorrectamente.add(auto.getKey().toString());
					   }
				   }
				   this.ventanaListaDeAutomoviles.notificarIngresosCorrectos(lineasIngresadasCorrectamente);
				} catch (FileNotFoundException e) {
				   e.printStackTrace();
				}
			   int modelo;
				if(this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getItemCount() == 0)modelo = 0;
				else modelo = ((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId();
				this.refrescarTablaAutos(modelo);
			}
		}else{
			this.ventanaListaDeAutomoviles.advertirUsuarioNoAutorizadoIngresarAutoparte();
		}
   }
	
	private void volverDesdeStockAutomovil(ActionEvent vds) {
		if (this.ventanaAutomovil.isVisible()) {
			this.ventanaAutomovil.cerrar();
		}
		this.ventanaListaDeAutomoviles.dispose();
		this.cg.inicializar();
	}

	//refresca tabla de ListaDeAutomovilrd
	private void refrescarTablaAutos(int idModelo) {
		this.automovilesEnTabla = this.automovil.obtenerAutomoviles(idModelo);
		this.ventanaListaDeAutomoviles.llenarTablaAutomovil(this.automovilesEnTabla);
	}
	
	public List<Triple<Integer,Integer,String>> obtenerFiltrosBusqueda(){
		List<Triple<Integer,Integer,String>> filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
		ModeloDTO modelo = (ModeloDTO) this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem();
		SucursalDTO sucursal = (SucursalDTO) this.ventanaListaDeAutomoviles.getComboBoxListaSucursales().getSelectedItem();
		String year = (String) this.ventanaListaDeAutomoviles.getComboBoxListaYear().getSelectedItem();
		if(modelo!=null) filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(0,0,Integer.toString(modelo.getId())));
		if(sucursal!=null) filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(1,0,Integer.toString(sucursal.getIdSucursal())));	
		if(year!=null && !year.equals("Todos")) filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(2,0,year));
		if(this.ventanaListaDeAutomoviles.getChckbx0Km().isSelected()) filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(3,0,Integer.toString(0)));
	
		return filtrosBusqueda;
	
	}
	
	//geter y seter de testing -_-////
	
	public ListaDeAutomoviles getVentanaListaDeAutomoviles() {
		return ventanaListaDeAutomoviles;
	}


	public VentanaAutomovil getVentanaAutomovil() {
		return ventanaAutomovil;
	}
	
	public Automovil getAutomovil() {
		return automovil;
	}


	public List<StockAutomovilDTO> getAutomovilesEnTabla() {
		return automovilesEnTabla;
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private VentanaVentas ventanaVentas;
	
	
	
	public void seleccionarAutomovilVenta(VentanaVentas ventana){
		this.ventanaVentas = ventana;
		this.marcasEnTabla = marca.obtenerMarca();
		this.ventanaListaDeAutomoviles.llenarMarca(this.marcasEnTabla);
		int marca;
		if(this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getItemCount() == 0)marca = 0;
		else marca = ((MarcaDTO)this.ventanaListaDeAutomoviles.getComboBoxListaMarcas().getSelectedItem()).getId();
		this.modelosEnTabla = this.modelo.obtenerModelo(marca);
		this.ventanaListaDeAutomoviles.llenarModelo(this.modelosEnTabla);
		this.sucursalesEnTabla = this.sucursal.obtenerSucursales();
		this.ventanaListaDeAutomoviles.llenarSucursales(this.sucursalesEnTabla);
		int modelo;
		if(this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getItemCount() == 0)modelo = 0;
		else modelo = ((ModeloDTO)this.ventanaListaDeAutomoviles.getComboBoxListaModelos().getSelectedItem()).getId();
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==3)this.refrescarTablaAutosPorSucusal(modelo, this.id_TipoUser_Sucursal.getRight().getIdSucursal());
		else this.refrescarTablaAutos(modelo);
		this.ventanaListaDeAutomoviles.setModoSeleccion(true);
		this.ventanaListaDeAutomoviles.setTipoSeleccion(1);    
		this.ventanaListaDeAutomoviles.setTitle("Seleccione un Automovil...");
		this.ventanaListaDeAutomoviles.getBtnVolver().setVisible(false);
		this.ventanaListaDeAutomoviles.setVentanaVentasLlamadora(this.ventanaVentas);//agregado
		this.ventanaListaDeAutomoviles.setLlamadoPorVentas(true);//agregado
		this.ventanaVentas.setEnabled(false);
		this.ventanaListaDeAutomoviles.mostrarVentana();
	}
	
	public void confirmarSeleccionAutomovil(MouseEvent venta){
		if(this.ventanaListaDeAutomoviles.getModoSeleccion()==true){
			if(venta.getClickCount() == 2){
				int fila = this.ventanaListaDeAutomoviles.getTablaAutomoviles().getSelectedRow();
				if(this.ventanaListaDeAutomoviles.getTipoSeleccion()==1) {
					if(this.automovilesEnTabla.get(fila).getStock() > 0){
//						this.cg.seleccionarGarantiaVenta();
						Object s = null;
						if(this.automovilesEnTabla.get(fila).getKilometraje()==0) {
						s = JOptionPane.showInputDialog(null,null,
								"Seleccione Una Garantia", 2, null,
								  this.cg.dameGarantiasVentas(),"Seleccione");
					}
						this.ventanaVentas.agregarAutomovil(this.automovilesEnTabla.get(fila),s);
						this.ventanaListaDeAutomoviles.cerrar();
							
						
					}else {
						this.ventanaVentas.advertirFaltaDeStockDeVehiculo();
					}
				}
			}
		}
	}
	
	private void volverDesdeStockAutomoviles(ActionEvent vdsa) {
		if (this.ventanaListaDeAutomoviles.isVisible()) {
			this.ventanaListaDeAutomoviles.cerrar();
		}
		this.cg.inicializar();
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}

//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursalUsuario = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

	public void setVentanaListaDeAutomoviles(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
	}

	private void refrescarTablaAutosPorSucusal(int idModelo, int idSucursal) {
		this.automovilesEnTabla = this.automovil.obtenerAutomovilesPorSucursal(idModelo, idSucursal);
		this.ventanaListaDeAutomoviles.llenarTablaAutomovil(this.automovilesEnTabla);
	}
	
//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
	
	
}
