package presentacion.controlador;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import dto.AutoparteDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import modelo.Autoparte;
import modelo.Marca;
import modelo.Modelo;

public class Lector {
    public static ArrayList<Pair <Integer, StockAutomovilDTO>> leerArchivoCSV(String nombre, Marca marca, Modelo modelo, SucursalDTO sucursal) throws FileNotFoundException{
        FileInputStream fis = new FileInputStream(nombre);
        Scanner scanner = new Scanner(fis);
        Pair <Integer, StockAutomovilDTO> automovilNuevo; 
        ArrayList<Pair <Integer, StockAutomovilDTO>> automivilesNuevos = new ArrayList<Pair <Integer, StockAutomovilDTO>>(); 
        int lineaDelArchivo = 1;
        while(scanner.hasNextLine()){
           String[] split = scanner.nextLine().split("\\;");
           if(split.length == 9){
           MarcaDTO marcaEncontrada = getMarcaIsExist(marca, split[1]); 
	           if(marcaEncontrada == null){
	        	   //error
	           }else{
	        	   ModeloDTO modeloEncontrado = getModeloIsExist(modelo, marcaEncontrada, split[2]);
	        	   if(modeloEncontrado == null){
	        		   //error
	        	   }else{
	        		   String year = split[4];
	        		   String color = split[5];
	        		   String cantidadPuertas = split[6];
	        		   int cantidad = Integer.parseInt(split[7]);
	        		   String[] splitPrecio = split[8].split("\\,");
	        		   double precio = Float.parseFloat(splitPrecio[0].substring(1)+"."+splitPrecio[1]);
	        		   StockAutomovilDTO auto = new StockAutomovilDTO(0,modeloEncontrado,year,color,cantidadPuertas,0,0,cantidad,sucursal,precio);
	        		   automovilNuevo = new ImmutablePair<Integer, StockAutomovilDTO> (lineaDelArchivo, auto);
	        		   automivilesNuevos.add(automovilNuevo);
	        	   }
	           }
           }
           lineaDelArchivo++;
        }
        scanner.close();
        return automivilesNuevos;
    }
    
    public static ArrayList<Pair <Integer, AutoparteDTO>> leerArchivoCSV(String nombre, Marca marca, Modelo modelo, Autoparte autoparte, SucursalDTO sucursal) throws FileNotFoundException{
        FileInputStream fis = new FileInputStream(nombre);
        Scanner scanner = new Scanner(fis);
        Pair <Integer, AutoparteDTO> autoparteNueva; 
        ArrayList<Pair <Integer, AutoparteDTO>> autopartesNuevas = new ArrayList<Pair <Integer, AutoparteDTO>>(); 
        int lineaDelArchivo = 1;
        while(scanner.hasNextLine()){
           String[] split = scanner.nextLine().split("\\;");
           if(split.length == 6){
           MarcaDTO marcaEncontrada = getMarcaIsExist(marca, split[1]); 
	           if(marcaEncontrada == null){
	        	   //error
	           }else{
	        	   ModeloDTO modeloEncontrado = getModeloIsExist(modelo, marcaEncontrada, split[2]);
	        	   if(modeloEncontrado == null){
	        		   //error
	        	   }else{
	        		   TipoAutoparteDTO tipoAutoparteEncontrada = getAutoparteIsExist(autoparte, split[0]);
	        		   if(tipoAutoparteEncontrada != null){
	        			   String year = split[3];
		        		   String cantidad = split[4];
		        		   String[] splitPrecio = split[5].split("\\,");
		        		   double precio = Float.parseFloat(splitPrecio[0].substring(1)+"."+splitPrecio[1]);
		        		   AutoparteDTO autoParte = new AutoparteDTO(0,tipoAutoparteEncontrada,modeloEncontrado,year,sucursal,0,Integer.parseInt(cantidad),precio);
		        		   autoparteNueva = new ImmutablePair<Integer, AutoparteDTO> (lineaDelArchivo, autoParte);
		        		   autopartesNuevas.add(autoparteNueva);
	        		   }
	        		   
	        	   }
	           }
           }
           lineaDelArchivo++;
        }
        scanner.close();
        return autopartesNuevas;
    }
    
    private static MarcaDTO getMarcaIsExist(Marca marcaModel, String marca){
    	MarcaDTO marcaEncontrada = null;
    	if(marca!=null){
	    	List<MarcaDTO> marcas = marcaModel.obtenerMarca();
	    	for(int i=0;i<marcas.size();i++){
	    		if(marcas.get(i).getNombre().equals(marca))marcaEncontrada = marcas.get(i);
	    	}
    	}
    	return marcaEncontrada;
    }
    
    private static ModeloDTO getModeloIsExist(Modelo modeloModel, MarcaDTO marca, String modelo){
    	ModeloDTO modeloEncontrado = null;
    	if(modelo!=null){
	    	List<ModeloDTO> modelos = modeloModel.obtenerModelo(marca.getId());	
	    	for(int i=0;i<modelos.size();i++){
	    		if(modelos.get(i).getModelo().equals(modelo) && modelos.get(i).getMarca().getNombre().equals(marca.getNombre()))modeloEncontrado = modelos.get(i);
	    	}
    	}
    	return modeloEncontrado;
    }
    
    private static TipoAutoparteDTO getAutoparteIsExist(Autoparte autoparteModel, String tipoAutoParte){
    	TipoAutoparteDTO tipoAutoparteEncontrada = null;
    	if(tipoAutoParte!=null){
	    	List<TipoAutoparteDTO> tiposDeAutopartes = autoparteModel.obtenerTiposAutoparte();
	    	for(int i=0;i<tiposDeAutopartes.size();i++){
	    		if(tiposDeAutopartes.get(i).getNombre().equals(tipoAutoParte))tipoAutoparteEncontrada = tiposDeAutopartes.get(i);
	    	}
    	}
    	return tipoAutoparteEncontrada;
    }
    
}