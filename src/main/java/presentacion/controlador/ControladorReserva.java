package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutomovilTallerDTO;
import dto.ClienteDTO;
import dto.EstadoReservaDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.MotivoDTO;
import dto.PaisDTO;
import dto.ReservaDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import modelo.AutomovilTaller;
import modelo.EstadoReserva;
import modelo.Marca;
import modelo.Modelo;
import modelo.Pais;
import modelo.Reserva;
import modelo.TipoDocumento;
import presentacion.vista.ListaDeReservas;
import presentacion.vista.VentanaReserva;

public class ControladorReserva {

	private List<ReservaDTO> reservasEnTabla;

	private VentanaReserva ventanaReserva;

	private ListaDeReservas ventanaListaDeReservas;

	private Reserva reserva;
	private EstadoReserva estadoReserva;
	private AutomovilTaller automovilTaller;
	private Pais pais;
	private TipoDocumento tipoDocumento;
	private Marca marca;
	private Modelo modelo;

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	private List<Triple<Integer,Integer,String>> filtrosBusqueda;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	public ControladorReserva(Reserva reserva, EstadoReserva estadoReserva, AutomovilTaller automovilTaller, Pais pais,
			TipoDocumento tipoDocumento, Marca marca, Modelo modelo) {
		// se cargan las clases de tipo modelo
		this.reserva = reserva;
		this.estadoReserva = estadoReserva;
		this.automovilTaller = automovilTaller;
		this.pais = pais;
		this.tipoDocumento = tipoDocumento;
		this.marca = marca;
		this.modelo = modelo;

		// se cargan las clases jframe
		this.ventanaListaDeReservas = ListaDeReservas.getInstance();
		this.ventanaReserva = VentanaReserva.getInstance();
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();

		// eventos ventana

		// se crean eventos para los elementos del jframe VentanaListaDeReservas
//		this.ventanaListaDeReservas.getBtnAgregarFiltro().addActionListener(busr -> buscarReserva(busr));
		this.ventanaListaDeReservas.getBtnAgregarFiltro().addActionListener(afb-> agregarFiltroBusqueda(afb));
		this.ventanaListaDeReservas.getBtnDeshacerFiltros().addActionListener(dfb-> deshacerFiltrosBusqueda(dfb));
		this.ventanaListaDeReservas.getBtnAgregar().addActionListener(ar -> ventanaAgregarReserva(ar));
		this.ventanaListaDeReservas.getBtnEditar().addActionListener(mr -> ventanaEditarReserva(mr));
		this.ventanaListaDeReservas.getBtnBorrar().addActionListener(cr -> cancelarReserva(cr));
		this.ventanaListaDeReservas.getBtnVolver().addActionListener(vdr -> volverDesdeReserva(vdr));
		this.ventanaListaDeReservas.getBtnFinalizar().addActionListener(fr -> finalizarReserva(fr));

		// se crean los eventos para la ventana Reserva
		this.ventanaReserva.getCmbPaisResidencia().addActionListener(atdr -> actualizarTipoDocumentoReserva(atdr));
		this.ventanaReserva.getCmbMarca().addActionListener(amar -> actualizarModeloAutoReserva(amar));
		this.ventanaReserva.getBtnSeleccionarCliente().addActionListener(scr -> seleccionarClienteReserva(scr));
		this.ventanaReserva.getBtnGuardar().addActionListener(gr -> guardarReserva(gr));
		this.ventanaReserva.getCmbEstado().addActionListener(hem -> habilitarEdicionMotivo(hem));
		this.ventanaReserva.getChckbxGarantia().addActionListener(gtg -> gestionarTiposGarantia(gtg));
	}

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		refrescarTablaReservas();
		this.ventanaListaDeReservas.mostrarVentana();
	}

	// evento que abre el jframe 'ventanaReserva' para agregar una reserva
	public void ventanaAgregarReserva(ActionEvent ar) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			this.ventanaReserva.setTipoVentana(1);
			this.ventanaReserva.setTitle("Crear Reserva");
			// bloqueamos los atributos que no se pueden modificar para la creacion
			// de la reserva
			this.ventanaReserva.getRdbtnNo().setSelected(true);
			this.ventanaReserva.getRdbtnSi().setEnabled(false);
			this.ventanaReserva.getRdbtnNo().setEnabled(false);
			this.ventanaReserva.getCmbEstado().setEnabled(false);
			this.ventanaReserva.getTxtMotivoCancelacion().setEnabled(false);
			// llenamos combobox
			this.ventanaReserva.llenarEstados(this.estadoReserva.obtenerEstados());
			this.ventanaReserva.llenarPaisesResidencia(this.pais.obtenerPais());
			this.ventanaReserva.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
					((PaisDTO) this.ventanaReserva.getCmbPaisResidencia().getSelectedItem()).getIdPais()));
			this.ventanaReserva.llenarPaisesPatente(this.pais.obtenerPais());
			this.ventanaReserva.llenarMarcas(this.marca.obtenerMarca());
			this.ventanaReserva.llenarModelos(
					this.modelo.obtenerModelo(((MarcaDTO) this.ventanaReserva.getCmbMarca().getSelectedItem()).getId()));
			this.ventanaReserva.setVentanaBloqueada(this.ventanaListaDeReservas);
			this.ventanaListaDeReservas.setEnabled(false);
			this.ventanaReserva.mostrarVentana();
		}else{
			this.ventanaListaDeReservas.advertirUsuarioNoAutorizadoAgregarReserva();
		}
	}

	// evento que abre el jframe 'ventanaReserva' para editar una reserva
	public void ventanaEditarReserva(ActionEvent mr) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			int[] filasSeleccionadas = this.ventanaListaDeReservas.getTablaPersonas().getSelectedRows();
			if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo
												// un valor para editar de la tabla
												// cliente
			{
				for (int fila : filasSeleccionadas) {
					if(!(this.reservasEnTabla.get(fila).getEstado().getEstado().equals("Finalizada"))){
						this.ventanaReserva.setTipoVentana(2); // setea tipo ventana
																// editar
						this.ventanaReserva.setTitle("Editar Reserva");
						// desbloqueamos los atributos para la modificacion
						this.ventanaReserva.getRdbtnNo().setSelected(true);
						this.ventanaReserva.getRdbtnSi().setEnabled(true);
						this.ventanaReserva.getRdbtnNo().setEnabled(true);
						this.ventanaReserva.getCmbEstado().setEnabled(true);
						this.ventanaReserva.getTxtMotivoCancelacion().setEnabled(false);
						if (this.reservasEnTabla.get(fila).getEstado().getIdEstado() == 3)
							this.ventanaReserva.getTxtMotivoCancelacion().setEnabled(true);
		
						// llenamos combobox
						this.ventanaReserva.llenarEstados(this.estadoReserva.obtenerEstados());
						this.ventanaReserva.llenarPaisesResidencia(this.pais.obtenerPais());
						this.ventanaReserva.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
								((PaisDTO) this.ventanaReserva.getCmbPaisResidencia().getSelectedItem()).getIdPais()));
						this.ventanaReserva.llenarPaisesPatente(this.pais.obtenerPais());
						this.ventanaReserva.llenarMarcas(this.marca.obtenerMarca());
						this.ventanaReserva.llenarModelos(this.modelo
								.obtenerModelo(((MarcaDTO) this.ventanaReserva.getCmbMarca().getSelectedItem()).getId()));
		
						// seteamos datos de la reserva a editar
						this.ventanaReserva.setIdReserva(this.reservasEnTabla.get(fila).getIdReserva());
						this.ventanaReserva.setIdMotivo(this.reservasEnTabla.get(fila).getMotivo().getId());
						try {
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
							Date fechaConvertida = format.parse(this.reservasEnTabla.get(fila).getFechaTurno());
							this.ventanaReserva.getFechaTurno().setDate(fechaConvertida);
						} catch (Exception ex) {
							this.ventanaReserva.advetirFallaCargarFechaTurno();
						}
						this.ventanaReserva.getCmbHorario().setSelectedItem(this.reservasEnTabla.get(fila).getHorario());
						this.ventanaReserva.setIdCliente(this.reservasEnTabla.get(fila).getCliente().getIdCliente());
						this.ventanaReserva.setNombre(this.reservasEnTabla.get(fila).getCliente().getNombre());
						this.ventanaReserva.setApellido(this.reservasEnTabla.get(fila).getCliente().getApellido());
						this.ventanaReserva.getCmbPaisResidencia()
								.setSelectedItem(this.reservasEnTabla.get(fila).getCliente().getPaisResidencia());
						this.ventanaReserva.getCmbTipoDoc()
								.setSelectedItem(this.reservasEnTabla.get(fila).getCliente().getTipoDocumento());
						this.ventanaReserva.setNumDoc(this.reservasEnTabla.get(fila).getCliente().getNumDocumento());
						this.ventanaReserva.setTelefono(this.reservasEnTabla.get(fila).getCliente().getTelefono());
						this.ventanaReserva.setMail(this.reservasEnTabla.get(fila).getCliente().getMail());
						this.ventanaReserva.getCmbPaisPatente()
								.setSelectedItem(this.reservasEnTabla.get(fila).getAuto().getPaisPatente());
						this.ventanaReserva.setPatente(this.reservasEnTabla.get(fila).getAuto().getPatente());
						this.ventanaReserva.getCmbMarca()
								.setSelectedItem(this.reservasEnTabla.get(fila).getAuto().getModelo().getMarca());
						this.ventanaReserva.getCmbModelo()
								.setSelectedItem(this.reservasEnTabla.get(fila).getAuto().getModelo());
						this.ventanaReserva.setYear(this.reservasEnTabla.get(fila).getAuto().getYear());
						this.ventanaReserva.getRdbtnSi().setSelected(this.reservasEnTabla.get(fila).isNotificada());
						this.ventanaReserva.getCmbEstado().setSelectedItem(this.reservasEnTabla.get(fila).getEstado());
						this.ventanaReserva.setMotivo(this.reservasEnTabla.get(fila).getMotivoCancelacion());
						MotivoDTO motivo = this.reservasEnTabla.get(fila).getMotivo();
						this.ventanaReserva.getChckbxPintura().setSelected(motivo.isPintura());
						this.ventanaReserva.getChckbxMecanica().setSelected(motivo.isMecanica());
						this.ventanaReserva.getChckbxElectronica_electricidad().setSelected(motivo.isElectronica_electricidad());
						this.ventanaReserva.getChckbxEsteticaYAccesorios().setSelected(motivo.isEsteticaYAccesorios());
						this.ventanaReserva.getChckbxGarantia().setSelected(this.reservasEnTabla.get(fila).isTieneGarantia());
						this.ventanaReserva.setVentanaBloqueada(this.ventanaListaDeReservas);
						this.ventanaListaDeReservas.setEnabled(false);
						
						// mostramos la ventana
						this.ventanaReserva.mostrarVentana();
					}else this.ventanaListaDeReservas.advertirEdicionReservaFinalizada();
				}
			} else
				this.ventanaListaDeReservas.advertirSeleccionIncorrecta();
			this.refrescarTablaReservas();
		}else{
			this.ventanaListaDeReservas.advertirUsuarioNoAutorizadoEditarReserva();
		}
	}

	// metodo que guarda una reserva nueva en la lista de reservas o guarda los
	// cambios de una reserva existente en la lista
	public void guardarReserva(ActionEvent p) {
		// datos cliente
		int idCliente = this.ventanaReserva.getIdCliente();
		String nombre = this.ventanaReserva.getTxtNombre().getText();
		String apellido = this.ventanaReserva.getTxtApellido().getText();
		PaisDTO paisResidencia = ((PaisDTO) this.ventanaReserva.getCmbPaisResidencia().getSelectedItem());
		TipoDocumentoDTO tipoDocumento = ((TipoDocumentoDTO) this.ventanaReserva.getCmbTipoDoc().getSelectedItem());
		String numDoc = this.ventanaReserva.getTxtNumDoc().getText();
		String mail = this.ventanaReserva.getTxtMail().getText();
		String tel = this.ventanaReserva.getTxtTelefono().getText();
		if((!ControladorCampos.CampoVacio(nombre))&&(!ControladorCampos.CampoVacio(apellido))&&(paisResidencia!=null)&&(tipoDocumento!=null)&&(!ControladorCampos.CampoVacio(mail))&&(!ControladorCampos.CampoVacio(tel))&&(!ControladorCampos.CampoVacio(numDoc))) {
			ClienteDTO cliente = new ClienteDTO(idCliente, nombre, apellido, paisResidencia, tipoDocumento, numDoc, mail, tel);

		// datos de auto para reserva
			int idAuto = this.ventanaReserva.getIdCliente();
			PaisDTO paisPatente = ((PaisDTO) this.ventanaReserva.getCmbPaisPatente().getSelectedItem());
			if(paisPatente!=null) {
				String patente = this.ventanaReserva.getTxtPatente().getText();
				if(ControladorCampos.PatenteValida(patente, paisPatente.getPais().toUpperCase())) {
					if(paisResidencia.equals(paisPatente)||tipoDocumento.getTipoDocumento().equals("PASAPORTE")){
						if(this.ventanaReserva.getCmbModelo().getSelectedItem()!=null) {
							ModeloDTO modelo = ((ModeloDTO) this.ventanaReserva.getCmbModelo().getSelectedItem());
							if(modelo!=null) {
								String year = this.ventanaReserva.getTxtYear().getText();
								if(!ControladorCampos.CampoVacio(year)) {
									if(ControladorCampos.anioValido(year)) {
										AutomovilTallerDTO auto = new AutomovilTallerDTO(0, paisPatente, patente, modelo, year);
										AutomovilTallerDTO autoExist = this.automovilTaller.obtenerAutomovilIfExist(auto);
										auto.setId(autoExist.getId());
	
			// datos reserva
										String fechaEmitida = castearFecha(this.ventanaReserva.getFechaEmitida().getDate().toString(),
										this.ventanaReserva.getFechaEmitida().getDate().getMonth());
										if(this.ventanaReserva.getFechaTurno().getDate()!=null) {
										String fechaTurno = castearFecha(this.ventanaReserva.getFechaTurno().getDate().toString(),
										this.ventanaReserva.getFechaTurno().getDate().getMonth());
											String horario = this.ventanaReserva.getCmbHorario().getSelectedItem().toString();
											if(horario!=null) {
												boolean notificada = this.ventanaReserva.getRdbtnSi().isSelected();
												EstadoReservaDTO estado = ((EstadoReservaDTO) this.ventanaReserva.getCmbEstado().getSelectedItem());
												if(estado!=null) {
													String motivoCancelacion = this.ventanaReserva.getTxtMotivoCancelacion().getText();
			
													boolean pintura = this.ventanaReserva.getChckbxPintura().isSelected();
													boolean mecanica = this.ventanaReserva.getChckbxMecanica().isSelected();
													boolean electronica_electricidad = this.ventanaReserva.getChckbxElectronica_electricidad().isSelected();
													boolean esteticaYAccesorios = this.ventanaReserva.getChckbxEsteticaYAccesorios().isSelected();
													if(pintura || mecanica || electronica_electricidad || esteticaYAccesorios) {
														boolean tieneGarantia = this.ventanaReserva.getChckbxGarantia().isSelected();
														String tipoGarantia = null;
														if(tieneGarantia) {
																if(this.ventanaReserva.getRdbtn15MKm().isSelected()) {
																	tipoGarantia = "15mk";
																} else if(this.ventanaReserva.getRdbtn30MKm().isSelected()) {
																	tipoGarantia = "30mk";
																} else if(this.ventanaReserva.getRdbtn45MKm().isSelected()) {
																	tipoGarantia = "45mk";
																}
															}
																													
														MotivoDTO nuevoMotivo = new MotivoDTO(0,pintura,mecanica,electronica_electricidad,esteticaYAccesorios);
														ReservaDTO nuevoReserva = new ReservaDTO(0, fechaEmitida, fechaTurno, horario, cliente, auto, notificada,estado, motivoCancelacion,nuevoMotivo,tieneGarantia,tipoGarantia);
			
														if (this.ventanaReserva.getTipoVentana() == 1) {
															if (!this.reserva.agregarReserva(nuevoReserva)) {
																this.ventanaReserva.errorAlIngregarNuevaReserva();
															} else {
																this.refrescarTablaReservas();
																this.ventanaReserva.cerrar();
															}
														} else {
															nuevoReserva.setIdReserva(ventanaReserva.getIdReserva());
															nuevoReserva.getMotivo().setId(ventanaReserva.getIdMotivo());
															if (!this.reserva.editarReserva(nuevoReserva)) {
																this.ventanaReserva.errorAlIngregarModificacionEnReserva();
															} else {
																this.refrescarTablaReservas();
																this.ventanaReserva.cerrar();
															}
														}
													
													}else this.ventanaReserva.advertirFaltaMotivo();
												}else this.ventanaReserva.advertirFaltaEstado();
											}else this.ventanaReserva.advertirFaltaHorario();
										}else this.ventanaReserva.advertirFaltaFecha();
									}else this.ventanaReserva.advertirYearInvalido();
								}else this.ventanaReserva.advertirFaltaYear();
							}else this.ventanaReserva.advertirFaltaModelo();
						}else this.ventanaReserva.advertirFaltaMarca();
					}else this.ventanaReserva.advertirDiferenciaPaisResidenciaConPaisPatente();
				}else this.ventanaReserva.advertirPatenteInvalida();
			}else this.ventanaReserva.advertirFaltaPais();
		} else this.ventanaReserva.advertirFaltaCargarCliente();
	}

	// evento que cancela una reserva de la lista de reservas
	public void cancelarReserva(ActionEvent cr) {
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			int[] filasSeleccionadas = this.ventanaListaDeReservas.getTablaPersonas().getSelectedRows();
			if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo
												// un valor para cancelar de la
												// tabla reserva
			{
				for (int fila : filasSeleccionadas) {
					String motivo = this.ventanaListaDeReservas.pedirMotivoCancelacion();
					if (!(motivo==null)) {
						if (!ControladorCampos.CampoVacio(motivo)) {
							if (!this.reserva.cancelarReserva(this.reservasEnTabla.get(fila), motivo))
								this.ventanaListaDeReservas.errorAlCancelarReserva();
						} else {
							this.ventanaListaDeReservas.advertirFaltaMotivoCancelacion();
						}
					}
				}
			} else
				this.ventanaListaDeReservas.advertirSeleccionIncorrecta();
			this.refrescarTablaReservas();
		}else{
			this.ventanaListaDeReservas.advertirUsuarioNoAutorizadoCancelarReserva();
		}
	}
	
	public void finalizarReserva(ActionEvent fr){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==1||this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			int[] filasSeleccionadas = this.ventanaListaDeReservas.getTablaPersonas().getSelectedRows();
			if (filasSeleccionadas.length == 1){
				for (int fila : filasSeleccionadas) {
					if(this.reservasEnTabla.get(fila).getEstado().getEstado().equals("Activa")){
						if(this.cg.crearMantenimiento(this.reservasEnTabla.get(fila))){
							this.refrescarTablaReservas();
						}
					}
				}
			}else this.ventanaListaDeReservas.advertirSeleccionIncorrectaAlFinalizar();
		}else{
			this.ventanaListaDeReservas.advertirUsuarioNoAutorizadoFinalizarReserva();
		}
	}

	public void actualizarTipoDocumentoReserva(ActionEvent atdr) {
		if (this.ventanaReserva.getCmbPaisResidencia().isLoaded())
			this.ventanaReserva.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
					((PaisDTO) this.ventanaReserva.getCmbPaisResidencia().getSelectedItem()).getIdPais()));
	}

	public void actualizarModeloAutoReserva(ActionEvent amar) {
		if (this.ventanaReserva.getCmbMarca().isLoaded())
			this.ventanaReserva.llenarModelos(this.modelo
					.obtenerModelo(((MarcaDTO) this.ventanaReserva.getCmbMarca().getSelectedItem()).getId()));
	}

	public void habilitarEdicionMotivo(ActionEvent hem) {
		if (this.ventanaReserva.getCmbEstado().isLoaded()) {
			if (((EstadoReservaDTO) this.ventanaReserva.getCmbEstado().getSelectedItem()).getIdEstado() == 3)
				this.ventanaReserva.getTxtMotivoCancelacion().setEnabled(true);
			else
				this.ventanaReserva.getTxtMotivoCancelacion().setEnabled(false);
		}
	}
	
	private void gestionarTiposGarantia(ActionEvent gtg) {
		boolean tieneGarantia = this.ventanaReserva.getChckbxGarantia().isSelected();
		if(tieneGarantia) {
			this.ventanaReserva.mostrarTiposGarantia();
		} else {
			this.ventanaReserva.ocultarTiposGarantia();
		}
	}
	
	private void seleccionarClienteReserva(ActionEvent scr) {
		this.cg.seleccionarClienteReserva(this.ventanaReserva);
	}

	public void buscarReserva(ActionEvent busr) {
		
	
		/*
		 * int indiceCampo;
		 * if(this.ventanaListaDeClientes.getRdbtnNombre().isSelected())
		 * indiceCampo=1; else
		 * if(this.ventanaListaDeClientes.getRdbtnApellido().isSelected())
		 * indiceCampo=2; else
		 * if(this.ventanaListaDeClientes.getRdbtnNdocumento().isSelected())
		 * indiceCampo=3; else
		 * if(this.ventanaListaDeClientes.getRdbtnEmail().isSelected())
		 * indiceCampo=4; else indiceCampo=5; String dato =
		 * this.ventanaListaDeClientes.getTxtBusqueda().getText();
		 * this.clientesEnTabla = cliente.obtenerClientesPor(dato, indiceCampo);
		 * this.ventanaListaDeClientes.llenarTabla(this.clientesEnTabla);
		 */
	}
	
	private void agregarFiltroBusqueda(ActionEvent afb) {
		String valor = this.ventanaListaDeReservas.getTxtBusqueda().getText();
		if(!ControladorCampos.CampoVacio(valor)) {
			int campo = -1;
			if(this.ventanaListaDeReservas.getRdbtnNombre().isSelected()) {
				campo = 0;
			} else if(this.ventanaListaDeReservas.getRdbtnApellido().isSelected()) {
				campo = 1;
			} else if(this.ventanaListaDeReservas.getRdbtnNdocumento().isSelected()) {
				campo = 2;
			} else if (this.ventanaListaDeReservas.getRdbtnEmail().isSelected()) {
				campo = 3;
			} else if (this.ventanaListaDeReservas.getRdbtnTelefono().isSelected()) {
				campo = 4;
			} else if (this.ventanaListaDeReservas.getRdbtnEstado().isSelected()) {
				campo = 5;
			} else if(this.ventanaListaDeReservas.getRdbtnFecha().isSelected()) {
				campo = 6;
			} else if(this.ventanaListaDeReservas.getRdbtnPatente().isSelected()) {
				campo = 7;
			}
			Triple<Integer,Integer,String> filtroNuevo = new ImmutableTriple<Integer,Integer,String>(campo,2,valor);
			this.filtrosBusqueda.add(filtroNuevo);
			this.reservasEnTabla = this.reserva.filtrarReservas(filtrosBusqueda);
			this.ventanaListaDeReservas.llenarTabla(reservasEnTabla);
		}
	}
	
	private void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.refrescarTablaReservas();
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}

	private void volverDesdeReserva(ActionEvent vdr) {
		if (this.ventanaReserva.isVisible()) {
			this.ventanaReserva.cerrar();
		}
		this.ventanaListaDeReservas.dispose();
		this.cg.inicializar();
	}

	// refresca tabla de ListaDeSucursales
	private void refrescarTablaReservas() {
		this.reservasEnTabla = reserva.obtenerReservas();
		this.ventanaListaDeReservas.llenarTabla(this.reservasEnTabla);
	}

	// metodo para formatear la fecha de JDataChooser a formato String
	// yyyy-mm-dd
	public String castearFecha(String dia, int mes) {
		try {
			int espacios = 0;
			int cont = 0;
			StringBuilder sb = new StringBuilder();
			String Anio = "";
			String Mes = "-" + (mes + 1);
			for (int i = 0; i < dia.length(); i++) {
				if (dia.charAt(i) == ' ') {
					espacios++;
				} else if (espacios == 2) {
					if (cont == 0) {
						sb.append("-" + dia.charAt(i));
						cont++;
					} else {
						sb.append(dia.charAt(i));
					}
				} else if (espacios == 5) {
					Anio = Anio + dia.charAt(i);
				}
			}
			sb.insert(0, Anio);
			sb.insert(Anio.length(), Mes);
			return sb.toString();
		} catch (NullPointerException ex) {
			return "";
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	// geter y seter de testing -_-////

	public VentanaReserva getVentanaReserva() {
		return ventanaReserva;
	}

	public ListaDeReservas getVentanaListaDeReservas() {
		return ventanaListaDeReservas;
	}
	
	public List<ReservaDTO> getReservasEnTabla() {
		return reservasEnTabla;
	}

	public Reserva getReserva() {
		return reserva;
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursal = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

	public void finalizarReservasVencidas() {
		this.reserva.finalizarReservasVencidas();
	
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}

}
