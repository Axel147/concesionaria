package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import dto.ClienteDTO;
import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import modelo.Cliente;
import modelo.Pais;
import modelo.TipoDocumento;
import presentacion.vista.ListaDeClientes;
import presentacion.vista.VentanaCliente;
import presentacion.vista.VentanaReserva;
import presentacion.vista.VentanaVentas;

public class ControladorCliente {

	private List<ClienteDTO> clientesEnTabla; // lista de clientes

	private VentanaCliente ventanaCliente; // ventana para agregar o modificar un cliente
	private VentanaReserva ventanaReserva;

	private ListaDeClientes ventanaListaDeClientes; // ventana con todos los clientes(ABM)

	private Cliente cliente;
	private Pais pais;
	private TipoDocumento tipoDocumento;

	private ControladorGeneral cg;
	private Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal;
	
	private List<Triple<Integer,Integer,String>> filtrosBusqueda;
//	private int idUsuario;
//	private TipoUsuarioDTO tipoUsuario;

	public ControladorCliente(Cliente cliente, Pais pais, TipoDocumento tipoDocumento) {
		// se cargan las clases de tipo modelo
		this.cliente = cliente;
		this.pais = pais;
		this.tipoDocumento = tipoDocumento;
		
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();

		// se cargan las clases jframe
		this.ventanaListaDeClientes = ListaDeClientes.getInstance();
		this.ventanaCliente = VentanaCliente.getInstance();
		this.ventanaVentas = VentanaVentas.getInstance();

		// eventos ventana

		// se crean eventos para los elementos del jframe VentanaListaDeClientes
		this.ventanaListaDeClientes.getBtnAgregarFiltro().addActionListener(afb -> agregarFiltroBusqueda(afb));
		this.ventanaListaDeClientes.getBtnDeshacerFiltros().addActionListener(dfb -> deshacerFiltrosBusqueda(dfb));
		this.ventanaListaDeClientes.getBtnAgregar().addActionListener(ac -> ventanaAgregarCliente(ac));
		this.ventanaListaDeClientes.getBtnEditar().addActionListener(mc -> ventanaEditarCliente(mc));
		this.ventanaListaDeClientes.getBtnBorrar().addActionListener(bc -> borrarCliente(bc));
		this.ventanaListaDeClientes.getBtnVolver().addActionListener(vdc -> volverDesdeCliente(vdc));
		this.ventanaListaDeClientes.getTablaPersonas().addMouseListener(new MouseListener() {@Override public void mouseClicked(MouseEvent e) {confirmarSeleccionCliente(e);}@Override public void mouseEntered(MouseEvent e) {}@Override public void mouseExited(MouseEvent e) {}@Override public void mousePressed(MouseEvent e) {}@Override public void mouseReleased(MouseEvent e) {}});
		
		// se crean eventos para los elementos del jframe ventanaCliente
		this.ventanaCliente.getBtnGuardar().addActionListener(gc -> guardarCliente(gc));
		this.ventanaCliente.getTxtNombre().addKeyListener(new KeyListener() {@Override public void keyTyped(KeyEvent e) {revisarEscrituraNombrePersona(e);}@Override public void keyPressed(KeyEvent e) {}@Override public void keyReleased(KeyEvent e) {}});
		this.ventanaCliente.getTxtApellido().addKeyListener(new KeyListener() {@Override public void keyTyped(KeyEvent e) {revisarEscrituraApellido(e);}@Override public void keyPressed(KeyEvent e) {}@Override public void keyReleased(KeyEvent e) {}});
		this.ventanaCliente.getTxtNumDoc().addKeyListener(new KeyListener() {@Override public void keyTyped(KeyEvent e) {revisarEscrituraNumeroDocumento(e);}@Override public void keyPressed(KeyEvent e) {}@Override public void keyReleased(KeyEvent e) {}});
		this.ventanaCliente.getTxtTelefono().addKeyListener(new KeyListener() {@Override public void keyTyped(KeyEvent e) {revisarEscrituraTelefono(e);}@Override public void keyPressed(KeyEvent e) {}@Override public void keyReleased(KeyEvent e) {}});
		this.ventanaCliente.getPaisResidencia().addActionListener(atdc -> actualizarTipoDocumentoCliente(atdc));

	}

	public void inicializar(ControladorGeneral cc) {
		this.cg = cc;
		this.refrescarTablaClientes();
		this.ventanaListaDeClientes.setModoSeleccion(false);
		this.ventanaListaDeClientes.setTitle("Lista De Clientes");
		this.ventanaListaDeClientes.getBtnVolver().setVisible(true);
		establecerPermisosListaCliente();
		this.ventanaListaDeClientes.mostrarVentana();
	}

	// evento que abre el jframe 'ventanaCliente' para agregar un cliente
	public void ventanaAgregarCliente(ActionEvent ac) {
		this.ventanaCliente.setTipoVentana(1);
		this.ventanaCliente.setTitle("Agregar Cliente");
		// llenamos combobox
		this.ventanaCliente.llenarPais(this.pais.obtenerPais());
		this.ventanaCliente.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
				((PaisDTO) this.ventanaCliente.getPaisResidencia().getSelectedItem()).getIdPais()));
		this.ventanaCliente.setVentanaBloqueada(this.ventanaListaDeClientes);
		this.ventanaListaDeClientes.setEnabled(false);
		this.ventanaCliente.mostrarVentana();
	}

	// evento que abre el jframe 'ventanaPersona' para editar un cliente
	public void ventanaEditarCliente(ActionEvent eModelo) {
		int[] filasSeleccionadas = this.ventanaListaDeClientes.getTablaPersonas().getSelectedRows();
		if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para editar de la tabla cliente
		{
			for (int fila : filasSeleccionadas) {
				this.ventanaCliente.setTipoVentana(2); // setea tipo ventana editar
				this.ventanaCliente.setTitle("Editar Cliente");
				// llenamos combobox
				this.ventanaCliente.llenarPais(this.pais.obtenerPais());
				this.ventanaCliente.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
						((PaisDTO) this.ventanaCliente.getPaisResidencia().getSelectedItem()).getIdPais()));

				// seteamos datos de la cliente a editar
				this.ventanaCliente.setIdCliente(this.clientesEnTabla.get(fila).getIdCliente());
				this.ventanaCliente.setNombre(this.clientesEnTabla.get(fila).getNombre());
				this.ventanaCliente.setApellido(this.clientesEnTabla.get(fila).getApellido());
				this.ventanaCliente.getPaisResidencia().setSelectedItem(this.clientesEnTabla.get(fila).getPaisResidencia());
				this.ventanaCliente.getTipoDoc().setSelectedItem(this.clientesEnTabla.get(fila).getTipoDocumento());
				this.ventanaCliente.setNumDoc(this.clientesEnTabla.get(fila).getNumDocumento());
				this.ventanaCliente.setTelefono(this.clientesEnTabla.get(fila).getTelefono());
				this.ventanaCliente.setMail(this.clientesEnTabla.get(fila).getMail());
				this.ventanaCliente.setVentanaBloqueada(this.ventanaListaDeClientes);
				this.ventanaListaDeClientes.setEnabled(false);
				
				// mostramos la ventana
				this.ventanaCliente.mostrarVentana();
			}
		} else
			this.ventanaListaDeClientes.advertirSeleccionIncorrecta();
		this.refrescarTablaClientes();

	}

	// metodo que guarda un cliente nuevo en la lista de clientes o guarda los cambios de un cliente existente en la lista
	public void guardarCliente(ActionEvent p) {

		String nombre = this.ventanaCliente.getTxtNombre().getText();
		String apellido = this.ventanaCliente.getTxtApellido().getText();
		PaisDTO paisResidencia = ((PaisDTO) this.ventanaCliente.getPaisResidencia().getSelectedItem());
		TipoDocumentoDTO tipoDocumento = ((TipoDocumentoDTO) this.ventanaCliente.getTipoDoc().getSelectedItem());
		String numDoc = this.ventanaCliente.getTxtNumDoc().getText();
		String mail = this.ventanaCliente.getTxtMail().getText();
		String tel = this.ventanaCliente.getTxtTelefono().getText();

		/*
		 * se controla que todos los campos menos el telefono(ya que este es
		 * opcional) no esten vacios // y que los campos que esten completados
		 * deben seguir la forma dada por el controlador de campos
		 */
		if (ControladorCampos.CampoVacio(nombre))
			this.ventanaCliente.advertirFaltaNombreCliente();
		else {
			if (!ControladorCampos.NombreValido(nombre))
				this.ventanaCliente.advertirNombreClienteInvalido();
			else {
				if (ControladorCampos.CampoVacio(apellido))
					this.ventanaCliente.advertirFaltaApellidoCliente();
				else {
					if (!ControladorCampos.ApellidoValido(apellido))
						this.ventanaCliente.advertirApellidoClienteInvalido();
					else {
						if (ControladorCampos.CampoVacio(numDoc))
							this.ventanaCliente.advertirFaltaNumDocCliente();
						else {
							if (!ControladorCampos.NumeroDocumentoValido(numDoc, tipoDocumento.getTipoDocumento()))
								this.ventanaCliente.advertirNumDocClienteInvalido();
							else {
								if (ControladorCampos.CampoVacio(mail))
									this.ventanaCliente.advertirFaltaMail();
								else {
									if (!ControladorCampos.EmailValido(mail))
										this.ventanaCliente.advertirMailInvalido();
									else {
										if (!ControladorCampos.TelefonoValido(tel))
											this.ventanaCliente.advertirTelefonoInvalido();
										else {
											ClienteDTO nuevoCliente = new ClienteDTO(0, nombre, apellido,
													paisResidencia, tipoDocumento, numDoc, mail, tel);
											if (this.ventanaCliente.getTipoVentana() == 1) {
												if (!this.cliente.agregarCliente(nuevoCliente)) {
													this.ventanaCliente.advertenciaClienteRepetidoAgregar();
												} else {
													this.refrescarTablaClientes();
													this.ventanaCliente.cerrar();
												}
											} else {
												nuevoCliente.setIdCliente(ventanaCliente.getIdCliente());
												if (!this.cliente.editarCliente(nuevoCliente)) {
													this.ventanaCliente.advertenciaClienteRepetidoModificar();
												} else {
													this.refrescarTablaClientes();
													this.ventanaCliente.cerrar();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	// evento que elimina un cliente de la lista de clientes
	public void borrarCliente(ActionEvent bc) {
		int[] filasSeleccionadas = this.ventanaListaDeClientes.getTablaPersonas().getSelectedRows();
		if (filasSeleccionadas.length == 1)// verificamos que seleccionara solo un valor para borrar de la tabla cliente
		{
			for (int fila : filasSeleccionadas) {
				if (!this.cliente.borrarCliente(this.clientesEnTabla.get(fila)))
					this.ventanaListaDeClientes.errorAlBorrarCliente();
			}
		} else
			this.ventanaListaDeClientes.advertirSeleccionIncorrecta();
		this.refrescarTablaClientes();
	}

	public void actualizarTipoDocumentoCliente(ActionEvent atdc) {
		if (this.ventanaCliente.getPaisResidencia().isLoaded())
			this.ventanaCliente.llenarTiposDeDocumentos(this.tipoDocumento.obtenerTiposDocumento(
					((PaisDTO) this.ventanaCliente.getPaisResidencia().getSelectedItem()).getIdPais()));
	}
	
	public void seleccionarClienteReserva(VentanaReserva v){
		this.ventanaReserva = v;
		this.refrescarTablaClientes();
		this.ventanaListaDeClientes.setModoSeleccion(true);
		this.ventanaListaDeClientes.setTipoSeleccion(1);
		this.ventanaListaDeClientes.setTitle("Selecciones un cliente...");
		this.ventanaListaDeClientes.getBtnVolver().setVisible(false);
		this.ventanaListaDeClientes.mostrarVentana();
	}
	
	public void confirmarSeleccionCliente(MouseEvent e){
		if(this.ventanaListaDeClientes.getModoSeleccion()==true){
			if(e.getClickCount() == 2){
				int fila = this.ventanaListaDeClientes.getTablaPersonas().getSelectedRow();
				if(this.ventanaListaDeClientes.getTipoSeleccion()==1) {
					//seteamos datos del cliente para crear la reserva
					this.ventanaReserva.setIdCliente(this.clientesEnTabla.get(fila).getIdCliente());
					this.ventanaReserva.setNombre(this.clientesEnTabla.get(fila).getNombre());
					this.ventanaReserva.setApellido(this.clientesEnTabla.get(fila).getApellido());
					this.ventanaReserva.getCmbPaisResidencia().setSelectedItem(this.clientesEnTabla.get(fila).getPaisResidencia());
					this.ventanaReserva.getCmbTipoDoc().setSelectedItem(this.clientesEnTabla.get(fila).getTipoDocumento());
					this.ventanaReserva.setNumDoc(this.clientesEnTabla.get(fila).getNumDocumento());
					this.ventanaReserva.setTelefono(this.clientesEnTabla.get(fila).getTelefono());
					this.ventanaReserva.setMail(this.clientesEnTabla.get(fila).getMail());
				}else if(this.ventanaListaDeClientes.getTipoSeleccion()==2) {
					this.ventanaVentas.setCliente(this.clientesEnTabla.get(fila));
					this.ventanaVentas.setTxtCliente(this.clientesEnTabla.get(fila).getApellido() + " " + this.clientesEnTabla.get(fila).getNombre() + "/" + this.clientesEnTabla.get(fila).getNumDocumento());
				}
				this.ventanaListaDeClientes.cerrar();
			}
		}
	}

	public void agregarFiltroBusqueda(ActionEvent afb) {
		int campo = -1;
		if (this.ventanaListaDeClientes.getRdbtnNombre().isSelected())
			campo = 0;
		else if (this.ventanaListaDeClientes.getRdbtnApellido().isSelected())
			campo = 1;
		else if (this.ventanaListaDeClientes.getRdbtnPais().isSelected())
			campo = 2;
		else if (this.ventanaListaDeClientes.getRdbtnNdocumento().isSelected())
			campo = 3;
		else if (this.ventanaListaDeClientes.getRdbtnEmail().isSelected())
			campo = 4;
		else if (this.ventanaListaDeClientes.getRdbtnTelefono().isSelected())
			campo = 5;
		if(campo != -1) {
		String valor = this.ventanaListaDeClientes.getTxtBusqueda().getText();
		this.filtrosBusqueda.add(new ImmutableTriple<Integer,Integer,String>(campo,2,valor));
		this.clientesEnTabla = cliente.filtrarClientes(filtrosBusqueda);
		this.ventanaListaDeClientes.llenarTabla(this.clientesEnTabla);
		}
	}
	
	public void deshacerFiltrosBusqueda(ActionEvent dfb) {
		this.refrescarTablaClientes();
		this.filtrosBusqueda = new ArrayList<Triple<Integer,Integer,String>>();
	}

	private void volverDesdeCliente(ActionEvent vdc) {
		if (this.ventanaCliente.isVisible()) {
			this.ventanaCliente.cerrar();
		}
		this.ventanaListaDeClientes.dispose();
		this.cg.inicializar();
	}

	
	//controladores de escritura de campos keylisterners///
	private void revisarEscrituraNombrePersona(KeyEvent cnomP){
		if(Character.isDigit(cnomP.getKeyChar())){
			cnomP.consume();
			this.ventanaCliente.advertirEscrituraNombreCliente();
           }
	}
			
	private void revisarEscrituraApellido(KeyEvent capell){
		if(Character.isDigit(capell.getKeyChar())){
			capell.consume();
			this.ventanaCliente.advertirEscrituraApellidoCliente();
           }
	}
			
	private void revisarEscrituraNumeroDocumento(KeyEvent cnumDoc){
		if(!Character.isDigit(cnumDoc.getKeyChar()) && cnumDoc.getKeyChar() != KeyEvent.VK_BACK_SPACE){
			cnumDoc.consume();
			this.ventanaCliente.advertirEscrituraNumeroDocumento();
		}
	}
			
	private void revisarEscrituraTelefono(KeyEvent ctel){
		if(!Character.isDigit(ctel.getKeyChar()) && ctel.getKeyChar() != KeyEvent.VK_BACK_SPACE){
			ctel.consume();
			this.ventanaCliente.advertirEscrituraTelefono();
		}
	}
	
	
	// refresca tabla de ListaDeClientes
	private void refrescarTablaClientes() {
		this.clientesEnTabla = cliente.obtenerClientes();
		this.ventanaListaDeClientes.llenarTabla(this.clientesEnTabla);
	}

	// geter y seter de testing -_-////
	public ListaDeClientes getVentanaListaDeCliente() {
		return ventanaListaDeClientes;
	}

	public VentanaCliente getVentanaCliente() {
		return ventanaCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public List<ClienteDTO> getClientesEnTabla() {
		return clientesEnTabla;
	}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private VentanaVentas ventanaVentas;
	
	public void seleccionarClienteVenta(VentanaVentas ventana){
		this.ventanaVentas = ventana;
		this.refrescarTablaClientes();
		this.ventanaListaDeClientes.setModoSeleccion(true);  
		this.ventanaListaDeClientes.setTipoSeleccion(2);
		this.ventanaListaDeClientes.setTitle("Seleccione un cliente...");
		this.ventanaListaDeClientes.getBtnVolver().setVisible(false);
		this.ventanaListaDeClientes.setVentanaVentasLlamadora(this.ventanaVentas);//agregado
		this.ventanaListaDeClientes.setLlamadoPorVentas(true);//agregado
		this.ventanaVentas.setEnabled(false);
		
		this.ventanaListaDeClientes.mostrarVentana();
	}
	
	public void setControladorGeneral(ControladorGeneral cg) {
		this.cg = cg;
	}
	
//	public void setSucursal(SucursalDTO sucursal) {
//		this.sucursal = sucursal;
//	}

	public void setId_TipoUser_Sucursal(Triple<Integer, TipoUsuarioDTO, SucursalDTO> id_TipoUser_Sucursal) {
		this.id_TipoUser_Sucursal = id_TipoUser_Sucursal;
	}

//	public void setTipoUsuario(TipoUsuarioDTO tipoUsuario) {
//		this.tipoUsuario = tipoUsuario;
//	}
	private void establecerPermisosListaCliente(){
		if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==4){
			this.ventanaListaDeClientes.getBtnBorrar().setEnabled(false);
		}else if(this.id_TipoUser_Sucursal.getMiddle().getIdTipo()==5){
			this.ventanaListaDeClientes.getBtnBorrar().setEnabled(false);
		}
	}
	
}
