package presentacion.vista;


import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.MarcaDTO;
import dto.PaisDTO;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;

public class VentanaSucursal extends JFrame{
	private static final long serialVersionUID = 1L;
	private static VentanaSucursal INSTANCE;
	
	//panel
	private JPanel contentPane;
	
	//campos de texto
	private JTextField txtNombre;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private ComboBoxBoleanos listadoPaises;
	
	//variables auxiliares
	private int tipoVentana;//sirve para indicar si la ventana esta en modo agregar o modificar
	private int idSucursal;//sirve para guardar el id del cliente que queremos modificar luego
	
	//botones
	private JButton btnGuardar;
	
	private ListaDeSucursales ventanaListaDeSucursales;

	//devuelve una instancia de este tipo de ventana
	public static VentanaSucursal getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VentanaSucursal(); 	
			return new VentanaSucursal();
		}else return INSTANCE;
	}
	
	private VentanaSucursal(){
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 356, 216);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 350, 187);
		contentPane.add(panel);
		panel.setLayout(null);
		
		//etiquetas
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(17, 14, 59, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Calle");
		lblApellido.setBounds(17, 39, 59, 24);
		panel.add(lblApellido);
		
		JLabel lblNumDoc = new JLabel("Altura");
		lblNumDoc.setBounds(17, 74, 59, 14);
		panel.add(lblNumDoc);
		
		
		//campos de textos
		txtNombre = new JTextField();
		txtNombre.setBounds(86, 11, 242, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtCalle = new JTextField();
		txtCalle.setColumns(10);
		txtCalle.setBounds(86, 40, 242, 20);
		panel.add(txtCalle);
		
		
		txtAltura = new JTextField();
		txtAltura.setColumns(10);
		txtAltura.setBounds(85, 71, 243, 20);
		panel.add(txtAltura);
		
		//boton
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(227, 147, 101, 23);
		panel.add(btnGuardar);
		
		JLabel lblPais = new JLabel("Pais");
		lblPais.setBounds(17, 109, 46, 14);
		panel.add(lblPais);
		
		listadoPaises = new ComboBoxBoleanos();
		listadoPaises.setBounds(86, 106, 242, 19);
		panel.add(listadoPaises);
		
		this.setVisible(false);
	}
	
	//hace visible la ventana
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public int getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}

	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setNombre(String Nombre) {
		this.txtNombre.setText(Nombre);
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}
	
	public void setCalle(String calle) {
		this.txtCalle.setText(calle);
	}

	public void setAltura(String altura) {
		this.txtAltura.setText(altura);
	}
	
	public JTextField getTxtAltura() {
		return txtAltura;
	}
	
	public ComboBoxBoleanos getListadoPaises() {
		return listadoPaises;
	}
	
	public void setListadoPaises(ComboBoxBoleanos listadoPaises) {
		this.listadoPaises = listadoPaises;
	}

	public JButton getBtnGuardar() 
	{
		return btnGuardar;
	}
	
	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}	
	
	public void advertirFaltaNombreSucursal(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun nombre o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaCalleSucursal(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ninguna calle o la calle esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaAlturaSucursal()	{
		JOptionPane.showMessageDialog(null,"Debe completar el campo de Altura.","Cuidado",2);
	}	
	
	public void advertirEscrituraAltura(){
		JOptionPane.showMessageDialog(null,"La altura solo acepta caracteres númericos.","Caracter Invalido",2);
	}
	
	public void advertenciaSucursalRepetida(){
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la nueva sucursal. \nRevise que no este queriendo agregar una sucursal que ya existe.","Falla al intentar agregar una sucursal",1);		
	}
	
	public void advertenciaSucursalRepetida2(){
		JOptionPane.showMessageDialog(null,  "No se pudo modificar la nueva sucursal. \nRevise que no este queriendo modificar con los datos de una sucursal que ya existe.","Falla al intentar modificar una sucursal",1);		
	}
	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtNombre.setText(null);
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.ventanaListaDeSucursales.setEnabled(true);
		this.dispose();
	}

	public void advertirNombreSucursalInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la nueva sucursal. \nEl formato del nombre es de maximo dos palabras sin numeros ni simbolos.","Falla al intentar agregar una sucursal",1);		
		
	}

	public void advertirCalleSucursalInvalida() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la nueva sucursal. \nEl formato de la calle no posee numeros ni simbolos.","Falla al intentar agregar una sucursal",1);		
	
	}

	public void advertirAlturaSucursalInvalida() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la nueva sucursal. \nEl formato de la altura es un numero de tres a cinco digitos numericos sin espacios ni simbolos.","Falla al intentar agregar una sucursal",1);		
	
	}

	public void advertenciaSucursalRepetidaAgregar() {
		JOptionPane.showMessageDialog(null,  "El nombre de la sucursal ya se encuentra registrado","Falla al intentar agregar una sucursal",1);
		
	}

	public void advertenciaSucursalRepetidaModificar() {
		JOptionPane.showMessageDialog(null,  "No se puede modificar la sucursal ya que el argumento ingresado se encuentra registrado","Falla al intentar agregar una sucursal",1);
		
	}

	public void llenarPaises(List<PaisDTO> paisesEnTabla) {
		this.listadoPaises.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel();
            this.listadoPaises.removeAllItems();
            this.listadoPaises.setModel(cmb);
            for(PaisDTO pais : paisesEnTabla){
                cmb.addElement(pais);
            }
            this.listadoPaises.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
		
	}

	public void setVentanaBloqueada(ListaDeSucursales ventanaListaDeSucursales) {
		this.ventanaListaDeSucursales = ventanaListaDeSucursales;
		
	}
}
