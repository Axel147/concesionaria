package presentacion.vista;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JDateChooser;

import dto.EstadoReservaDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.PaisDTO;
import dto.TipoDocumentoDTO;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import com.toedter.components.JLocaleChooser;
import javax.swing.JCheckBox;

public class VentanaReserva extends JFrame{
	private static final long serialVersionUID = 1L;
	private static VentanaReserva INSTANCE;
	
	//panel
	private JPanel contentPane;
	
	//campos de reserva
	private JDateChooser fechaEmitida;
	private JDateChooser fechaTurno;
	private ComboBoxBoleanos horario;
	private JRadioButton rdbtnSi;
	private JRadioButton rdbtnNo;
	private ButtonGroup rdbtngroup;
	private ComboBoxBoleanos cmbEstado;
	private JTextArea txtMotivoCancelacion;
	
	private JCheckBox chckbxPintura;
	private JCheckBox chckbxMecanica;
	private JCheckBox chckbxElectronica_electricidad;
	private JCheckBox chckbxEsteticaYAccesorios;
	private JCheckBox chckbxGarantia;
	
	private JRadioButton rdbtn15MKm;
	private JRadioButton rdbtn30MKm;
	private JRadioButton rdbtn45MKm;
	
	private ButtonGroup rdbtnGroupTipoGarantia;
	
	private JLabel lblTipoService;
	
	//campos de cliente
	private JTextField txtNombre;
	private JTextField txtApellido;
	private ComboBoxBoleanos paisResidencia;
	private ComboBoxBoleanos tipoDoc;
	private JTextField txtNumDoc;
	private JTextField txtMail;
	private JTextField txtTelefono;
	private JButton btnSeleccionarCliente;
	
	//campos del auto
	private ComboBoxBoleanos cmbPaisPatente;
	private JTextField txtPatente;
	private ComboBoxBoleanos cmbMarca;
	private ComboBoxBoleanos cmbModelo;
	private JTextField txtYear;	
	
	//variables auxiliares
	private int tipoVentana;//sirve para indicar si la ventana esta en modo agregar o modificar
	private int idReserva;//sirve para guardar el id de la reserva que queremos modificar luego
	private int idCliente;//sirve para guardar el id del cliente que queremos modificar luego
	private int idMotivo;
	//botones
	private JButton btnGuardar;
	
	private ListaDeReservas ventanaListaDeReservas;
	

	//devuelve una instancia de este tipo de ventana
	public static VentanaReserva getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VentanaReserva(); 	
			return new VentanaReserva();
		}else return INSTANCE;
	}
	
	private VentanaReserva(){
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 843, 660);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setLocationRelativeTo(null);
		setResizable(false);
		
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 817, 609);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		//etiquetas reserva
		JLabel lblDatosDeReserva = new JLabel("Datos de Reserva");
		lblDatosDeReserva.setBounds(7, 11, 112, 14);
		panel.add(lblDatosDeReserva);
		
		JSeparator separador_reserva = new JSeparator();
		separador_reserva.setBounds(7, 23, 395, 2);
		panel.add(separador_reserva);
		
		JLabel lblFechaDeEmision = new JLabel("Fecha de Emision");
		lblFechaDeEmision.setBounds(7, 36, 112, 14);
		panel.add(lblFechaDeEmision);
		
		JLabel lblFechaDeTurno = new JLabel("Fecha de Turno");
		lblFechaDeTurno.setBounds(7, 64, 112, 14);
		panel.add(lblFechaDeTurno);
		
		JLabel lblHorario = new JLabel("Horario");
		lblHorario.setBounds(7, 89, 112, 14);
		panel.add(lblHorario);
		
		JLabel lblNotificada = new JLabel("Notificada");
		lblNotificada.setBounds(7, 117, 112, 14);
		panel.add(lblNotificada);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setBounds(7, 142, 112, 14);
		panel.add(lblEstado);
		
		JLabel lblMotivoCancelacion = new JLabel("Motivo de Cancelación");
		lblMotivoCancelacion.setBounds(7, 312, 132, 14);
		panel.add(lblMotivoCancelacion);
		
		
		//campos de reserva
		fechaEmitida = new JDateChooser();
		fechaEmitida.setBounds(129, 30, 273, 20);
		fechaEmitida.setDate(new Date());
		fechaEmitida.setEnabled(false);
		panel.add(fechaEmitida);
		
		fechaTurno = new JDateChooser();
		fechaTurno.setBounds(129, 58, 273, 20);
		fechaTurno.setMinSelectableDate(new Date());
		panel.add(fechaTurno);
		
		horario = new ComboBoxBoleanos();
		horario.setBounds(129, 86, 273, 20);
		llenarHorarios();
		panel.add(horario);
		
		rdbtnSi = new JRadioButton("Si");
		rdbtnSi.setBounds(129, 113, 50, 23);
		panel.add(rdbtnSi);
		
		rdbtnNo = new JRadioButton("No");
		rdbtnNo.setBounds(215, 113, 57, 23);
		panel.add(rdbtnNo);
		
		rdbtngroup = new ButtonGroup();
		rdbtngroup.add(rdbtnSi);
		rdbtngroup.add(rdbtnNo);
		rdbtngroup.clearSelection();
		rdbtnNo.setSelected(true);
		
		cmbEstado = new ComboBoxBoleanos();
		cmbEstado.setBounds(129, 139, 273, 20);
		panel.add(cmbEstado);
		
		txtMotivoCancelacion = new JTextArea();
		txtMotivoCancelacion.setBounds(149, 312, 253, 72);
		panel.add(txtMotivoCancelacion);
		
		
		//etiquetas cliente
		JLabel lblDatosDeCliente = new JLabel("Datos de Cliente");
		lblDatosDeCliente.setBounds(7, 395, 112, 14);
		panel.add(lblDatosDeCliente);
		
		JSeparator separador_cliente = new JSeparator();
		separador_cliente.setBounds(7, 407, 395, 2);
		panel.add(separador_cliente);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(7, 420, 112, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(7, 445, 112, 14);
		panel.add(lblApellido);
		
		JLabel lblTipoDoc = new JLabel("Tipo de Documento");
		lblTipoDoc.setBounds(7, 495, 112, 14);
		panel.add(lblTipoDoc);
		
		JLabel lblPaisResidencia = new JLabel("Pais de Residencia");
		lblPaisResidencia.setBounds(7, 470, 112, 14);
		panel.add(lblPaisResidencia);
		
		JLabel lblNumDoc = new JLabel("N\u00BADocumento");
		lblNumDoc.setBounds(7, 523, 112, 14);
		panel.add(lblNumDoc);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(7, 548, 112, 14);
		panel.add(lblMail);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setHorizontalAlignment(SwingConstants.CENTER);
		lblTelfono.setBounds(235, 523, 71, 14);
		panel.add(lblTelfono);
		
		
		//campos de cliente
		txtNombre = new JTextField();
		txtNombre.setEditable(false);
		txtNombre.setBounds(149, 417, 253, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setEditable(false);
		txtApellido.setColumns(10);
		txtApellido.setBounds(149, 442, 253, 20);
		panel.add(txtApellido);
		
		paisResidencia = new ComboBoxBoleanos();
		paisResidencia.setEnabled(false);
		paisResidencia.setBounds(149, 467, 253, 20);
		panel.add(paisResidencia);
		
		tipoDoc = new ComboBoxBoleanos();
		tipoDoc.setEnabled(false);
		tipoDoc.setBounds(149, 492, 253, 20);
		panel.add(tipoDoc);
		
		txtNumDoc = new JTextField();
		txtNumDoc.setEditable(false);
		txtNumDoc.setColumns(10);
		txtNumDoc.setBounds(149, 520, 76, 20);
		panel.add(txtNumDoc);
		
		txtMail = new JTextField();
		txtMail.setEditable(false);
		txtMail.setColumns(10);
		txtMail.setBounds(149, 545, 253, 20);
		panel.add(txtMail);
		
		txtTelefono = new JTextField();
		txtTelefono.setEditable(false);
		txtTelefono.setBounds(316, 520, 86, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		btnSeleccionarCliente = new JButton("Seleccionar Cliente");
		btnSeleccionarCliente.setBounds(7, 573, 395, 23);
		panel.add(btnSeleccionarCliente);
		
		
		//etiquetas del auto
		JLabel lblDatosDelAuto = new JLabel("Datos del Auto");
		lblDatosDelAuto.setBounds(431, 11, 112, 14);
		panel.add(lblDatosDelAuto);
		
		JSeparator separador_auto = new JSeparator();
		separador_auto.setBounds(431, 23, 375, 2);
		panel.add(separador_auto);
		
		JLabel lblPaisPatente = new JLabel("Pais de la patente");
		lblPaisPatente.setBounds(431, 37, 112, 14);
		panel.add(lblPaisPatente);
		
		JLabel lblPatente = new JLabel("Patente");
		lblPatente.setBounds(431, 64, 112, 14);
		panel.add(lblPatente);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(431, 89, 112, 14);
		panel.add(lblMarca);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(431, 117, 112, 14);
		panel.add(lblModelo);
		
		JLabel lblYear = new JLabel("Año");
		lblYear.setBounds(431, 142, 112, 14);
		panel.add(lblYear);

		
		//campos del auto
		cmbPaisPatente = new ComboBoxBoleanos();
		cmbPaisPatente.setBounds(553, 33, 253, 20);
		panel.add(cmbPaisPatente);
		
		txtPatente = new JTextField();
		txtPatente.setColumns(10);
		txtPatente.setBounds(553, 61, 253, 20);
		panel.add(txtPatente);
		
		cmbMarca = new ComboBoxBoleanos();
		cmbMarca.setBounds(553, 86, 253, 20);
		panel.add(cmbMarca);
				
		cmbModelo = new ComboBoxBoleanos();
		cmbModelo.setBounds(553, 114, 253, 20);
		panel.add(cmbModelo);		
				
		txtYear = new JTextField();
		txtYear.setColumns(10);
		txtYear.setBounds(553, 139, 253, 20);
		panel.add(txtYear);					
	
		
		//boton
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(706, 573, 101, 23);
		panel.add(btnGuardar);
		
		chckbxPintura = new JCheckBox("Pintura");
		chckbxPintura.setBounds(129, 166, 95, 23);
		panel.add(chckbxPintura);
		
		chckbxMecanica = new JCheckBox("Mecánica");
		chckbxMecanica.setBounds(129, 195, 95, 23);
		panel.add(chckbxMecanica);
		
		chckbxElectronica_electricidad = new JCheckBox("Electrónica/Electricidad");
		chckbxElectronica_electricidad.setBounds(226, 166, 176, 23);
		panel.add(chckbxElectronica_electricidad);
		
		chckbxEsteticaYAccesorios = new JCheckBox("Estética y Accesorios");
		chckbxEsteticaYAccesorios.setBounds(226, 195, 176, 23);
		panel.add(chckbxEsteticaYAccesorios);
		
		JLabel lblMotivo = new JLabel("Motivo/s");
		lblMotivo.setBounds(7, 167, 112, 14);
		panel.add(lblMotivo);
		
		JLabel lblGarantia = new JLabel("Garantía");
		lblGarantia.setBounds(7, 225, 112, 14);
		panel.add(lblGarantia);
		
		chckbxGarantia = new JCheckBox("Tiene Garantía");
		chckbxGarantia.setBounds(129, 221, 121, 23);
		panel.add(chckbxGarantia);
		
		lblTipoService = new JLabel("Tipo de Service");
		lblTipoService.setBounds(7, 260, 112, 14);
		panel.add(lblTipoService);
		lblTipoService.setVisible(false);
				
		rdbtn15MKm = new JRadioButton("15.000 Km");
		rdbtn15MKm.setBounds(129, 256, 132, 23);
		panel.add(rdbtn15MKm);
		rdbtn15MKm.setVisible(false);
		
		rdbtn30MKm = new JRadioButton("30.000 Km");
		rdbtn30MKm.setBounds(129, 282, 143, 23);
		panel.add(rdbtn30MKm);
		rdbtn30MKm.setVisible(false);
		
		rdbtn45MKm = new JRadioButton("45.000 Km");
		rdbtn45MKm.setBounds(263, 256, 143, 23);
		panel.add(rdbtn45MKm);
		rdbtn45MKm.setVisible(false);
		
		rdbtnGroupTipoGarantia = new ButtonGroup();
		
		rdbtnGroupTipoGarantia.add(rdbtn15MKm);
		rdbtnGroupTipoGarantia.add(rdbtn30MKm);
		rdbtnGroupTipoGarantia.add(rdbtn45MKm);
		
		
		this.setVisible(false);
	}
	
	//hace visible la ventana
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void llenarEstados(List<EstadoReservaDTO> estados)
	{
		try{  
			this.cmbEstado.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbEstado.removeAllItems();
            this.cmbEstado.setModel(cmb);
            for(EstadoReservaDTO estado: estados){
                cmb.addElement(estado);
            }
            this.cmbEstado.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los estados","Error",0);
        }
	}
	
	public void llenarPaisesResidencia(List<PaisDTO> paises)
	{
		try{  
			this.paisResidencia.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.paisResidencia.removeAllItems();
            this.paisResidencia.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.paisResidencia.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarTiposDeDocumentos(List<TipoDocumentoDTO> tiposDeDocumentos)
	{
		try{    
			this.tipoDoc.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.tipoDoc.removeAllItems();
            this.tipoDoc.setModel(cmb);
            for(TipoDocumentoDTO tiposDeDocumento: tiposDeDocumentos){
                cmb.addElement(tiposDeDocumento);
            }
            this.tipoDoc.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con tipo de documentos","Error",0);
        }
	}
	
	public void llenarPaisesPatente(List<PaisDTO> paises)
	{
		try{    
			this.cmbPaisPatente.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbPaisPatente.removeAllItems();
            this.cmbPaisPatente.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.cmbPaisPatente.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises de las patentes","Error",0);
        }
	}
	
	public void llenarMarcas(List<MarcaDTO> marcas)
	{
		try{    
			this.cmbMarca.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbMarca.removeAllItems();
            this.cmbMarca.setModel(cmb);
            for(MarcaDTO marca: marcas){
                cmb.addElement(marca);
            }
            this.cmbMarca.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las marcas","Error",0);
        }
	}
	
	public void llenarModelos(List<ModeloDTO> modelos)
	{
		try{    
			this.cmbModelo.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbModelo.removeAllItems();
            this.cmbModelo.setModel(cmb);
            for(ModeloDTO modelo: modelos){
                cmb.addElement(modelo);
            }
            this.cmbModelo.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los modelos","Error",0);
        }
	}
	
	private void llenarHorarios()
	{
		try{    
			String[] horas = {"08:00","08:30","09:00","09:30","10:00","10:30","11:00","11:30"
								,"12:00","12:30","13:00","13:30","14:00","14:30","15:00","15:30"
								,"16:00","16:30","17:00","17:30","18:00","18:30","19:00"};
			this.horario.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.horario.removeAllItems();
            this.horario.setModel(cmb);
            for(String hora: horas){
                cmb.addElement(hora);
            }
            this.horario.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los horarios","Error",0);
        }
	}
	
	public int getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}
	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	public JDateChooser getFechaEmitida(){
		return fechaEmitida;
	}
	
	public JDateChooser getFechaTurno(){
		return fechaTurno;
	}
	
	public void setFechaTurno(Date date)
	{
		this.fechaTurno.setDate(date);
	}
	
	public ComboBoxBoleanos getCmbHorario(){
		return horario;
	}
	
	public JRadioButton getRdbtnSi() {
		return rdbtnSi;
	}

	public JRadioButton getRdbtnNo() {
		return rdbtnNo;
	}

	public ComboBoxBoleanos getCmbEstado() {
		return cmbEstado;
	}

	public JTextArea getTxtMotivoCancelacion() {
		return txtMotivoCancelacion;
	}

	public void setMotivo(String txtMotivo) {
		this.txtMotivoCancelacion.setText(txtMotivo);
	}

	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setNombre(String Nombre) {
		this.txtNombre.setText(Nombre);
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setApellido(String Apellido) {
		this.txtApellido.setText(Apellido);
	}
	
	public ComboBoxBoleanos getCmbPaisResidencia() {
		return paisResidencia;
	}
	
	public void setCmbPaisResidencia(String objeto) {
		paisResidencia.setSelectedItem(objeto);
	}

	public ComboBoxBoleanos getCmbTipoDoc() {
		return tipoDoc;
	}
	
	public void setCmbTipoDoc(String objeto) {
		tipoDoc.setSelectedItem(objeto);
	}

	public JTextField getTxtNumDoc() {
		return txtNumDoc;
	}

	public void setNumDoc(String NumDoc) {
		this.txtNumDoc.setText(NumDoc);
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public void setMail(String Mail) {
		this.txtMail.setText(Mail);;
	}
	
	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public void setTelefono(String Telefono) {
		this.txtTelefono.setText(Telefono);
	}
	
	public JButton getBtnSeleccionarCliente(){
		return btnSeleccionarCliente;
	}
	
	public ComboBoxBoleanos getCmbPaisPatente() {
		return cmbPaisPatente;
	}

	public JTextField getTxtPatente() {
		return txtPatente;
	}

	public void setPatente(String txtPatente) {
		this.txtPatente.setText(txtPatente);
	}

	public ComboBoxBoleanos getCmbMarca() {
		return cmbMarca;
	}

	public ComboBoxBoleanos getCmbModelo() {
		return cmbModelo;
	}

	public JTextField getTxtYear() {
		return txtYear;
	}

	public void setYear(String txtYear) {
		this.txtYear.setText(txtYear);
	}

	public JButton getBtnGuardar() 
	{
		return btnGuardar;
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}	
	
	public int getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(int idMotivo) {
		this.idMotivo = idMotivo;
	}

	public JCheckBox getChckbxGarantia() {
		return chckbxGarantia;
	}

	public JCheckBox getChckbxPintura() {
		return chckbxPintura;
	}

	public JCheckBox getChckbxMecanica() {
		return chckbxMecanica;
	}

	public JCheckBox getChckbxElectronica_electricidad() {
		return chckbxElectronica_electricidad;
	}

	public JCheckBox getChckbxEsteticaYAccesorios() {
		return chckbxEsteticaYAccesorios;
	}

	public ButtonGroup getRdbtngroup() {
		return rdbtngroup;
	}

	public JRadioButton getRdbtn15MKm() {
		return rdbtn15MKm;
	}

	public JRadioButton getRdbtn30MKm() {
		return rdbtn30MKm;
	}

	public JRadioButton getRdbtn45MKm() {
		return rdbtn45MKm;
	}

	public ButtonGroup getRdbtnGroupTipoGarantia() {
		return rdbtnGroupTipoGarantia;
	}

	public JLabel getLblTipoService() {
		return lblTipoService;
	}

	public void errorAlIngregarNuevaReserva(){
		JOptionPane.showMessageDialog(null,  "No se pudo ingresar la reserva.","Falla al intentar ingresar una reserva",2);
	}
	
	public void errorAlIngregarModificacionEnReserva(){
		JOptionPane.showMessageDialog(null,  "No se pudo modificar la reserva.","Falla al intentar modificar una reserva",2);
	}
	
	public void advetirFallaCargarFechaTurno(){
		JOptionPane.showMessageDialog(null,  "No se pudo cargar la fecha del turno.","Falla al intentar cargar una fecha",2);
	}
	
	public void advertirFaltaCargarCliente(){
		JOptionPane.showMessageDialog(null,  "Por favor seleccione un Cliente.","Falla al intentar cargar una reserva",2);
	}
	
	public void advertirFaltaPais(){
		JOptionPane.showMessageDialog(null,  "Por favor seleccione un país.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaPatente(){
		JOptionPane.showMessageDialog(null,  "Por favor ingrese la patente del auto.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaMarca() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione una marca.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaModelo() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione un modelo.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaYear() {
		JOptionPane.showMessageDialog(null,  "Por favor ingrese un año.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaFecha() {
		JOptionPane.showMessageDialog(null,  "Por favor ingrese una fecha para el turno.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaHorario() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione un horario para el turno.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaEstado() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione un estado.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirFaltaMotivo() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione al menos un motivo de reserva.","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirPatenteInvalida() {
		JOptionPane.showMessageDialog(null,  "Por favor ingrese número de patente válido para el país seleccionado","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirYearInvalido() {
		JOptionPane.showMessageDialog(null,  "Por favor ingrese un año mayor o igual a 2000 y menor o igual al año actual","Falla al intentar ingresar/modificar una reserva",2);
	}
	
	public void advertirDiferenciaPaisResidenciaConPaisPatente(){
		JOptionPane.showMessageDialog(null,  "El pais de residencia debe coincidir con el pais de la patente o en su defecto presentar un pasaporte.","Falla al intentar realizar una reserva",2);
	}
	public void advertirFaltaTipoDeGarantia() {
		JOptionPane.showMessageDialog(null,  "Si la reserva tiene garantía, debe especificar el tipo de la misma.","Falla al intentar ingresar/modificar una reserva",2);		
	}

	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtMotivoCancelacion.setText(null);
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.txtNumDoc.setText(null);
		this.txtMail.setText(null);
		this.txtTelefono.setText(null);
		this.txtPatente.setText(null);
		this.txtYear.setText(null);
		this.ventanaListaDeReservas.setEnabled(true);
		this.fechaTurno.setDate(null);
		this.horario.setSelectedIndex(0);
		this.getChckbxElectronica_electricidad().setSelected(false);
		this.getChckbxEsteticaYAccesorios().setSelected(false);
		this.getChckbxPintura().setSelected(false);
		this.getChckbxMecanica().setSelected(false);
		this.chckbxGarantia.setSelected(false);
		this.cmbMarca.setSelectedIndex(0);
		this.cmbPaisPatente.setSelectedIndex(0);
		this.txtPatente.setText(null);
		this.rdbtnGroupTipoGarantia.clearSelection();
		this.dispose();
	}

	public void setVentanaBloqueada(ListaDeReservas ventanaListaDeReservas) {
		this.ventanaListaDeReservas = ventanaListaDeReservas;
		
	}

	public void mostrarTiposGarantia() {
		this.lblTipoService.setVisible(true);
		this.rdbtn15MKm.setVisible(true);
		this.rdbtn30MKm.setVisible(true);
		this.getRdbtn45MKm().setVisible(true);
		
	}

	public void ocultarTiposGarantia() {
		this.rdbtnGroupTipoGarantia.clearSelection();
		this.lblTipoService.setVisible(false);
		this.rdbtn15MKm.setVisible(false);
		this.rdbtn30MKm.setVisible(false);
		this.getRdbtn45MKm().setVisible(false);
		
	}

}