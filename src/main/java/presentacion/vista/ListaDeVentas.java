package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import dto.UsuarioDTO;
import dto.VentaDTO;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Dimension;

public class ListaDeVentas extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static ListaDeVentas INSTANCE;
	
	private JTable tablaVentas;
	private DefaultTableModel modelVentas;
	private  String[] nombreColumnas = {"Nº de Factura","Fecha","Cliente","Tipo Documento","Nro Documento","Opcion de Pago", "Medio de Pago", "Nro Tarjeta", "Monto", "Estado","Sucursal"};
	
	private JButton btnNotificarPago;
	private JButton btnCancelar;
	private JButton btnVolver;
	private JButton btnGenerarFactura;
	private JPanel panelsuperior;
	private JLabel lblBuscar;
	private JRadioButton rdbtnNombre;
	private JRadioButton rdbtnApellido;
	private JRadioButton rdbtnNroDocumento;
	private JRadioButton rdbtnFecha;
	private JRadioButton rdbtnEstado;
	private JRadioButton rdbtnSucursal;
	private ButtonGroup rdbtnGroup;
	private JTextField txtBusqueda;
	private JButton btnAgregarFiltro;
	private JButton btnDeshacerFiltros;
	
	public static ListaDeVentas getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new ListaDeVentas(); 	
			return new ListaDeVentas();
		}
		else
			return INSTANCE;
	}
	
	public ListaDeVentas() {
		setMinimumSize(new Dimension(902, 182));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 600);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setTitle("Lista de Ventas");
		getContentPane().setLayout(new BorderLayout(0, 0));
		setLocationRelativeTo(null);
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		modelVentas = new DefaultTableModel(null,nombreColumnas);
		tablaVentas = new JTable(modelVentas);
		tablaVentas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaVentas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPane.setViewportView(tablaVentas);
		
		JPanel panelInferior = new JPanel();
		getContentPane().add(panelInferior, BorderLayout.SOUTH);
		
		btnNotificarPago = new JButton("Notificar Pago");
		panelInferior.add(btnNotificarPago);
		
		btnCancelar = new JButton("Cancelar Pago\r\n");
		panelInferior.add(btnCancelar);
		
		btnGenerarFactura = new JButton("Generar Factura");
		panelInferior.add(btnGenerarFactura);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelInferior.add(btnVolver);
		
		panelsuperior = new JPanel();
		getContentPane().add(panelsuperior, BorderLayout.NORTH);
		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("214px:grow"),
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblBuscar = new JLabel("Buscar por:");
		panelsuperior.add(lblBuscar, "1, 1, center, default");
		
		rdbtnNombre = new JRadioButton("Nombre del Cliente");
		panelsuperior.add(rdbtnNombre, "2, 1, center, center");
		
		rdbtnApellido = new JRadioButton("Apellido del Cliente");
		panelsuperior.add(rdbtnApellido, "3, 1, center, center");
		
		rdbtnNroDocumento = new JRadioButton("Nro Documento");
		panelsuperior.add(rdbtnNroDocumento, "4, 1, center, center");
		
		rdbtnFecha = new JRadioButton("Fecha");
		panelsuperior.add(rdbtnFecha, "5, 1, center, center");
		
		rdbtnEstado = new JRadioButton("Estado");
		panelsuperior.add(rdbtnEstado, "6, 1, center, center");
		
		rdbtnSucursal = new JRadioButton("Sucursal");
		panelsuperior.add(rdbtnSucursal, "7, 1, default, center");
		
		txtBusqueda = new JTextField();
		panelsuperior.add(txtBusqueda, "1, 2");
		txtBusqueda.setColumns(10);
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		panelsuperior.add(btnAgregarFiltro, "2, 2, center, center");
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		panelsuperior.add(btnDeshacerFiltros, "3, 2, center, center");
		
		rdbtnGroup = new ButtonGroup();
		
		rdbtnGroup.add(rdbtnApellido);
		rdbtnGroup.add(rdbtnNombre);
		rdbtnGroup.add(rdbtnEstado);
		rdbtnGroup.add(rdbtnFecha);
		rdbtnGroup.add(rdbtnNroDocumento);
		rdbtnGroup.add(rdbtnSucursal);
		
	}
	
	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaVentas();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }
	
	public void llenarTabla(List<VentaDTO> ventasEnTabla) {
		if(ventasEnTabla.size()>0)tablaVentas.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		else tablaVentas.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.getModelVentas().setRowCount(0);
		this.getModelVentas().setColumnCount(0);
		this.getModelVentas().setColumnIdentifiers(this.getNombreColumnas());

		for (VentaDTO v : ventasEnTabla)
		{
			int idVenta = v.getId();
			String fecha = v.getFecha();
			String cliente = v.getCliente().getNombre() + " " + v.getCliente().getApellido();
			String tipoDoc = v.getCliente().getTipoDocumento().getTipoDocumento();
			String numDoc = v.getCliente().getNumDocumento();
			String opcionPago = v.getOpcionPago().getOpcion();
			String medioPago = v.getOpcionPago().getMedioPago().getNombre();
			String nroTarjeta;
			if(v.getTarjeta()!=null) {
			nroTarjeta = v.getTarjeta().getNumero();
			} else {
				nroTarjeta = null;
			}
			String monto = Double.toString(v.getMonto());
			String estado = v.getEstado().getEstado();
			String sucursal = v.getSucursal().getNombre();
					
			Object[] fila = {"0000"+idVenta,fecha,cliente,tipoDoc,numDoc,opcionPago,medioPago,nroTarjeta,monto,estado,sucursal};
			this.getModelVentas().addRow(fila);
			this.redefinirDimensionTabla();
		}
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void cerrar() {
		this.dispose();
	}

	public JTable getTablaVentas() {
		return tablaVentas;
	}

	public DefaultTableModel getModelVentas() {
		return modelVentas;
	}

	public JButton getBtnNotificarPago() {
		return btnNotificarPago;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JButton getBtnGenerarFactura() {
		return btnGenerarFactura;
	}

	public JRadioButton getRdbtnNombre() {
		return rdbtnNombre;
	}

	public JRadioButton getRdbtnApellido() {
		return rdbtnApellido;
	}

	public JRadioButton getRdbtnNroDocumento() {
		return rdbtnNroDocumento;
	}

	public JRadioButton getRdbtnFecha() {
		return rdbtnFecha;
	}

	public JRadioButton getRdbtnEstado() {
		return rdbtnEstado;
	}

	public JRadioButton getRdbtnSucursal() {
		return rdbtnSucursal;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}

	public void advertenciaEstado() {
		JOptionPane.showMessageDialog(null,  "Solo puede realizar esta funcion sobre una venta cuyo estado sea 'Realizada'","Seleccion Incorrecta",1);		
		
	}

	public void errorSeleccion() {
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla. \nPara notificar o cancelar un pago debe seleccionar un elemento de la tabla.","Seleccion Incorrecta",1);		
		
	}

	public void mensajeNotificacionExitosa() {
		JOptionPane.showMessageDialog(null,  "Se ha notificado el pago corectamente","",1);
		
	}

	public void errorNotificar() {
			JOptionPane.showMessageDialog(null,  "No se pudo notificar el pago.","Falla al intentar notificar el pago",0);		
	}

	public void errorCancelar() {
		JOptionPane.showMessageDialog(null,  "No se pudo cancelar el pago.","Falla al intentar cancelar el pago",0);		
		
	}

	public void mensajeCancelacionExitosa() {
		JOptionPane.showMessageDialog(null,  "Se ha cancelado el pago corectamente","",1);
		
	}
	
	public void advertirUsuarioTallerNoPermiteGenerarFactura(){
		JOptionPane.showMessageDialog(null,  "Un usuario de taller no puede generar el reporte de una venta que no es de mantenimiento","Falla al intentar generar una factura",2);
	}
	
	public void advertirUsuarioNoPerteneceASucursalGenerarFactura(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede generar una factura de otra sucursal","Falla al intentar generar una factura",2);
	}
	
	public void advertirUsuarioNoAutorizadoCancelarPago(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede cancelar el pago de una venta","Falla al intentar cancelar un pago",2);
	}
	
	public void advertirUsuarioNoPerteneceASucursalCancelarPago(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede cancelar el pago de una venta de otra sucursal","Falla al intentar cancelar un pago",2);
	}
	
	public void advertirUsuarioNoAutorizadoNotificarPago(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede notificar el pago de una venta","Falla al intentar notificar un pago",2);
	}
	
	public void advertirUsuarioNoPerteneceASucursalNotificarPago(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede notificar el pago de una venta de otra sucursal","Falla al intentar notificar un pago",2);
	}
	
	}
	
	

