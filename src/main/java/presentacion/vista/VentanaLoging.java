package presentacion.vista;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


import java.awt.Color;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Image;

public class VentanaLoging extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	

	private JButton btnConfirmar;
	private static VentanaLoging INSTANCE;
	private JTextField txtUsuario;
	private JPasswordField txtPassword;
	
	public static VentanaLoging getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaLoging(); 	
			return new VentanaLoging();
		}
		else
			return INSTANCE;
	}

	private VentanaLoging() 
	{
		setTitle("Iniciar Sesion");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 524, 193);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setResizable(false);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 489, 132);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblUsuario.setBounds(40, 26, 113, 25);
		panel.add(lblUsuario);

		JLabel lblpassword = new JLabel("Contraseña");
		lblpassword.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblpassword.setBounds(40, 62, 113, 25);
		panel.add(lblpassword);
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(200, 26, 164, 25);
		panel.add(txtUsuario);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(200, 62, 164, 25);
		panel.add(txtPassword);
	
		
		//btn
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(374, 102, 105, 23);
		panel.add(btnConfirmar);
		
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public JTextField getTxtUsuario() 
	{
		return txtUsuario;
	}

	public JPasswordField getTxtPassword() {
		return txtPassword;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmar;
	}
	
	public void errorLoging() {
		JOptionPane.showMessageDialog(null, "No se ha podido iniciar sesion, por favor revise que el usuario y la contraseña sean correctos",
			      "Error al inciar sesion", JOptionPane.WARNING_MESSAGE);
	}
	
	public void cerrar()
	{
		this.txtUsuario.setText(null);
		this.txtPassword.setText(null);
		this.dispose();
	}
}