package presentacion.vista;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.BorderLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.FlowLayout;

public class ListaDeAutomoviles extends JFrame{

	private static final long serialVersionUID = 1L;
	private static ListaDeAutomoviles INSTANCE;
	private JTable tablaAutomoviles;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnConfigurarMarcas;
	private JButton btnConfigurarModelos;
	private JLabel lblSeleccioneMarca;
	private ComboBoxBoleanos comboBoxListaMarcas;
	private JLabel lblSeleccioneModelo;
	private ComboBoxBoleanos comboBoxListaModelos;
	private ComboBoxBoleanos comboBoxListaSucursales;
	private ComboBoxBoleanos comboBoxListaYear;
	private JCheckBox chckbx0Km;
	private DefaultTableModel modelAutomovil;
	private  String[] nombreColumnas = {"Año", "Color", "Puertas","Kilometros", "Stock Mínimo", "Stock Actual", "Sucursal", "Precio"};
	private List<StockAutomovilDTO> autosACargar;
	private JFileChooser buscadorDeArchivos;
	private boolean modoSeleccion;
	private int tipoSeleccion;
	//------
	private boolean llamadoPorVentas;
	private VentanaVentas ventanaVentasLlamadora;
	
	private String[] arrayYear = new String[Calendar.getInstance().get(Calendar.YEAR)-2000+2];
	private JPanel panel_1;
	private JButton btnIngresarStockPorCSV;
	private JButton btnVolver;
	
	
	public static ListaDeAutomoviles getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new ListaDeAutomoviles(); 	
			return new ListaDeAutomoviles();
		}
		else
			return INSTANCE;
	}
	
	
	
	public ListaDeAutomoviles() {
		setMinimumSize(new Dimension(526, 290));
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(100, 100, 465, 494);
		this.autosACargar = new ArrayList<StockAutomovilDTO>();
		
//		setResizable(false);
		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		setLocationRelativeTo(null);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        cerrar();
		    }
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel2 = new JPanel();
		panel.add(panel2, BorderLayout.CENTER);
		
		panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel5 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel5.getLayout();
		flowLayout.setHgap(70);
		panel_1.add(panel5, BorderLayout.SOUTH);
		
		btnIngresarStockPorCSV = new JButton("Ingresar Stock Por CSV");
		panel5.add(btnIngresarStockPorCSV);
		
		btnVolver = new JButton("Volver");
		panel5.add(btnVolver);
		
		JPanel panel3 = new JPanel();
		panel_1.add(panel3, BorderLayout.CENTER);
		
		JPanel panel4 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel4.getLayout();
		flowLayout_1.setHgap(50);
		panel.add(panel4, BorderLayout.SOUTH);
		modelAutomovil= new DefaultTableModel(null,nombreColumnas){
			@Override 
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		panel3.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("max(6dlu;default)"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("122px"),
				ColumnSpec.decode("max(33dlu;pref):grow"),
				ColumnSpec.decode("max(33dlu;default)"),
				ColumnSpec.decode("20dlu"),
				ColumnSpec.decode("51dlu"),
				ColumnSpec.decode("right:max(41dlu;default):grow"),
				ColumnSpec.decode("4dlu:grow"),
				ColumnSpec.decode("default:grow"),
				ColumnSpec.decode("default:grow"),
				ColumnSpec.decode("default:grow"),
				ColumnSpec.decode("30px"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("21px"),
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("22px"),
				FormSpecs.LINE_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		lblSeleccioneMarca = new JLabel("Seleccione Marca");
		lblSeleccioneMarca.setBounds(10, 15, 130, 14);
		panel3.add(lblSeleccioneMarca, "3, 2, left, fill");
		
		comboBoxListaMarcas = new ComboBoxBoleanos();
		comboBoxListaMarcas.setBounds(150, 11, 228, 23);
		panel3.add(comboBoxListaMarcas, "4, 2, 9, 1, fill, fill");
		
		ImageIcon imagenConfig = new ImageIcon(ListaDeAutomoviles.class.getResource("/iconos/config.png"));
		imagenConfig = new ImageIcon(imagenConfig.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		btnConfigurarMarcas = new JButton("");
		btnConfigurarMarcas.setIcon(imagenConfig);
		btnConfigurarMarcas.setBounds(388, 11, 40, 23);
		panel3.add(btnConfigurarMarcas, "13, 2, left, center");
		
		lblSeleccioneModelo = new JLabel("Seleccione Modelo");
		lblSeleccioneModelo.setBounds(10, 49, 130, 14);
		panel3.add(lblSeleccioneModelo, "3, 4, left, fill");
		
		comboBoxListaModelos = new ComboBoxBoleanos();
		comboBoxListaModelos.setBounds(150, 45, 228, 23);
		panel3.add(comboBoxListaModelos, "4, 4, 9, 1, fill, fill");
		
		btnConfigurarModelos = new JButton("");
		btnConfigurarModelos.setIcon(imagenConfig);
		btnConfigurarModelos.setBounds(388, 45, 40, 23);
		panel3.add(btnConfigurarModelos, "13, 4, left, center");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		panel2.setLayout(new BorderLayout(0, 0));
		
		
		
		JScrollPane spAutomoviles = new JScrollPane();
		spAutomoviles.setToolTipText("");
		spAutomoviles.setBounds(10, 234, 418, 175);
		panel2.add(spAutomoviles);
		
		//modelAutomovil = new DefaultTableModel(null,nombreColumnas);
		tablaAutomoviles = new JTable(modelAutomovil);
		tablaAutomoviles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaAutomoviles.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaAutomoviles.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaAutomoviles.getColumnModel().getColumn(0).setResizable(false);
		spAutomoviles.setViewportView(tablaAutomoviles);
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				if(getWidth()<617){
					tablaAutomoviles.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				}else tablaAutomoviles.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			}
		});
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(10, 420, 89, 23);
		panel4.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(175, 420, 89, 23);
		panel4.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(339, 420, 89, 23);
		panel4.add(btnBorrar);
		
		JLabel lblSeleccioneSucursal = new JLabel("Seleccione Sucursal");
		lblSeleccioneSucursal.setBounds(10, 83, 130, 14);
		panel3.add(lblSeleccioneSucursal, "3, 6, left, fill");
		
		comboBoxListaSucursales = new ComboBoxBoleanos();
		comboBoxListaSucursales.setBounds(150, 79, 228, 23);
		panel3.add(comboBoxListaSucursales, "4, 6, 9, 1, fill, fill");
		
		JLabel lblSeleccioneYear = new JLabel("Seleccione Año");
		lblSeleccioneYear.setBounds(10, 117, 130, 14);
		panel3.add(lblSeleccioneYear, "3, 8, left, fill");
		
		chckbx0Km = new JCheckBox("0 Kilometros");
		chckbx0Km.setBounds(10, 148, 130, 23);
		panel3.add(chckbx0Km, "3, 10, left, fill");
		
		llenarYear();
		comboBoxListaYear = new ComboBoxBoleanos();
		comboBoxListaYear.setModel(new DefaultComboBoxModel(arrayYear));
		comboBoxListaYear.setBounds(150, 113, 228, 23);
		panel3.add(comboBoxListaYear, "4, 8, 9, 1, fill, fill");
		
		
		
		buscadorDeArchivos = new JFileChooser(); 
        buscadorDeArchivos.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos csv","csv");
        buscadorDeArchivos.setFileFilter(filtro);
		
		
	}

	
	public void llenarMarca(List<MarcaDTO> marcas)
	{
		this.comboBoxListaMarcas.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaMarcas.removeAllItems();
            this.comboBoxListaMarcas.setModel(cmb);
            for(MarcaDTO marca: marcas){
                cmb.addElement(marca);
            }
            this.comboBoxListaMarcas.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las marcas","Error",0);
        }
	}
	
	public void llenarSucursales(List<SucursalDTO> sucursales)
	{
		this.comboBoxListaSucursales.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel();
            this.comboBoxListaSucursales.removeAllItems();
            this.comboBoxListaSucursales.setModel(cmb);
            for(SucursalDTO sucursal: sucursales){
                cmb.addElement(sucursal);
            }
            this.comboBoxListaSucursales.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las sucursales","Error",0);
        }

	}
	
	public void llenarYear() {
		int yearActual = Calendar.getInstance().get(Calendar.YEAR);
		arrayYear[0] = "Todos";
		for(int i = 2000; i<=yearActual; i++) {
			arrayYear[i-2000+1] = Integer.toString(i);
		}
	}	
	public void llenarModelo(List<ModeloDTO> modelos)
	{
		this.comboBoxListaModelos.setLoaded(false);
		this.getModelAutomovil().setRowCount(0);// parche dos arreglar
//        this.comboBoxListaProvincias.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaModelos.removeAllItems();
            this.comboBoxListaModelos.setModel(cmb);
            for(ModeloDTO modelo: modelos){
                cmb.addElement(modelo);
            }
            this.comboBoxListaModelos.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los modelos","Error",0);
        }
	}
	
	public void llenarTablaAutomovil(List<StockAutomovilDTO> listaAutomoviles) {
		this.getModelAutomovil().setRowCount(0); //Para vaciar la tabla
		this.getModelAutomovil().setColumnCount(0);
		this.getModelAutomovil().setColumnIdentifiers(this.getNombreColumnas());
		this.autosACargar = listaAutomoviles;
		
		for (StockAutomovilDTO a : listaAutomoviles)
		{
			String year = a.getYear();
			String color =a.getColor();
			String cantidadPuertas = a.getCantidadPuertas();
			int kilometros = a.getKilometraje();
			String stockMinimo = Integer.toString(a.getStockMinimo());
			String stock = Integer.toString(a.getStock());
			String precio = Double.toString(a.getPrecio());

			Object[] fila = {year,color,cantidadPuertas,kilometros,stockMinimo,stock,a.getSucursal(),precio};
			this.getModelAutomovil().addRow(fila);
			redefinirDimensionTabla();
		}
		
	}
	
	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaAutomoviles();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }

	public JTable getTablaAutomoviles() {
		return tablaAutomoviles;
	}


	public void setTablaAutomoviles(JTable tablaAutomoviles) {
		this.tablaAutomoviles = tablaAutomoviles;
	}


	public JButton getBtnAgregar() {
		return btnAgregar;
	}


	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public void setBtnEditar(JButton btnEditar) {
		this.btnEditar = btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}
	
	public JButton getBtnConfigurarMarcas() {
		return btnConfigurarMarcas;
	}
	
	public JButton getBtnConfigurarModelos() {
		return btnConfigurarModelos;
	}

	public ComboBoxBoleanos getComboBoxListaMarcas() {
		return comboBoxListaMarcas;
	}

	public ComboBoxBoleanos getComboBoxListaModelos() {
		return comboBoxListaModelos;
	}

	public DefaultTableModel getModelAutomovil() {
		return modelAutomovil;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JFileChooser getBuscadorArchivos() {
		return buscadorDeArchivos;
	}
	
	public JButton getBtnIngresarStockPorCSV() {
		return btnIngresarStockPorCSV;
	}

	public void setNombreColumnas(String[] nombreColumnas) {
		this.nombreColumnas = nombreColumnas;
	}
	
	public ComboBoxBoleanos getComboBoxListaSucursales() {
		return comboBoxListaSucursales;
	}

	public ComboBoxBoleanos getComboBoxListaYear() {
		return comboBoxListaYear;
	}

	public JCheckBox getChckbx0Km() {
		return chckbx0Km;
	}

	public void mostrarVentana()
	{
		this.setVisible(true);
	}

	public List<StockAutomovilDTO> getAutosACargar() {
		return autosACargar;
	}

	public void advertenciaAutomovilesUtilizandose() {
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar el automovil.","Cuidado",2);		
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar por favor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void notificarIngresosCorrectos(List<String> lineasCorrectas){
		if(lineasCorrectas.size()>=1)JOptionPane.showMessageDialog(null,  "Termino la carga del archivo. Las lineas del archivo que se pudieron ingresar correctamente son: \n -Lineas: "+lineasCorrectas.toString(),"Seleccion Incorrecta",1);		
		else JOptionPane.showMessageDialog(null,  "Termino la carga del archivo. No se pudo ingresar ninguna linea correctamente.","Seleccion Incorrecta",1);		
	}
		//--------------------------------------------------------------------------------------------------------------------------------------------------------


	public boolean getModoSeleccion() {
		return modoSeleccion;
	}
	
	public void setModoSeleccion(boolean modoSeleccion) {
		this.modoSeleccion = modoSeleccion;
	}
		
	public int getTipoSeleccion() {
		return tipoSeleccion;
	}
	
	public void setTipoSeleccion(int tipoSeleccion) {
		this.tipoSeleccion = tipoSeleccion;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}
	//------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public boolean isLlamadoPorVentas() {
		return llamadoPorVentas;
	}

	public void setLlamadoPorVentas(boolean llamadoPorVentas) {
		this.llamadoPorVentas = llamadoPorVentas;
	}
	
	public VentanaVentas getVentanaVentasLlamadora() {
		return ventanaVentasLlamadora;
	}

	public void setVentanaVentasLlamadora(VentanaVentas ventanaVentasLlamadora) {
		this.ventanaVentasLlamadora = ventanaVentasLlamadora;
	}

	public void advertirUsuarioNoAutorizadoIngresarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede ingresar automoviles","Falla al intentar ingresar automoviless",2);	
	}
	
	public void advertirUsuarioNoAutorizadoEditarAutomovil(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede editar una automovil","Falla al intentar editar un automovil",2);	
	}

	public void advertirUsuarioNoAutorizadoEliminarAutomovil(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede eliminar un automovil","Falla al intentar eliminar un automovil",2);	
	}
	
	public void advertirUsuarioNoAutorizadoConfigurarMarcas(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede configurar las marcas del sistema","Falla al intentar configurar las marcas del sistema",2);	
	}
	
	public void advertirUsuarioNoAutorizadoConfigurarModelos(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede configurar los modelos del sistema","Falla al intentar configurar los modelos del sistema",2);	
	}	
	
	public void advertirUsuarioNoAutorizadoEditarAutomovilOtraSucursal(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede editar una automovil de otra sucursal","Falla al intentar editar un automovil",2);	
	}

	public void cerrar() {
		if(isLlamadoPorVentas()){
			this.ventanaVentasLlamadora.setEnabled(true);
			this.llamadoPorVentas = false;
		}
		this.dispose();
	}
	
	
}
