package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.GarantiaDTO;
import dto.MarcaDTO;

public class VentanaGarantia extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnSeleccionar;
	private ComboBoxBoleanos cmbGarantia;
	private static VentanaGarantia INSTANCE;


	public static VentanaGarantia getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaGarantia(); 	
			return new VentanaGarantia();
		}
		else
			return INSTANCE;
	}
	

	private VentanaGarantia() {
			
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 297, 133);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 272, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblGarantia = new JLabel("Garantia");
		lblGarantia.setBounds(10, 11, 113, 14);
		panel.add(lblGarantia);
		
		
		cmbGarantia = new ComboBoxBoleanos();
		cmbGarantia.setBounds(69, 11, 190, 20);
		panel.add(cmbGarantia);
		
		
		btnSeleccionar = new JButton("Seleccionar");
		btnSeleccionar.setBounds(158, 53, 101, 23);
		panel.add(btnSeleccionar);
		
		
		
		this.setVisible(false);
	}
	

	public JButton getBtnSeleccionar() {
		return btnSeleccionar;
	}


	public void setBtnSeleccionar(JButton btnSeleccionar) {
		this.btnSeleccionar = btnSeleccionar;
	}


	public ComboBoxBoleanos getCmbGarantia() {
		return cmbGarantia;
	}


	public void setCmbGarantia(ComboBoxBoleanos cmbGarantia) {
		this.cmbGarantia = cmbGarantia;
	}


	public void mostrarVentana() {
		this.setVisible(true);
		
	}
	
	public void llenarGarantias(List<GarantiaDTO> garantias)
	{
		this.cmbGarantia.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel();
            this.cmbGarantia.removeAllItems();
            this.cmbGarantia.setModel(cmb);
            for(GarantiaDTO garantia: garantias){
                cmb.addElement(garantia);
            }
            this.cmbGarantia.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las garantias","Error",0);
        }
	}
	
	public void cerrar()
	{
		this.dispose();
	}

}