package presentacion.vista;


import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.MarcaDTO;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;

public class VentanaModelo extends JFrame{
	private static final long serialVersionUID = 1L;
	private static VentanaModelo INSTANCE;
	private ComboBoxBoleanos listadoMarcas;
	
	//panel
	private JPanel contentPane;
	
	//campos de texto
	private JTextField txtModelo;
	
	//variables auxiliares
	private int tipoVentana;//sirve para indicar si la ventana esta en modo agregar o modificar
	private int idModelo;//sirve para guardar el id del modelo que queremos modificar luego
	
	//botones
	private JButton btnGuardar;

	private ListaDeModelos ventanaListaDeModelos;
	private boolean llamadoPorListaDeModelos;
	
	//devuelve una instancia de este tipo de ventana
	public static VentanaModelo getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VentanaModelo(); 	
			return new VentanaModelo();
		}else return INSTANCE;
	}
	
	private VentanaModelo(){
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 353, 139);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(350, 100, 353, 139));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 347, 110);
		contentPane.add(panel);
		panel.setLayout(null);
		
		//etiquetas
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(17, 14, 59, 14);
		panel.add(lblModelo);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(17, 39, 59, 24);
		panel.add(lblMarca);
		
		
		//campos de textos
		txtModelo = new JTextField();
		txtModelo.setBounds(86, 11, 242, 20);
		panel.add(txtModelo);
		txtModelo.setColumns(10);
		
		//boton
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(227, 75, 101, 23);
		panel.add(btnGuardar);
		
		listadoMarcas = new ComboBoxBoleanos();
		listadoMarcas.setBounds(86, 40, 242, 24);
		panel.add(listadoMarcas);
		
		this.setVisible(false);
	}
	
	public void llenarMarcas(List<MarcaDTO> marcas)
	{
		this.listadoMarcas.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel();
            this.listadoMarcas.removeAllItems();
            this.listadoMarcas.setModel(cmb);
            for(MarcaDTO marca: marcas){
                cmb.addElement(marca);
            }
            this.listadoMarcas.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las marcas","Error",0);
        }
	}
	
	
	
	//hace visible la ventana
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public int getIdModelo() {
		return idModelo;
	}

	public void setIdModelo(int idModelo) {
		this.idModelo = idModelo;
	}

	public JTextField getTxtNombreModelo() 
	{
		return txtModelo;
	}
	
	public void setNombreModelo(String nombre) {
		this.txtModelo.setText(nombre);
	}

	public JButton getBtnGuardar() 
	{
		return btnGuardar;
	}
	
	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}	
	
	public ComboBoxBoleanos getListadoMarcas() {
		return listadoMarcas;
	}


	public void setListadoMarcas(ComboBoxBoleanos listadoMarcas) {
		this.listadoMarcas = listadoMarcas;
	}
	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtModelo.setText(null);
		if(isLlamadoPorListaDeModelos()) {
			this.ventanaListaDeModelos.setEnabled(true);
			this.llamadoPorListaDeModelos = false;
		}
		this.dispose();
	}

	public void advertenciaMarcaNoSeleccionada() {
		JOptionPane.showMessageDialog(null,  "No se esta seleccionando ninguna marca en el cual crear el modelo. \nEsto puede pasar debido a que no existe ninguna marca cargado en el sistema. Cargue un nuevo pais para poder ingresar una provincia","Cuidado",2);
	}

	public void advertenciaFaltaNombreModelo() {
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para el modelo o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);		
	}
	
	public void advertenciaNombreModeloInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el modelo. \nEl formato del nombre es una palabra (agregar formato).","Falla al intentar agregar/modificar el modelo",1);		
	}
	
	public void advertenciaModeloRepetido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el modelo. \nRevise que no este almacenando un modelo ya registrado.","Falla al intentar crear/modificar el modelo",1);		
	}

	public void advertenciaModeloAgregado() {
		JOptionPane.showMessageDialog(null,  "Se agrego el modelo correctamente","Modelo Agregado",2);
	}

	public void setVentanaModeloLlamadora(ListaDeModelos ventanaListaDeModelos) {
		this.ventanaListaDeModelos = ventanaListaDeModelos;
	}

	public boolean isLlamadoPorListaDeModelos() {
		return llamadoPorListaDeModelos;
	}

	public void setLlamadoPorListaDeModelos(boolean llamadoPorListaDeModelos) {
		this.llamadoPorListaDeModelos = llamadoPorListaDeModelos;
	}
}
