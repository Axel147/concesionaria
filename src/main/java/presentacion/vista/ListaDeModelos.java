package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import dto.MarcaDTO;
import dto.ModeloDTO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Dimension;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class ListaDeModelos extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private static ListaDeModelos INSTANCE;
	private JTable tablaModelos;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private ComboBoxBoleanos comboBoxListaMarcas;
	private JLabel lblSeleccioneMarca;
	private DefaultTableModel modelModelos;
	private  String[] nombreColumnas = {"Modelos"};
	private JTextField txtBusqueda;
	private JButton btnBuscar;
	private JButton btnVolver;
	private ListaDeAutomoviles ventanaListaDeAutomoviles;
	private int llamadoDesde;
	private VentanaAutopartes ventanaAutopartes;
	private JPanel panelsuperior;
	private JPanel panelinferior;
	private JPanel panelsuperioralto;
	private JPanel panelsuperiorbajo;
	
	public static ListaDeModelos getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new ListaDeModelos(); 	
			return new ListaDeModelos();
		}
		else
			return INSTANCE;
	}
	
	
	
	public ListaDeModelos() {
		super();
		setMinimumSize(new Dimension(440, 340));
		initialize();
	}

	
	private void initialize() {
		
		setBounds(100, 100, 433, 333);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		/*
		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Personalizar Provincias");
		*/

		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        cerrar();
		    }
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		setTitle("Lista De Modelos");
		panel.setLayout(new BorderLayout(0, 0));
		JScrollPane spModelos = new JScrollPane();
		spModelos.setToolTipText("");
		panel.add(spModelos);
		
		panelinferior = new JPanel();
		getContentPane().add(panelinferior, BorderLayout.SOUTH);
		
		panelsuperior = new JPanel();
		getContentPane().add(panelsuperior, BorderLayout.NORTH);
		panelsuperior.setLayout(new BorderLayout(0, 0));
		
		panelsuperioralto = new JPanel();
		panelsuperior.add(panelsuperioralto, BorderLayout.NORTH);
		
		panelsuperiorbajo = new JPanel();
		panelsuperior.add(panelsuperiorbajo, BorderLayout.SOUTH);

	
		modelModelos = new DefaultTableModel(null,nombreColumnas);
		tablaModelos = new JTable(modelModelos);
		
		tablaModelos.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaModelos.getColumnModel().getColumn(0).setResizable(false);

		spModelos.setViewportView(tablaModelos);
		
		btnAgregar = new JButton("Agregar");
		panelinferior.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		panelinferior.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		panelinferior.add(btnBorrar);
		panelsuperioralto.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("120px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("pref:grow"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("20px"),}));
		
		lblSeleccioneMarca = new JLabel("Seleccione Marca");
		panelsuperioralto.add(lblSeleccioneMarca, "3, 2, left, center");
		
		comboBoxListaMarcas = new ComboBoxBoleanos();
		comboBoxListaMarcas.setMinimumSize(new Dimension(160, 20));
		panelsuperioralto.add(comboBoxListaMarcas, "5, 2");
		panelsuperiorbajo.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("pref:grow"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("65px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("97px"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("30px"),}));
		
		txtBusqueda = new JTextField();
		panelsuperiorbajo.add(txtBusqueda, "3, 2, default, center");
		txtBusqueda.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		panelsuperiorbajo.add(btnBuscar, "5, 2, left, center");
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelsuperiorbajo.add(btnVolver, "7, 2, left, top");
		
		
		
	}

	public void llenarMarca(List<MarcaDTO> marcas)
	{
		this.comboBoxListaMarcas.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.comboBoxListaMarcas.removeAllItems();
            this.comboBoxListaMarcas.setModel(cmb);
            for(MarcaDTO marca: marcas){
                cmb.addElement(marca);
            }
            this.comboBoxListaMarcas.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las marcas","Error",0);
        }
	}
	
	public void llenarTablaModelos(List<ModeloDTO> listaModelos) {
		this.getModelModelos().setRowCount(0); //Para vaciar la tabla
		this.getModelModelos().setColumnCount(0);
		this.getModelModelos().setColumnIdentifiers(this.getNombreColumnas());

		for (ModeloDTO m : listaModelos)
		{
			Object[] fila = {m};
			this.getModelModelos().addRow(fila);
		}
		
	}
	

	public JTable getTablaModelos() {
		return tablaModelos;
	}


	public JButton getBtnAgregar() {
		return btnAgregar;
	}
	

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}
	
	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public JTextField getTxtBusqueda() {
		return this.txtBusqueda;
	}
	
	public ComboBoxBoleanos getComboBoxMarca() {
		return comboBoxListaMarcas;
	}

	public DefaultTableModel getModelModelos() {
		return modelModelos;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
	
	public JButton getBtnVolver() {
		return btnVolver;
	}

		public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertenciaModeloUtilizandose(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar el modelo. \nRevise que no este siendo utilizada por algun automovil. De ser asi debe primero eliminar o modificar todos los automoviles que la esten utilizando antes de eliminar el modelo.","Cuidado",2);		
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public ListaDeAutomoviles getVentanaListaDeAutomoviles() {
		return ventanaListaDeAutomoviles;
	}

	
	public void setVentanaListaDeAutomoviles(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
	}



	public void cerrar() {
		if(this.llamadoDesde == 1) {
			this.ventanaListaDeAutomoviles.setEnabled(true);
		}else if(this.llamadoDesde == 2) {
			this.ventanaAutopartes.setEnabled(true);
		}
		this.dispose();
	}

	public VentanaAutopartes getVentanaAutopartes() {
		return ventanaAutopartes;
	}

	public void setVentanaAutopartes(VentanaAutopartes ventanaAutopartes) {
		this.ventanaAutopartes = ventanaAutopartes;
		
	}

	public void setLlamadoDesde(int llamadoDesde) {
		this.llamadoDesde = llamadoDesde;		
	}
	
	public int getLlamadoDesde() {
		return llamadoDesde;
	}
}
