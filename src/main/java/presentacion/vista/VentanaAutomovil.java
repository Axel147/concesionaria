package presentacion.vista;


import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.StockAutomovilDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class VentanaAutomovil extends JFrame{
	private static final long serialVersionUID = 1L;
	private static VentanaAutomovil INSTANCE;
	//private ComboBoxBoleanos listadoSucursales;
	
	//panel
	private JPanel contentPane;
	private JTextField txtYear;
	private JTextField textStockMinimo;
	private JTextField txtStock;
	private JTextField txtCantidadPuertas;
	private JTextField txtKilometros;
	private JTextField txtColor;
	private JTextField textPrecio;
	private ComboBoxBoleanos cmbMarca;
	private ComboBoxBoleanos cmbModelo;
	private ComboBoxBoleanos cmbSucursal;
	
	//variables auxiliares
	private int tipoVentana;//sirve para indicar si la ventana esta en modo agregar o modificar
	private int idAutomovil;//sirve para guardar el id del automovil que queremos modificar luego
	
	private JButton btnGuardar;
	private JLabel lblSucursal;
	private JLabel lblPrecio;
	
	private ListaDeAutomoviles ventanaListaDeAutomoviles;
	

	//devuelve una instancia de este tipo de ventana
	public static VentanaAutomovil getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VentanaAutomovil(); 	
			return new VentanaAutomovil();
		}else return INSTANCE;
	}
	
	private VentanaAutomovil(){
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 290, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 282, 364);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(17, 14, 46, 14);
		panel.add(lblMarca);
		
		cmbMarca = new ComboBoxBoleanos();
		cmbMarca.setBounds(96, 14, 155, 21);
		panel.add(cmbMarca);
		
		//etiquetas
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(17, 44, 59, 14);
		panel.add(lblModelo);
		
		cmbModelo = new ComboBoxBoleanos();
		cmbModelo.setBounds(96, 44, 155, 22);
		panel.add(cmbModelo);
		
		JLabel lblYear = new JLabel("Año/Modelo");
		lblYear.setBounds(17, 74, 92, 14);
		panel.add(lblYear);
		
		txtYear = new JTextField();
		txtYear.setColumns(10);
		txtYear.setBounds(96, 74, 155, 20);
		panel.add(txtYear);
		
		JLabel lblCantidadPuertas = new JLabel("Puertas");
		lblCantidadPuertas.setBounds(17, 104, 90, 14);
		panel.add(lblCantidadPuertas);
		
		txtCantidadPuertas = new JTextField();
		txtCantidadPuertas.setColumns(10);
		txtCantidadPuertas.setBounds(96, 104, 155, 20);
		panel.add(txtCantidadPuertas);
		
		JLabel lblKilometros = new JLabel("Kilometros");
		lblKilometros.setBounds(17, 135, 69, 14);
		panel.add(lblKilometros);
		
		txtKilometros = new JTextField();
		txtKilometros.setBounds(96, 133, 155, 20);
		panel.add(txtKilometros);
		txtKilometros.setColumns(10);
		
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(17, 164, 59, 21);
		panel.add(lblColor);
		
		txtColor = new JTextField();
		txtColor.setColumns(10);
		txtColor.setBounds(96, 164, 155, 20);
		panel.add(txtColor);
		
		JLabel lblStockMinimo = new JLabel("Stock Mínimo");
		lblStockMinimo.setBounds(17, 194, 80, 21);
		panel.add(lblStockMinimo);
		
		textStockMinimo = new JTextField();
		textStockMinimo.setColumns(10);
		textStockMinimo.setBounds(96, 194, 155, 20);
		panel.add(textStockMinimo);
		
		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(17, 224 , 59, 21);
		panel.add(lblStock);
		
		txtStock = new JTextField();
		txtStock.setColumns(10);
		txtStock.setBounds(96, 224, 155, 20);
		panel.add(txtStock);
		
		lblSucursal = new JLabel("Sucursal");
		lblSucursal.setBounds(17, 254, 59, 21);
		panel.add(lblSucursal);
		
		cmbSucursal = new ComboBoxBoleanos();
		cmbSucursal.setBounds(96, 254, 155, 22);
		panel.add(cmbSucursal);
		
		lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(17, 284, 59, 21);
		panel.add(lblPrecio);
		
		textPrecio = new JTextField();
		textPrecio.setColumns(10);
		textPrecio.setBounds(96, 284, 155, 20);
		panel.add(textPrecio);
		
		//boton
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnGuardar.setBounds(86, 330, 111, 23);
		panel.add(btnGuardar);
		

		
		this.setVisible(false);
	}
	
	//hace visible la ventana
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public int getIdAutomovil() {
		return idAutomovil;
	}

	public void setIdAutomovil(int idAutomovil) {
		this.idAutomovil = idAutomovil;
	}
	
	public JTextField getTxtYear() {
		return txtYear;
	}

	public void setTxtYear(String txtYear) {
		this.txtYear.setText(txtYear);
	}

	public JTextField getTextStockMinimo() {
		return textStockMinimo;
	}

	public void setTextStockMinimo(String textStockMinimo) {
		this.textStockMinimo.setText(textStockMinimo);
	}

	public JTextField getTxtStock() {
		return txtStock;
	}

	public void setTxtStock(String txtStock) {
		this.txtStock.setText(txtStock);
	}

	public JTextField getTxtCantidadPuertas() {
		return txtCantidadPuertas;
	}

	public void setTxtCantidadPuertas(String txtCantidadPuertas) {
		this.txtCantidadPuertas.setText(txtCantidadPuertas);
	}

	public JTextField getTxtKilometros() {
		return txtKilometros;
	}

	public void setTxtKilometros(String txtKilometros) {
		this.txtKilometros.setText(txtKilometros);
	}

	public JTextField getTxtColor() {
		return txtColor;
	}

	public void setTxtColor(String txtColor) {
		this.txtColor.setText(txtColor);
	}

	public ComboBoxBoleanos getCmbMarca() {
		return cmbMarca;
	}

	public ComboBoxBoleanos getCmbModelo() {
		return cmbModelo;
	}

	public ComboBoxBoleanos getCmbSucursal() {
		return cmbSucursal;
	}

	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
	
	public JTextField getTextPrecio() {
		return textPrecio;
	}

	public void setTextPrecio(String textPrecio) {
		this.textPrecio.setText(textPrecio);
	}

	//--------------------------------------------------------------------------
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtYear.setText(null);
		this.txtColor.setText(null);
		this.txtCantidadPuertas.setText(null);
		this.txtStock.setText(null);
		this.textStockMinimo.setText(null);
		this.textPrecio.setText(null);
		this.ventanaListaDeAutomoviles.setEnabled(true);
		this.dispose();
	}
	//---------------------------------------------------------------------------
	public void advertirFaltaAnioAutomovil() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun Año de automovil o el Año esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaColorAutomovil() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun color de automovil o el color esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaCantidadPuertasAutomovil() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ninguna cantidad de puertas de automovil o la cantidad de puertas esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaStockMinimo() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun stock mínimo para el automovil o el stock esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaPrecio() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun precio para el automovil o precio esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirFaltaStockAutomovil() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun stock de automovil o el stock esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}

	public void advertenciaAutomovilRepetidoAgregar() {
		JOptionPane.showMessageDialog(null,"No se a ingresado el automovil ya que se encuentra almacenado. \nNo se ingreso automovil.","Cuidado",2);
		
	}

	public void advertenciaAutomovilRepetidoModificar() {
		JOptionPane.showMessageDialog(null,"No se a modificado el automovil seleccionado.","Cuidado",2);
		
	}
	
	public void advertenciaAnioAutomovilInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato del anio son numeros de cuatro digitos mayor o igual a 2000 y menor o igual al año actual .","Falla al intentar agregar/modificar el automovil",1);		
		
	}
	public void advertenciaColorAutomovilInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato del color es una palabra.","Falla al intentar agregar/modificar el automovil",1);		
		
	}
	public void advertenciaCantidadPuertasAutoInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato de la cantidad de puertas es un caracter numericos de entre 1 y 5.","Falla al intentar agregar/modificar el automovil",1);		
		
	}
	public void advertenciaStockAutomovilInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato del stock  son caracteres numericos de 0 a 99.","Falla al intentar agregar/modificar el automovil",1);		
	}
	
	public void advertenciaPrecioInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato del precio consiste de uno a siete caracteres numéricos.","Falla al intentar agregar/modificar el automovil",1);		
	}
	
	public void llenarSucursales(List<SucursalDTO> sucursales) {
		this.cmbSucursal.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel();
            this.cmbSucursal.removeAllItems();
            this.cmbSucursal.setModel(cmb);
            for(SucursalDTO sucursal: sucursales){
                cmb.addElement(sucursal);
            }
            this.cmbSucursal.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las sucursales","Error",0);
        }
 
	}
	
	
	public void llenarMarca(List<MarcaDTO> marcas)
	{
		 this.cmbMarca.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbMarca.removeAllItems();
            this.cmbMarca.setModel(cmb);
            for(MarcaDTO marca: marcas){
                cmb.addElement(marca);
            }
            this.cmbMarca.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las marcas","",0);
        }
	}
	
	public void llenarModelo(List<ModeloDTO> modelos)
	{
        this.cmbModelo.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbModelo.removeAllItems();
            this.cmbModelo.setModel(cmb);
            for(ModeloDTO modelo : modelos){
                cmb.addElement(modelo);
            }
            this.cmbModelo.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los modelos" ,"Error",0);
        }
	}

	public void advertenciaAutomovilAgregadoCorrectamente() {
		JOptionPane.showMessageDialog(null,  "Se agrego/modifico corectamente","",1);
		
	}

	public void setVentanaBloqueada(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
		
	}

	public ListaDeAutomoviles getVentanaListaBloqueada() {
		return ventanaListaDeAutomoviles;
	}
	
	public void advertirKilometrajeVacio() {
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun kilometraje para el automovil o esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}

	public void advertenciaKilometrajeInvalido() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar/modificar el automovil. \nEl formato del kilometraje son caracteres numericos de entre 1 a 6 digitos.","Falla al intentar agregar/modificar el automovil",1);		
	}
}
