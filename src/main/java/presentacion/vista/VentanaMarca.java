package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class VentanaMarca extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtMarca;
	private JButton btnGuardar;
	private int tipoVentana;
	private static VentanaMarca INSTANCE;
	private int idMarca;
	private ListaDeMarcas ventanaListaDeMarcas;
	private boolean llamadoPorListaDeMarca;
	private boolean llamadoDesdeStock;

	public static VentanaMarca getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaMarca(); 	
			return new VentanaMarca();
		}
		else
			return INSTANCE;
	}
	

	private VentanaMarca() {
	
		
		super();
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 297, 133);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 272, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblMarca = new JLabel("Marca");
		lblMarca.setBounds(10, 11, 113, 14);
		panel.add(lblMarca);
		
		
		txtMarca = new JTextField();
		txtMarca.setBounds(69, 11, 190, 20);
		panel.add(txtMarca);
		txtMarca.setColumns(10);
		
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(158, 53, 101, 23);
		panel.add(btnGuardar);
		
		
		
		this.setVisible(false);
	}
	
	public JTextField getTxtNombre() {
		return txtMarca;
	}

	public void setTxtNombre(String txtNombre) {
		this.txtMarca.setText(txtNombre);
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}


	public int getId() {
		return idMarca;
	}


	public void setId(int id) {
		this.idMarca = id;
	}


	public int getTipoVentana() {
		return tipoVentana;
	}
  

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}


	public void mostrarVentana() {
		this.setVisible(true);
		
	}

	public void advertirFaltaNombreMarca() {
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para la marca o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirNombreMarcaInvalido() {
		JOptionPane.showMessageDialog(null,"El nombre de la marca no es valido el formato corresponde a un minimo de tres letras con un espacio, sin simbolos o numeros, escriba el nombre correctamente.","Cuidado",2);
	}
	
	public void advertenciaMarcaRepetidaAgregar() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la marca. \nRevise que no este queriendo agregar una marca que ya existe.","Falla al intentar agregar la marca",1);	
	}

	public void advertenciaMarcaRepetidaModificar() {
		JOptionPane.showMessageDialog(null,  "No se pudo modificar la marca. \nRevise que no este queriendo modificar con los datos de una marca que ya existe.","Falla al intentar modificar la marca",1);
	}
	
	public void cerrar()
	{
		this.txtMarca.setText(null);
		if(isLlamadoPorListaDeMarca()) {
			this.ventanaListaDeMarcas.setEnabled(true);
			this.llamadoPorListaDeMarca = false;
		}
		this.dispose();
	}


	public void setVentanaMarcaLlamadora(ListaDeMarcas ventanaListaDeMarcas) {
		this.ventanaListaDeMarcas = ventanaListaDeMarcas;		
	}


	public boolean isLlamadoPorListaDeMarca() {
		return llamadoPorListaDeMarca;
	}


	public void setLlamadoPorListaDeMarca(boolean llamadoPorListaDeMarca) {
		this.llamadoPorListaDeMarca = llamadoPorListaDeMarca;
	}


	

}
