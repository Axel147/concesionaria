package presentacion.vista;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.ClienteDTO;
import dto.GarantiaDTO;
import dto.MantenimientoDTO;
import dto.MedioPagoDTO;
import dto.OpcionPagoDTO;
import dto.ProductoDTO;
import dto.StockAutomovilDTO;
import presentacion.vista.ComboBoxBoleanos;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Dimension;
import java.awt.Image;

import javax.swing.JPanel;
import java.awt.CardLayout;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class VentanaVentas extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JTextField txtCliente;
	private JButton btnCargarCliente;
	private ComboBoxBoleanos cmbModoPago;
	private ComboBoxBoleanos cmbAccion;
	private JButton btnCargarAutomovil;
	private JButton btnCargarAutoparte;
	private JButton btnQuitar;
	private JButton btnAceptar;
	private JButton btnVolver;
	private static VentanaVentas INSTANCE;
	private JLabel lblCliente;
	private String[] nombreColumnas = {"Descripcion", "Precio"};
	private DefaultTableModel modelVentas;
	private JTable tablaVentas;
//	private List<AutoparteDTO> carritoAutopartes;
//	private List<StockAutomovilDTO> carritoAutomoviles;
//	private List<ProductoDTO> carritoCompra;
	private List<Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>> carritoCompra;
	private ClienteDTO cliente;
	private JPanel panelPagos;
	private JPanel panelVacio;
	private JPanel panelTarjetas;
	private JTextField txtNumeroTarjeta;
	private JTextField txtNombreTarjeta;
	private JTextField txtCodigoTarjeta;
	private JTextField txtVencimiento;
	private JTextField txtTotalPagar;
	private MantenimientoDTO mantenimiento;
	private boolean ventaMantenimiento;
	
	public static VentanaVentas getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaVentas(); 	
			return new VentanaVentas();
		}
		else
			return INSTANCE;
	}
	
	private VentanaVentas() {
		
		super();
		frame = new JFrame();
		setBounds(100, 100, 626, 536);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		getContentPane().setLayout(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		lblCliente = new JLabel("Cliente");
		lblCliente.setBounds(26, 26, 46, 14);  
		getContentPane().add(lblCliente);
		
		txtCliente= new JTextField();
		txtCliente.setEditable(false);
		txtCliente.setBounds(103, 23, 321, 20);
		getContentPane().add(txtCliente);
		txtCliente.setColumns(10);
		
		btnCargarCliente = new JButton("Añadir Cliente");
		btnCargarCliente.setBounds(434, 22, 121, 23);
		getContentPane().add(btnCargarCliente);
		
		JLabel lblModoPago = new JLabel("Modo Pago");
		lblModoPago.setBounds(26, 63, 67, 14);
		getContentPane().add(lblModoPago);
		
		cmbModoPago = new ComboBoxBoleanos();
		cmbModoPago.setBounds(103, 59, 160, 23);
		getContentPane().add(cmbModoPago);
		
		JLabel lblAccion = new JLabel("Cuotas");
		lblAccion.setBounds(321, 63, 46, 14);
		getContentPane().add(lblAccion);
		
		cmbAccion = new ComboBoxBoleanos();
		cmbAccion.setBounds(377, 59, 178, 23);
		getContentPane().add(cmbAccion);
		
		btnCargarAutomovil = new JButton("Añadir Automovil");
		btnCargarAutomovil.setBounds(45, 216, 131, 23);
		getContentPane().add(btnCargarAutomovil);
		
		btnCargarAutoparte = new JButton("Añadir Autoparte");
		btnCargarAutoparte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCargarAutoparte.setBounds(218, 216, 149, 23);
		getContentPane().add(btnCargarAutoparte);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(136, 463, 131, 23);
		getContentPane().add(btnAceptar);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		btnVolver.setBounds(321, 463, 146, 23);
		getContentPane().add(btnVolver);
		
		JScrollPane spVentas = new JScrollPane();
		spVentas.setToolTipText("");
		spVentas.setBounds(45, 250, 506, 146);
		getContentPane().add(spVentas);
		
		modelVentas = new DefaultTableModel(null,nombreColumnas){
			@Override 
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		tablaVentas = new JTable(modelVentas);
		tablaVentas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tablaVentas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaVentas.getColumnModel().getColumn(0).setResizable(false);
		spVentas.setViewportView(tablaVentas);
		
		panelPagos = new JPanel();
		panelPagos.setBounds(26, 101, 574, 104);
		getContentPane().add(panelPagos);
		panelPagos.setLayout(new CardLayout(0, 0));
		
		panelVacio = new JPanel();
		panelPagos.add(panelVacio, "panelVacio");
		panelVacio.setLayout(null);
		
		panelTarjetas = new JPanel();
		panelPagos.add(panelTarjetas, "panelTarjetas");
		panelTarjetas.setLayout(null);
		
		JLabel lblNumero = new JLabel("Numero de Tarjeta");
		lblNumero.setBounds(20, 11, 140, 14);
		panelTarjetas.add(lblNumero);
		
		txtNumeroTarjeta = new JTextField();
		txtNumeroTarjeta.setBounds(20, 25, 284, 20);
		panelTarjetas.add(txtNumeroTarjeta);
		txtNumeroTarjeta.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nombre en la Tarjeta");
		lblNewLabel.setBounds(20, 58, 178, 14);
		panelTarjetas.add(lblNewLabel);
		
		txtNombreTarjeta = new JTextField();
		txtNombreTarjeta.setBounds(20, 73, 284, 20);
		panelTarjetas.add(txtNombreTarjeta);
		txtNombreTarjeta.setColumns(10);
		
		JLabel lblCodigoTarjeta = new JLabel("Codigo Tarjeta");
		lblCodigoTarjeta.setBounds(350, 11, 145, 14);
		panelTarjetas.add(lblCodigoTarjeta);
		
		txtCodigoTarjeta = new JTextField();
		txtCodigoTarjeta.setBounds(350, 25, 145, 20);
		panelTarjetas.add(txtCodigoTarjeta);
		txtCodigoTarjeta.setColumns(10);
		
		JLabel lblVencimiento = new JLabel("Mes/Año Vencimiento");
		lblVencimiento.setBounds(354, 59, 136, 14);
		panelTarjetas.add(lblVencimiento);
		
		txtVencimiento = new JTextField();
		txtVencimiento.setBounds(350, 73, 145, 20);
		panelTarjetas.add(txtVencimiento);
		txtVencimiento.setColumns(10);
		
		JLabel lblTotalAPagar = new JLabel("Total a pagar");
		lblTotalAPagar.setBounds(345, 410, 79, 14);
		getContentPane().add(lblTotalAPagar);
		
		txtTotalPagar = new JTextField();
		txtTotalPagar.setText("US$ 0");
		txtTotalPagar.setHorizontalAlignment(SwingConstants.RIGHT);
		txtTotalPagar.setEnabled(false);
		txtTotalPagar.setEditable(false);
		txtTotalPagar.setBounds(430, 407, 121, 20);
		getContentPane().add(txtTotalPagar);
		txtTotalPagar.setColumns(10);
		
		btnQuitar = new JButton("Quitar Producto");
		btnQuitar.setBounds(399, 216, 149, 23);
		getContentPane().add(btnQuitar);
		
//		carritoCompra = new ArrayList<ProductoDTO>();
//		carritoAutopartes = new ArrayList<AutoparteDTO>();
//		carritoAutomoviles = new ArrayList<StockAutomovilDTO>();
		carritoCompra = new ArrayList<Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>>();
	}
	
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTxtCliente() {
		return txtCliente;
	}

	public void setTxtCliente(String cliente) {
		this.txtCliente.setText(cliente);
	}

	public JButton getBtnCargarCliente() {
		return btnCargarCliente;
	}

	public ComboBoxBoleanos getCmbModoPago() {
		return cmbModoPago;
	}

	public ComboBoxBoleanos getCmbAccion() {
		return cmbAccion;
	}

	public JButton getBtnCargarAutomovil() {
		return btnCargarAutomovil;
	}

	public JButton getBtnCargarProducto() {
		return btnCargarAutoparte;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JTable getTablaVentas() {
		return tablaVentas;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	public ClienteDTO getCliente() {
		return this.cliente;
	}

	public void setTablaAutomoviles(JTable tablaVentas) {
		this.tablaVentas= tablaVentas;
	}
	
	public DefaultTableModel getModelVentas() {
		return modelVentas;
	}

//	public List<AutoparteDTO> getCarritoAutopartes() {
//		return carritoAutopartes;
//	}
//
//	public void setCarritoAutopartes(List<AutoparteDTO> carritoAutopartes) {
//		this.carritoAutopartes = carritoAutopartes;
//	}
//
//	public List<StockAutomovilDTO> getCarritoAutomoviles() {
//		return carritoAutomoviles;
//	}
//
//	public void setCarritoAutomoviles(List<StockAutomovilDTO> carritoAutomoviles) {
//		this.carritoAutomoviles = carritoAutomoviles;
//	}
	

//	public List<ProductoDTO> getCarritoCompra() {
//		return carritoCompra;
//	}

	public List<Triple<Integer, Pair<StockAutomovilDTO, GarantiaDTO>, AutoparteDTO>> getCarritoCompra() {
		return carritoCompra;
	}

//	public void setCarritoCompra(List<ProductoDTO> carritoCompra) {
//		this.carritoCompra = carritoCompra;
//	}

	public JPanel getPanelPagos() {
		return panelPagos;
	}

	public void setPanelPagos(JPanel panelPagos) {
		this.panelPagos = panelPagos;
	}

	public JPanel getPanelVacio() {
		return panelVacio;
	}

	public void setPanelVacio(JPanel panelVacio) {
		this.panelVacio = panelVacio;
	}

	public JPanel getPanelTarjetas() {
		return panelTarjetas;
	}

	public void setPanelTarjetas(JPanel panelTarjetas) {
		this.panelTarjetas = panelTarjetas;
	}

	public JTextField getTxtNumeroTarjeta() {
		return txtNumeroTarjeta;
	}

	public void setTxtNumeroTarjeta(String numeroTarjeta) {
		this.txtNumeroTarjeta.setText(numeroTarjeta);
	}

	public JTextField getTxtNombreTarjeta() {
		return txtNombreTarjeta;
	}

	public void setTxtNombreTarjeta(String nombreTarjeta) {
		this.txtNombreTarjeta.setText(nombreTarjeta);
	}

	public JTextField getTxtCodigoTarjeta() {
		return txtCodigoTarjeta;
	}

	public void setTxtCodigoTarjeta(String codigoTarjeta) {
		this.txtCodigoTarjeta.setText(codigoTarjeta);
	}

	public JTextField getTxtVencimiento() {
		return txtVencimiento;
	}

	public void setTxtVencimiento(String vencimiento) {
		this.txtVencimiento.setText(vencimiento);
	}
	
	public JTextField getTxtTotalPagar() {
		return txtTotalPagar;
	}

	public void setTxtTotalPagar(JTextField txtTotalPagar) {
		this.txtTotalPagar = txtTotalPagar;
	}

	public JButton getBtnQuitar() {
		return btnQuitar;
	}

	public MantenimientoDTO getMantenimiento() {
		return mantenimiento;
	}

	public void setMantenimiento(MantenimientoDTO mantenimiento) {
		this.mantenimiento = mantenimiento;
	}

	public boolean isVentaMantenimiento() {
		return ventaMantenimiento;
	}

	public void setVentaMantenimiento(boolean ventaMantenimiento) {
		this.ventaMantenimiento = ventaMantenimiento;
	}

	public void cerrar()
	{
		vaciarCampos();
		this.dispose();
	}

	public void vaciarCampos() {
		this.txtCliente.setText(null);
		this.cliente = null;
		this.carritoCompra = new ArrayList<Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>>();
		this.getModelVentas().setRowCount(0); //Para vaciar la tabla
		this.txtNumeroTarjeta.setText(null);
		this.txtCodigoTarjeta.setText(null);
		this.txtNombreTarjeta.setText(null);
		this.txtVencimiento.setText(null);
		this.txtTotalPagar.setText("US$ 0");
	}
	
	public void agregarAutomovil(StockAutomovilDTO automovil, Object garantiaObjeto) {
		this.getModelVentas().setColumnIdentifiers(this.getNombreColumnas());
		if (automovil!=null&&garantiaObjeto!=null) {
			if(automovil.getKilometraje()==0) {
			try{
				GarantiaDTO garantia = (GarantiaDTO)garantiaObjeto;
				double totalPagar = 0;
				if(this.txtTotalPagar.getText() == null || this.txtTotalPagar.getText().isEmpty()) {
					totalPagar = automovil.getPrecio() + garantia.getPrecio();
				}else {
					totalPagar = Double.parseDouble(this.txtTotalPagar.getText().substring(4)) + automovil.getPrecio() + garantia.getPrecio();
				}
				this.txtTotalPagar.setText("US$ "+String.valueOf(totalPagar));
				Pair<StockAutomovilDTO, GarantiaDTO> autoYgarantia = new MutablePair<StockAutomovilDTO, GarantiaDTO>(automovil, garantia); 
				
				Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO> product = new ImmutableTriple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>(this.getTablaVentas().getRowCount(), autoYgarantia, null);
				carritoCompra.add(product);
//				carritoCompra.add(automovil);
//				carritoAutomoviles.add(automovil);
				Object[] fila = {automovil.getModelo().getMarca().getNombre() + " " + automovil.getModelo().getModelo() + " " + automovil.getYear() + " " + automovil.getColor() + " P" + automovil.getCantidadPuertas() + " + Garantia "+ garantia.getGarantia(), automovil.getPrecio() + garantia.getPrecio()};
				this.getModelVentas().addRow(fila);
			}catch(Exception ex){
				 JOptionPane.showMessageDialog(null,"Error al cargar el automovil al carrito","Error",0);
			}
			}
		} else if(automovil!=null && garantiaObjeto==null) {
			if(automovil.getKilometraje()>0) {
				try{
					double totalPagar = 0;
					if(this.txtTotalPagar.getText() == null || this.txtTotalPagar.getText().isEmpty()) {
						totalPagar = automovil.getPrecio();
					}else {
						totalPagar = Double.parseDouble(this.txtTotalPagar.getText().substring(4)) + automovil.getPrecio();
					}
					this.txtTotalPagar.setText("US$ "+String.valueOf(totalPagar));
					Pair<StockAutomovilDTO, GarantiaDTO> autoYgarantia = new MutablePair<StockAutomovilDTO, GarantiaDTO>(automovil, null); 
					
					Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO> product = new ImmutableTriple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>(this.getTablaVentas().getRowCount(), autoYgarantia, null);
					carritoCompra.add(product);
//					carritoCompra.add(automovil);
//					carritoAutomoviles.add(automovil);
					Object[] fila = {automovil.getModelo().getMarca().getNombre() + " " + automovil.getModelo().getModelo() + " " + automovil.getYear() + " " + automovil.getColor() + " P" + automovil.getCantidadPuertas(), automovil.getPrecio()};
					this.getModelVentas().addRow(fila);
				}catch(Exception ex){
					 JOptionPane.showMessageDialog(null,"Error al cargar el automovil al carrito","Error",0);
				}
			}
		}
		
	}
	
	
	
	public void agregarAutoparte(AutoparteDTO autoparte, boolean tieneGarantia) {
		this.getModelVentas().setColumnIdentifiers(this.getNombreColumnas());
		
		if(autoparte!=null) {
			try{
				double totalPagar = 0;
				if(this.txtTotalPagar.getText() == null || this.txtTotalPagar.getText().isEmpty()) {
					if(!tieneGarantia)totalPagar = autoparte.getPrecio();
				}else {
					totalPagar = Double.parseDouble(this.txtTotalPagar.getText().substring(4));
					if(!tieneGarantia)totalPagar = totalPagar + autoparte.getPrecio();
				}
				this.txtTotalPagar.setText("US$ "+String.valueOf(totalPagar));
				
				Triple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO> product = new ImmutableTriple<Integer,Pair<StockAutomovilDTO,GarantiaDTO>,AutoparteDTO>(this.getTablaVentas().getRowCount(), null, autoparte);
				carritoCompra.add(product);
//				carritoCompra.add(autoparte);
				Object[] fila;
				if(!tieneGarantia)fila = new Object[]{autoparte.getTipo() + " " + autoparte.getModelo().getMarca().getNombre() + " " + autoparte.getModelo().getModelo() , autoparte.getPrecio()};
				else fila = new Object[]{autoparte.getTipo() + " " + autoparte.getModelo().getMarca().getNombre() + " " + autoparte.getModelo().getModelo() , 0};
				this.getModelVentas().addRow(fila);
			}catch(Exception ex){
				 JOptionPane.showMessageDialog(null,"Error al cargar la autoparte al carrito","Error",0);
			}
		}
	}
	
	public void agregarManoDeObra(double manoDeObra) {
		this.getModelVentas().setColumnIdentifiers(this.getNombreColumnas());
			try{
				double totalPagar = Double.parseDouble(this.txtTotalPagar.getText().substring(4)) + manoDeObra;
				
				this.txtTotalPagar.setText("US$ "+String.valueOf(totalPagar));
				
				Object[] fila = {"Mano de Obra (Horas)" , manoDeObra};
				this.getModelVentas().addRow(fila);
			}catch(Exception ex){
				 JOptionPane.showMessageDialog(null,"Error al cargar la mano de Obra","Error",0);
			}
		
	}
	
	public void llenarMediosDePago(List<MedioPagoDTO> medioPago)
	{
		this.cmbModoPago.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbModoPago.removeAllItems();
            this.cmbModoPago.setModel(cmb);
            for(MedioPagoDTO pago: medioPago){
                cmb.addElement(pago);
            }
            this.cmbModoPago.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los medios de pago","Error",0);
        }
	}
	
	public void llenarOpcionesPago(List<OpcionPagoDTO> opcionesPago)
	{
		this.cmbAccion.setLoaded(false);
		try{    
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.cmbAccion.removeAllItems();
            this.cmbAccion.setModel(cmb);
            for(OpcionPagoDTO opcion: opcionesPago){
                cmb.addElement(opcion);
            }
            this.cmbAccion.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con las opciones de los pagos","Error",0);
        }
	}
	
	/*public void agregarGarantia(GarantiaDTO garantia) {
		if(garantia!=null) {
			try{
				double totalPagar = 0;
				if(this.txtTotalPagar.getText() == null || this.txtTotalPagar.getText().isEmpty()) {
					totalPagar = garantia.getPrecio();
				}else {
					totalPagar = Double.parseDouble(this.txtTotalPagar.getText().substring(4)) + garantia.getPrecio();
				}
				this.txtTotalPagar.setText("US$ "+String.valueOf(totalPagar));
				carritoCompra.add(garantia);
				Object[] fila = {garantia.getTipo() + " " + garantia.getModelo().getMarca().getNombre() + " " + autoparte.getModelo().getModelo() , autoparte.getPrecio()};
				this.getModelVentas().addRow(fila);
			}catch(Exception ex){
				 JOptionPane.showMessageDialog(null,"Error al cargar la autoparte al carrito","Error",0);
			}
		}
	}
	*/
	
	public void advertirFaltaDeStockDeVehiculo(){
		JOptionPane.showMessageDialog(null,"No hay stock de este vehiculo.","Falla al intentar seleccionar un vehiculo",2);
	}
	
	public void advertirFaltaDeStockDeAutoparte(){
		JOptionPane.showMessageDialog(null,"No hay stock de este articulo.","Falla al intentar seleccionar una autoparte",2);
	}

	public void errorSeleccionQuitar() {
		JOptionPane.showMessageDialog(null,"Debe seleccionar un producto para llevar a cabo esta función","Error",0);
	}
	
	public void advertirFaltaSeleccionCliente() {
		JOptionPane.showMessageDialog(null,"No seleccionó ningun cliente","Falla al intentar realizar una venta",2);
	}
	
	public void advertirFaltaSeleccionProducto() {
		JOptionPane.showMessageDialog(null,"No seleccionó ningun producto(automovil o autoparte). Debe seleccionar algun producto para poder realizar una venta.","Falla al intentar realizar una venta",2);
	}
	
	public void advertirFaltaSucursal() {
		JOptionPane.showMessageDialog(null,"No se pudo identificar la sucursal del usuario. Este puede ser debido a un error al logearse.\nPorfavor cierre el sistema e inicie sesion nuevamente.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFaltaUsuario() {
		JOptionPane.showMessageDialog(null,"No se pudo identificar el usuario logeado. Este puede ser debido a un error al logearse.\nPorfavor cierre el sistema e inicie sesion nuevamente.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFaltaNumeroTarjeta() {
		JOptionPane.showMessageDialog(null,"No se ingreso el número de tarjeta.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFaltaCodigoTarjeta() {
		JOptionPane.showMessageDialog(null,"No se ingreso el código de la tarjeta.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFaltaTitularTarjeta(){
		JOptionPane.showMessageDialog(null,"No se ingreso el nombre del titular de la tarjeta.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFaltaFechaVencimiento() {
		JOptionPane.showMessageDialog(null,"No se ingresó la fecha de vencimiento tarjeta.","Error al intentar realizar una venta",0);
	}
	
	
	public void advertirNumeroTarjetaInvalido() {
		JOptionPane.showMessageDialog(null,"El número de tarjeta ingresado no es valido. Por favor ingrese los 16 dígitos del numero sin espacios ni guiones.","Error al intentar realizar una venta",0);
	}
	
	public void advertirCodigoTarjetaInvalido() {
		JOptionPane.showMessageDialog(null,"El código de tarjeta ingresado no es valido. Por favor ingrese los últimos 3 dígitos que figuran en el código de la tarjeta.","Error al intentar realizar una venta",0);
	}
	
	public void advertirFechaVencimientoInvalida() {
		JOptionPane.showMessageDialog(null,"La fecha de vencimiento ingresada no es válida. Por favor ingrese mes y año de vencimiento con el siguiente formato: MM/AA.","Error al intentar realizar una venta",0);
	}

	public void notificarVentaExitosa() {
		JOptionPane.showMessageDialog(null, "La venta se ha cargado con éxito","Venta Exitosa",JOptionPane.INFORMATION_MESSAGE);
	}

	public void errorVentaFallida() {
		JOptionPane.showMessageDialog(null, "Falla al intentar cargar la venta, verifique los datos ingresados e intentelo nuevamente","Venta Fallida",JOptionPane.ERROR_MESSAGE);
		
	}

}
