package presentacion.vista;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Image;

public class VentanaConexion extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtIP;
	private JTextField txtPuerto;
	

	private JButton btnConfirmar;
	private static VentanaConexion INSTANCE;
	private JTextField txtUsuario;
	private JPasswordField txtPassword;
	
	public static VentanaConexion getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaConexion(); 	
			return new VentanaConexion();
		}
		else
			return INSTANCE;
	}

	private VentanaConexion() 
	{
		super();
		setTitle("Establecer Conexion");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 524, 264);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 11, 489, 208);
		contentPane.add(panel);
		panel.setLayout(null);
		
		
		//labels
		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblPuerto.setBounds(40, 60, 105, 20);
		panel.add(lblPuerto);
		
		JLabel lblIP = new JLabel("Dirección de IP");
		lblIP.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblIP.setBounds(40, 20, 192, 14);
		panel.add(lblIP);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblUsuario.setBounds(40, 100, 113, 14);
		panel.add(lblUsuario);

		JLabel lblpassword = new JLabel("Password");
		lblpassword.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblpassword.setBounds(40, 140, 113, 14);
		panel.add(lblpassword);
		
		
		//text fields		
		txtIP = new JTextField();
		txtIP.setBounds(200, 20, 164, 20);
		panel.add(txtIP);
		txtIP.setColumns(10);
		
		txtPuerto = new JTextField();
		txtPuerto.setBounds(200, 60, 164, 20);
		panel.add(txtPuerto);
		txtPuerto.setColumns(10);
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(200, 100, 164, 20);
		panel.add(txtUsuario);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(200, 140, 164, 20);
		panel.add(txtPassword);
	
		
		//btn
		btnConfirmar = new JButton("Confirmar");
		//btnConfirmar.addActionListener(new ActionListener() {
		//	public void actionPerformed(ActionEvent arg0) {
		//	}
		//});
		btnConfirmar.setBounds(378, 174, 105, 23);
		panel.add(btnConfirmar);
		
			
		this.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtUsuario() 
	{
		return txtUsuario;
	}

	
	public JPasswordField getTxtPassword() {
		return txtPassword;
	}

	

	public JTextField getTxtIP() {
		return txtIP;
	}

	public JTextField getTxtPuerto() {
		return txtPuerto;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmar;
	}
	
	public void errorConexion() {
		JOptionPane.showMessageDialog(null, "No se ha podido establecer una conexión a la base de datos, por favor ingrese los datos de conexión",
			      "Advertencia de Conexión", JOptionPane.WARNING_MESSAGE);
	}
	public void inicializar() {
		
		this.txtIP.setText("localhost");
		this.txtPuerto.setText("3306");
		this.txtUsuario.setText("root");
		
	}
	
	public void cerrar()
	{
		this.txtIP.setText(null);
		this.txtPuerto.setText(null);
		this.txtUsuario.setText(null);
		this.txtPassword.setText(null);
		
		
		this.dispose();
	}
}
