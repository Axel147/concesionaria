package presentacion.vista;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;




import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class VentanaReporte extends JFrame {
	private static final long serialVersionUID = 1L;
	private static VentanaReporte INSTANCE;
	private JPanel panel;
	private JRadioButton rdbtnReporteCliente;
	private JRadioButton rdbtnReporteReserva;
	private JRadioButton rdbtnReporteReservaYear;
	private JRadioButton rdbtnReporteReservaDiarias;
	private JRadioButton rdbtnReporteStock_auto_sucursal;
	private JRadioButton rdbtnReporteDetalleVentasXMarca;
	private JRadioButton rdbtnReporteEntregasXSucursal;
	private JRadioButton rdbtnReporteEntregasXYear;
	private JRadioButton rdbtnReporteEntregaModeloAuto;
	private JRadioButton rdbtnReporteEntregasMarca;
	private JButton btnGenerarReportes;
	private JButton btnCancelar;
	private JPanel contentPane;
	private JRadioButton rdbtnReporteVentaSucursalDetalle;
	private JRadioButton rdbtnReporteIngresosSucursal;
	private JRadioButton rdbtnReporteEncuesta;

	public static VentanaReporte getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaReporte(); 	
			return new VentanaReporte();
		}
		else
			return INSTANCE;
	}
	
	private VentanaReporte() 
	{
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 762, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setTitle("Generar Reportes");
		setResizable(false);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
//		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
//		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(panel);
//		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setBounds(10, 11, 736, 435);
		contentPane.add(panel);
		panel.setLayout(null);
		
		rdbtnReporteCliente = new JRadioButton("Reporte Clientes");
		rdbtnReporteCliente.setBounds(10, 67, 525, 23);
		panel.add(rdbtnReporteCliente);
		
		rdbtnReporteReserva = new JRadioButton("Reporte Reservas");
		rdbtnReporteReserva.setBounds(10, 93, 525, 23);
		panel.add(rdbtnReporteReserva);
		
		rdbtnReporteReservaYear = new JRadioButton("Reporte Reservas Por Año");
		rdbtnReporteReservaYear.setBounds(10, 119, 525, 23);
		panel.add(rdbtnReporteReservaYear);
		
		rdbtnReporteReservaDiarias = new JRadioButton("Reporte Reservas Diarias");
		rdbtnReporteReservaDiarias.setBounds(10, 41, 525, 23);
		panel.add(rdbtnReporteReservaDiarias);
		
		rdbtnReporteStock_auto_sucursal = new JRadioButton("Reporte Stock Agrupado Por Sucursal");
		rdbtnReporteStock_auto_sucursal.setBounds(10, 145, 525, 23);
		panel.add(rdbtnReporteStock_auto_sucursal);
		
		rdbtnReporteDetalleVentasXMarca = new JRadioButton("Reporte Detalle de Ventas por Marca");
		rdbtnReporteDetalleVentasXMarca.setBounds(10, 171, 525, 23);
		panel.add(rdbtnReporteDetalleVentasXMarca);
		
		
		JLabel lblSeleccionesLosReportes = new JLabel("Selecciones los reportes que quiere generar:");
		lblSeleccionesLosReportes.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSeleccionesLosReportes.setBounds(10, 11, 263, 23);
		panel.add(lblSeleccionesLosReportes);
		
		btnGenerarReportes = new JButton("Generar Reportes");
		btnGenerarReportes.setBounds(462, 392, 165, 23);
		panel.add(btnGenerarReportes);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(637, 392, 89, 23);
		panel.add(btnCancelar);
		
		rdbtnReporteEntregasXSucursal = new JRadioButton("Reporte Entregas por Sucursal");
		rdbtnReporteEntregasXSucursal.setBounds(10, 197, 525, 23);
		panel.add(rdbtnReporteEntregasXSucursal);
		
		rdbtnReporteEntregasXYear = new JRadioButton("Reporte Entregas por Año");
		rdbtnReporteEntregasXYear.setBounds(10, 223, 525, 23);
		panel.add(rdbtnReporteEntregasXYear);
		
		rdbtnReporteEntregaModeloAuto = new JRadioButton("Reporte Entrega Modelo Auto");
		rdbtnReporteEntregaModeloAuto.setBounds(10, 249, 525, 23);
		panel.add(rdbtnReporteEntregaModeloAuto);
		
		rdbtnReporteEntregasMarca = new JRadioButton("Reporte Entregas Marca");
		rdbtnReporteEntregasMarca.setBounds(10, 275, 525, 23);
		panel.add(rdbtnReporteEntregasMarca);
		
		rdbtnReporteVentaSucursalDetalle = new JRadioButton("Reporte Venta Sucursal Detalle");
		rdbtnReporteVentaSucursalDetalle.setBounds(10, 301, 525, 23);
		panel.add(rdbtnReporteVentaSucursalDetalle);
		
		rdbtnReporteIngresosSucursal = new JRadioButton("Reporte Ingresos Sucursal");
		rdbtnReporteIngresosSucursal.setBounds(10, 327, 525, 23);
		panel.add(rdbtnReporteIngresosSucursal);
		
		rdbtnReporteEncuesta = new JRadioButton("Reporte Encuesta");
		rdbtnReporteEncuesta.setBounds(10, 353, 525, 23);
		panel.add(rdbtnReporteEncuesta);
		
		
		setVisible(false);
	}
	
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JRadioButton getRdbtnReporteCliente() {
		return rdbtnReporteCliente;
	}




	public void setRdbtnReporteCliente(JRadioButton rdbtnReporteCliente) {
		this.rdbtnReporteCliente = rdbtnReporteCliente;
	}




	public JRadioButton getRdbtnReporteReserva() {
		return rdbtnReporteReserva;
	}




	public void setRdbtnReporteReserva(JRadioButton rdbtnReporteReserva) {
		this.rdbtnReporteReserva = rdbtnReporteReserva;
	}




	public JRadioButton getRdbtnReporteReservaYear() {
		return rdbtnReporteReservaYear;
	}




	public void setRdbtnReporteReservaYear(JRadioButton rdbtnReporteReservaYear) {
		this.rdbtnReporteReservaYear = rdbtnReporteReservaYear;
	}




	public JRadioButton getRdbtnReporteReservaDiarias() {
		return rdbtnReporteReservaDiarias;
	}




	public void setRdbtnReporteReservaDiarias(JRadioButton rdbtnReporteReservaDiarias) {
		this.rdbtnReporteReservaDiarias = rdbtnReporteReservaDiarias;
	}

	public JRadioButton getRdbtnReporteStockPorSucursal() {
		return rdbtnReporteStock_auto_sucursal;
	}

	public void setRdbtnReporteStockPorSucursal(JRadioButton rdbtnReporteStockPorSucursal) {
		this.rdbtnReporteStock_auto_sucursal = rdbtnReporteStockPorSucursal;
	}

	public JButton getBtnGenerarReportes() {
		return btnGenerarReportes;
	}

	public void setBtnGenerarReportes(JButton btnGenerarReportes) {
		this.btnGenerarReportes = btnGenerarReportes;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}
	
	public JRadioButton getRdbtnReporteStock_auto_sucursal() {
		return rdbtnReporteStock_auto_sucursal;
	}

	public JRadioButton getRdbtnReporteEntregasXSucursal() {
		return rdbtnReporteEntregasXSucursal;
	}

	public JRadioButton getRdbtnReporteEntregasXYear() {
		return rdbtnReporteEntregasXYear;
	}

	public JRadioButton getRdbtnReporteEntregaModeloAuto() {
		return rdbtnReporteEntregaModeloAuto;
	}

	public JRadioButton getRdbtnReporteEntregasMarca() {
		return rdbtnReporteEntregasMarca;
	}

	public JRadioButton getRdbtnReporteVentaSucursalDetalle() {
		return rdbtnReporteVentaSucursalDetalle;
	}

	public JRadioButton getRdbtnReporteIngresosSucursal() {
		return rdbtnReporteIngresosSucursal;
	}

	public JRadioButton getRdbtnReporteDetalleVentasXMarca() {
		return rdbtnReporteDetalleVentasXMarca;
	}
	
	public JRadioButton getRdbtnReporteEncuesta() {
		return rdbtnReporteEncuesta;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public void cerrar()
	{
		this.rdbtnReporteReservaDiarias.setSelected(false);
		this.rdbtnReporteCliente.setSelected(false);
		this.rdbtnReporteReserva.setSelected(false);
		this.rdbtnReporteReservaYear.setSelected(false);
		this.rdbtnReporteStock_auto_sucursal.setSelected(false);
		this.dispose();
	}
}
