package presentacion.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.AutoparteDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import presentacion.controlador.ControladorCampos;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaAutopartes extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static VentanaAutopartes INSTANCE;
	private String[] arrayYear = new String[Calendar.getInstance().get(Calendar.YEAR)-2000+1];
	
	private int modo;// 0-> agregar, 1-> editar 
	
	private JPanel contentPane;
	
	private JLabel lblDescripcion;
	
	private JComboBox<TipoAutoparteDTO> comboTipos;
	private JComboBox<MarcaDTO> comboMarca;
	private JComboBox<ModeloDTO> comboModelos;
	private JComboBox comboYear;
	private JComboBox<SucursalDTO> comboSucursales;

	private JTextField txtStock;
	private JTextField txtStockMinimno;
	private JTextField txtPrecio;

	
	private JButton btnAutoparte;

	private JLabel lblErrorTipo;
	private JLabel lblErrorModelo;
	private JLabel lblErrorMarca;
	private JLabel lblErrorYear;
	private JLabel lblErrorSucursal;
	private JLabel lblErrorStockMinimo;
	private JLabel lblStockMinimo;
	private JLabel lblPrecio;
	private JLabel lblErrorStock;
	private JLabel lblErrorPrecio;
	private JButton btnAbmMarcas;
	private JButton btnAbmModelos;
	
	private ListaDeAutopartes ventanaListaDeAutopartes;
	
	
	public static VentanaAutopartes getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaAutopartes(); 	
			return new VentanaAutopartes();
		}
		else
			return INSTANCE;
	}
	
	private VentanaAutopartes() {
		super();
		
		setBounds(100, 100, 487, 562);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(null);
		
		lblDescripcion = new JLabel();
		lblDescripcion.setBounds(30, 11, 343, 15);
		panel.add(lblDescripcion);
		
		JLabel lblTipoAutoparte = new JLabel("Tipo de Autoparte*");
		lblTipoAutoparte.setBounds(44, 75, 125, 15);
		panel.add(lblTipoAutoparte);
		
		JLabel lblModelo = new JLabel("Modelo*");
		lblModelo.setBounds(44, 175, 125, 15);
		panel.add(lblModelo);
		
		JLabel lblMarca = new JLabel("Marca*");
		lblMarca.setBounds(44, 125, 125, 15);
		panel.add(lblMarca);
		
		JLabel lblColor = new JLabel("Año*");
		lblColor.setBounds(44, 225, 125, 15);
		panel.add(lblColor);
		
		JLabel lblSucursal = new JLabel("Sucursal*");
		lblSucursal.setBounds(44, 278, 125, 15);
		panel.add(lblSucursal);
				
		JLabel lblStock = new JLabel("Cantidad en Stock*");
		lblStock.setBounds(44, 378, 125, 15);
		panel.add(lblStock);
		
		btnAutoparte = new JButton("New button");
		btnAutoparte.setBounds(311, 484, 150, 33);
		panel.add(btnAutoparte);
		
		JLabel lblCamposObligatorios = new JLabel("Los campos marcados con un asterisco son obligatorios");
		lblCamposObligatorios.setBounds(30, 36, 343, 15);
		panel.add(lblCamposObligatorios);
		
		
		
		
		comboTipos = new JComboBox<TipoAutoparteDTO>();
		comboTipos.setSelectedIndex(-1);
		comboTipos.setBounds(213, 72, 160, 20);
		panel.add(comboTipos);
		
				
		comboModelos = new JComboBox<ModeloDTO>();
		comboModelos.setSelectedIndex(-1);
		comboModelos.setBounds(213, 172, 160, 20);
		panel.add(comboModelos);
		
		comboMarca = new JComboBox<MarcaDTO>();
		comboMarca.setBounds(213, 122, 160, 20);
		panel.add(comboMarca);
		
		llenarYear();
		comboYear = new JComboBox(arrayYear);
		comboYear.setSelectedIndex(-1);
		comboYear.setBounds(214, 222, 160, 20);
		panel.add(comboYear);
		
		comboSucursales = new JComboBox<SucursalDTO>();
		comboSucursales.setSelectedIndex(-1);
		comboSucursales.setBounds(213, 275, 160, 20);
		panel.add(comboSucursales);
		
		
		txtStock = new JTextField();
		txtStock.setBounds(213, 375, 160, 20);
		panel.add(txtStock);
		txtStock.setColumns(10);
		
		lblErrorModelo = new JLabel("");
		lblErrorModelo.setForeground(Color.RED);
		lblErrorModelo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorModelo.setBounds(213, 190, 215, 10);
		lblErrorModelo.setVisible(false);
		panel.add(lblErrorModelo);
		
		lblErrorTipo = new JLabel();
		lblErrorTipo.setForeground(Color.RED);
		lblErrorTipo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorTipo.setBounds(213, 91, 215, 10);
		lblErrorTipo.setVisible(false);
		panel.add(lblErrorTipo);
		
		lblErrorYear = new JLabel("");
		lblErrorYear.setForeground(Color.RED);
		lblErrorYear.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorYear.setBounds(213, 240, 215, 10);
		lblErrorYear.setVisible(false);
		panel.add(lblErrorYear);
					
		lblErrorSucursal = new JLabel("");
		lblErrorSucursal.setForeground(Color.RED);
		lblErrorSucursal.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorSucursal.setBounds(213, 298, 215, 10);
		lblErrorSucursal.setVisible(false);
		panel.add(lblErrorSucursal);
		
		lblErrorStockMinimo = new JLabel("");
		lblErrorStockMinimo.setForeground(Color.RED);
		lblErrorStockMinimo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorStockMinimo.setBounds(213, 348, 215, 10);
		lblErrorStockMinimo.setVisible(false);
		panel.add(lblErrorStockMinimo);
		
		

		
		lblErrorMarca = new JLabel("");
		lblErrorMarca.setForeground(Color.RED);
		lblErrorMarca.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorMarca.setBounds(213, 142, 215, 10);
		lblErrorMarca.setVisible(false);
		panel.add(lblErrorMarca);
		
		lblStockMinimo = new JLabel("Stock Mínimo*");
		lblStockMinimo.setBounds(44, 328, 125, 15);
		panel.add(lblStockMinimo);
		
		txtStockMinimno = new JTextField();
		txtStockMinimno.setColumns(10);
		txtStockMinimno.setBounds(213, 325, 160, 20);
		panel.add(txtStockMinimno);
		
		lblPrecio = new JLabel("Precio*");
		lblPrecio.setBounds(44, 428, 125, 15);
		panel.add(lblPrecio);
		
		txtPrecio = new JTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(213, 425, 160, 20);
		panel.add(txtPrecio);
		
		lblErrorStock = new JLabel("");
		lblErrorStock.setForeground(Color.RED);
		lblErrorStock.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorStock.setBounds(213, 393, 215, 10);
		lblErrorStock.setVisible(false);
		panel.add(lblErrorStock);
		
		lblErrorPrecio = new JLabel("");
		lblErrorPrecio.setForeground(Color.RED);
		lblErrorPrecio.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorPrecio.setBounds(213, 443, 215, 10);
		lblErrorPrecio.setVisible(false);
		panel.add(lblErrorPrecio);
		
		ImageIcon imagenConfig = new ImageIcon(ListaDeAutomoviles.class.getResource("/iconos/config.png"));
		imagenConfig = new ImageIcon(imagenConfig.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		btnAbmMarcas = new JButton("");
		btnAbmMarcas.setIcon(imagenConfig);
		btnAbmMarcas.setBounds(388, 122, 40, 23);
		panel.add(btnAbmMarcas);
		
		btnAbmModelos = new JButton("");
		btnAbmModelos.setIcon(imagenConfig);
		btnAbmModelos.setBounds(388, 172, 40, 23);
		panel.add(btnAbmModelos);

		
	}
	
	public void mostrarVentana() {
		
		this.setVisible(true);
		
	}
	
	public void modoAgregar() {
		
		seleccionCombosNula();
		this.modo = 0;
		this.lblDescripcion.setText("Por favor introduzca los datos de la autoparte a agregar");
		this.setTitle("Agregar nueva autoparte");
		this.btnAutoparte.setText("Agregar");
	
	}
	
	public void modoEditar() {
		
		this.modo = 1;
		this.lblDescripcion.setText("Por favor modifique los datos de la autoparte a editar");
		this.setTitle("Editar autoparte");
		this.btnAutoparte.setText("Guardar Cambios");
		
	}
	
	public void mostrarAutoparte(AutoparteDTO autoparte){
		
		this.comboMarca.setSelectedItem(autoparte.getModelo().getMarca());
		this.comboModelos.setSelectedItem(autoparte.getModelo());
		this.comboTipos.setSelectedItem(autoparte.getTipo());
		this.comboYear.setSelectedItem(autoparte.getYear());
		this.comboSucursales.setSelectedItem(autoparte.getSucursal());
		this.txtStock.setText(Integer.toString(autoparte.getStock()));
		this.txtStockMinimno.setText(Integer.toString(autoparte.getStockMinimo()));
		this.txtPrecio.setText(Double.toString(autoparte.getPrecio()));
//	
	}
	
	public void cerrar() {
		
		
		seleccionCombosNula();
		this.txtStock.setText(null);
		this.txtStockMinimno.setText(null);
		this.txtPrecio.setText(null);

		ocultarLabelsError();
		this.ventanaListaDeAutopartes.setEnabled(true);
		this.dispose();
	}

	public void seleccionCombosNula() {
		this.comboTipos.setSelectedIndex(-1);
		this.comboMarca.setSelectedIndex(-1);
		this.comboYear.setSelectedIndex(-1);
		this.comboSucursales.setSelectedIndex(-1);
	}

	public void ocultarLabelsError() {
		this.lblErrorTipo.setVisible(false);
		this.lblErrorMarca.setVisible(false);
		this.lblErrorModelo.setVisible(false);
		this.lblErrorYear.setVisible(false);
		this.lblErrorSucursal.setVisible(false);
		this.lblErrorStockMinimo.setVisible(false);
		this.lblErrorPrecio.setVisible(false);
	}
	
	public void cargarTiposAutoparte(List<TipoAutoparteDTO> tiposAutoparte){
		TipoAutoparteDTO[] arrayTipos = new TipoAutoparteDTO[tiposAutoparte.size()];
		int i = 0;
		for(TipoAutoparteDTO tipoAutoparte : tiposAutoparte ) {
			arrayTipos[i] = tipoAutoparte;
			i++;
		}
		comboTipos.setModel(new DefaultComboBoxModel<TipoAutoparteDTO>(arrayTipos));
	}
	
	public void cargarModelos(List<ModeloDTO> modelosAuto) {
		ModeloDTO[] arrayModelos = new ModeloDTO[modelosAuto.size()];
		int i = 0;
		for(ModeloDTO modeloAuto : modelosAuto ) {
			arrayModelos[i] = modeloAuto;
			i++;
		}
		comboModelos.setModel(new DefaultComboBoxModel<ModeloDTO>(arrayModelos));		
	}
	
	public void cargarMarcas(List<MarcaDTO> marcasAuto) {
		MarcaDTO[] arrayMarcas = new MarcaDTO[marcasAuto.size()];
		int i = 0;
		for(MarcaDTO marcaAuto : marcasAuto ) {
			arrayMarcas[i] = marcaAuto;
			i++;
					}
		comboMarca.setModel(new DefaultComboBoxModel<MarcaDTO>(arrayMarcas));		
	}
	
	public void cargarSucursales(List<SucursalDTO> sucursales) {
		SucursalDTO[] arraySucursales = new SucursalDTO[sucursales.size()];
		int i = 0;
		for(SucursalDTO sucursal : sucursales ) {
			arraySucursales[i] = sucursal;
			i++;
		}
		comboSucursales.setModel(new DefaultComboBoxModel<SucursalDTO>(arraySucursales));		
	}
	
	public void llenarYear() {
		int yearActual = Calendar.getInstance().get(Calendar.YEAR);
		for(int i = 2000; i<=yearActual; i++) {
			arrayYear[i-2000] = Integer.toString(i);
		}
	}
	
	
	public boolean validarCampos() {
		//uso todos estos ifs para que me marque todos los campos erroneos de una
		boolean esValido = true;
		if(!validarTipo()) {
			esValido = false;
		}
		if(!validarMarca()) {
			esValido = false;
		}
		if(!validarModelo()) {
			esValido = false;
		}
		if(!validarYear()) {
			esValido = false;
		}
		if(!validarColor()) {
			esValido = false;
		}
		if(!validarSucursal()) {
			esValido = false;
		}
		if(!validarStock()) {
			esValido = false;
		}
		if(!validarStockMinimo())
		{
			esValido = false;
		}
		if(!validarPrecio()) {
			esValido = false;
		}

		return esValido;
	}
	

	
	private boolean validarTipo() {
		boolean esValido = true;
		if(this.comboTipos.getSelectedItem()==null) {
			this.lblErrorTipo.setText("Debe seleccionar un tipo de autoparte");
			esValido=false;
		}
		if(!esValido) {
				this.lblErrorTipo.setVisible(true);
			}
			
			return esValido;
		
	}
	

	private boolean validarYear() {
		boolean esValido = true;
		if(this.comboYear.getSelectedItem()==null) {
			this.lblErrorYear.setText("Debe seleccionar un año");
			esValido=false;
		}
		if(!esValido) {
				this.lblErrorYear.setVisible(true);
			}
			
			return esValido;
	}

	private boolean validarModelo() {
		boolean esValido = true;
		if(this.comboModelos.getSelectedItem()==null) {
			this.lblErrorModelo.setText("Debe seleccionar un modelo de auto");
			esValido=false;
		}
		if(!esValido) {
				this.lblErrorModelo.setVisible(true);
			}
			
			return esValido;
	}
	
	private boolean validarMarca() {
		boolean esValido = true;
		if(this.comboMarca.getSelectedItem()==null) {
			this.lblErrorMarca.setText("Debe seleccionar una marca auto");
			esValido=false;
		}
		if(!esValido) {
				this.lblErrorMarca.setVisible(true);
			}
			
			return esValido;	
	}

	private boolean validarColor() {
		return true;
	}
	
	private boolean validarSucursal(){
		boolean esValido = true;
		if(this.comboSucursales.getSelectedItem()==null) {
			this.lblErrorSucursal.setText("Debe seleccionar una sucursal");
			esValido = false;
		}
		if(!esValido) {
			this.lblErrorSucursal.setVisible(true);
		}
		return esValido;
	}
	
	private boolean validarStock() {
		boolean esValido = true;
		String textoStock = this.getTxtStock().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoStock)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.validarStock(textoStock)){
			esValido = false;
			if(textoStock.length()>45) {
			textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Número de stock Inválido";
			}
		}
		if(!esValido) {
			this.lblErrorStock.setText(textoError);
			this.lblErrorStock.setVisible(true);
		}
		
		return esValido;
		
	}
	
	private boolean validarStockMinimo() {
		boolean esValido = true;
		String textoStockMinimo = this.getTxtStockMinimno().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoStockMinimo)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.validarStock(textoStockMinimo)){
			esValido = false;
			if(textoStockMinimo.length()>45) {
			textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Número de stock Inválido";
			}
		}
		if(!esValido) {
			this.lblErrorStockMinimo.setText(textoError);
			this.lblErrorStockMinimo.setVisible(true);
		}
		
		return esValido;
		
	}
	
	
	private boolean validarPrecio() {
		boolean esValido = true;
		String textoPrecio = this.getTxtPrecio().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoPrecio)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.validarPrecio(textoPrecio)){
			esValido = false;
			if(textoPrecio.length()>45) {
			textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Precio Inválido";
			}
		}
		if(!esValido) {
			this.lblErrorPrecio.setText(textoError);
			this.lblErrorPrecio.setVisible(true);
		}
		
		return esValido;
		
	}
	
	

	public void cargarYear() {
		llenarYear();
		comboYear.setModel(new DefaultComboBoxModel(arrayYear));
	}
	
	public void errorAgregar() {
		JOptionPane.showMessageDialog(null,  "No se ha podido agregar la nueva autoparte. \nVerifique los datos e inténtelo nuevamente.","Error al intentar agregar un nuevo usuario",1);		
	}
	
	public void errorEditar() {
		JOptionPane.showMessageDialog(null,  "No se han podido modificar los datos de la autoparte. \nVerifique los datos e inténtelo nuevamente.","Error al intentar editar un usuario",1);		
	}
	
	public void errorCargar() {
		JOptionPane.showMessageDialog(null,  "Uno o mas de los de los campos es inválido. Revise los datos antes de ingresarlos nuevamente","Error",1);		
	}

	public int getModo() {
		return modo;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JComboBox<TipoAutoparteDTO> getComboTipos() {
		return comboTipos;
	}

	public JButton getBtnAutoparte() {
		return btnAutoparte;
	}
	
	public void setComboTipos(JComboBox<TipoAutoparteDTO> comboTipos) {
		this.comboTipos = comboTipos;
	}
	
	public JComboBox getComboYear() {
		return comboYear;
	}

	public void setComboYear(JComboBox comboYear) {
		this.comboYear = comboYear;
	}


	public JComboBox<ModeloDTO> getComboModelos() {
		return comboModelos;
	}

	public void setComboModelos(JComboBox<ModeloDTO> comboModelos) {
		this.comboModelos = comboModelos;
	}

	public JComboBox<SucursalDTO> getComboSucursales() {
		return comboSucursales;
	}
	
	

	public JComboBox<MarcaDTO> getComboMarca() {
		return comboMarca;
	}

	public void setComboMarca(JComboBox<MarcaDTO> comboMarca) {
		this.comboMarca = comboMarca;
	}

	public void setComboSucursales(JComboBox<SucursalDTO> comboSucursales) {
		this.comboSucursales = comboSucursales;
	}

	public JTextField getTxtStock() {
		return txtStock;
	}

	public void setTxtStock(String string) {
		this.txtStock.setText(string);
	}

	public JTextField getTxtStockMinimno() {
		return txtStockMinimno;
	}

	public void setTxtStockMinimno(String txtStockMinimno) {
		this.txtStockMinimno.setText(txtStockMinimno);
	}

	public JTextField getTxtPrecio() {
		return txtPrecio;
	}

	public void setTxtPrecio(String txtPrecio) {
		this.txtPrecio.setText(txtPrecio);
	}

	public void setVentanaBloqueada(ListaDeAutopartes ventanaListaDeAutopartes) {
		this.ventanaListaDeAutopartes = ventanaListaDeAutopartes;
	}
	
	public JButton getBtnAbmMarcas() {
		return btnAbmMarcas;
	}

	public JButton getBtnAbmModelos() {
		return btnAbmModelos;
	}
}
