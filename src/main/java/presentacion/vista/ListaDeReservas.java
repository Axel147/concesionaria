package presentacion.vista;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.AutomovilTallerDTO;
import dto.ClienteDTO;
import dto.EstadoReservaDTO;
import dto.ReservaDTO;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.Dimension;

public class ListaDeReservas extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaDeReservas INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaClientes;
	private DefaultTableModel modelClientes;
	private  String[] nombreColumnas = {"Fecha Emitida","Fecha Turno","Horario","Nombre y Apellido Cliente","Tipo De Documento Cliente","Documento Cliente","Pais De La Patente Del Vehiculo","Patente Del Vehiculo","Notificada","Estado","Motivo/s","Tiene Garantía","Tipo de Service","Motivo Cancelacion"};
	
	//botones
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnAgregarFiltro;
	private JButton btnVolver;
	private JButton btnDeshacerFiltros;
	private JButton btnFinalizar;
	
	
	//campo de busqueda
	private JTextField txtBusqueda;
	
	//etiquta de busqueda
		private JLabel lblBuscarPor;
	
	//botones seleccionables
	private JRadioButton rdbtnNombre;
	private JRadioButton rdbtnApellido;
	private JRadioButton rdbtnNdocumento;
	private JRadioButton rdbtnEmail;
	private JRadioButton rdbtnTelefono;
	private JRadioButton rdbtnEstado;
	private JRadioButton rdbtnFecha;
	private JRadioButton rdbtnPatente;

	ButtonGroup rdbtngroup;
	
	
	
	
	public static ListaDeReservas getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeReservas(); 	
			return new ListaDeReservas();
		}else return INSTANCE;
	}

	public ListaDeReservas(){
		super();
		setMinimumSize(new Dimension(1080, 158));
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setTitle("Reservas");
		setLocationRelativeTo(null);
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelClientes = new DefaultTableModel(null,nombreColumnas){@Override public boolean isCellEditable(int row, int column) {return false;}};
		
		JScrollPane spClientes = new JScrollPane();
		panel.add(spClientes);
		tablaClientes = new JTable(modelClientes);
		tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		
		tablaClientes.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaClientes.getColumnModel().getColumn(0).setResizable(false);
		tablaClientes.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaClientes.getColumnModel().getColumn(1).setResizable(false);
		
		spClientes.setViewportView(tablaClientes);
		
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel.add(panelsuperior, BorderLayout.NORTH);
//		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
//				ColumnSpec.decode("216px"),
//				ColumnSpec.decode("72px"),
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,},
//			new RowSpec[] {
//				RowSpec.decode("38px"),
//				RowSpec.decode("23px"),}));
		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("214px"),
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblBuscarPor = new JLabel("Buscar por:");
		panelsuperior.add(lblBuscarPor, "1, 1");
		
		rdbtnNombre = new JRadioButton("Nombre");
		rdbtnNombre.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnNombre, "2, 1, fill, default");
		
		rdbtnApellido = new JRadioButton("Apellido");
		rdbtnApellido.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnApellido, "3, 1, fill, default");
		
		rdbtnNdocumento = new JRadioButton("NºDocumento");
		rdbtnNdocumento.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnNdocumento, "4, 1, fill, default");
	
		rdbtnEmail = new JRadioButton("Email");
		rdbtnEmail.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnEmail, "5, 1, fill, default");
		
		rdbtnTelefono = new JRadioButton("Telefono");
		rdbtnTelefono.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnTelefono, "6, 1, fill, default");
		
		rdbtnEstado = new JRadioButton("Estado");
		rdbtnEstado.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnEstado, "7, 1, fill, default");
		
		rdbtnFecha = new JRadioButton("Fecha Del Turno");
		rdbtnFecha.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnFecha, "8, 1, fill, default");

		rdbtnPatente = new JRadioButton("Patente del Auto");
		rdbtnPatente.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnPatente, "9, 1, fill, default");

		

		
		rdbtngroup = new ButtonGroup();
		rdbtngroup.add(rdbtnNombre);
		rdbtngroup.add(rdbtnApellido);
		rdbtngroup.add(rdbtnNdocumento);
		rdbtngroup.add(rdbtnEmail);
		rdbtngroup.add(rdbtnTelefono);
		rdbtngroup.add(rdbtnEstado);
		rdbtngroup.add(rdbtnFecha);
		rdbtngroup.add(rdbtnPatente);
		rdbtngroup.clearSelection();
		rdbtnNombre.setSelected(true);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setHorizontalAlignment(SwingConstants.LEFT);
		txtBusqueda.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelsuperior.add(txtBusqueda, "1, 2, fill, fill");
		txtBusqueda.setColumns(10);
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		btnDeshacerFiltros.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnDeshacerFiltros, "2, 2, fill, fill");
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		btnAgregarFiltro.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnAgregarFiltro, "3, 2, fill, fill");
		
		btnAgregar = new JButton("Agregar");
		panelsuperior.add(btnAgregar, "4, 2, fill, fill");
		btnAgregar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnEditar = new JButton("Editar");
		panelsuperior.add(btnEditar, "5, 2, fill, fill");
		btnEditar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnBorrar = new JButton("Cancelar");
		panelsuperior.add(btnBorrar, "6, 2, fill, fill");
		btnBorrar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelsuperior.add(btnVolver, "7, 2, fill, fill");
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnFinalizar = new JButton("Finalizar");
		panelsuperior.add(btnFinalizar, "8, 2, fill, fill");
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);
		
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	public JRadioButton getRdbtnNombre() {
		return rdbtnNombre;
	}

	public JRadioButton getRdbtnApellido() {
		return rdbtnApellido;
	}

	public JRadioButton getRdbtnNdocumento() {
		return rdbtnNdocumento;
	}

	public JRadioButton getRdbtnEmail() {
		return rdbtnEmail;
	}

	public JRadioButton getRdbtnTelefono() {
		return rdbtnTelefono;
	}

	public JRadioButton getRdbtnFecha() {
		return rdbtnFecha;
	}

	public JRadioButton getRdbtnPatente() {
		return rdbtnPatente;
	}

	public ButtonGroup getRdbtngroup() {
		return rdbtngroup;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}
	
	public JButton getBtnVolver() 
	{
		return btnVolver;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}

	public JButton getBtnFinalizar() {
		return btnFinalizar;
	}

	public DefaultTableModel getModelPersonas() 
	{
		return modelClientes;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaClientes;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public JRadioButton getRdbtnEstado() {
		return rdbtnEstado;
	}

	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaPersonas();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;

            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
                renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }

	public void llenarTabla(List<ReservaDTO> reservasEnTabla) {
		if(reservasEnTabla.size()>0)tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		else tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (ReservaDTO r : reservasEnTabla)
		{
			String fechaEmitida = r.getFechaEmitida();
			String fechaTurno = r.getFechaTurno();
			String horario = r.getHorario();
			ClienteDTO cliente = r.getCliente();
			boolean notificada = r.isNotificada();
			EstadoReservaDTO estado = r.getEstado();
			String motivoCancelacion = r.getMotivoCancelacion();
			String motivo = r.getMotivo().toString();
			AutomovilTallerDTO auto = r.getAuto();
			String tieneGarantia;
			if(r.isTieneGarantia()) {
				tieneGarantia = "Si";
			} else {
				tieneGarantia = "No";
			}
			String txtTipoService = null;
			String tipoService = r.getService();
			if(tipoService!=null) {
			switch(tipoService) {
				case "15mk" :
					txtTipoService = "15.000 Km";
					break;
				case "30mk" :
					txtTipoService = "30.000 Km";
					break;
				case "45mk" :
					txtTipoService = "45.000 Km";
					break;
				}
			}
			Object[] fila = {fechaEmitida, fechaTurno, horario, cliente.getNombre() + " "+ cliente.getApellido(), cliente.getTipoDocumento().getTipoDocumento(),cliente.getNumDocumento(),auto.getPaisPatente(),auto.getPatente(),notificada,estado,motivo,tieneGarantia,txtTipoService,motivoCancelacion};
			this.getModelPersonas().addRow(fila);
			redefinirDimensionTabla();
		}
		
	}
	
	public String pedirMotivoCancelacion(){
		String motivo = JOptionPane.showInputDialog("Ingrese un motivo de cancelacion:");
		return motivo;
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	public void advertirSeleccionIncorrectaAlFinalizar(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla o esta seleccionando mas de uno. \nPara finalizar una reserva porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	public void errorAlCancelarReserva(){
		JOptionPane.showMessageDialog(null,  "No se pudo cancelar la reserva.","Falla al intentar cancelar una reserva",0);		
	}
	
	public void advertirFaltaMotivoCancelacion(){
		JOptionPane.showMessageDialog(null,  "Debe ingresar un motivo para cancelar la reserva.","Falla al intentar cancelar una reserva",2);
	}
	
	public void advertirEdicionReservaFinalizada(){
		JOptionPane.showMessageDialog(null,  "La reserva que esta queriendo editar ya se encuentra finalizada.","Falla al intentar editar una reserva",2);
	}
	
	public void advertirUsuarioNoAutorizadoAgregarReserva(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede realizar una reserva","Falla al intentar realizar una reserva",2);	
	}
	
	public void advertirUsuarioNoAutorizadoEditarReserva(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede editar una reserva","Falla al intentar editar una reserva",2);	
	}
	
	public void advertirUsuarioNoAutorizadoCancelarReserva(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede cancelar una reserva","Falla al intentar cancelar una reserva",2);	
	}
	
	public void advertirUsuarioNoAutorizadoFinalizarReserva(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede finalizar una reserva","Falla al intentar finalizar una reserva",2);	
	}
}