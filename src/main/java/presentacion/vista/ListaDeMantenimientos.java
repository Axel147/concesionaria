package presentacion.vista;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.ClienteDTO;
import dto.MantenimientoDTO;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;

public class ListaDeMantenimientos extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaDeMantenimientos INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaMantenimientos;
	private DefaultTableModel modelMantenimientos;
	private  String[] nombreColumnas = {"Cliente","Tipo de Documento","Numero de Documento","Telefono","EMail","Pais Patente","Patente", "Modelo", "Año", "Motivo", "Mano de Obra", "Finalizado", "Nº de Factura"};
	
	//botones
	private JButton btnVerAutopartesUtilizadas;
	private JButton btnFinalizar;
	private JButton btnAgregarFiltro;
	private JButton btnVolver;
	private JButton btnModificarManoDeObra;
	private JPanel panelinferior;
	private JButton btnDeshacerFiltros;
	private JRadioButton rdbtnNombreCliente;
	private JRadioButton rdbtnApellidoCliente;
	private JRadioButton rdbtnNroDocumentoCliente;
	private JRadioButton rdbtnEmailCliente;
	private JRadioButton rdbtnPatenteAuto;
	private ButtonGroup rdbtnGroup;
	private JTextField txtBusqueda;
	
	
	
	
	
	public static ListaDeMantenimientos getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeMantenimientos(); 	
			return new ListaDeMantenimientos();
		}else return INSTANCE;
	}

	public ListaDeMantenimientos(){
		setMinimumSize(new Dimension(987, 181));
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
		@Override
	    public void windowClosing(WindowEvent e) {
			cerrar();
	    }
		});
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(100, 100, 1100, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		setLocationRelativeTo(null);
		setTitle("Mantenimientos");
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelMantenimientos = new DefaultTableModel(null,nombreColumnas){@Override public boolean isCellEditable(int row, int column) {return false;}};
		
		JScrollPane spClientes = new JScrollPane();
		panel.add(spClientes);
		tablaMantenimientos = new JTable(modelMantenimientos);
		tablaMantenimientos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaMantenimientos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tablaMantenimientos.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaMantenimientos.getColumnModel().getColumn(0).setResizable(false);
		tablaMantenimientos.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaMantenimientos.getColumnModel().getColumn(1).setResizable(false);
		
		spClientes.setViewportView(tablaMantenimientos);
			
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel.add(panelsuperior, BorderLayout.NORTH);
		
		txtBusqueda = new JTextField();
		panelsuperior.add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		rdbtnNombreCliente = new JRadioButton("Nombre Cliente");
		panelsuperior.add(rdbtnNombreCliente);
		
		rdbtnApellidoCliente = new JRadioButton("Apellido Cliente");
		panelsuperior.add(rdbtnApellidoCliente);
		
		rdbtnNroDocumentoCliente = new JRadioButton("Nro Documento Cliente");
		panelsuperior.add(rdbtnNroDocumentoCliente);
		
		rdbtnEmailCliente = new JRadioButton("Email Cliente");
		panelsuperior.add(rdbtnEmailCliente);
		
		rdbtnPatenteAuto = new JRadioButton("Patente Auto");
		panelsuperior.add(rdbtnPatenteAuto);
		
		rdbtnGroup = new ButtonGroup();
		
		rdbtnGroup.add(rdbtnNombreCliente);
		rdbtnGroup.add(rdbtnApellidoCliente);
		rdbtnGroup.add(rdbtnNroDocumentoCliente);
		rdbtnGroup.add(rdbtnEmailCliente);
		rdbtnGroup.add(rdbtnPatenteAuto);
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		btnAgregarFiltro.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnAgregarFiltro, "2, 2, fill, fill");
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		panelsuperior.add(btnDeshacerFiltros);
		
		panelinferior = new JPanel();
		panel.add(panelinferior, BorderLayout.SOUTH);
		
		btnVerAutopartesUtilizadas = new JButton("VerAutopartesUtilizadas");
		panelinferior.add(btnVerAutopartesUtilizadas);
		btnVerAutopartesUtilizadas.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnFinalizar = new JButton("Finalizar");
		panelinferior.add(btnFinalizar);
		btnFinalizar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnModificarManoDeObra = new JButton("ModificarManoDeObra");
		panelinferior.add(btnModificarManoDeObra);
		btnModificarManoDeObra.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelinferior.add(btnVolver);
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);
		
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}

	public JButton getBtnVerAutopartesUtilizadas() 
	{
		return btnVerAutopartesUtilizadas;
	}
	
	public JButton getBtnFinalizar() 
	{
		return btnFinalizar;
	}
	
	public JButton getBtnBuscar() {
		return btnAgregarFiltro;
	}
	
	public JButton getBtnVolver() {
		return btnVolver;
	}

	public DefaultTableModel getModelMantenimientos() 
	{
		return modelMantenimientos;
	}
	
	public JTable getTablaMantenimientos()
	{
		return tablaMantenimientos;
	}

	public JButton getBtnModificarManoDeObra() {
		return btnModificarManoDeObra;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JPanel getPanelsuperior() {
		return panelsuperior;
	}

	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}

	public JPanel getPanelinferior() {
		return panelinferior;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}

	public JRadioButton getRdbtnNombreCliente() {
		return rdbtnNombreCliente;
	}

	public JRadioButton getRdbtnApellidoCliente() {
		return rdbtnApellidoCliente;
	}

	public JRadioButton getRdbtnNroDocumentoCliente() {
		return rdbtnNroDocumentoCliente;
	}

	public JRadioButton getRdbtnEmailCliente() {
		return rdbtnEmailCliente;
	}

	public JRadioButton getRdbtnPatenteAuto() {
		return rdbtnPatenteAuto;
	}

	public ButtonGroup getRdbtnGroup() {
		return rdbtnGroup;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaMantenimientos();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }

	public void llenarTabla(List<MantenimientoDTO> mantenimientosEnTabla) {
		if(mantenimientosEnTabla.size()>0)tablaMantenimientos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		else tablaMantenimientos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		this.getModelMantenimientos().setRowCount(0); //Para vaciar la tabla
		this.getModelMantenimientos().setColumnCount(0);
		this.getModelMantenimientos().setColumnIdentifiers(this.getNombreColumnas());

		for (MantenimientoDTO m : mantenimientosEnTabla)
		{
			String nombre = m.getReserva().getCliente().getNombre();
			String apellido =  m.getReserva().getCliente().getApellido();
			String tipoDoc =  m.getReserva().getCliente().getTipoDocumento().getTipoDocumento();
			String numDoc =  m.getReserva().getCliente().getNumDocumento();
			String mail =  m.getReserva().getCliente().getMail();
			String tel =  m.getReserva().getCliente().getTelefono();
			String paisPatente = m.getReserva().getAuto().getPaisPatente().getPais();
			String patente = m.getReserva().getAuto().getPatente();
			String modelo = m.getReserva().getAuto().getModelo().getModelo();
			String year = m.getReserva().getAuto().getYear();
			String motivo = m.getReserva().getMotivo().toString();
			String manoDeObra = "US$ " + String.valueOf(m.getManoDeObra());
			String finalizado = "No";
			if(m.isFinalizado())finalizado = "Si";
			int idVenta = m.getVenta();
			String numFactura = "";
			if(idVenta!=0)numFactura = "0000" + idVenta;
			Object[] fila = {nombre + " " + apellido, tipoDoc, numDoc, tel, mail, paisPatente, patente, modelo, year, motivo, manoDeObra, finalizado, numFactura};
			this.getModelMantenimientos().addRow(fila);
			redefinirDimensionTabla();
		}
		
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla o esta seleccionando mas de uno. \nPorfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertirErrorModificarManoDeObra(){
		JOptionPane.showMessageDialog(null,  "No se pudo actualizar la mano de obra.","Error al actualizar la mano de obra",0);		
	}
	
	public void advertirUsuarioNoAutorizadoFinalizarMantenimiento(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede finalizar un mantenimiento","Falla al intentar finalizar mantenimiento",2);	
	}
	
	public void advertirUsuarioNoAutorizadoModificarManoDeObra(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede modificar la mano de obra un mantenimiento","Falla al intentar modificar la mano de obra de un mantenimiento",2);	
	}
	
	public void advertirUsuarioNoAutorizadoAgregarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede agrega una autoparte al mantenimiento","Falla al intentar agregar una autoparte a un mantenimiento",2);	
	}
	
	public void advertirUsuarioNoAutorizadoEliminarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede eliminar una autoparte del mantenimiento","Falla al intentar eliminar una autoparte de un mantenimiento",2);	
	}
	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.dispose();
	}
}
