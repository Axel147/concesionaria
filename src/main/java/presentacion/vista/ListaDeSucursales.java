package presentacion.vista;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
//import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
//import javax.swing.table.TableCellRenderer;
//import javax.swing.table.TableColumn;

//import dto.ClienteDTO;
import dto.PaisDTO;
import dto.SucursalDTO;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.Image;

import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Dimension;

public class ListaDeSucursales extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaDeSucursales INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaSucursal;
	private DefaultTableModel modelSucursal;
	private  String[] nombreColumnas = {"Nombre","Calle","Altura","Pais"};
	
	//botones
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnBuscar;
	private JButton btnVolver;
	
	
	//campo de busqueda
	private JTextField txtBusqueda;
	
	//etiquta de busqueda
		private JLabel lblBuscarPor;
	
	//botones seleccionables
	private JRadioButton rdbtnNombre;
	private JRadioButton rdbtnCalle;
	private JRadioButton rdbtnAltura;
	
	ButtonGroup rdbtngroup;
	
	
	public static ListaDeSucursales getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeSucursales(); 	
			return new ListaDeSucursales();
		}else return INSTANCE;
	}

	public ListaDeSucursales(){
		super();
		setMinimumSize(new Dimension(710, 142));
		
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1100, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		setTitle("Lista De Sucursales");
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelSucursal = new DefaultTableModel(null,nombreColumnas);
		
		JScrollPane spClientes = new JScrollPane();
		panel.add(spClientes);
		tablaSucursal = new JTable(modelSucursal);
//		tablaSucursal.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		
		tablaSucursal.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaSucursal.getColumnModel().getColumn(0).setResizable(false);
		tablaSucursal.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaSucursal.getColumnModel().getColumn(1).setResizable(false);
		
		spClientes.setViewportView(tablaSucursal);
		
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel.add(panelsuperior, BorderLayout.NORTH);
//		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
//				ColumnSpec.decode("216px"),
//				ColumnSpec.decode("72px"),
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,},
//			new RowSpec[] {
//				RowSpec.decode("38px"),
//				RowSpec.decode("23px"),}));
		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("214px"),
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblBuscarPor = new JLabel("Buscar por:");
		panelsuperior.add(lblBuscarPor, "1, 1");
		
		rdbtnNombre = new JRadioButton("Nombre");
		rdbtnNombre.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnNombre, "2, 1, fill, default");
		
		rdbtnCalle = new JRadioButton("Calle");
		rdbtnCalle.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnCalle, "3, 1, fill, default");
		
		rdbtnAltura = new JRadioButton("Altura");
		rdbtnAltura.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnAltura, "4, 1, fill, default");
	
		rdbtngroup = new ButtonGroup();
		rdbtngroup.add(rdbtnNombre);
		rdbtngroup.add(rdbtnCalle);
		rdbtngroup.add(rdbtnAltura);
		rdbtngroup.clearSelection();
		rdbtnNombre.setSelected(true);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setHorizontalAlignment(SwingConstants.LEFT);
		txtBusqueda.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelsuperior.add(txtBusqueda, "1, 2, fill, fill");
		txtBusqueda.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnBuscar, "2, 2, fill, fill");
		
		btnAgregar = new JButton("Agregar");
		panelsuperior.add(btnAgregar, "3, 2, fill, fill");
		btnAgregar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnEditar = new JButton("Editar");
		panelsuperior.add(btnEditar, "4, 2, fill, fill");
		btnEditar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnBorrar = new JButton("Borrar");
		panelsuperior.add(btnBorrar, "5, 2, fill, fill");
		btnBorrar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelsuperior.add(btnVolver, "6, 2, fill, fill");
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);

		
	}
	
//	private void redefinirDimensionTabla(){
//		JTable tabla = this.getTablaSucursales();
//        for (int i = 0; i < tabla.getColumnCount(); i++) {
//            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
//            TableColumn col = colModel.getColumn(i);
//            int width = 100;
//
//            TableCellRenderer renderer = col.getHeaderRenderer();
//            for (int r = 0; r < tabla.getRowCount(); r++) {
//                renderer = tabla.getCellRenderer(r, i);
//                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
//                width = Math.max(width, comp.getPreferredSize().width);
//            }
//            col.setPreferredWidth(width + 25);
//        }
//    }

	public void llenarTabla(List<SucursalDTO> sucursalesEnTabla) {
		this.getModelSucursales().setRowCount(0); //Para vaciar la tabla
		this.getModelSucursales().setColumnCount(0);
		this.getModelSucursales().setColumnIdentifiers(this.getNombreColumnas());
		
		for (SucursalDTO s : sucursalesEnTabla)
		{
			String nombre = s.getNombre();
			String apellido = s.getCalle();
			String altura = s.getAltura();
			int idPais = s.getPais().getIdPais();
			String nombrePais = s.getPais().getPais();
			PaisDTO pais = new PaisDTO(idPais, nombrePais);
			Object[] fila = {nombre, apellido, altura, pais};
			this.getModelSucursales().addRow(fila);
//			redefinirDimensionTabla();
		}
		
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar/borrar o esta seleccionando mas de uno. \nPara editar/borrar por favor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	public JRadioButton getRdbtnNombre() {
		return rdbtnNombre;
	}

	public JRadioButton getRdbtnCalle() {
		return rdbtnCalle;
	}

	public JRadioButton getRdbtnAltura() {
		return rdbtnAltura;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public DefaultTableModel getModelSucursales() 
	{
		return modelSucursal;
	}
	
	public JTable getTablaSucursales()
	{
		return tablaSucursal;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public void advertenciaSucursalUtilizandose() {
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar la sucursal. \nRevise que no este siendo utilizada por algun automovil. De ser asi debe primero eliminar o modificar todos los automoviles  que la esten utilizando antes de eliminar la sucursal.","Cuidado",2);
	}

}
