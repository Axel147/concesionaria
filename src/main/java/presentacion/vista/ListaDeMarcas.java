package presentacion.vista;

import java.awt.Image;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import dto.MarcaDTO;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class ListaDeMarcas extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaDeMarcas INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaMarcas;
	private DefaultTableModel modelMarcas;
	private  String[] nombreColumnas = {"Nombre"};
	
	//botones
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnBuscar;
	private JButton btnVolver;
	
	//campo de busqueda
	private JTextField txtBusqueda;
	
	//etiquta de busqueda
	private JLabel lblBuscarPor;
	
	private ListaDeAutomoviles ventanaListaDeAutomoviles;
	private int llamadoDesde;
	private VentanaAutopartes ventanaAutopartes;
	
	public static ListaDeMarcas getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeMarcas(); 	
			return new ListaDeMarcas();
		}else return INSTANCE;
	}

	public ListaDeMarcas(){
		super();
		setMinimumSize(new Dimension(348, 177));
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        cerrar();
		    }
		});
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(100, 100, 320, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setTitle("Lista De Marcas");

		setLocationRelativeTo(null);
//		setResizable(false);
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelMarcas = new DefaultTableModel(null,nombreColumnas);
		
		JScrollPane spMarcas = new JScrollPane();
		panel.add(spMarcas);
		tablaMarcas = new JTable(modelMarcas);
		tablaMarcas.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		
		tablaMarcas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaMarcas.getColumnModel().getColumn(0).setResizable(false);
		
		spMarcas.setViewportView(tablaMarcas);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new BorderLayout(0, 0));
		panel.add(panel2,BorderLayout.NORTH); 
		
		JPanel panel3 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel3.getLayout();
		flowLayout.setHgap(33);
		panel2.add(panel3,BorderLayout.SOUTH); 
		
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel2.add(panelsuperior, BorderLayout.NORTH);
//		panel.add(panelsuperior, BorderLayout.NORTH);
//		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
//				ColumnSpec.decode("216px"),
//				ColumnSpec.decode("72px"),
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,},
//			new RowSpec[] {
//				RowSpec.decode("38px"),
//				RowSpec.decode("23px"),}));
		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("90px"),
				ColumnSpec.decode("max(88dlu;pref):grow"),
				ColumnSpec.decode("left:67px"),
				ColumnSpec.decode("max(16dlu;pref)"),},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblBuscarPor = new JLabel("Buscar:");
		panelsuperior.add(lblBuscarPor, "1, 2");
			
		txtBusqueda = new JTextField();
		txtBusqueda.setHorizontalAlignment(SwingConstants.LEFT);
		txtBusqueda.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelsuperior.add(txtBusqueda, "2, 2, fill, fill");
		txtBusqueda.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnBuscar, "3, 2, fill, fill");
		
		btnAgregar = new JButton("Agregar");
//		panelsuperior.add(btnAgregar, "1, 3, fill, fill");
		panel3.add(btnAgregar);
		btnAgregar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnEditar = new JButton("Editar");
//		panelsuperior.add(btnEditar, "2, 3, fill, fill");
		panel3.add(btnEditar);
		btnEditar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnBorrar = new JButton("Borrar");
//		panelsuperior.add(btnBorrar, "3, 3, fill, fill");
		panel3.add(btnBorrar);
		btnBorrar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelsuperior.add(btnVolver, "1, 1, fill, fill");
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnBuscar() {
		return btnBuscar;
	}

	public DefaultTableModel getModelMarcas() 
	{
		return modelMarcas;
	}
	
	public JTable getTablaMarcas()
	{
		return tablaMarcas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

	public void llenarTabla(List<MarcaDTO> marcasEnTabla) {
		this.getModelMarcas().setRowCount(0); //Para vaciar la tabla
		this.getModelMarcas().setColumnCount(0);
		this.getModelMarcas().setColumnIdentifiers(this.getNombreColumnas());

		for (MarcaDTO marca : marcasEnTabla)
		{
			String nombre = marca.getNombre();
			Object[] fila = {nombre};
			this.getModelMarcas().addRow(fila);
		}
		
	}
	
	public void cerrar() {
		if(isLlamadoDesde()== 1) {
			this.ventanaListaDeAutomoviles.setEnabled(true);
		}else if(isLlamadoDesde() == 2) {
			this.ventanaAutopartes.setEnabled(true);
		}
		this.dispose();
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para editar o esta seleccionando mas de uno. \nPara editar por favor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertirSeleccionBorrarIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla para borrar o esta seleccionando mas de uno. \nPara borrar por favor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}

	public void advertenciaMarcaUtilizandose() {
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar la marca. \nRevise que no este siendo utilizada por algun modelo. De ser asi debe primero eliminar o \nmodificar todos los elementos que la esten utilizando antes de eliminar la marca.","Cuidado",2);		
	}

	public ListaDeAutomoviles getVentanaListaDeAutomoviles() {
		return ventanaListaDeAutomoviles;
	}

	public void setVentanaListaDeAutomoviles(ListaDeAutomoviles ventanaListaDeAutomoviles) {
		this.ventanaListaDeAutomoviles = ventanaListaDeAutomoviles;
	}

	public int isLlamadoDesde() {
		return llamadoDesde;
	}

	public void setLlamadoDesde(int llamadoDesde) {
		this.llamadoDesde = llamadoDesde;
	}

	public void setVentanaAutopartes(VentanaAutopartes ventanaAutopartes) {
		this.ventanaAutopartes = ventanaAutopartes;
	}

	public VentanaAutopartes getVentanaAutopartes() {
		return ventanaAutopartes;
	}
	
	

}