package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;
import presentacion.controlador.ControladorCampos;

import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JRadioButton;

public class VentanaUsuario extends JFrame {

	private static final long serialVersionUID = 1L;
	private static VentanaUsuario INSTANCE;
	
	private int modo;// 0-> agregar, 1-> editar 
	
	private JPanel contentPane;
	
	private JLabel lblDescripcion;
	
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtDocumento;
	private JTextField txtUsuario;
	private JTextField txtMail;
	private JTextField txtPassword;
	
	private JComboBox<PaisDTO> comboPaises;
	private JComboBox<TipoDocumentoDTO> comboTiposDoc;
	private JComboBox<TipoUsuarioDTO> comboTiposUsr;
	private JComboBox<SucursalDTO> comboSucursales;
	
	private JRadioButton rdbtnSexoMasculino;
	private JRadioButton rdbtnSexoFemenino;
	private ButtonGroup rdbtnGroupSexo;
	
	private JLabel lblErrorNombre;
	private JLabel lblErrorApellido;
	private JLabel lblErrorSexo;
	private JLabel lblErrorPais;
	private JLabel lblErrorTipoDoc;
	private JLabel lblErrorDocumento;
	private JLabel lblErrorUsuario;
	private JLabel lblErrorMail;
	private JLabel lblErrorPassword;
	private JLabel lblErrorTipoUsr;
	private JLabel lblErrorSucursal;
	
	private JButton btnUsuario;

	private ListaDeUsuarios ventanaListaDeUsuarios;
	
	public static VentanaUsuario getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaUsuario(); 	
			return new VentanaUsuario();
		}
		else
			return INSTANCE;
	}
	
	private VentanaUsuario() {
		super();
		
		setBounds(100, 100, 487, 720);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(null);
		
		lblDescripcion = new JLabel();
		lblDescripcion.setBounds(30, 11, 343, 15);
		panel.add(lblDescripcion);
		
		JLabel lblNombre = new JLabel("Nombre*");
		lblNombre.setBounds(44, 75, 125, 15);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido*");
		lblApellido.setBounds(44, 125, 125, 15);
		panel.add(lblApellido);
		
		JLabel lblSexo = new JLabel("Sexo*");
		lblSexo.setBounds(44, 175, 125, 15);
		panel.add(lblSexo);
		
		
		JLabel lblPaisResidencia = new JLabel("Pais de Residencia*");
		lblPaisResidencia.setBounds(44, 225, 125, 15);
		panel.add(lblPaisResidencia);
		
		
		JLabel lblTipoDocumento = new JLabel("Tipo de Documento*");
		lblTipoDocumento.setBounds(44, 275, 125, 15);
		panel.add(lblTipoDocumento);

		
		JLabel lblDocumento = new JLabel("Nro Documento*");
		lblDocumento.setBounds(44, 325, 125, 15);
		panel.add(lblDocumento);
		
		JLabel lblUsuario = new JLabel("Usuario*");
		lblUsuario.setBounds(44, 375, 125, 15);
		panel.add(lblUsuario);
		
		JLabel lblMail = new JLabel("Mail*");
		lblMail.setBounds(44, 425, 125, 15);
		panel.add(lblMail);
		
		JLabel lblPassword = new JLabel("Password*");
		lblPassword.setBounds(44, 475, 125, 15);
		panel.add(lblPassword);
		
		JLabel lblTipoUsuario = new JLabel("Tipo de Usuario*");
		lblTipoUsuario.setBounds(44, 525, 125, 15);
		panel.add(lblTipoUsuario);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(214, 75, 200, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(214, 125, 200, 20);
		panel.add(txtApellido);
		
		txtDocumento = new JTextField();
		txtDocumento.setColumns(10);
		txtDocumento.setBounds(214, 325, 200, 20);
		panel.add(txtDocumento);
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(214, 375, 200, 20);
		panel.add(txtUsuario);
		
		txtMail = new JTextField();
		txtMail.setColumns(10);
		txtMail.setBounds(214, 425, 200, 20);
		panel.add(txtMail);
		
		txtPassword = new JTextField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(214, 475, 200, 20);
		panel.add(txtPassword);
		
		comboPaises = new JComboBox<PaisDTO>();
		comboPaises.setBounds(214, 225, 200, 20);
		panel.add(comboPaises);
		
		comboTiposDoc = new JComboBox<TipoDocumentoDTO>();
		comboTiposDoc.setBounds(214, 275, 200, 20);
		panel.add(comboTiposDoc);

		comboTiposUsr = new JComboBox<TipoUsuarioDTO>();
		comboTiposUsr.setBounds(214, 525, 200, 20);
		panel.add(comboTiposUsr);
		
		rdbtnSexoMasculino = new JRadioButton("Masculino");
		rdbtnSexoMasculino.setBounds(214, 175, 100, 20);
		panel.add(rdbtnSexoMasculino);
		
		rdbtnSexoFemenino = new JRadioButton("Femenino");
		rdbtnSexoFemenino.setBounds(314, 175, 100, 20);
		panel.add(rdbtnSexoFemenino);
		
		rdbtnGroupSexo = new ButtonGroup();
		rdbtnGroupSexo.add(rdbtnSexoMasculino);
		rdbtnGroupSexo.add(rdbtnSexoFemenino);
		rdbtnGroupSexo.clearSelection();

		
		btnUsuario = new JButton("New button");
		btnUsuario.setBounds(311, 638, 150, 33);
		panel.add(btnUsuario);
		
		JLabel lblCamposObligatorios = new JLabel("Los campos marcados con un asterisco son obligatorios");
		lblCamposObligatorios.setBounds(30, 36, 343, 15);
		panel.add(lblCamposObligatorios);
		
		lblErrorNombre = new JLabel();
		lblErrorNombre.setForeground(Color.RED);
		lblErrorNombre.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorNombre.setBounds(214, 95, 215, 10);
		lblErrorNombre.setVisible(false);
		panel.add(lblErrorNombre);
		
		lblErrorApellido = new JLabel();
		lblErrorApellido.setForeground(Color.RED);
		lblErrorApellido.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorApellido.setBounds(214, 145, 215, 10);
		lblErrorApellido.setVisible(false);
		panel.add(lblErrorApellido);
		
		lblErrorSexo = new JLabel("");
		lblErrorSexo.setForeground(Color.RED);
		lblErrorSexo.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorSexo.setBounds(214, 195, 215, 10);
		lblErrorSexo.setVisible(false);
		panel.add(lblErrorSexo);
		
		lblErrorPais = new JLabel("");
		lblErrorPais.setForeground(Color.RED);
		lblErrorPais.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorPais.setBounds(214, 245, 215, 10);
		lblErrorPais.setVisible(false);
		panel.add(lblErrorPais);
		
		lblErrorTipoDoc = new JLabel("");
		lblErrorTipoDoc.setForeground(Color.RED);
		lblErrorTipoDoc.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorTipoDoc.setBounds(214, 295, 215, 10);
		lblErrorTipoDoc.setVisible(false);
		panel.add(lblErrorTipoDoc);
		
		lblErrorDocumento = new JLabel();
		lblErrorDocumento.setForeground(Color.RED);
		lblErrorDocumento.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorDocumento.setBounds(214, 345, 215, 10);
		lblErrorDocumento.setVisible(false);
		panel.add(lblErrorDocumento);
		
		lblErrorUsuario = new JLabel();
		lblErrorUsuario.setForeground(Color.RED);
		lblErrorUsuario.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorUsuario.setBounds(214, 395, 215, 10);
		lblErrorUsuario.setVisible(false);
		panel.add(lblErrorUsuario);
		
		lblErrorMail = new JLabel();
		lblErrorMail.setForeground(Color.RED);
		lblErrorMail.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorMail.setBounds(214, 445, 215, 10);
		lblErrorMail.setVisible(false);
		panel.add(lblErrorMail);
		
		lblErrorPassword = new JLabel();
		lblErrorPassword.setForeground(Color.RED);
		lblErrorPassword.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorPassword.setBounds(214, 495, 215, 10);
		lblErrorPassword.setVisible(false);
		panel.add(lblErrorPassword);
		
		lblErrorTipoUsr = new JLabel();
		lblErrorTipoUsr.setForeground(Color.RED);
		lblErrorTipoUsr.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorTipoUsr.setBounds(214, 545, 215, 10);
		lblErrorTipoUsr.setVisible(false);
		panel.add(lblErrorTipoUsr);
		
		JLabel lblSucursal = new JLabel("Sucursal*");
		lblSucursal.setBounds(44, 575, 125, 15);
		panel.add(lblSucursal);
		
		comboSucursales = new JComboBox<SucursalDTO>();
		comboSucursales.setBounds(214, 575, 200, 20);
		panel.add(comboSucursales);
		
		lblErrorSucursal = new JLabel("");
		lblErrorSucursal.setForeground(Color.RED);
		lblErrorSucursal.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblErrorSucursal.setBounds(214, 595, 215, 10);
		lblErrorSucursal.setVisible(false);
		panel.add(lblErrorSucursal);
		
	}
	
	public void mostrarVentana() {
		
		this.setVisible(true);
		
	}
	
	public void modoAgregar() {
		
		this.modo = 0;
		this.lblDescripcion.setText("Por favor introduzca los datos del usuario a agregar");
		this.setTitle("Agregar nuevo usuario");
		this.btnUsuario.setText("Agregar");
		this.getComboPaises().setSelectedIndex(-1);
		this.getComboTiposUsr().setSelectedIndex(-1);
		
	}
	
	public void modoEditar() {
		
		this.modo = 1;
		this.lblDescripcion.setText("Por favor modifique los datos del usuario a editar");
		this.setTitle("Editar usuario");
		this.btnUsuario.setText("Guardar Cambios");
		
	}
	
	public void mostrarUsuario(UsuarioDTO usuario) {
		
		this.txtNombre.setText(usuario.getNombre());
		this.txtApellido.setText(usuario.getApellido());
		
		switch(usuario.getSexo()) {
		case "Masculino":
			this.rdbtnSexoMasculino.setSelected(true);
			break;
		case "Femenino":
			this.rdbtnSexoFemenino.setSelected(true);
		}
		
		this.comboPaises.setSelectedItem(usuario.getPaisResidencia());
		this.comboTiposDoc.setSelectedItem(usuario.getTipoDocumento());
		this.txtDocumento.setText(usuario.getNroDocumento());
		this.txtUsuario.setText(usuario.getNombreUsuario());
		this.txtMail.setText(usuario.getMail());
		this.txtPassword.setText(usuario.getPass());
		this.comboTiposUsr.setSelectedItem(usuario.getTipoUsuario());
		this.comboSucursales.setSelectedItem(usuario.getSucursal());
	
	}
	
	public void cerrar() {
		
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.comboPaises.setSelectedIndex(-1);
		this.comboTiposDoc.setSelectedIndex(-1);
		this.txtDocumento.setText(null);
		this.txtUsuario.setText(null);
		this.txtPassword.setText(null);
		this.txtMail.setText(null);
		this.comboTiposUsr.setSelectedIndex(-1);
		this.rdbtnGroupSexo.clearSelection();
		this.ventanaListaDeUsuarios.setEnabled(true);
		ocultarLabelsError();
		
		this.dispose();
	}

	public void ocultarLabelsError() {
		this.lblErrorNombre.setVisible(false);
		this.lblErrorApellido.setVisible(false);
		this.lblErrorSexo.setVisible(false);
		this.lblErrorPais.setVisible(false);
		this.lblErrorTipoUsr.setVisible(false);
		this.lblErrorDocumento.setVisible(false);
		this.lblErrorUsuario.setVisible(false);
		this.lblErrorPassword.setVisible(false);
		this.lblErrorMail.setVisible(false);
		this.lblErrorTipoUsr.setVisible(false);
		this.lblErrorSucursal.setVisible(false);
	}
	
	public void cargarPaises(List<PaisDTO> paises){
		PaisDTO[] arrayPaises = new PaisDTO[paises.size()];
		int i = 0;
		for(PaisDTO pais : paises ) {
			arrayPaises[i] = pais;
			i++;
		}
		comboPaises.setModel(new DefaultComboBoxModel<PaisDTO>(arrayPaises));
	}
	
	public void cargarTiposDocumento(List<TipoDocumentoDTO> tiposDoc){
		TipoDocumentoDTO[] arrayTiposDoc = new TipoDocumentoDTO[tiposDoc.size()];
		int i = 0;
		for(TipoDocumentoDTO tipoDoc : tiposDoc ) {
			arrayTiposDoc[i] = tipoDoc;
			i++;
		}
		comboTiposDoc.setModel(new DefaultComboBoxModel<TipoDocumentoDTO>(arrayTiposDoc));
	}
	
	public void cargarTiposUsuario(List<TipoUsuarioDTO> tiposUsuario){
		TipoUsuarioDTO[] arrayTiposUsr = new TipoUsuarioDTO[tiposUsuario.size()];
		int i = 0;
		for(TipoUsuarioDTO tipoUsuario : tiposUsuario ) {
			arrayTiposUsr[i] = tipoUsuario;
			i++;
		}
		comboTiposUsr.setModel(new DefaultComboBoxModel<TipoUsuarioDTO>(arrayTiposUsr));
	}
	
	public void cargarSucursales(List<SucursalDTO> sucursales){
		SucursalDTO[] arraySucursales = new SucursalDTO[sucursales.size()];
		int i = 0;
		for(SucursalDTO sucursal : sucursales ) {
			arraySucursales[i] = sucursal;
			i++;
		}
		comboSucursales.setModel(new DefaultComboBoxModel<SucursalDTO>(arraySucursales));
	}

	
	public void errorAgregar() {
		JOptionPane.showMessageDialog(null,  "No se ha podido agregar el nuevo usuario. \nVerifique los datos e inténtelo nuevamente.","Error al intentar agregar un nuevo usuario",1);		
	}
	
	public void errorEditar() {
		JOptionPane.showMessageDialog(null,  "No se han podido modificar los datos del usuario. \nVerifique los datos e inténtelo nuevamente.","Error al intentar editar un usuario",1);		
	}
	
	public void errorCargar() {
		JOptionPane.showMessageDialog(null,  "Uno o mas de los de los campos es inválido. Revise los datos antes de ingresarlos nuevamente","Error",1);		
	}

	public int getModo() {
		return modo;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JTextField getTxtDocumento() {
		return txtDocumento;
	}

	public JTextField getTxtUsuario() {
		return txtUsuario;
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public JTextField getTxtPassword() {
		return txtPassword;
	}

	public JComboBox<PaisDTO> getComboPaises() {
		return comboPaises;
	}

	public JComboBox<TipoDocumentoDTO> getComboTiposDoc() {
		return comboTiposDoc;
	}

	public JComboBox<TipoUsuarioDTO> getComboTiposUsr() {
		return comboTiposUsr;
	}

	public JRadioButton getRdbtnSexoMasculino() {
		return rdbtnSexoMasculino;
	}

	public JRadioButton getRdbtnSexoFemenino() {
		return rdbtnSexoFemenino;
	}

	public ButtonGroup getRdbtnGroupSexo() {
		return rdbtnGroupSexo;
	}

	public JButton getBtnUsuario() {
		return btnUsuario;
	}
	
	public String getSexo() {
		String sexo = null;
		if(this.rdbtnSexoMasculino.isSelected()) {
			sexo = "Masculino";
		} else if(this.rdbtnSexoFemenino.isSelected()) {
			sexo = "Femenino";
		}
		return sexo;
	}
	

	public void setTxtNombre(String txtNombre) {
		this.txtNombre.setText(txtNombre);
	}

	public void setTxtApellido(String txtApellido) {
		this.txtApellido.setText(txtApellido);
	}

	public void setTxtDocumento(String txtDocumento) {
		this.txtDocumento.setText(txtDocumento);
	}

	public void setTxtUsuario(String txtUsuario) {
		this.txtUsuario.setText(txtUsuario);
	}

	public void setTxtMail(String txtMail) {
		this.txtMail.setText(txtMail);
	}

	public void setTxtPassword(String txtPassword) {
		this.txtPassword.setText(txtPassword);
	}

	public void setComboTipos(JComboBox<TipoUsuarioDTO> comboTipos) {
		this.comboTiposUsr = comboTipos;
	}

	public JComboBox<SucursalDTO> getComboSucursales() {
		return comboSucursales;
	}

	public JLabel getLblErrorSucursal() {
		return lblErrorSucursal;
	}

	//validaciones
	public boolean validarCampos() {
		//uso todos estos ifs para que me marque todos los campos erroneos de una
		boolean esValido = true;
		if(!validarNombre()) {
			esValido = false;
		}
		if(!validarApellido()) {
			esValido = false;
		}
		if(!validarSexo()) {
			esValido = false;
		}
		if(!validarPais()) {
			esValido = false;
		}
		if(!validarTipoDoc()) {
			esValido = false;
		}
		if(!validarDocumento()) {
			esValido = false;
		}
		if(!validarUsuario()) {
			esValido = false;
		}
		if(!validarMail()) {
			esValido = false;
		}
		if(!validarPassword()) {
			esValido = false;
		}
		if(!validarTipoUsr()) {
			esValido = false;
		}
		if(!validarSucursal()) {
			esValido = false;
		}
		
		return esValido;
	}
	
	public boolean validarNombre() {
		boolean esValido = true;
		String textoNombre = this.getTxtNombre().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoNombre)) {
			esValido=false;
			textoError = "Campo Obligatorio";
		} else if(!ControladorCampos.NombreValido(textoNombre)) {
			esValido=false;
			if (textoNombre.length()>45) {
				textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Nombre Inválido";
			}
			}
		if(!esValido) {
			this.lblErrorNombre.setText(textoError);
			this.lblErrorNombre.setVisible(true);
		}
		
		return esValido;
			
	}
	
	public boolean validarApellido() {
		boolean esValido = true;
		String textoApellido = this.getTxtApellido().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoApellido)) {
			this.lblErrorApellido.setText("Campo Obligatorio");
			esValido=false;
		} else if(!ControladorCampos.ApellidoValido(textoApellido)) {
			esValido=false;
			 if(textoApellido.length()>45) {
				this.lblErrorApellido.setText("Máximo de caracteres excedido");
			} else {
				this.lblErrorApellido.setText("Apellido Inválido");
				}
		}
		
		if(!esValido) {
			this.lblErrorApellido.setVisible(true);
		}
		
		return esValido;
	}
	
	public boolean validarSexo() {
		boolean esValido = true;
		if(this.rdbtnGroupSexo.getSelection()==null) {
			this.lblErrorSexo.setText("Debe seleccionar una opcion");
			esValido = false;
		}
		if(!esValido) {
			this.lblErrorSexo.setVisible(true);
		}
		return esValido;
	}
	
	public boolean validarPais() {
		boolean esValido = true;
		if(this.comboPaises.getSelectedItem()==null) {
			this.lblErrorPais.setText("Debe seleccionar un país");
			esValido = false;
		}
		if(!esValido) {
			this.lblErrorPais.setVisible(true);
		}
		return esValido;
	}
	
	public boolean validarTipoDoc() {
		boolean esValido = true;
		if(this.comboTiposDoc.getSelectedItem()==null) {
			this.lblErrorTipoDoc.setText("Debe seleccionar un tipo de documento");
			esValido = false;
		}
		if(!esValido) {
			this.lblErrorTipoDoc.setVisible(true);
		}
		return esValido;
	}
	
	public boolean validarDocumento() {
		boolean esValido = true;
		String textoDocumento = this.getTxtDocumento().getText();
		String tipoDocumento = ((TipoDocumentoDTO) this.getComboTiposDoc().getSelectedItem()).getTipoDocumento();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoDocumento)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.NumeroDocumentoValido(textoDocumento,tipoDocumento)){
			esValido = false;
			if(textoDocumento.length()>45) {
			textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Número de Documento Inválido";
			}
		}
		if(!esValido) {
			this.lblErrorDocumento.setText(textoError);
			this.lblErrorDocumento.setVisible(true);
		}
		
		return esValido;
		
	}
	
	public boolean validarUsuario() {
		boolean esValido = true;
		String textoUsuario = this.getTxtUsuario().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoUsuario)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		}else if(!ControladorCampos.UsuarioValido(textoUsuario)){
			esValido=false;
			if(textoUsuario.length()>45) {
			textoError = "Máximo de caracteres excedido";
			}
			textoError = "Usuario Inválido";
		}
		if(!esValido) {
			this.lblErrorUsuario.setText(textoError);
			this.lblErrorUsuario.setVisible(true);
		}
		
		return esValido;
	}
	
	public boolean validarMail() {
		boolean esValido = true;
		String textoMail = this.getTxtMail().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoMail)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.EmailValido(textoMail)){
			esValido=false;
			if(textoMail.length()>45){
				textoError = "Máximo de caracteres excedido";
			} else {
				textoError = "Mail Inválido";
			}
		}
			
		if(!esValido) {
			this.lblErrorMail.setText(textoError);
			this.lblErrorMail.setVisible(true);
		}
		
		return esValido;
	}
	
	public boolean validarPassword() {
		boolean esValido = true;
		String textoPassword = this.getTxtPassword().getText();
		String textoError = null;
		if(ControladorCampos.CampoVacio(textoPassword)) {
			textoError = "Campo Obligatorio";
			esValido=false;
		} else if(!ControladorCampos.PasswordValida(textoPassword)) {
			esValido=false;
			if(textoPassword.length()<8) {
				textoError = "La contraseña debe tener al menos 8 caracteres";
			} else if(textoPassword.length()>20) {
				textoError = "La contraseña puede tener hasta 20 caracteres";
			} else {
				textoError = "La contraseña solo puede tener letras y números";
			}
		}
		
		if(!esValido) {
			this.lblErrorPassword.setText(textoError);
			this.lblErrorPassword.setVisible(true);
		}
		return esValido;
	}
	
	public boolean validarTipoUsr() {
		boolean esValido = true;
		if(this.comboTiposUsr.getSelectedItem()==null) {
			this.lblErrorTipoUsr.setText("Debe seleccionar un tipo de usuario");
			esValido=false;
		}
		if(!esValido) {
				this.lblErrorTipoUsr.setVisible(true);
			}
			
			return esValido;
		
	}
	
	public boolean validarSucursal() {
		boolean esValido = true;
		if(this.comboSucursales.getSelectedItem()==null) {
			this.lblErrorSucursal.setText("Debe seleccionar una sucursal");
			esValido = false;
		}
		if(!esValido) {
			this.lblErrorSucursal.setVisible(true);
		}
		return esValido;
	}
	
	public void setVentanaBloqueada(ListaDeUsuarios ventanaListaDeUsuarios) {
		this.ventanaListaDeUsuarios = ventanaListaDeUsuarios;
	}
	
}
