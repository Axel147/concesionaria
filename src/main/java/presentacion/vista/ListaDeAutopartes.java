package presentacion.vista;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.AutoparteDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.SucursalDTO;
import dto.TipoAutoparteDTO;
import dto.UsuarioDTO;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.table.TableModel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;

public class ListaDeAutopartes extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static ListaDeAutopartes INSTANCE;

	private static final String[] camposBusqueda = {"Tipos de autoparte","Marca/Modelo","Año","Sucursal"};
	private DefaultTableModel modelAutopartes;
	private  String[] nombreColumnas = {"Autoparte","Año","Sucursal","Stock Mínimo" ,"Stock Actual","Precio"};
	private String[] arrayYear = new String[Calendar.getInstance().get(Calendar.YEAR)-2000+1];
	private JButton btnBuscar;
	private JButton btnRefrescar;
	private boolean modoSeleccion;
	private int tipoSeleccion;
	private JFileChooser buscadorDeArchivos;
	private VentanaVentas ventanaVentasLlamadora;
	private boolean llamadoPorVentas;
	private ListaAutopartesXmantenimiento listaAutopartesXmantenimientoLlamadora;
	private boolean llamadoPorMantenimiento;
	private JScrollPane scrollPane;
	private JTable tablaAutopartes;
	private JPanel panelsuperior;
	private JPanel panelsuperiorizquierdo;
	private JLabel lblFiltrar;
	private JComboBox comboCampos;
	private JPanel panelsuperiorderecho;
	private JButton btnAgregarFiltro;
	private JButton btnDeshacerFiltros;
	private JPanel panelBusqueda;
	private JPanel panelVacio;
	private JPanel panelCombo;
	private JPanel panelDobleCombo;
	private JComboBox comboValor_2;
	private JPanel panelinferior;
	private JButton btnIngresarStockPor;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnVolver;
	private JComboBox comboValor_1;
	private JComboBox comboValor;
	private boolean tablaVacia;

	public static ListaDeAutopartes getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeAutopartes(); 	
			return new ListaDeAutopartes();
		}else return INSTANCE;
	}
	
	private ListaDeAutopartes() 
	{
		super();
		setFont(new Font("Verdana", Font.BOLD, 14));
		setTitle("Vista de Autopartes");
		initialize();
	}


	private void initialize() 
	{
		llenarYear();
		setMinimumSize(new Dimension(714, 188));
		setBounds(100, 100, 917, 463);
		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setLocationRelativeTo(null);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		modelAutopartes = new DefaultTableModel(null,nombreColumnas){@Override public boolean isCellEditable(int row, int column) {return false;}};
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		
		ImageIcon imagenConfig = new ImageIcon(ListaDeAutomoviles.class.getResource("/iconos/config.png"));
		imagenConfig = new ImageIcon(imagenConfig.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		tablaAutopartes = new JTable(modelAutopartes);
		tablaAutopartes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPane.setViewportView(tablaAutopartes);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				if(getWidth()<794){
					if(tablaVacia)tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					else tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				}
				else tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			}
		});
		
		panelsuperior = new JPanel();
		getContentPane().add(panelsuperior, BorderLayout.NORTH);
		panelsuperior.setLayout(new BorderLayout(0, 0));
		
		panelsuperiorizquierdo = new JPanel();
		panelsuperior.add(panelsuperiorizquierdo, BorderLayout.WEST);
		panelsuperiorizquierdo.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("160px"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("14px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),}));
		
		lblFiltrar = new JLabel("Filtrar por:");
		panelsuperiorizquierdo.add(lblFiltrar, "2, 2, center, center");
		
		comboCampos = new JComboBox(new DefaultComboBoxModel(camposBusqueda));
		panelsuperiorizquierdo.add(comboCampos, "2, 4");
		
		panelsuperiorderecho = new JPanel();
		panelsuperior.add(panelsuperiorderecho, BorderLayout.EAST);
		panelsuperiorderecho.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("170px"),},
			new RowSpec[] {
				RowSpec.decode("25px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		panelsuperiorderecho.add(btnAgregarFiltro, "1, 1, fill, fill");
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		panelsuperiorderecho.add(btnDeshacerFiltros, "1, 3");
		
		panelBusqueda = new JPanel();
		panelsuperior.add(panelBusqueda, BorderLayout.CENTER);
		panelBusqueda.setLayout(new CardLayout(0, 0));
		
		panelVacio = new JPanel();
		panelBusqueda.add(panelVacio, "panelVacio");
		
		panelCombo = new JPanel();
		panelBusqueda.add(panelCombo, "panelCombo");
		panelCombo.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("15dlu"),
				ColumnSpec.decode("160px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("14px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		comboValor = new JComboBox();
		panelCombo.add(comboValor, "2, 4, fill, default");
		
		panelDobleCombo = new JPanel();
		panelBusqueda.add(panelDobleCombo, "panelDobleCombo");
		panelDobleCombo.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("15px"),
				ColumnSpec.decode("160px"),
				ColumnSpec.decode("15px"),
				ColumnSpec.decode("160px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;min):grow"),},
			new RowSpec[] {
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("14px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),}));
		
		comboValor_1 = new JComboBox();
		panelDobleCombo.add(comboValor_1, "2, 4, fill, default");
		
		comboValor_2 = new JComboBox();
		panelDobleCombo.add(comboValor_2, "4, 4");
		
		panelinferior = new JPanel();
		getContentPane().add(panelinferior, BorderLayout.SOUTH);
		
		btnIngresarStockPor = new JButton("Ingresar Stock Por CSV");
		panelinferior.add(btnIngresarStockPor);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelinferior.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelinferior.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelinferior.add(btnBorrar);
		
		btnVolver = new JButton("Volver");
		btnVolver.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelinferior.add(btnVolver);
		
		buscadorDeArchivos = new JFileChooser(); 
        buscadorDeArchivos.setAcceptAllFileFilterUsed(false);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos csv","csv");
        buscadorDeArchivos.setFileFilter(filtro);
		
	}
	
	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaAutopartes();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }
	
	public JTable getTablaAutopartes(){
		return tablaAutopartes;
	}
	
	public DefaultTableModel getModelAutopartes(){
		return modelAutopartes;
	}
	
	public String[] getNombreColumnas(){
		return nombreColumnas;
	}
	
	public JButton getBtnAgregar(){
		return btnAgregar;
	}

	public JButton getBtnBorrar(){
		return btnBorrar;
	}
	
	public JButton getBtnIngresarStockPorCSV() {
		return btnIngresarStockPor;
	}
	
	public JFileChooser getBuscadorArchivos() {
		return buscadorDeArchivos;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JPanel getPanelBusqueda() {
		return panelBusqueda;
	}

	public JPanel getPanelVacio() {
		return panelVacio;
	}

	public JPanel getPanelCombo() {
		return panelCombo;
	}

	public JPanel getPanelDobleCombo() {
		return panelDobleCombo;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}

	public JComboBox getComboCampos() {
		return comboCampos;
	}

	public JComboBox getComboValor() {
		return comboValor;
	}

	public JComboBox getComboValor_1() {
		return comboValor_1;
	}

	public JComboBox getComboValor_2() {
		return comboValor_2;
	}

	public void mostrarVentana(){
		this.setVisible(true);
	}

	public boolean isModoSeleccion() {
		return modoSeleccion;
	}

	public void setModoSeleccion(boolean modoSeleccion) {
		this.modoSeleccion = modoSeleccion;
	}

	public int getTipoSeleccion() {
		return tipoSeleccion;
	}

	public void setTipoSeleccion(int tipoSeleccion) {
		this.tipoSeleccion = tipoSeleccion;
	}


	public void llenarTabla(List<AutoparteDTO> autopartesEnTabla) {
		if(autopartesEnTabla.size()>0){
			tablaVacia = false;
			if(getWidth()<794)tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			else tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}else{
			tablaVacia = true;
			tablaAutopartes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}
		this.getModelAutopartes().setRowCount(0);
		this.getModelAutopartes().setColumnCount(0);
		this.getModelAutopartes().setColumnIdentifiers(this.getNombreColumnas());

		for (AutoparteDTO ap : autopartesEnTabla)
		{
			
			String nombreAutoparte = ap.toString();
			String year = ap.getYear();
			String sucursal = ap.getSucursal().toString();
			String stockMinimo = Integer.toString(ap.getStockMinimo());
			String stock = Integer.toString(ap.getStock());
			String precio = Double.toString(ap.getPrecio());
			
			Object[] fila = {nombreAutoparte,year,sucursal,stockMinimo,stock,precio};
			this.getModelAutopartes().addRow(fila);
			this.redefinirDimensionTabla();
		}
		
	}
	
	public void cargarTiposAutoparte(List<TipoAutoparteDTO> tiposAutoparte){
		TipoAutoparteDTO[] arrayTipos = new TipoAutoparteDTO[tiposAutoparte.size()];
		int i = 0;
		for(TipoAutoparteDTO tipoAutoparte : tiposAutoparte ) {
			arrayTipos[i] = tipoAutoparte;
			i++;
		}
		comboValor.setModel(new DefaultComboBoxModel(arrayTipos));
	}
	
	public void cargarModelos(List<ModeloDTO> modelosAuto) {
		ModeloDTO[] arrayModelos = new ModeloDTO[modelosAuto.size()];
		int i = 0;
		for(ModeloDTO modeloAuto : modelosAuto ) {
			arrayModelos[i] = modeloAuto;
			i++;
		}
		comboValor_2.setModel(new DefaultComboBoxModel(arrayModelos));		
	}
	
	public void cargarMarcas(List<MarcaDTO> marcasAuto) {
		MarcaDTO[] arrayMarcas = new MarcaDTO[marcasAuto.size()];
		int i = 0;
		for(MarcaDTO marcaAuto : marcasAuto ) {
			arrayMarcas[i] = marcaAuto;
			i++;
					}
		comboValor_1.setModel(new DefaultComboBoxModel(arrayMarcas));		
	}
	
	public void cargarSucursales(List<SucursalDTO> sucursales) {
		SucursalDTO[] arraySucursales = new SucursalDTO[sucursales.size()];
		int i = 0;
		for(SucursalDTO sucursal : sucursales ) {
			arraySucursales[i] = sucursal;
			i++;
		}
		comboValor.setModel(new DefaultComboBoxModel(arraySucursales));		
	}
	
	public void llenarYear() {
		int yearActual = Calendar.getInstance().get(Calendar.YEAR);
		for(int i = 2000; i<=yearActual; i++) {
			arrayYear[i-2000] = Integer.toString(i);
		}
	}
	
	public void cargarYear() {
		comboValor.setModel(new DefaultComboBoxModel(arrayYear));
	}
	
	
	public void datosBusquedaNulos(){
		this.comboValor.setSelectedIndex(-1);
		this.comboValor_1.setSelectedIndex(-1);
		this.comboValor_2.setSelectedIndex(-1);
	}

	

	public void errorSeleccion() {
		JOptionPane.showMessageDialog(null,  "Debe seleccionar una autoparte para ejecutar esta función","Error: Falta Selección",1);		
	}

	public void errorBorrar() {
		JOptionPane.showMessageDialog(null,  "No se ha podido borrar la autoparte seleccionada. \nAsegúrese de que la autoparte aún exista.","Error al intentar borrar un usuario",1);		
	}
	
	public void errorFiltrar() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione o complete todos los campos correspondientes al tipo de filtro elegido","Error",1);		
	}
	
	public void errorComboFiltro() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione uno de los campos para filtrar","Error",1);		
	}

	public void notificarIngresosCorrectos(List<String> lineasCorrectas){
		if(lineasCorrectas.size()>=1)JOptionPane.showMessageDialog(null,  "Termino la carga del archivo. Las lineas del archivo que se pudieron ingresar correctamente son: \n -Lineas: "+lineasCorrectas.toString(),"Lectura Finalizada",1);		
		else JOptionPane.showMessageDialog(null,  "Termino la carga del archivo. No se pudo ingresar ninguna linea correctamente.","Lectura Finalizada",1);		
	}
	
	public void cerrar()
	{
		if(isLlamadoPorVentas()) {
			this.ventanaVentasLlamadora.setEnabled(true);
			this.llamadoPorVentas = false;
		}else if(isLlamadoPorMantenimiento()) {
			this.listaAutopartesXmantenimientoLlamadora.setEnabled(true);
			this.llamadoPorMantenimiento = false;
		}
		this.dispose();
	}

	public void setVentanaVentasLlamadora(VentanaVentas ventanaVentas) {
		this.ventanaVentasLlamadora = ventanaVentas;
		
	}

	public boolean isLlamadoPorVentas() {
		return llamadoPorVentas;
	}

	public void setLlamadoPorVentas(boolean llamadoPorVentas) {
		this.llamadoPorVentas = llamadoPorVentas;
	}

	public void setVentanaAutopartesXmantenimientoLlamadora(ListaAutopartesXmantenimiento listaAutopartesXmantenimiento) {
		this.listaAutopartesXmantenimientoLlamadora = listaAutopartesXmantenimiento;
		
	}

	public boolean isLlamadoPorMantenimiento() {
		return llamadoPorMantenimiento;
	}

	public void setLlamadoPorMantenimiento(boolean llamadoPorMantenimiento) {
		this.llamadoPorMantenimiento = llamadoPorMantenimiento;
	}
	
	public void advertirUsuarioNoAutorizadoEditarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede editar una autoparte","Falla al intentar editar una autoparte",2);	
	}

	public void advertirUsuarioNoAutorizadoEliminarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede eliminar una autoparte","Falla al intentar eliminar una autoparte",2);	
	}
	
	public void advertirUsuarioNoAutorizadoIngresarAutoparte(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede ingresar autopartes","Falla al intentar ingresar autopartes",2);	
	}
	
	public void advertirUsuarioNoAutorizadoEditarAutoparteOtraSucursal(){
		JOptionPane.showMessageDialog(null,  "Su usuario no puede editar una autoparte de otra sucursal","Falla al intentar editar una autoparte",2);	
	}
	
}
