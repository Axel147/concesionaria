package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;


public class VentanaEncuesta extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnGuardar;
	private int tipoVentana;
	private static VentanaEncuesta INSTANCE;
	private int idMarca;
	private JTextPane txtpnAsdasd;
	private JTextPane textPane;
	private JTextPane textPane_1;
	private JRadioButton rdb1EstOp1;
	private JLabel label;
	private JRadioButton rdb2EstOp1;
	private JLabel label_1;
	private JRadioButton rdb3EstOp1;
	private JLabel label_2;
	private JRadioButton rdb4EstOp1;
	private JLabel label_3;
	private JRadioButton rdb5EstOp1;
	private JLabel label_4;
	private JRadioButton rdb1EstOp2;
	private JLabel label_5;
	private JRadioButton rdb2EstOp2;
	private JLabel label_6;
	private JRadioButton rdb3EstOp2;
	private JLabel label_7;
	private JRadioButton rdb4EstOp2;
	private JLabel label_8;
	private JRadioButton rdb5EstOp2;
	private JLabel label_9;
	private JRadioButton rdb1EstOp3;
	private JLabel label_10;
	private JRadioButton rdb2EstOp3;
	private JLabel label_11;
	private JRadioButton rdb3EstOp3;
	private JLabel label_12;
	private JRadioButton rdb4EstOp3;
	private JLabel label_13;
	private JRadioButton rdb5EstOp3;
	private JLabel label_14;
	private ButtonGroup rdbGroup1;
	private ButtonGroup rdbGroup2;
	private ButtonGroup rdbGroup3;
	private ImageIcon imagenPuntuacion1Estrella;
	private ImageIcon imagenPuntuacion2Estrella;
	private ImageIcon imagenPuntuacion3Estrella;
	private ImageIcon imagenPuntuacion4Estrella;
	private ImageIcon imagenPuntuacion5Estrella;

	public static VentanaEncuesta getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaEncuesta(); 	
			return new VentanaEncuesta();
		}
		else
			return INSTANCE;
	}
	

	private VentanaEncuesta() {
		setTitle("Encuesta De Satisfaccion");
	
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 512, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		setResizable(false);
		
		
		btnGuardar = new JButton("Responder");
		btnGuardar.setBounds(395, 283, 101, 23);
		contentPane.add(btnGuardar);
		
		txtpnAsdasd = new JTextPane();
		txtpnAsdasd.setEnabled(false);
		txtpnAsdasd.setEditable(false);
		txtpnAsdasd.setText("En cuanto a la atencion para la reserva: ¿Que valor le asignarias? \nTener en cuenta que 1 es atencion muy mala y 5 atencion excelente.");
		txtpnAsdasd.setBounds(10, 11, 486, 42);
		contentPane.add(txtpnAsdasd);
		
		textPane = new JTextPane();
		textPane.setText("En cuanto a la atencion del taller: ¿Que valor le asignarias? \nTener en cuenta que 1 es atencion muy mala y 5 atencion excelente.");
		textPane.setEnabled(false);
		textPane.setEditable(false);
		textPane.setBounds(10, 98, 486, 42);
		contentPane.add(textPane);
		
		textPane_1 = new JTextPane();
		textPane_1.setText("En cuanto a el servicio del taller: ¿Que valor le asignarias? \nTener en cuenta que 1 es servicio muy malo y 5 servicio excelente.");
		textPane_1.setEnabled(false);
		textPane_1.setEditable(false);
		textPane_1.setBounds(10, 194, 486, 42);
		contentPane.add(textPane_1);
		
		rdb1EstOp1 = new JRadioButton("");
		rdb1EstOp1.setBounds(10, 64, 21, 23);
		contentPane.add(rdb1EstOp1);
		
		imagenPuntuacion1Estrella = new ImageIcon(VentanaEncuesta.class.getResource("/iconos/puntuacion1Estrella.png"));
		imagenPuntuacion1Estrella = new ImageIcon(imagenPuntuacion1Estrella.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
		
		label = new JLabel("");
		label.setIcon(imagenPuntuacion1Estrella);
		label.setBounds(37, 64, 21, 23);
		contentPane.add(label);
		
		rdb2EstOp1 = new JRadioButton("");
		rdb2EstOp1.setBounds(64, 64, 21, 23);
		contentPane.add(rdb2EstOp1);
		
		imagenPuntuacion2Estrella = new ImageIcon(VentanaEncuesta.class.getResource("/iconos/puntuacion2Estrellas.png"));
		imagenPuntuacion2Estrella = new ImageIcon(imagenPuntuacion2Estrella.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		
		label_1 = new JLabel("");
		label_1.setIcon(imagenPuntuacion2Estrella);
		label_1.setBounds(91, 64, 30, 23);
		contentPane.add(label_1);
		
		rdb3EstOp1 = new JRadioButton("");
		rdb3EstOp1.setBounds(127, 64, 21, 23);
		contentPane.add(rdb3EstOp1);
		
		imagenPuntuacion3Estrella = new ImageIcon(VentanaEncuesta.class.getResource("/iconos/puntuacion3Estrellas.png"));
		imagenPuntuacion3Estrella = new ImageIcon(imagenPuntuacion3Estrella.getImage().getScaledInstance(40, 20, Image.SCALE_DEFAULT));
		
		label_2 = new JLabel("");
		label_2.setIcon(imagenPuntuacion3Estrella);
		label_2.setBounds(154, 64, 40, 23);
		contentPane.add(label_2);
		
		rdb4EstOp1 = new JRadioButton("");
		rdb4EstOp1.setBounds(197, 64, 21, 23);
		contentPane.add(rdb4EstOp1);
		
		imagenPuntuacion4Estrella = new ImageIcon(VentanaEncuesta.class.getResource("/iconos/puntuacion4Estrellas.png"));
		imagenPuntuacion4Estrella = new ImageIcon(imagenPuntuacion4Estrella.getImage().getScaledInstance(50, 20, Image.SCALE_DEFAULT));
		
		label_3 = new JLabel("");
		label_3.setIcon(imagenPuntuacion4Estrella);
		label_3.setBounds(224, 64, 50, 23);
		contentPane.add(label_3);
		
		rdb5EstOp1 = new JRadioButton("");
		rdb5EstOp1.setBounds(280, 64, 21, 23);
		contentPane.add(rdb5EstOp1);
		
		imagenPuntuacion5Estrella = new ImageIcon(VentanaEncuesta.class.getResource("/iconos/puntuacion5Estrellas.png"));
		imagenPuntuacion5Estrella = new ImageIcon(imagenPuntuacion5Estrella.getImage().getScaledInstance(60, 20, Image.SCALE_DEFAULT));
		
		label_4 = new JLabel("");
		label_4.setIcon(imagenPuntuacion5Estrella);
		label_4.setBounds(307, 64, 60, 23);
		contentPane.add(label_4);
		
		rdb1EstOp2 = new JRadioButton("");
		rdb1EstOp2.setBounds(10, 150, 21, 23);
		contentPane.add(rdb1EstOp2);
		
		label_5 = new JLabel("");
		label_5.setIcon(imagenPuntuacion1Estrella);
		label_5.setBounds(37, 151, 21, 23);
		contentPane.add(label_5);
		
		rdb2EstOp2 = new JRadioButton("");
		rdb2EstOp2.setBounds(64, 150, 21, 23);
		contentPane.add(rdb2EstOp2);
		
		label_6 = new JLabel("");
		label_6.setIcon(imagenPuntuacion2Estrella);
		label_6.setBounds(91, 151, 30, 23);
		contentPane.add(label_6);
		
		rdb3EstOp2 = new JRadioButton("");
		rdb3EstOp2.setBounds(127, 150, 21, 23);
		contentPane.add(rdb3EstOp2);
		
		label_7 = new JLabel("");
		label_7.setIcon(imagenPuntuacion3Estrella);
		label_7.setBounds(154, 151, 40, 23);
		contentPane.add(label_7);
		
		rdb4EstOp2 = new JRadioButton("");
		rdb4EstOp2.setBounds(197, 150, 21, 23);
		contentPane.add(rdb4EstOp2);
		
		label_8 = new JLabel("");
		label_8.setIcon(imagenPuntuacion4Estrella);
		label_8.setBounds(224, 151, 50, 23);
		contentPane.add(label_8);
		
		rdb5EstOp2 = new JRadioButton("");
		rdb5EstOp2.setBounds(280, 150, 21, 23);
		contentPane.add(rdb5EstOp2);
		
		label_9 = new JLabel("");
		label_9.setIcon(imagenPuntuacion5Estrella);
		label_9.setBounds(307, 151, 60, 23);
		contentPane.add(label_9);
		
		rdb1EstOp3 = new JRadioButton("");
		rdb1EstOp3.setBounds(10, 247, 21, 23);
		contentPane.add(rdb1EstOp3);
		
		label_10 = new JLabel("");
		label_10.setIcon(imagenPuntuacion1Estrella);
		label_10.setBounds(37, 248, 21, 23);
		contentPane.add(label_10);
		
		rdb2EstOp3 = new JRadioButton("");
		rdb2EstOp3.setBounds(64, 247, 21, 23);
		contentPane.add(rdb2EstOp3);
		
		label_11 = new JLabel("");
		label_11.setIcon(imagenPuntuacion2Estrella);
		label_11.setBounds(91, 248, 30, 23);
		contentPane.add(label_11);
		
		rdb3EstOp3 = new JRadioButton("");
		rdb3EstOp3.setBounds(127, 247, 21, 23);
		contentPane.add(rdb3EstOp3);
		
		label_12 = new JLabel("");
		label_12.setIcon(imagenPuntuacion3Estrella);
		label_12.setBounds(154, 248, 40, 23);
		contentPane.add(label_12);
		
		rdb4EstOp3 = new JRadioButton("");
		rdb4EstOp3.setBounds(197, 247, 21, 23);
		contentPane.add(rdb4EstOp3);
		
		label_13 = new JLabel("");
		label_13.setIcon(imagenPuntuacion4Estrella);
		label_13.setBounds(224, 248, 50, 23);
		contentPane.add(label_13);
		
		rdb5EstOp3 = new JRadioButton("");
		rdb5EstOp3.setBounds(280, 247, 21, 23);
		contentPane.add(rdb5EstOp3);
		
		label_14 = new JLabel("");
		label_14.setIcon(imagenPuntuacion5Estrella);
		label_14.setBounds(307, 248, 60, 23);
		contentPane.add(label_14);
		
		rdbGroup1 = new ButtonGroup();
		rdbGroup1.add(rdb1EstOp1);
		rdbGroup1.add(rdb2EstOp1);
		rdbGroup1.add(rdb3EstOp1);
		rdbGroup1.add(rdb4EstOp1);
		rdbGroup1.add(rdb5EstOp1);
		
		rdbGroup2 = new ButtonGroup();
		rdbGroup2.add(rdb1EstOp2);
		rdbGroup2.add(rdb2EstOp2);
		rdbGroup2.add(rdb3EstOp2);
		rdbGroup2.add(rdb4EstOp2);
		rdbGroup2.add(rdb5EstOp2);
		
		rdbGroup3 = new ButtonGroup();
		rdbGroup3.add(rdb1EstOp3);
		rdbGroup3.add(rdb2EstOp3);
		rdbGroup3.add(rdb3EstOp3);
		rdbGroup3.add(rdb4EstOp3);
		rdbGroup3.add(rdb5EstOp3);
		
		this.setVisible(false);
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}


	public int getId() {
		return idMarca;
	}


	public void setId(int id) {
		this.idMarca = id;
	}


	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}

	public JRadioButton getRdb1EstOp1() {
		return rdb1EstOp1;
	}


	public JRadioButton getRdb2EstOp1() {
		return rdb2EstOp1;
	}


	public JRadioButton getRdb3EstOp1() {
		return rdb3EstOp1;
	}


	public JRadioButton getRdb4EstOp1() {
		return rdb4EstOp1;
	}


	public JRadioButton getRdb5EstOp1() {
		return rdb5EstOp1;
	}


	public JRadioButton getRdb1EstOp2() {
		return rdb1EstOp2;
	}


	public JRadioButton getRdb2EstOp2() {
		return rdb2EstOp2;
	}


	public JRadioButton getRdb3EstOp2() {
		return rdb3EstOp2;
	}


	public JRadioButton getRdb4EstOp2() {
		return rdb4EstOp2;
	}


	public JRadioButton getRdb5EstOp2() {
		return rdb5EstOp2;
	}


	public JRadioButton getRdb1EstOp3() {
		return rdb1EstOp3;
	}


	public JRadioButton getRdb2EstOp3() {
		return rdb2EstOp3;
	}


	public JRadioButton getRdb3EstOp3() {
		return rdb3EstOp3;
	}


	public JRadioButton getRdb4EstOp3() {
		return rdb4EstOp3;
	}


	public JRadioButton getRdb5EstOp3() {
		return rdb5EstOp3;
	}


	public void mostrarVentana() {
		this.setVisible(true);
		
	}

	public void advertirFaltaNombreMarca() {
		JOptionPane.showMessageDialog(null,  "No se a ingresado ningun nombre para la marca o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
		
	}
	
	public void advertirNombreMarcaInvalido() {
		JOptionPane.showMessageDialog(null,"El nombre de la marca no es valido el formato corresponde a un minimo de tres letras con un espacio, sin simbolos o numeros, escriba el nombre correctamente.","Cuidado",2);
	}
	
	public void advertenciaMarcaRepetidaAgregar() {
		JOptionPane.showMessageDialog(null,  "No se pudo agregar la marca. \nRevise que no este queriendo agregar una marca que ya existe.","Falla al intentar agregar la marca",1);	
	}

	public void advertenciaMarcaRepetidaModificar() {
		JOptionPane.showMessageDialog(null,  "No se pudo modificar la marca. \nRevise que no este queriendo modificar con los datos de una marca que ya existe.","Falla al intentar modificar la marca",1);
	}
	
	public void cerrar()
	{
		this.rdbGroup1.clearSelection();
		this.rdbGroup2.clearSelection();
		this.rdbGroup3.clearSelection();
		this.dispose();
	}


	
}
