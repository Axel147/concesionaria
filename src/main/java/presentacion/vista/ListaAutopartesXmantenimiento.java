package presentacion.vista;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.tuple.Pair;


import dto.AutoparteDTO;
import dto.MantenimientoDTO;
import dto.MarcaDTO;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

public class ListaAutopartesXmantenimiento extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaAutopartesXmantenimiento INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaAutopartesXmantenimiento;
	private DefaultTableModel modelAutopartesXmantenimiento;
	private  String[] nombreColumnas = {"Autoparte","Modelo","Marca","Año","Cantidad"};
	
	//botones
	private JButton btnAgregar;
	private JButton btnEliminar;
		
	private MantenimientoDTO mantenimiento;
	private ListaDeMantenimientos ventanaListaDeMantenimientos;
	
	public static ListaAutopartesXmantenimiento getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaAutopartesXmantenimiento(); 	
			return new ListaAutopartesXmantenimiento();
		}else return INSTANCE;
	}

	public ListaAutopartesXmantenimiento(){
		
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(100, 100, 320, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setTitle("Autopartes Utilizadas");

		setLocationRelativeTo(null);
		setResizable(false);
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelAutopartesXmantenimiento = new DefaultTableModel(null,nombreColumnas);
		
		JScrollPane spMarcas = new JScrollPane();
		panel.add(spMarcas);
		tablaAutopartesXmantenimiento = new JTable(modelAutopartesXmantenimiento);
		tablaAutopartesXmantenimiento.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		
		tablaAutopartesXmantenimiento.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaAutopartesXmantenimiento.getColumnModel().getColumn(0).setResizable(false);
		
		spMarcas.setViewportView(tablaAutopartesXmantenimiento);
		
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel.add(panelsuperior, BorderLayout.NORTH);
		
		
		btnAgregar = new JButton("Agregar");
		panelsuperior.add(btnAgregar, "1, 2, fill, fill");
		btnAgregar.setVerticalAlignment(SwingConstants.BOTTOM);
		
		btnEliminar = new JButton("Eliminar");
		panelsuperior.add(btnEliminar, "3, 2, fill, fill");
		btnEliminar.setVerticalAlignment(SwingConstants.BOTTOM);
		
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEliminar() 
	{
		return btnEliminar;
	}

	public MantenimientoDTO getMantenimiento() {
		return mantenimiento;
	}

	public void setMantenimiento(MantenimientoDTO mantenimiento) {
		this.mantenimiento = mantenimiento;
	}

	public DefaultTableModel getModelAutopartesXmantenimiento() 
	{
		return modelAutopartesXmantenimiento;
	}
	
	public JTable getTablaAutopartesXmantenimiento()
	{
		return tablaAutopartesXmantenimiento;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public void llenarTabla(List<Pair<AutoparteDTO, Integer>> autopartesXmantenimiento) {
		this.getModelAutopartesXmantenimiento().setRowCount(0); //Para vaciar la tabla
		this.getModelAutopartesXmantenimiento().setColumnCount(0);
		this.getModelAutopartesXmantenimiento().setColumnIdentifiers(this.getNombreColumnas());

		for (Pair<AutoparteDTO, Integer> autoparte : autopartesXmantenimiento)
		{
			String tipoAutoparte = autoparte.getLeft().getTipo().getNombre();
			String modelo = autoparte.getLeft().getModelo().getModelo();
			String marca = autoparte.getLeft().getModelo().getMarca().getNombre();
			String year = autoparte.getLeft().getYear();
			Integer cantidad = autoparte.getRight();
			Object[] fila = {tipoAutoparte, modelo, marca, year, cantidad};
			this.getModelAutopartesXmantenimiento().addRow(fila);
		}
		
	}
	
	public void advertirSeleccionBorrarIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla o esta seleccionando mas de uno. \nPor favor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void advertirFaltaDeStockDeAutoparte(){
		JOptionPane.showMessageDialog(null,"No hay stock de este articulo.","Falla al intentar seleccionar una autoparte",2);
	}
	
	public void errorAlSeleccionarAutoparte(){
		JOptionPane.showMessageDialog(null,"Ocurrio un error al intentar seleccionar una autoparte.","Error al intentar seleccionar una autoparte",0);
	}
	
	public void advertenciaFallaEliminacion(){
		JOptionPane.showMessageDialog(null,"No se pudo eliminar la autoparte de la lista de autopartes utilizadas en un mantenimiento.","Falla al intentar eliminar una autoparte",2);
	}
	
	public void cerrar(){
		this.ventanaListaDeMantenimientos.setEnabled(true);
		this.dispose();
	}

	public void setVentanaBloqueada(ListaDeMantenimientos ventanaListaDeMantenimientos) {
		this.ventanaListaDeMantenimientos = ventanaListaDeMantenimientos;
	}
		
}