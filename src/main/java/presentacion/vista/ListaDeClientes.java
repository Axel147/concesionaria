package presentacion.vista;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.ClienteDTO;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Dimension;

public class ListaDeClientes extends JFrame{
	private static final long serialVersionUID = 1L;
	private static ListaDeClientes INSTANCE;
	
	//paneles
	private JPanel contentPane;
	private JPanel panelsuperior;
	
	//tabla
	private JTable tablaClientes;
	private DefaultTableModel modelClientes;
	private  String[] nombreColumnas = {"Nombre","Apellido","Pais de Residencia","TipoDocumento","NºDocumento","Email","Telefono"};
	
	//botones
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnAgregarFiltro;
	private JButton btnDeshacerFiltros;
	private JButton btnVolver;
	
	
	
	//campo de busqueda
	private JTextField txtBusqueda;
	
	//etiquta de busqueda
		private JLabel lblBuscarPor;
	
	//botones seleccionables
	private JRadioButton rdbtnNombre;
	private JRadioButton rdbtnApellido;
	private JRadioButton rdbtnPais;
	private JRadioButton rdbtnNdocumento;
	private JRadioButton rdbtnEmail;
	private JRadioButton rdbtnTelefono;
	ButtonGroup rdbtngroup;
	private boolean llamadoPorVentas;
	private VentanaVentas ventanaVentasLlamadora;
	private boolean tablaVacia;
	
	//variable para determinar si es ABM normal o para seleccionar algun cliente de la lista para usar en otro parte del sistema
	private boolean modoSeleccion;
	private int tipoSeleccion;
	
	public static ListaDeClientes getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeClientes(); 	
			return new ListaDeClientes();
		}else return INSTANCE;
	}

	public ListaDeClientes(){
		super();
		setMinimumSize(new Dimension(822, 159));
		
		
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        cerrar();
		    }
		});
		
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(100, 100, 1100, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		setTitle("Lista De Clientes");
		
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		modelClientes = new DefaultTableModel(null,nombreColumnas){@Override public boolean isCellEditable(int row, int column) {return false;}};
		
		JScrollPane spClientes = new JScrollPane();
		panel.add(spClientes);
		tablaClientes = new JTable(modelClientes);
//		tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tablaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		tablaClientes.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaClientes.getColumnModel().getColumn(0).setResizable(false);
		tablaClientes.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaClientes.getColumnModel().getColumn(1).setResizable(false);
		
		spClientes.setViewportView(tablaClientes);
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				if(getWidth()<949){
					if(tablaVacia)tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					else tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				}
				else tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			}
		});
		
		panelsuperior = new JPanel();
		panelsuperior.setBounds(0, 0, 200, 100);
		panel.add(panelsuperior, BorderLayout.NORTH);
//		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
//				ColumnSpec.decode("216px"),
//				ColumnSpec.decode("72px"),
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,
//				FormSpecs.BUTTON_COLSPEC,},
//			new RowSpec[] {
//				RowSpec.decode("38px"),
//				RowSpec.decode("23px"),}));
		panelsuperior.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("214px"),
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblBuscarPor = new JLabel("Buscar por:");
		panelsuperior.add(lblBuscarPor, "1, 1");
		
		rdbtnNombre = new JRadioButton("Nombre");
		rdbtnNombre.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnNombre, "2, 1, fill, default");
		
		rdbtnApellido = new JRadioButton("Apellido");
		rdbtnApellido.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnApellido, "3, 1, fill, default");
		
		rdbtnPais = new JRadioButton("Pais");
		rdbtnPais.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnPais, "4, 1, fill, default");
		
		rdbtnNdocumento = new JRadioButton("NºDocumento");
		rdbtnNdocumento.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnNdocumento, "5, 1, fill, default");
	
		rdbtnEmail = new JRadioButton("Email");
		rdbtnEmail.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnEmail, "6, 1, fill, default");
		
		rdbtnTelefono = new JRadioButton("Telefono");
		rdbtnTelefono.setHorizontalAlignment(SwingConstants.CENTER);
		panelsuperior.add(rdbtnTelefono, "7, 1, fill, default");
		
		rdbtngroup = new ButtonGroup();
		rdbtngroup.add(rdbtnNombre);
		rdbtngroup.add(rdbtnApellido);
		rdbtngroup.add(rdbtnNdocumento);
		rdbtngroup.add(rdbtnEmail);
		rdbtngroup.add(rdbtnTelefono);
		rdbtngroup.add(rdbtnPais);
		rdbtngroup.clearSelection();
		rdbtnNombre.setSelected(true);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setHorizontalAlignment(SwingConstants.LEFT);
		txtBusqueda.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelsuperior.add(txtBusqueda, "1, 2, fill, fill");
		txtBusqueda.setColumns(10);
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		btnAgregarFiltro.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnAgregarFiltro, "2, 2, fill, fill");
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		btnDeshacerFiltros.setHorizontalAlignment(SwingConstants.LEFT);
		panelsuperior.add(btnDeshacerFiltros, "3, 2, fill, fill");
		
		btnAgregar = new JButton("Agregar");
		panelsuperior.add(btnAgregar, "4, 2, fill, fill");
		
		btnEditar = new JButton("Editar");
		panelsuperior.add(btnEditar, "5, 2, fill, fill");
		
		btnBorrar = new JButton("Borrar");
		panelsuperior.add(btnBorrar, "6, 2, fill, fill");
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		panelsuperior.add(btnVolver, "7, 2, fill, fill");
		btnVolver.setVerticalAlignment(SwingConstants.BOTTOM);
		
	}
	
	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	public JRadioButton getRdbtnNombre() {
		return rdbtnNombre;
	}

	public JRadioButton getRdbtnApellido() {
		return rdbtnApellido;
	}

	public JRadioButton getRdbtnNdocumento() {
		return rdbtnNdocumento;
	}

	public JRadioButton getRdbtnEmail() {
		return rdbtnEmail;
	}

	public JRadioButton getRdbtnTelefono() {
		return rdbtnTelefono;
	}

	public boolean getModoSeleccion() {
		return modoSeleccion;
	}

	public void setModoSeleccion(boolean modoSeleccion) {
		this.modoSeleccion = modoSeleccion;
	}

	public int getTipoSeleccion() {
		return tipoSeleccion;
	}

	public void setTipoSeleccion(int tipoSeleccion) {
		this.tipoSeleccion = tipoSeleccion;
	}

	public JTextField getTxtBusqueda() {
		return txtBusqueda;
	}

	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}
	
	public JButton getBtnVolver() {
		return btnVolver;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}

	public JRadioButton getRdbtnPais() {
		return rdbtnPais;
	}

	public DefaultTableModel getModelPersonas() 
	{
		return modelClientes;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaClientes;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaPersonas();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }

	public void llenarTabla(List<ClienteDTO> clientesEnTabla) {
		if(clientesEnTabla.size()>0){
			tablaVacia = false;
			if(getWidth()<949)tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			else tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}else{
			tablaVacia = true;
			tablaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (ClienteDTO c : clientesEnTabla)
		{
			String nombre = c.getNombre();
			String apellido = c.getApellido();
			String paisResidencia = c.getPaisResidencia().getPais();
			String tipoDoc = c.getTipoDocumento().getTipoDocumento();
			String numDoc = c.getNumDocumento();
			String mail = c.getMail();
			String tel = c.getTelefono();
			Object[] fila = {nombre, apellido, paisResidencia, tipoDoc, numDoc, mail, tel};
			this.getModelPersonas().addRow(fila);
			redefinirDimensionTabla();
		}
		
	}
	
	public void advertirSeleccionIncorrecta(){
		JOptionPane.showMessageDialog(null,  "No esta seleccionando ningun elemento de la tabla o esta seleccionando mas de uno. \nPara editar o borrar porfavor seleccione un elemento de la tabla.","Seleccion Incorrecta",1);		
	}
	
	public void errorAlBorrarCliente(){
		JOptionPane.showMessageDialog(null,  "No se pudo eliminar el cliente. \nRevise que no existan reservas asignadas a este cliente. De ser asi elimine primero todas las reservas que tenga o haya realizado este cliente.","Falla al intentar borrar un cliente",0);		
	}
	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtBusqueda.setText(null);
		if(isLlamadoPorVentas()) {
			this.ventanaVentasLlamadora.setEnabled(true);
			this.llamadoPorVentas = false;
		}
		this.dispose();
	}
	
	
	public boolean isLlamadoPorVentas() {
		return llamadoPorVentas;
	}

	public void setLlamadoPorVentas(boolean b) {
		this.llamadoPorVentas = b;
		
	}

	public VentanaVentas getVentanaVentasLlamadora() {
		return ventanaVentasLlamadora;
	}

	public void setVentanaVentasLlamadora(VentanaVentas ventanaVentasLlamadora) {
		this.ventanaVentasLlamadora = ventanaVentasLlamadora;
	}
	
	
}
