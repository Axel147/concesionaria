package presentacion.vista;


import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.PaisDTO;
import dto.TipoDocumentoDTO;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;

public class VentanaCliente extends JFrame{
	private static final long serialVersionUID = 1L;
	private static VentanaCliente INSTANCE;
	
	//panel
	private JPanel contentPane;
	
	//campos de texto
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtNumDoc;
	private JTextField txtMail;
	private JTextField txtTelefono;
	
	//combobox
	ComboBoxBoleanos tipoDoc;
	ComboBoxBoleanos paisResidencia;
	
	//variables auxiliares
	private int tipoVentana;//sirve para indicar si la ventana esta en modo agregar o modificar
	private int idCliente;//sirve para guardar el id del cliente que queremos modificar luego
	
	//botones
	private JButton btnGuardar;
	
	private ListaDeClientes ventanaListaDeClientes;

	//devuelve una instancia de este tipo de ventana
	public static VentanaCliente getInstance(){
		if(INSTANCE == null){
			INSTANCE = new VentanaCliente(); 	
			return new VentanaCliente();
		}else return INSTANCE;
	}
	
	private VentanaCliente(){
		super();
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(350, 100, 415, 272);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		
		setResizable(false);
		
//		ImageIcon imagen = new ImageIcon(new File("").getAbsolutePath()+"/Imagenes/logoAgenda.png");
//        imagen = new ImageIcon(imagen.getImage().getScaledInstance(680, 820, Image.SCALE_DEFAULT));
//		setIconImage(imagen.getImage());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
				cerrar();
		    }
		});
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 391, 221);
		contentPane.add(panel);
		panel.setLayout(null);
		
		//etiquetas
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(7, 14, 112, 14);
		panel.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(7, 43, 112, 14);
		panel.add(lblApellido);
		
		JLabel lblNumDoc = new JLabel("N\u00BADocumento");
		lblNumDoc.setBounds(7, 134, 112, 14);
		panel.add(lblNumDoc);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(7, 159, 112, 14);
		panel.add(lblMail);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setHorizontalAlignment(SwingConstants.CENTER);
		lblTelfono.setBounds(215, 134, 71, 14);
		panel.add(lblTelfono);
		
		
		//campos de textos
		txtNombre = new JTextField();
		txtNombre.setBounds(129, 11, 253, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		txtApellido.setBounds(129, 39, 253, 20);
		panel.add(txtApellido);
		
		
		txtNumDoc = new JTextField();
		txtNumDoc.setColumns(10);
		txtNumDoc.setBounds(129, 131, 76, 20);
		panel.add(txtNumDoc);
		
		txtMail = new JTextField();
		txtMail.setColumns(10);
		txtMail.setBounds(129, 156, 253, 20);
		panel.add(txtMail);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(296, 131, 86, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		//boton
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(280, 187, 101, 23);
		panel.add(btnGuardar);
		
		JLabel lblTipoDoc = new JLabel("Tipo de Documento");
		lblTipoDoc.setBounds(7, 106, 112, 14);
		panel.add(lblTipoDoc);
		
		tipoDoc = new ComboBoxBoleanos();
		tipoDoc.setBounds(129, 103, 253, 20);
		panel.add(tipoDoc);
		
		paisResidencia = new ComboBoxBoleanos();
		paisResidencia.setBounds(129, 70, 253, 20);
		panel.add(paisResidencia);
		
		JLabel lblPaisResidencia = new JLabel("Pais de Residencia");
		lblPaisResidencia.setBounds(7, 73, 112, 14);
		panel.add(lblPaisResidencia);
		
		this.setVisible(false);
	}
	
	//hace visible la ventana
	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public void llenarPais(List<PaisDTO> paises)
	{
		try{  
			this.paisResidencia.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.paisResidencia.removeAllItems();
            this.paisResidencia.setModel(cmb);
            for(PaisDTO pais: paises){
                cmb.addElement(pais);
            }
            this.paisResidencia.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con los paises","Error",0);
        }
	}
	
	public void llenarTiposDeDocumentos(List<TipoDocumentoDTO> tiposDeDocumentos)
	{
		try{    
			this.tipoDoc.setLoaded(false);
            DefaultComboBoxModel cmb = new DefaultComboBoxModel(); 
            this.tipoDoc.removeAllItems();
            this.tipoDoc.setModel(cmb);
            for(TipoDocumentoDTO tiposDeDocumento: tiposDeDocumentos){
                cmb.addElement(tiposDeDocumento);
            }
            this.tipoDoc.setLoaded(true);
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null,"Error al cargar lista desplegable con tipo de documentos","Error",0);
        }
	}
	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}
	
	public void setNombre(String Nombre) {
		this.txtNombre.setText(Nombre);
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public void setApellido(String Apellido) {
		this.txtApellido.setText(Apellido);
	}
	
	public ComboBoxBoleanos getPaisResidencia() {
		return paisResidencia;
	}

	public void setPaisResidencia(ComboBoxBoleanos paisResidencia) {
		this.paisResidencia = paisResidencia;
	}

	public ComboBoxBoleanos getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(ComboBoxBoleanos tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public JTextField getTxtNumDoc() {
		return txtNumDoc;
	}

	public void setNumDoc(String NumDoc) {
		this.txtNumDoc.setText(NumDoc);
	}

	public JTextField getTxtMail() {
		return txtMail;
	}

	public void setMail(String Mail) {
		this.txtMail.setText(Mail);;
	}
	
	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public void setTelefono(String Telefono) {
		this.txtTelefono.setText(Telefono);
	}
	
	public JButton getBtnGuardar() 
	{
		return btnGuardar;
	}
	
	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}
	
	public int getTipoVentana() {
		return tipoVentana;
	}

	public void setTipoVentana(int tipoVentana) {
		this.tipoVentana = tipoVentana;
	}	
	
	public void advertirFaltaNombreCliente(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun nombre o el nombre esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaApellidoCliente(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun apellido o el apellido esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaNumDocCliente(){
		JOptionPane.showMessageDialog(null,"No se a ingresado ningun número de documento o el número de documento esta constituido solo de espacios. \nDebe establecer al menos un caracter.","Cuidado",2);
	}
	
	public void advertirFaltaMail(){
		JOptionPane.showMessageDialog(null,"Debe completar el campo de Email.","Cuidado",2);
	}
	
	public void advertirNombreClienteInvalido(){
		JOptionPane.showMessageDialog(null,"El nombre del cliente no es valido, escriba el nombre correctamente.","Cuidado",2);
	}
	
	public void advertirApellidoClienteInvalido(){
		JOptionPane.showMessageDialog(null,"El apellido del cliente no es valido, escriba el apellido correctamente.","Cuidado",2);
	}
	
	public void advertirNumDocClienteInvalido(){
		JOptionPane.showMessageDialog(null,"El número de documento del cliente no es valido, escriba el número de documento correctamente.","Cuidado",2);
	}
	
	public void advertirMailInvalido(){
		JOptionPane.showMessageDialog(null,"El mail del cliente no es valido, escriba un formato de mail valido.","Cuidado",2);
	}
	
	public void advertirTelefonoInvalido(){
		JOptionPane.showMessageDialog(null,"El telefono del cliente no es valido, escriba un formato de telefono valido.","Cuidado",2);
	}
	
	public void advertirEscrituraNombreCliente(){
		JOptionPane.showMessageDialog(null,"El campo nombre de un cliente no acepta caracteres númericos.","Caracter Invalido",2);
	}
	
	public void advertirEscrituraApellidoCliente(){
		JOptionPane.showMessageDialog(null,"El campo apellido de un cliente no acepta caracteres númericos.","Caracter Invalido",2);
	}
	
	public void advertirEscrituraNumeroDocumento(){
		JOptionPane.showMessageDialog(null,"El campo número de documento solo acepta caracteres númericos.","Caracter Invalido",2);
	}
	
	public void advertirEscrituraTelefono(){
		JOptionPane.showMessageDialog(null,"El telefono solo acepta caracteres númericos.","Caracter Invalido",2);
	}
	
	public void advertenciaClienteRepetidoAgregar(){
		JOptionPane.showMessageDialog(null,  "No se pudo agregar el nuevo cliente. \nRevise que no este queriendo agregar un cliente que ya existe.","Falla al intentar agregar un cliente",1);		
	}
	
	public void advertenciaClienteRepetidoModificar(){
		JOptionPane.showMessageDialog(null,  "No se pudo modificar el nuevo cliente. \nRevise que no este queriendo modificar con los datos de un cliente que ya existe.","Falla al intentar modificar un cliente",1);		
	}
	
	//borra todos los datos de los campos y oculta la ventana
	public void cerrar(){
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
		this.txtNumDoc.setText(null);
		this.txtMail.setText(null);
		this.txtTelefono.setText(null);
		this.ventanaListaDeClientes.setEnabled(true);
		this.dispose();
	}

	public void setVentanaBloqueada(ListaDeClientes ventanaListaDeClientes) {
		this.ventanaListaDeClientes = ventanaListaDeClientes;
	}
	
	
}
