package presentacion.vista;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import dto.PaisDTO;
import dto.SucursalDTO;
import dto.TipoDocumentoDTO;
import dto.TipoUsuarioDTO;
import dto.UsuarioDTO;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.ListSelectionModel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.CardLayout;
import java.awt.Component;

import javax.swing.JTextField;
import java.awt.Button;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.SwingConstants;
import javax.swing.table.TableModel;
import java.awt.Dimension;

public class ListaDeUsuarios extends JFrame
{
	
	private static final long serialVersionUID = 1L;
	private static ListaDeUsuarios INSTANCE;
	private DefaultTableModel modelUsuarios;
	private static final String[] nombreColumnas = {"Nombre Completo","Sexo","País de Residencia","Tipo de Documento","Nro de Documento","Usuario","Mail", "Password","Tipo de Usuario", "Sucursal"};
	private static final String[] camposBusqueda = {"Nombre","Apellido","Sexo","País de Residencia","Documento","Usuario","Mail", "Password","Tipo de Usuario","Sucursal"};
	private static final int[] nrosCampo = {0,1,2,3,5,6,7,8,9,10};
	private static final String[] condiciones = {"Idéntico a","Empieza Con", "Contiene"};
	private static final int[] camposTextfield = {0,1,5,6,7};
	private static final String[] arraySexo = {"Masculino", "Femenino"};
	
	private JButton btnAgregar;
	private JComboBox comboCampos;
	private JComboBox comboValor;
	private JComboBox comboCondicion; 
	private JTextField txtValor;
	private JLabel lblTipoDoc;
	private JComboBox comboTipoDoc;
	private JTextField txtValor_2;
	private JLabel lblNumeroDoc;
	private JLabel lblPais;
	private JComboBox comboPaises;
	private JPanel panelsuperior;
	private JLabel lblFiltrar;
	private JPanel panelSuperiorIzquierdo;
	private JPanel panelSuperiorDerecho;
	private JButton btnAgregarFiltro;
	private JScrollPane spUsuarios;
	private JTable tablaUsuarios;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnDeshacerFiltros;
	private JButton btnVolver;
	private JPanel panelBusqueda;
	private JPanel panelVacio;
	private JPanel panelCamposCombo;
	private JPanel panelCamposTextField;
	private JPanel panelDobleCampo; 
	private boolean tablaVacia;
	

	public static ListaDeUsuarios getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ListaDeUsuarios(); 	
			return new ListaDeUsuarios();
		}else return INSTANCE;
	}
	
	private ListaDeUsuarios() 
	{
		super();
		setMinimumSize(new Dimension(1010, 184));
		setFont(new Font("Verdana", Font.BOLD, 14));
		
		initialize();
	}


	private void initialize() 
	{
		setBounds(100, 100, 1009, 460);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setTitle("Vista de Usuarios");
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		modelUsuarios = new DefaultTableModel(null,nombreColumnas);
		
		spUsuarios = new JScrollPane();
		getContentPane().add(spUsuarios, BorderLayout.CENTER);
		
		tablaUsuarios = new JTable(modelUsuarios);
		tablaUsuarios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		spUsuarios.setViewportView(tablaUsuarios);

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				if(getWidth()<1284){
					if(tablaVacia)tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					else tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				}
				else tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			}
		});
		
		panelsuperior = new JPanel();
		getContentPane().add(panelsuperior, BorderLayout.NORTH);
		panelsuperior.setLayout(new BorderLayout(0, 0));
		
		panelSuperiorIzquierdo = new JPanel();
		panelsuperior.add(panelSuperiorIzquierdo, BorderLayout.WEST);
		panelSuperiorIzquierdo.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblFiltrar = new JLabel("Filtrar por:");
		lblFiltrar.setHorizontalAlignment(SwingConstants.CENTER);
		panelSuperiorIzquierdo.add(lblFiltrar);
		
		comboCampos = new JComboBox(camposBusqueda);
		panelSuperiorIzquierdo.add(comboCampos);
		
		panelSuperiorDerecho = new JPanel();
		panelsuperior.add(panelSuperiorDerecho, BorderLayout.EAST);
		panelSuperiorDerecho.setLayout(new GridLayout(0, 1, 0, 0));
		
		btnAgregarFiltro = new JButton("Agregar Filtro");
		panelSuperiorDerecho.add(btnAgregarFiltro);
		
		btnDeshacerFiltros = new JButton("Deshacer Filtros");
		panelSuperiorDerecho.add(btnDeshacerFiltros);
		
		panelBusqueda = new JPanel();
		panelsuperior.add(panelBusqueda, BorderLayout.CENTER);
		panelBusqueda.setLayout(new CardLayout(0, 0));
		
		panelVacio = new JPanel();
		panelBusqueda.add(panelVacio, "panelVacio");
		
		panelCamposTextField = new JPanel();
		panelBusqueda.add(panelCamposTextField, "panelTxt");
		panelCamposTextField.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;min)"),
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(160dlu;min)"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("86px"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		comboCondicion = new JComboBox(condiciones);
		panelCamposTextField.add(comboCondicion, "3, 4");
		
		txtValor = new JTextField();
		panelCamposTextField.add(txtValor, "6, 4");
		txtValor.setColumns(10);
		
		panelCamposCombo = new JPanel();
		panelBusqueda.add(panelCamposCombo, "panelCombo");
		panelCamposCombo.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;min)"),
				ColumnSpec.decode("28px"),},
			new RowSpec[] {
				FormSpecs.LINE_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,}));
		
		comboValor = new JComboBox();
		panelCamposCombo.add(comboValor, "3, 4");
		
		panelDobleCampo = new JPanel();
		panelBusqueda.add(panelDobleCampo, "panelComboDoc");
		panelDobleCampo.setLayout(new FormLayout(new ColumnSpec[] {
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;min):grow"),
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(120dlu;min)"),
				FormSpecs.DEFAULT_COLSPEC,
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(160dlu;pref)"),
				FormSpecs.BUTTON_COLSPEC,
				FormSpecs.BUTTON_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("25px"),
				RowSpec.decode("25px"),}));
		
		lblPais = new JLabel("Pais");
		panelDobleCampo.add(lblPais, "3, 1, center, default");
		
		lblTipoDoc = new JLabel("Tipo");
		panelDobleCampo.add(lblTipoDoc, "6, 1, center, default");
		
		lblNumeroDoc = new JLabel("Número");
		panelDobleCampo.add(lblNumeroDoc, "9, 1, center, default");
		
		comboPaises = new JComboBox();
		panelDobleCampo.add(comboPaises, "3, 2");
		
		comboTipoDoc = new JComboBox();
		panelDobleCampo.add(comboTipoDoc, "6, 2");
		
		txtValor_2 = new JTextField();
		panelDobleCampo.add(txtValor_2, "9, 2");
		txtValor_2.setColumns(10);
		
		JPanel panelInferior = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panelInferior.getLayout();
		flowLayout.setHgap(50);
		getContentPane().add(panelInferior, BorderLayout.SOUTH);
		
		btnAgregar = new JButton("Agregar");
		panelInferior.add(btnAgregar);
		btnAgregar.setFont(new Font("Verdana", Font.PLAIN, 12));
		
		btnEditar = new JButton("Editar");
		btnEditar.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelInferior.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelInferior.add(btnBorrar);
		
		btnVolver = new JButton("Volver");
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(25, 15, Image.SCALE_DEFAULT));
		btnVolver.setIcon(imagenflecha);
		btnVolver.setFont(new Font("Verdana", Font.PLAIN, 12));
		panelInferior.add(btnVolver);
		

		
	}
	
	public JTable getTablaUsuarios(){
		return tablaUsuarios;
	}
	
	public DefaultTableModel getModelUsuarios(){
		return modelUsuarios;
	}
	
	public String[] getNombreColumnas(){
		return nombreColumnas;
	}
	
	public JButton getBtnAgregar(){
		return btnAgregar;
	}

	public JButton getBtnBorrar(){
		return btnBorrar;
	}
	

	public JButton getBtnEditar() {
		return btnEditar;
	}

	
	public JButton getBtnVolver() {
		return btnVolver;
	}

	public JTextField getTxtValor() {
		return txtValor;
	}

	public void setTxtValor(JTextField txtValor) {
		this.txtValor = txtValor;
	}	

	public JComboBox getComboPaises() {
		return comboPaises;
	}

	public JTextField getTxtValor_2() {
		return txtValor_2;
	}

	public void setTxtValor_2(JTextField txtValor_2) {
		this.txtValor_2 = txtValor_2;
	}

	public JPanel getPanelCamposTextField() {
		return panelCamposTextField;
	}

	public JPanel getPanelCamposCombo() {
		return panelCamposCombo;
	}

	public JPanel getPanelDobleCampo() {
		return panelDobleCampo;
	}

	public JLabel getLblTipoDoc() {
		return lblTipoDoc;
	}

	public JComboBox getComboTipoDoc() {
		return comboTipoDoc;
	}
	
	public JComboBox getComboCampos() {
		return comboCampos;
	}

	public JPanel getPanelBusqueda() {
		return panelBusqueda;
	}

	public JComboBox getComboValor() {
		return comboValor;
	}

	public JComboBox getComboCondicion() {
		return comboCondicion;
	}

	public JButton getBtnAgregarFiltro() {
		return btnAgregarFiltro;
	}

	public JButton getBtnDeshacerFiltros() {
		return btnDeshacerFiltros;
	}
	
	

	public static int[] getCampostextfield() {
		return camposTextfield;
	}	

	public static String[] getCamposbusqueda() {
		return camposBusqueda;
	}
	
	

	public static int[] getNroscampo() {
		return nrosCampo;
	}

	public void mostrarVentana(){
		this.setVisible(true);
	}
	
	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaUsuarios();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }
	
	public void cargarComboSexo(){
		this.comboValor.setModel(new DefaultComboBoxModel(arraySexo));
	}
	
	public void cargarComboPais(List<PaisDTO> paises) {
		PaisDTO[] arrayPaises = new PaisDTO[paises.size()];
		int i = 0;
		for(PaisDTO pais : paises ) {
			arrayPaises[i] = pais;
			i++;
		}
		
		this.comboValor.setModel(new DefaultComboBoxModel(arrayPaises));
	}
	
	public void cargarCombosDoc(List<PaisDTO> paises) {
		PaisDTO[] arrayPaises = new PaisDTO[paises.size()];
		int i = 0;
		for(PaisDTO pais : paises ) {
			arrayPaises[i] = pais;
			i++;
		}
		this.comboPaises.setModel(new DefaultComboBoxModel(arrayPaises));
	}
	
	public void cargarTiposUsuario(List<TipoUsuarioDTO> tiposUsuario) {
			TipoUsuarioDTO[] arrayTiposUsr = new TipoUsuarioDTO[tiposUsuario.size()];
			int i = 0;
			for(TipoUsuarioDTO tipoUsuario : tiposUsuario ) {
				arrayTiposUsr[i] = tipoUsuario;
				i++;
			}
			this.comboValor.setModel(new DefaultComboBoxModel(arrayTiposUsr));
	}
	
	public void cargarTiposDoc(List<TipoDocumentoDTO> tiposDoc) {
		TipoDocumentoDTO[] arrayTiposDoc = new TipoDocumentoDTO[tiposDoc.size()];
		int i = 0;
		for(TipoDocumentoDTO tipoDoc : tiposDoc ) {
			arrayTiposDoc[i] = tipoDoc;
			i++;
		}
		this.comboTipoDoc.setModel(new DefaultComboBoxModel(arrayTiposDoc));

	}
	
	public void cargarSucursales(List<SucursalDTO> sucursales) {
		SucursalDTO[] arraySucursales = new SucursalDTO[sucursales.size()];
		int i=0;
		for(SucursalDTO sucursal : sucursales) {
			arraySucursales[i] = sucursal;
			i++;
		}
		this.comboValor.setModel(new DefaultComboBoxModel(arraySucursales));
	}
	
	public void llenarTabla(List<UsuarioDTO> usuariosEnTabla) {
		if(usuariosEnTabla.size()>0){
			tablaVacia = false;
			if(getWidth()<1284)tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			else tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}else{ 
			tablaVacia = true;
			tablaUsuarios.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}
		this.getModelUsuarios().setRowCount(0);
		this.getModelUsuarios().setColumnCount(0);
		this.getModelUsuarios().setColumnIdentifiers(this.getNombreColumnas());

		for (UsuarioDTO u : usuariosEnTabla)
		{
			
			String nombreCompleto = u.toString();
			String sexo = u.getSexo();
			String paisResidencia = u.getPaisResidencia().getPais();
			String tipoDocumento = u.getTipoDocumento().getTipoDocumento();
			String nroDocumento = u.getNroDocumento();
			String usuario = u.getNombreUsuario();
			String mail = u.getMail();
			String password = u.getPass();
			String tipoUsuario = u.getTipoUsuario().toString();
			String sucursal = u.getSucursal().getNombre();
					
			Object[] fila = {nombreCompleto,sexo,paisResidencia,tipoDocumento,nroDocumento,usuario,mail,password,tipoUsuario,sucursal};
			this.getModelUsuarios().addRow(fila);
			this.redefinirDimensionTabla();
		}
		
	}
	
	public void datosBusquedaNulos(){
		this.comboCondicion.setSelectedIndex(-1);
		this.comboPaises.setSelectedIndex(-1);
		this.comboTipoDoc.setSelectedIndex(-1);
		this.comboValor.setSelectedIndex(-1);
		this.txtValor.setText(null);
		this.txtValor_2.setText(null);
	}

	public void errorSeleccion() {
		JOptionPane.showMessageDialog(null,  "Debe seleccionar un usuario para ejecutar esta función","Error: Falta Selección",1);		
	}

	public void errorBorrar() {
		JOptionPane.showMessageDialog(null,  "No se ha podido borra el usuario seleccionado. \nAsegúrese de que el usuario aún exista.","Error al intentar borrar un usuario",1);		
	}
	
	public void errorFiltrar() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione o complete todos los campos correspondientes al tipo de filtro elegido","Error",1);		
	}
	
	public void errorComboFiltro() {
		JOptionPane.showMessageDialog(null,  "Por favor seleccione uno de los campos para filtrar","Error",1);		
	}
}
