package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;
import javax.swing.JComboBox;
import java.awt.Toolkit;
import javax.swing.SwingConstants;
import java.awt.GridLayout;

public class VentanaPrincipal extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private static VentanaPrincipal INSTANCE;
	
	private JButton btnABMClientes;
	private JButton btnABMUsuarios;
	private JButton btnABMSucursales;
	private JButton btnABMMarcas;
	private JButton btnABMModelos;
	private JButton btnABMStockAutomoviles;
	private JButton btnABMReservas;
	private JButton btnABMAutopartes;
	private JButton btnControlStock;
	private JButton btnRealizarVenta;
	private JButton btnMantenimientos;
	private JButton btnReportes;
	private JButton btnListaDeVentas;
	private JPanel panel_1;
	private JLabel label_3;
	
	public VentanaPrincipal() {
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		setBounds(new Rectangle(0, 0, 800, 600));
		setTitle("Menu Principal");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		ImageIcon imagenClientes = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/icono_clientes.png"));
		imagenClientes = new ImageIcon(imagenClientes.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		panel.setLayout(new GridLayout(0, 3, 5, 30));
		ImageIcon imagenUsuario = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/usuario.png"));
		imagenUsuario = new ImageIcon(imagenUsuario.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenSucursal = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/sucursal.png"));
		imagenSucursal = new ImageIcon(imagenSucursal.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenReserva = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/reserva.png"));
		imagenReserva = new ImageIcon(imagenReserva.getImage().getScaledInstance(40, 36, Image.SCALE_DEFAULT));
		ImageIcon imagenMarcas = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/marca.png"));
		imagenMarcas = new ImageIcon(imagenMarcas.getImage().getScaledInstance(35, 35, Image.SCALE_DEFAULT));
		ImageIcon imagenModelos = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/modelos.png"));
		imagenModelos = new ImageIcon(imagenModelos.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenAutomoviles = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/autos.png"));
		imagenAutomoviles = new ImageIcon(imagenAutomoviles.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenAutopartes = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/autopartes.png"));
		imagenAutopartes = new ImageIcon(imagenAutopartes.getImage().getScaledInstance(40,36, Image.SCALE_DEFAULT));
		ImageIcon imagenControlStock = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/control_stock.png"));
		imagenControlStock = new ImageIcon(imagenControlStock.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenVentas = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/ventas.png"));
		imagenVentas = new ImageIcon(imagenVentas.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenMantenimiento = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/mantenimiento.png"));
		imagenMantenimiento = new ImageIcon(imagenMantenimiento.getImage().getScaledInstance(40, 35, Image.SCALE_DEFAULT));
		ImageIcon imagenReportes = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/reporte.png"));
		imagenReportes = new ImageIcon(imagenReportes.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		ImageIcon imagenListaVentas = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/lista_ventas.png"));
		imagenListaVentas = new ImageIcon(imagenListaVentas.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		
		btnABMClientes = new JButton("Clientes");
		btnABMClientes.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMClientes.setIcon(imagenClientes);
		panel.add(btnABMClientes);
		
		btnABMUsuarios = new JButton("Usuarios");
		btnABMUsuarios.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMUsuarios.setIcon(imagenUsuario);
		panel.add(btnABMUsuarios);
		
		btnABMSucursales = new JButton("Sucursales");
		btnABMSucursales.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMSucursales.setIcon(imagenSucursal);
		panel.add(btnABMSucursales);
		
		btnABMReservas = new JButton("Reservas");
		btnABMReservas.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMReservas.setIcon(imagenReserva);
		panel.add(btnABMReservas);
		
		btnABMMarcas = new JButton("Marcas");
		btnABMMarcas.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMMarcas.setIcon(imagenMarcas);
		panel.add(btnABMMarcas);
		
		btnABMModelos = new JButton("Modelos");
		btnABMModelos.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMModelos.setIcon(imagenModelos);
		panel.add(btnABMModelos);
		
		btnABMAutopartes = new JButton("Stock de Autopartes");
		btnABMAutopartes.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMAutopartes.setIcon(imagenAutopartes);
		panel.add(btnABMAutopartes);
		
		btnABMStockAutomoviles = new JButton("Stock de Automoviles");
		btnABMStockAutomoviles.setHorizontalAlignment(SwingConstants.LEFT);
		btnABMStockAutomoviles.setIcon(imagenAutomoviles);
		panel.add(btnABMStockAutomoviles);
		
		btnControlStock = new JButton("Control de Stock");
		btnControlStock.setHorizontalAlignment(SwingConstants.LEFT);
		btnControlStock.setIcon(imagenControlStock);
		panel.add(btnControlStock);
		
		btnRealizarVenta = new JButton("Realizar Venta");
		btnRealizarVenta.setHorizontalAlignment(SwingConstants.LEFT);
		btnRealizarVenta.setIcon(imagenVentas);
		panel.add(btnRealizarVenta);
		
		btnMantenimientos = new JButton("Mantenimientos");
		btnMantenimientos.setHorizontalAlignment(SwingConstants.LEFT);
		btnMantenimientos.setIcon(imagenMantenimiento);
		panel.add(btnMantenimientos);
		
		btnReportes = new JButton("Reportes");
		btnReportes.setHorizontalAlignment(SwingConstants.LEFT);
		btnReportes.setIcon(imagenReportes);
		panel.add(btnReportes);
		
		label_3 = new JLabel("");
		panel.add(label_3);
		
		btnListaDeVentas = new JButton("Lista de Ventas");
		btnListaDeVentas.setHorizontalAlignment(SwingConstants.LEFT);
		btnListaDeVentas.setIcon(imagenListaVentas);
		panel.add(btnListaDeVentas);
		
		
		panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Stormwind");
		panel_1.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
	}

	public static VentanaPrincipal getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPrincipal(); 	
			return new VentanaPrincipal();
		}
		else
			return INSTANCE;
	}
	
	public void mostrarVentana() {
		this.setVisible(true);
	}
	
	public void cerrar() {
		this.dispose();
	}

	public JButton getBtnABMUsuarios() {
		return btnABMUsuarios;
	}

	public JButton getBtnABMClientes() {
		return btnABMClientes;
	}

	public JButton getBtnABMSucursales() {
		return btnABMSucursales;
	}
	
	public JButton getBtnABMMarcas() {
		return btnABMMarcas;
	}
	
	public JButton getBtnABMModelos() {
		return btnABMModelos;
	}
	
	public JButton getBtnABMStockAutomoviles() {
		return btnABMStockAutomoviles;
	}

	public JButton getBtnABMReservas() {
		return btnABMReservas;
	}
	
	public JButton getBtnABMAutopartes() {
		return btnABMAutopartes;
	}

	public JButton getBtnControlStock() {
		return btnControlStock;
	}

	public JButton getBtnRealizarVenta() {
		return btnRealizarVenta;
	}

	public JButton getBtnMantenimientos() {
		return btnMantenimientos;
	}
	
	public JButton getBtnReportes() {
		return btnReportes;
	}

	public JButton getBtnListaDeVentas() {
		return btnListaDeVentas;
	}
}
