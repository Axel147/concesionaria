package presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.conexion.Conexion;

public class ReporteVentaSucursalDetalle
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteVentaSucursalDetalle.class);
	
    public ReporteVentaSucursalDetalle()
    {
    	try	{
    		Map<String, Object> parametersMap = new HashMap<String, Object>();
    		String desde = JOptionPane.showInputDialog("Ingrese una fecha de incio \nUtilice un formato yyyy-mm-dd ||ejemplo(2020-12-17): ");
    		String hasta = JOptionPane.showInputDialog("Ingrese una fecha de fin \nUtilice un formato yyyy-mm-dd ||ejemplo(2020-12-17): ");
    		String sucursal = JOptionPane.showInputDialog("Ingrese nombre de la sucursal: ");
    		parametersMap.put("fechaDesde", desde);	
    		parametersMap.put("fechaHasta", hasta);
    		parametersMap.put("Sucursal", sucursal);
    		File n = new File("");
            String dir = n.getAbsolutePath()+"\\reportes\\Venta_Sucursal_Detalle.jrxml";         
            this.reporte = JasperCompileManager.compileReport(dir);
			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
					Conexion.getConexion().getSQLConexion());
    		log.info("Se cargó correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurrió un error mientras se cargaba el archivo Venta_Sucursal_Detalle.Jasper", ex);
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, "Error al cargar el reporte VentaSucursalDetalle");
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	