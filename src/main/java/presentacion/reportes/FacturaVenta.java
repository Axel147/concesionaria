package presentacion.reportes;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.conexion.Conexion;

public class FacturaVenta
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(FacturaVenta.class);
	
    public FacturaVenta(int idVenta)
    {
    	try	{
    		Map<String, Object> parametersMap = new HashMap<String, Object>();
    		parametersMap.put("idVenta", idVenta);
    		File n = new File("");
            String dir = n.getAbsolutePath()+"\\reportes\\FacturaVenta.jrxml";         
            this.reporte = JasperCompileManager.compileReport(dir);
			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
					Conexion.getConexion().getSQLConexion());
    		log.info("Se cargó correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurrió un error mientras se cargaba el archivo FacturaVenta.Jasper", ex);
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, "Error al cargar el reporte FacturaVenta");
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	