package presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.conexion.Conexion;

public class ReporteStockPorSucursal
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteStockPorSucursal.class);
	//Recibe la lista de personas para armar el reporte
    public ReporteStockPorSucursal()
    {
    	//Hardcodeado
		Map<String, Object> parametersMap = new HashMap<String, Object>();
//		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));	
//		parametersMap.put("Imagen", new File("").getAbsolutePath()+"\\imagenes\\logoAgenda.png");
    	try		{
    		File n = new File("");
            String dir = n.getAbsolutePath()+"\\reportes\\stock_auto_sucursal.jrxml";         
            this.reporte = JasperCompileManager.compileReport(dir);
			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
					Conexion.getConexion().getSQLConexion());
    		log.info("Se cargó correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurrió un error mientras se cargaba el archivo stock_auto_sucursal.Jasper", ex);
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	