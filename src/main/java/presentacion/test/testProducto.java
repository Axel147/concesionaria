package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;

import org.junit.Test;

import dto.AutoparteDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.Marca;
import modelo.Modelo;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import persistencia.dao.mysql.DAOSQLFactory;

import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.Notificador;
import presentacion.vista.ListaDeAutopartes;

import presentacion.vista.VentanaAutopartes;

public class testProducto {
	
	private VentanaAutopartes ventana;
	private ListaDeAutopartes ventanaAutoparte;
	private ControladorAutopartes controlador;
	private ControladorGeneral controladorGeneral;
	

	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorSucursal cs;
	private ControladorReserva cr;
	private ControladorStockAutomoviles cas;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	
	
	
	private List<AutoparteDTO> listaAutopartes;
	
	public void constructorTest()
	{
		ventana = VentanaAutopartes.getInstance();
		ventanaAutoparte = ListaDeAutopartes.getInstance();
		
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		au = new Autoparte(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cu = new ControladorUsuario(u,p,td);
		this.cs = new ControladorSucursal(s,p);
		this.cr = new ControladorReserva(r,er,at,p,td,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.notificador = new Notificador(r);
		
		
		controlador = new ControladorAutopartes(au,s,m,mo);
		controlador.inicializar();
		listaAutopartes = new ArrayList<AutoparteDTO>();
		
	}
	
	public void agregarAutoparteTest()
	{
		this.controlador.ventanaAgregarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnAgregar(), 1, "guardarSucursal()"));
	}
	
	@Test
	public void testAgregarAutoparte() 
	{
		this.constructorTest();
		agregarAutoparteTest();
		
		this.controlador.getVentanaAutopartes().getComboTipos().setSelectedIndex(2);
		this.controlador.getVentanaAutopartes().getComboModelos().setSelectedIndex(1);
		this.controlador.getVentanaAutopartes().getComboYear().setSelectedItem("1980");
		this.controlador.getVentanaAutopartes().getComboColor().setSelectedItem("Blanco");
		this.controlador.getVentanaAutopartes().setTxtStock("4");
		
		this.controlador.guardarAutoparte(new ActionEvent(this.controlador.getVentanaAutopartes().getBtnAutoparte(), 1, ""));
		
		this.listaAutopartes = au.obtenerAutopartes();
		
		
		assertTrue(this.listaAutopartes.get(0).getTipo().getNombre().equals("Llanta"));
		assertEquals(this.listaAutopartes.get(0).getModelo().getId(), 2);
		assertTrue(this.listaAutopartes.get(0).getYear().equals("1980"));
		assertTrue(this.listaAutopartes.get(0).getColor().equals("Blanco"));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
	}
	
	
	public void testAgregarAutoparteRepetido()
	{
		this.constructorTest();
		agregarAutoparte();
		
		int sinAgregadoRepetido = au.obtenerAutopartes().size();
		
		agregarAutoparte();
		
		this.controlador.guardarAutoparte(new ActionEvent(this.controlador.getVentanaAutopartes().getBtnAutoparte(), 1, ""));
		
		this.listaAutopartes = au.obtenerAutopartes();
		
		assertEquals(this.listaAutopartes.size(), sinAgregadoRepetido);
	}
	
	
	public void testBorrarAutoparte()
	{
		this.constructorTest();
		agregarAutoparte();
		
		int sinBorrar = au.obtenerAutopartes().size();
		
		this.controlador.getVentanaListaDeAutopartes().getTablaAutopartes().changeSelection(this.listaAutopartes.size()-1, 0, false, false);
		
		this.controlador.borrarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnBorrar(), 1, ""));
		
		this.listaAutopartes = au.obtenerAutopartes();
		
		assertEquals(this.listaAutopartes.size(), sinBorrar -1);
	}
	
	
	public void testBorrarAutoparteSinSeleccion()
	{
		this.constructorTest();
		agregarAutoparte();
		
		this.controlador.borrarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnBorrar(), 1, ""));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
	}
	
	
	public void testBorrarAutoparteMasSeleccion()
	{
		this.constructorTest();
		agregarAutoparte();
		
		this.controlador.getVentanaAutopartes().getComboTipos().setSelectedIndex(1);
		this.controlador.getVentanaAutopartes().getComboModelos().setSelectedIndex(1);
		this.controlador.getVentanaAutopartes().getComboYear().setSelectedItem("1988");
		this.controlador.getVentanaAutopartes().getComboColor().setSelectedItem("Violeta");
		this.controlador.getVentanaAutopartes().setTxtStock("5");
		
		this.controlador.getVentanaListaDeAutopartes().getTablaAutopartes().setRowSelectionInterval(0, 1);
		
		this.controlador.borrarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnBorrar(), 1, ""));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(1));
	}
	
	
	public void agregarAutoparte()
	{
		this.constructorTest();
		agregarAutoparteTest();
		
		this.controlador.getVentanaAutopartes().getComboTipos().setSelectedIndex(0);
		this.controlador.getVentanaAutopartes().getComboModelos().setSelectedIndex(0);
		this.controlador.getVentanaAutopartes().getComboYear().setSelectedItem("1980");
		this.controlador.getVentanaAutopartes().getComboColor().setSelectedItem("Blanco");
		this.controlador.getVentanaAutopartes().setTxtStock("4");
		
		this.controlador.guardarAutoparte(new ActionEvent(this.controlador.getVentanaAutopartes().getBtnAutoparte(), 1, ""));
		
		this.listaAutopartes = au.obtenerAutopartes();
	}
	
	
	public void testEditarAutopartes()
	{
		this.constructorTest();
		agregarAutoparte();
		
		this.controlador.getVentanaListaDeAutopartes().getTablaAutopartes().changeSelection(this.listaAutopartes.size()-1, 0, false, false);
		
		this.controlador.ventanaEditarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnEditar(), 1, ""));
		
		this.controlador.getVentanaAutopartes().getComboTipos().setSelectedIndex(1);
		this.controlador.getVentanaAutopartes().getComboModelos().setSelectedIndex(1);
		this.controlador.getVentanaAutopartes().getComboYear().setSelectedItem("1981");
		this.controlador.getVentanaAutopartes().getComboColor().setSelectedItem("Verde");
		this.controlador.getVentanaAutopartes().setTxtStock("4");
		
		this.controlador.guardarAutoparte(new ActionEvent(this.controlador.getVentanaAutopartes().getBtnAutoparte(), 1, ""));
		
		this.listaAutopartes = au.obtenerAutopartes();
		
		assertTrue(this.listaAutopartes.get(0).getTipo().getNombre().equals("Bujias"));
		assertEquals(this.listaAutopartes.get(0).getModelo().getId(), 2);
		assertTrue(this.listaAutopartes.get(0).getYear().equals("1981"));
		assertTrue(this.listaAutopartes.get(0).getColor().equals("Verde"));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
	}
	
	
	public void testEditarSinSeleccion()
	{
		this.constructorTest();
		agregarAutoparte();
		
		this.controlador.ventanaEditarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnEditar(), 1, ""));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
	}
	
	
	public void testEditarConMasSeleccion()
	{
		this.constructorTest();
		agregarAutoparte();
		
		agregarAutoparte();//por el momento aprovecho que puede agregar el mismo  y hacer el test :D
		
		this.controlador.getVentanaListaDeAutopartes().getTablaAutopartes().setRowSelectionInterval(0, 1);
		
		this.controlador.ventanaEditarAutoparte(new ActionEvent(this.controlador.getVentanaListaDeAutopartes().getBtnEditar(), 1, ""));
		
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(0));
		this.controlador.getAutoparte().borrarAutoparte(this.listaAutopartes.get(1));
	}
	
	

}
