package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dto.MarcaDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.Marca;
import modelo.Modelo;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.Notificador;
import presentacion.vista.ListaDeMarcas;
import presentacion.vista.VentanaMarca;

public class testMarca {

	private VentanaMarca ventana;
	private ListaDeMarcas ventanaMarca;
	private ControladorGeneral controladorGeneral;
	private ControladorMarca controlador;
	
	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorSucursal cs;
	private ControladorReserva cr;
	private ControladorStockAutomoviles cas;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	
	
	private List<MarcaDTO> listaMarcas;
	
	public void constructorTest()
	{
		ventana = VentanaMarca.getInstance();
		ventanaMarca = ListaDeMarcas.getInstance();
		ventanaMarca = ListaDeMarcas.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		au = new Autoparte(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cu = new ControladorUsuario(u,p,td);
		this.cs = new ControladorSucursal(s,p);
		this.cr = new ControladorReserva(r,er,at,p,td,m,mo);
		this.ca = new ControladorAutopartes(au,s,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.notificador = new Notificador(r);
		
		controlador = new ControladorMarca(m);
		controladorGeneral = new ControladorGeneral(cc,cu,cs,cr,cas,ca,cmodel,controlador,notificador);
		
		listaMarcas = new ArrayList<MarcaDTO>();
		controlador.inicializar(controladorGeneral);
		
		
	}
	
	
	public void testAgregarMarca()
	{
		this.controlador.getVentanaMarca().setTipoVentana(1);
		this.controlador.getVentanaMarca().setTitle("Agregar Marca");
		this.controlador.getVentanaMarca().mostrarVentana();
	}
	
	
	public void testCampoNombreVacio() 
	{
		constructorTest();
		testAgregarMarca();
		
		controlador.getVentanaMarca().setTxtNombre("");
		
		int listaSinAgregado = m.obtenerMarca().size();
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarMarca()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinAgregado, listaMarcas.size());
		
	}
	
	
	public void testCampoNombreConTresLetras() 
	{
		constructorTest();
		testAgregarMarca();
		
		controlador.getVentanaMarca().setTxtNombre("asd");
		
		int listaSinAgregado = m.obtenerMarca().size();
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarMarca()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinAgregado+1, listaMarcas.size()); 
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
	}
	
	
	public void testCampoNombreConNumeros()
	{
		constructorTest();
		testAgregarMarca();
		
		controlador.getVentanaMarca().setTxtNombre("134556");
		
		int listaSinAgregado = m.obtenerMarca().size();
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinAgregado, listaMarcas.size()); 
	}
	
	
	public void testCampoNombreConSimbolos()
	{
		constructorTest();
		testAgregarMarca();
		
		controlador.getVentanaMarca().setTxtNombre(")))))((((///%&&&&%%%");
		
		int listaSinAgregado = m.obtenerMarca().size();
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaMarcas = m.obtenerMarca();
		assertEquals(listaSinAgregado, listaMarcas.size()); //revisado
	}
	
	
	public void testCampoNombreConEspacio()
	{
		constructorTest();
		testAgregarMarca();

		controlador.getVentanaMarca().setTxtNombre("Brayan Axel");
		
		int listaSinAgregado = m.obtenerMarca().size();
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinAgregado+1, listaMarcas.size()); //revisado
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
		
	}
	
	public void agregarMarca(String nombre)
	{
		constructorTest();
		testAgregarMarca();
		controlador.getVentanaMarca().setTxtNombre(nombre);
		
		controlador.guardarMarca(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaMarcas = m.obtenerMarca();
		
	}

//	Esta comentado porque se necesita que este relacionado con modelos
//	public void testBorrarMarcaConModelos()
//	{
//		constructorTest();
//		
//		int listaSinBorrar = m.obtenerMarca().size();
//		
//		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
//		this.controlador.borrarMarca(new ActionEvent(this.controlador.getVentanaListaDeMarcas().getBtnBorrar(), 1, "borrarSucursal()"));
//		
//		listaMarcas = m.obtenerMarca();
//		
//		assertEquals(listaSinBorrar+1, listaMarcas.size());
//	}
	

	public void testBorrarMarcaSinModelos()
	{
		constructorTest();
		int listaSinBorrar = m.obtenerMarca().size();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(1, 0, false, false);
		this.controlador.borrarMarca(new ActionEvent(this.controlador.getVentanaListaDeMarcas().getBtnBorrar(), 1, "borrarMarca()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinBorrar-1, listaMarcas.size());
	}
	
	@Test
	public void testBorrarMarcaMasSeleccion()
	{
		this.constructorTest();
		this.agregarMarca("Bangho");
		this.agregarMarca("Fiat");
		
		int listaSinBorrar = m.obtenerMarca().size();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().setRowSelectionInterval(0, 1);
		
		this.controlador.borrarMarca(new ActionEvent(this.controlador.getVentanaListaDeMarcas().getBtnBorrar(), 1, "borrarMarca()"));
		
		listaMarcas = m.obtenerMarca();
		
		assertEquals(listaSinBorrar, listaMarcas.size());
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
		this.controlador.getMarca().borrarMarca(listaMarcas.get(1));
		
	}
	
	public void testEditarMarca()
	{
		this.constructorTest();
		this.agregarMarca("Bangho");
		
		String nomreSinModificar = this.listaMarcas.get(0).getNombre();
		int t = listaMarcas.size();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getVentanaMarca().setTxtNombre("Bango");
		
		controlador.guardarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "guardarMarca()"));
		
		this.listaMarcas = m.obtenerMarca();

		assertFalse(listaMarcas.get(0).getNombre().equals(nomreSinModificar));
		assertEquals(t, listaMarcas.size());
		
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
		
	}
	
	
	public void testEditarMasSelecciones()
	{
		this.constructorTest();
		this.agregarMarca("Bangho");
		this.agregarMarca("Fiat");
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().setRowSelectionInterval(0, 1);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
		this.controlador.getMarca().borrarMarca(listaMarcas.get(1));
	}
	
	
	public void testEditarSinSeleccion()
	{
		this.constructorTest();
		this.agregarMarca("Bangho");
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
	}
	
	
	public void testEditarNombreVacio()
	{
		this.constructorTest();
		this.agregarMarca("Fiat");
		
		String nombreSinModificar = this.listaMarcas.get(0).getNombre();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getVentanaMarca().setTxtNombre("");
		
		controlador.guardarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "guardarMarca()"));
		
		this.listaMarcas = m.obtenerMarca();

		assertTrue(listaMarcas.get(0).getNombre().equals(nombreSinModificar));
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
		
	}
	
	
	public void testEditarNombreConNumeros()
	{
		this.constructorTest();
		this.agregarMarca("Fiat");
		
		String nombreSinModificar = this.listaMarcas.get(0).getNombre();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getVentanaMarca().setTxtNombre("242546");
		
		controlador.guardarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "guardarMarca()"));
		
		this.listaMarcas = m.obtenerMarca();

		assertTrue(listaMarcas.get(0).getNombre().equals(nombreSinModificar));
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
	}
	
	
	public void testEditarNombreConSimbolos()
	{
		this.constructorTest();
		this.agregarMarca("Fiat");
		
		String nombreSinModificar = this.listaMarcas.get(0).getNombre();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getVentanaMarca().setTxtNombre("(()=?¡))=%&&$%");
		
		controlador.guardarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "guardarMarca()"));
		
		this.listaMarcas = m.obtenerMarca();

		assertTrue(listaMarcas.get(0).getNombre().equals(nombreSinModificar));
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
	}
	
	
	public void testEditarNombreConEspacios()
	{
		this.constructorTest();
		this.agregarMarca("Fiat");
		
		String nombreSinModificar = this.listaMarcas.get(0).getNombre();
		
		this.controlador.getVentanaListaDeMarcas().getTablaMarcas().changeSelection(0, 0, false, false);
		
		this.controlador.ventanaEditarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "editarMarca()"));
		
		this.controlador.getVentanaMarca().setTxtNombre("Bangho Pro");
		
		controlador.guardarMarca(new ActionEvent(this.controlador.getVentanaMarca().getBtnGuardar(), 1, "guardarMarca()"));
		
		this.listaMarcas = m.obtenerMarca();

		assertFalse(listaMarcas.get(0).getNombre().equals(nombreSinModificar));
		
		this.controlador.getMarca().borrarMarca(listaMarcas.get(0));
	}

}
