package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.Notificador;
import dto.ClienteDTO;
import dto.SucursalDTO;
import presentacion.vista.ListaDeSucursales;
import presentacion.vista.VentanaCliente;
import presentacion.vista.VentanaSucursal;

import org.junit.Test;

import dto.SucursalDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import modelo.Marca;
import modelo.Modelo;
import persistencia.dao.mysql.DAOSQLFactory;

public class testSucursal {
	
	private VentanaSucursal ventana;
	private ControladorGeneral controladorGeneral;
	private ControladorSucursal controlador;

	
	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorStockAutomoviles cas;
	private ControladorReserva cr;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private ControladorMarca cmarca;
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	
	private List<SucursalDTO> listaSucursales;
	
	public void constructorTest()
	{
		ventana = VentanaSucursal.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		au = new Autoparte(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cu = new ControladorUsuario(u,p,td);
		this.cas = new ControladorStockAutomoviles(as,m,mo,s);
		this.cr = new ControladorReserva(r,er,at,p,td,m,mo);
		this.ca = new ControladorAutopartes(au,s,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.cmarca = new ControladorMarca(m);
		this.notificador = new Notificador(r);
		
		controlador = new ControladorSucursal(s,p);
		controladorGeneral = new ControladorGeneral(cc,cu,controlador,cr,cas,ca,cmodel,cmarca,notificador);
		controlador.inicializar(controladorGeneral);
		listaSucursales = new ArrayList<SucursalDTO>();
		
	}
	
	public void agregarSucursalTest()
	{
		this.controlador.getVentanaSucursal().setTipoVentana(1);
		this.controlador.getVentanaSucursal().setTitle("Agregar Sucursal");
		this.controlador.getVentanaSucursal().mostrarVentana();
	
	}

	public void testCampoNombreVacio() 
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("");
		controlador.getVentanaSucursal().setCalle("CalleFalsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); 
	}
	
	
	public void testCampoNombreConNumeros()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("134556");
		controlador.getVentanaSucursal().setCalle("CallesFalsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); 
	}
	
	
	public void testCampoNombreConSimbolos()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre(")))))((((///%&&&&%%%");
		controlador.getVentanaSucursal().setCalle("Calles Falsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	}
	
	
	public void testCampoNombreConEspacio()
	{
		constructorTest();
		agregarSucursalTest();

		controlador.getVentanaSucursal().setNombre("Brayan Axel");
		controlador.getVentanaSucursal().setCalle("CallesFalsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado+1, listaSucursales.size()); //revisado
	}
	
	
	public void testCampoCalleVacio()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	}
	
	
	
	public void testCampoCalleConNumeros()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("123143564");
		controlador.getVentanaSucursal().setAltura("1234");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	}
	
	
	public void testCampoCalleConSimbolos()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("(()))==??ยก)(/&%$%$");
		controlador.getVentanaSucursal().setAltura("1234");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	}
	
	
	public void testCampoCalleConEspacio()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado+1, listaSucursales.size()); //revisado
	}
	
	
	public void testCampoAlturaVacio()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	
	}
	
	
	public void testCampoAlturaConLetras()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("asfsdgd");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	
	}
	
	
	public void testCampoAlturaConSimbolos()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("/&(/(&)//(%&%$%#$");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	
	}
	
	
	public void testCampoAlturaConEspacios()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("12 34");
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	
	}
	
	
	public void testCampoAlturaSuperiorACuatro()
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("1212454");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	
	}
	
	
	public void testBorrarSucursal() 
	{	
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre("Axel");
		controlador.getVentanaSucursal().setCalle("Calle Falsa");
		controlador.getVentanaSucursal().setAltura("1234");
		
		int listaSinAgregado = s.obtenerSucursales().size();
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));

		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.borrarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnBorrar(), 1, "borrarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertEquals(listaSinAgregado, listaSucursales.size()); //revisado
	}
	
	
	public void agregarSucursal(String nombre, String calle, String altura)
	{
		constructorTest();
		agregarSucursalTest();
		
		controlador.getVentanaSucursal().setNombre(nombre);
		controlador.getVentanaSucursal().setCalle(calle);
		controlador.getVentanaSucursal().setAltura(altura);
		
		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
	}
	
	
	public void testEditarSucursal()
	{
		constructorTest();
		agregarSucursal("Barbi","Einstein","2222");
		
		String calleSinModificar = listaSucursales.get(listaSucursales.size()-1).getCalle();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("CalleFalsa");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertFalse(listaSucursales.get(listaSucursales.size()-1).getCalle().equals(calleSinModificar));
	}
	

	public void testEditarSucursalSinSeleccion()
	{
		constructorTest();
		agregarSucursal("Axel","Calle Falsa","123");
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("CalleFalsa");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
	}
	
	
	public void testEditarMasSelecciones()
	{
		constructorTest();
		agregarSucursal("Pepito Fulanito","Rivadavia","1234");
		agregarSucursal("Cosme","Paraguya","1234");
		agregarSucursal("jsdahkjf","roberto","1234");
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().setRowSelectionInterval(0, s.obtenerSucursales().size()-1);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("Rivada via");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertFalse(listaSucursales.get(listaSucursales.size()-1).getCalle().equals("Rivada via"));
	}
	
	
	public void testEditarNombreVacio()
	{
		constructorTest();
		agregarSucursal("Pepito","Rivadavia","1234");
		
		String nombre = listaSucursales.get(listaSucursales.size()-1).getNombre();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setNombre("");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getNombre().equals(nombre));
	}
	
	
	public void testEditarNombreConSimbolos()
	{
		constructorTest();
		agregarSucursal("Anonimus","Rivadavia","1234");
		
		String nombre = listaSucursales.get(listaSucursales.size()-1).getNombre();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setNombre("()//)/)¡?¡?¡?%#%&/");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getNombre().equals(nombre));
	}
	
	
	public void testEditarNombreConEspacios()
	{
		constructorTest();
		agregarSucursal("Antonimus","Rivadavia","1234");
		
		String nombre = listaSucursales.get(listaSucursales.size()-1).getNombre();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setNombre("Antonimus operus");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertFalse(listaSucursales.get(listaSucursales.size()-1).getNombre().equals(nombre));
	}
	
	
	public void testEditarNombreSoloEspacios()
	{
		constructorTest();
		agregarSucursal("Bangho","Rivadavia","1234");
		
		String nombre = listaSucursales.get(listaSucursales.size()-1).getNombre();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setNombre("   ");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getNombre().equals(nombre));
	}
	
	
	public void testEditarCalleVacio()
	{
		constructorTest();
		agregarSucursal("Pepito","Rivadavia","1234");
		
		String calle = listaSucursales.get(listaSucursales.size()-1).getCalle();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getNombre().equals(calle));
	}
	
	
	public void testEditarCalleConSimbolos()
	{
		constructorTest();
		agregarSucursal("Anonimus","Rivadavia","1234");
		
		String calle = listaSucursales.get(listaSucursales.size()-1).getCalle();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("()//)/)¡?¡?¡?%#%&/");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getCalle().equals(calle));
	}
	
	
	public void testEditarCalleConEspacios()
	{
		constructorTest();
		agregarSucursal("Antonimus","Rivadavia","1234");
		
		String calle = listaSucursales.get(listaSucursales.size()-1).getCalle();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("Rivadavia Vive");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertFalse(listaSucursales.get(listaSucursales.size()-1).getCalle().equals(calle));
	}
	
	
	public void testEditarCalleSoloEspacios()
	{
		constructorTest();
		agregarSucursal("Bangho","Rivadavia","1234");
		
		String calle = listaSucursales.get(listaSucursales.size()-1).getCalle();
		
		this.controlador.getVentanaListaDeSucursales().getTablaSucursales().changeSelection(s.obtenerSucursales().size()-1, 0, false, false);
		
		this.controlador.ventanaEditarSucursal(new ActionEvent(this.controlador.getVentanaListaDeSucursales().getBtnEditar(), 1, "editarSucursal()"));
		
		controlador.getVentanaSucursal().setCalle("   ");

		controlador.guardarSucursal(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarSucursal()"));
		
		listaSucursales = s.obtenerSucursales();
		
		assertTrue(listaSucursales.get(listaSucursales.size()-1).getCalle().equals(calle));
	}
}
