package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dto.UsuarioDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import modelo.Marca;
import modelo.Modelo;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.Notificador;
import presentacion.vista.VentanaUsuario;

public class testUsuario {
	
	private VentanaUsuario ventana;
	private ControladorUsuario controlador;
	private ControladorGeneral controladorGeneral;
	
	private ControladorSucursal cs;
	private ControladorCliente cc;
	private ControladorStockAutomoviles cas;
	private ControladorReserva cr;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private ControladorMarca cmarca;
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	private List<UsuarioDTO> listaUsuarios;
	
	public void constructorTest()
	{
		ventana = VentanaUsuario.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		au = new Autoparte(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cs = new ControladorSucursal(s,p);
		this.cas = new ControladorStockAutomoviles(as,m,mo,s);
		this.cr = new ControladorReserva(r,er,at,p,td,m,mo);
		this.ca = new ControladorAutopartes(au,s,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.cmarca = new ControladorMarca(m);
		this.notificador = new Notificador(r);
	
		controlador = new ControladorUsuario(u,p,td);
		controladorGeneral = new ControladorGeneral(cc,controlador,cs,cr,cas,ca,cmodel,cmarca,notificador);
		controlador.inicializar(controladorGeneral);
		listaUsuarios = new ArrayList<UsuarioDTO>();
		
	}
	
	public void agregarUsuarioTest()
	{
		this.ventana.modoAgregar();
	}
	
	public void editarUsuarioTest()
	{
		this.ventana.modoEditar();
	}
	
	@Test
	public void testCampoNombreVacio() 
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("");
		controlador.getVentanaUsuario().setTxtApellido("Perrez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size()); //No agrego el contacto
	}
	
	@Test
	public void testCampoNombreConNumeros()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("1232436");
		controlador.getVentanaUsuario().setTxtApellido("Perrez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoNombreConSimbolos()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("()_////////");
		controlador.getVentanaUsuario().setTxtApellido("Perrez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoNombreConEspacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Brian Axel");
		controlador.getVentanaUsuario().setTxtApellido("Perrez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(1, listaUsuarios.size());
	}
	
	@Test
	public void testCampoApellidoVacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	
	@Test
	public void testCampoApellidoConNumeros()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("12334354");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoApellidoConSimbolos()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("()()(/////////////");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoApellidoConEspacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Dominguez Cejas");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(1, listaUsuarios.size());
	}
	
	@Test
	public void testCampoDocumentoVacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoDocumentoConLetras()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("asfdgfhh");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoDocumentoConSimbolos()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("()()()//////&%$");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoDocumentoConEspacios()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768 555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoUsuarioVacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoUsuarioSoloEspacios()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("    ");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoUsuarioConSimbolos()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("()())()()(///////////");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoUsuarioConEspacios()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan 95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailVacio()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailConNumeros()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("12354");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailSinPatron()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("@asfghjgh");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailConDosArrobas()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailComenzoSimbolo()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Lucas");
		controlador.getVentanaUsuario().setTxtApellido("Sanches");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("_perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailSoloSimbolos()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Lopez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("()((////%%&");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoEmailConEspacios()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Lopez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail. com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoPasswordMenorAOcho()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Lopez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("elbraya");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoPasswordSinPatron()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Lopez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan95");
		controlador.getVentanaUsuario().setTxtPassword("())//??¡?ASASA");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(0, listaUsuarios.size());
	}
	
	@Test
	public void testCampoPasswordCorrecto()
	{
		constructorTest();
		agregarUsuarioTest();
		
		controlador.getVentanaUsuario().setTxtNombre("Ricardo");
		controlador.getVentanaUsuario().setTxtApellido("Lopez");
		controlador.getVentanaUsuario().setTxtDocumento("39768555");
		controlador.getVentanaUsuario().setTxtUsuario("Brayan");
		controlador.getVentanaUsuario().setTxtPassword("elbrayan");
		controlador.getVentanaUsuario().setTxtMail("perez@gmail.com");
		
		controlador.guardarUsuario(new ActionEvent(ventana.getBtnUsuario(), 1, "guardarUsuario()"));
		
		listaUsuarios = u.obtenerUsuarios();
		
		assertEquals(1, listaUsuarios.size());
	}
	
}
