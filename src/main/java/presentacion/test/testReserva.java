package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;

import dto.ClienteDTO;
import dto.ReservaDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.Marca;
import modelo.Modelo;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.Notificador;
import presentacion.vista.VentanaCliente;

class testReserva {
	
	private VentanaCliente ventana;
	private ControladorReserva controlador;
	private ControladorGeneral controladorGeneral;
	
	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorSucursal cs;
	private ControladorStockAutomoviles cas;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private ControladorMarca cmarca;
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	
	private List<ReservaDTO> listaReservas;
	private List<ClienteDTO> listaClientes;
	
	public void constructorTest()
	{
		ventana = VentanaCliente.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cu = new ControladorUsuario(u,p,td);
		this.cs = new ControladorSucursal(s,p);
		this.ca = new ControladorAutopartes(au,s,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.cmarca = new ControladorMarca(m);
		this.notificador = new Notificador(r);
	
		controlador = new ControladorReserva(r,er,at,p,td,m,mo);
		controladorGeneral = new ControladorGeneral(cc,cu,cs,controlador,cas,ca,cmodel,cmarca,notificador);
		listaReservas = new ArrayList<ReservaDTO>();
		this.listaClientes = new ArrayList<ClienteDTO>();
		controlador.inicializar(controladorGeneral);
		
	}
	
	public void agregarReservaTest()
	{
		this.controlador.ventanaAgregarReserva(new ActionEvent(this.controlador.getVentanaListaDeReservas().getBtnAgregar(),1,""));
	}
	
	public void seleccionarClienteTest()
	{
		this.listaClientes = c.obtenerClientes();
		
		this.controlador.getVentanaReserva().setIdCliente(this.listaClientes.get(0).getIdCliente());
		this.controlador.getVentanaReserva().setNombre(this.listaClientes.get(0).getNombre());
		this.controlador.getVentanaReserva().setTelefono(this.listaClientes.get(0).getTelefono());
		this.controlador.getVentanaReserva().setApellido(this.listaClientes.get(0).getApellido());
		this.controlador.getVentanaReserva().setMail(this.listaClientes.get(0).getMail());
		this.controlador.getVentanaReserva().setNumDoc(this.listaClientes.get(0).getNumDocumento());
		this.controlador.getVentanaReserva().getCmbPaisResidencia().setSelectedItem(this.listaClientes.get(0).getPaisResidencia());
		this.controlador.getVentanaReserva().getCmbTipoDoc().setSelectedItem(this.listaClientes.get(0).getTipoDocumento());
		
	}
	

	
	void testAgregarReserva() 
	{
		this.constructorTest();
		this.agregarReservaTest();
		seleccionarClienteTest();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date fechaConvertida = format.parse("2020-12-23");
			this.controlador.getVentanaReserva().setFechaTurno(fechaConvertida);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.controlador.getVentanaReserva().setPatente("ASD 123");
		this.controlador.getVentanaReserva().setYear("2000");
		
		int sinGuardar = this.r.obtenerReservas().size();
		
		this.controlador.guardarReserva(new ActionEvent(this.controlador.getVentanaReserva().getBtnGuardar(),1,""));
		
		this.listaReservas = r.obtenerReservas();
		
		assertEquals(this.listaReservas.size(), sinGuardar+1);
		
//		this.controlador.getReserva().cancelarReserva(this.listaReservas.get(this.listaReservas.size()-1), "porque quise");
		
	}
	
	public void agregarReserva()
	{
		this.constructorTest();
		this.agregarReservaTest();
		seleccionarClienteTest();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date fechaConvertida = format.parse("2020-12-23");
			this.controlador.getVentanaReserva().setFechaTurno(fechaConvertida);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.controlador.getVentanaReserva().setPatente("ASD 123");
		this.controlador.getVentanaReserva().setYear("2000");
		
		this.controlador.guardarReserva(new ActionEvent(this.controlador.getVentanaReserva().getBtnGuardar(),1,""));
		
		this.listaReservas = r.obtenerReservas();
		
		
	}
	
	@Test
	public void testEditarReserva()
	{
		agregarReserva();
		
		this.controlador.getVentanaListaDeReservas().getTablaPersonas().changeSelection(this.listaReservas.size()-1, 0, false, false);
		
		this.controlador.ventanaEditarReserva(new ActionEvent(this.cs.getVentanaListaDeSucursales().getBtnEditar(),1,""));
		
		this.controlador.getVentanaReserva().setPatente("ASD 345");
		
		this.controlador.guardarReserva(new ActionEvent(this.controlador.getVentanaReserva().getBtnGuardar(),1,""));
		
		assertTrue(this.listaReservas.get(this.listaReservas.size()-1).getAuto().equals("ASD 345"));
		
	}

}
