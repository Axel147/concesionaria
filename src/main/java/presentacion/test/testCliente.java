package presentacion.test;

import static org.junit.Assert.*;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import presentacion.controlador.ControladorCliente;

//import org.junit.jupiter.api.Test;

import presentacion.controlador.ControladorGeneral;
import dto.ClienteDTO;
import presentacion.vista.VentanaCliente;
import modelo.Automovil;
import modelo.Cliente;
import modelo.EstadoReserva;
import modelo.AutomovilTaller;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import modelo.Marca;
import modelo.Modelo;
import persistencia.dao.mysql.DAOSQLFactory;

public class testCliente {
	
	private VentanaCliente ventana;
	private ControladorGeneral controladogeneral;
	private Cliente c;
	private ControladorCliente cc;
	private Pais p;
	private TipoDocumento td;
	private List<ClienteDTO> listaClientes;
	
	public void constructorTest()
	{
		ventana = VentanaCliente.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		cc = new ControladorCliente(c,p,td);
	
		controladogeneral = new ControladorGeneral(cc,null,null,null,null,null,null,null,null);
		listaClientes = new ArrayList<ClienteDTO>();
		cc.inicializar(controladogeneral);
		
	}
	
	public void agregarClienteTest()
	{
		this.ventana.setTipoVentana(1);
		this.ventana.setTitle("Agregar Cliente");
		this.ventana.mostrarVentana();
	
	}
	
	@Test
	public void testAgregarClienteValido()
	{
		constructorTest();
		agregarClienteTest();
		
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Pedro");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setApellido("Perez Pereyra");
		cc.getVentanaCliente().setNumDoc("39625020");
		cc.getVentanaCliente().setMail("pobrepintorportuguez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado +1,listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		
	}
	
	@Test
	public void testCampoNombreVacio() 
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size()); //No agrego el contacto
	}
	
	@Test
	public void testCampoNombreNombreCorto()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Liz");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado + 1, listaClientes.size()); 
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
	}
	
	@Test
	public void testCampoNombreNombreLargo()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("RobertoGuillermoSegundoMardonadoadsafdgfdhfdhsdf");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size()); 
		
	}
	
	@Test
	public void testCampoNombreConEspacios()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Bayan Axel");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado + 1, listaClientes.size()); 
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		
	}
	
	@Test
	public void testCampoNombreSoloEspacios()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("  ");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size()); //No agrego el contacto
	}
	
	@Test
	public void testCampoNombreConNumeros()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("1232436");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoNombreConSimbolos() 
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("()_////////");
		cc.getVentanaCliente().setApellido("Perrez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		cc.getVentanaCliente().setTelefono("1145678909");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
	
		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoApellidoVacio()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoApellidoSoloEspacios()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("  ");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoApellidoConPocosCaracteres()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("asd");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado + 1, listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		
	}
	
	@Test
	public void testCampoApellidoLargo()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("asassfghjghkhgfdsdvcnmdsfasasdfdghjhgfadsfhgj");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado + 1, listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		
	}
	
	@Test
	public void testCampoApellidoConEspacio()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Dominguez Cejas");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado + 1, listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
	}
	
	@Test
	public void testCampoApellidoConNumeros()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("12334354");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes= c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoApellidoConSimbolos()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("()()(/////////////");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado,listaClientes.size());
		
	}
	
	@Test
	public void testCampoDocumentoVacio()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes= c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoDocumentoConLetras()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("asfdgfhh");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoDocumentoConEspacios()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768 555");
		cc.getVentanaCliente().setTelefono("1145678909");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoTelefonoVacio()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoTelefonoConSimbolos()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("()())()()(///////////");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoTelefonoConEspacios()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1111111 95");
		cc.getVentanaCliente().setMail("perez@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	@Test
	public void testCampoEmailVacio()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("1111111111");
		cc.getVentanaCliente().setMail("");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoEmailSinPatron()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("Brayan95");
		cc.getVentanaCliente().setMail("@asfghjgh");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();

		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoEmailConDosArrobas()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Sanches");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("Brayan95");
		cc.getVentanaCliente().setMail("perez@@gmail.com");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado,listaClientes.size());
	}
	
	@Test
	public void testCampoEmailSoloSimbolos()
	{
		constructorTest();
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre("Ricardo");
		cc.getVentanaCliente().setApellido("Lopez");
		cc.getVentanaCliente().setNumDoc("39768555");
		cc.getVentanaCliente().setTelefono("Brayan95");
		cc.getVentanaCliente().setMail("()((////%%&");
		
		int listaSinAgregado = c.obtenerClientes().size();
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(listaSinAgregado, listaClientes.size());
	}
	
	
	public void agregarCliente(String nombre, String apellido, String numDoc, String telefono, String mail)
	{
		agregarClienteTest();
		this.cc.ventanaAgregarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "ventana"));
		
		cc.getVentanaCliente().setNombre(nombre);
		cc.getVentanaCliente().setApellido(apellido);
		cc.getVentanaCliente().setNumDoc(numDoc);
		cc.getVentanaCliente().setTelefono(telefono);
		cc.getVentanaCliente().setMail(mail);
		
		cc.guardarCliente(new ActionEvent(ventana.getBtnGuardar(), 1, "guardarCliente()"));
		
		listaClientes = c.obtenerClientes();
	}
	
	@Test
	public void testBorrarCliente()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");
		int sinBorrar = c.obtenerClientes().size();
		
		this.cc.getVentanaListaDeCliente().getTablaPersonas().changeSelection(this.listaClientes.size()-1, 0, false, false);
		
		this.cc.borrarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnBorrar(), 1, "borrarCliente"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(sinBorrar -1, listaClientes.size());
		
	}
	
	@Test
	public void testBorrarSinSeleccion()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");
		int sinBorrar = c.obtenerClientes().size();
		
		this.cc.borrarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnBorrar(), 1, "borrarCliente"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(sinBorrar, listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
	}
	
	@Test
	public void testBorrarConMasSelecciones()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");
		this.agregarCliente("Lucho", "Port", "13153143", "1122456789", "luchitom@gmail.com");
		int sinBorrar = c.obtenerClientes().size();
		
		this.cc.getVentanaListaDeCliente().getTablaPersonas().setRowSelectionInterval(0, 1);
		
		this.cc.borrarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnBorrar(), 1, "borrarCliente"));
		
		listaClientes = c.obtenerClientes();
		
		assertEquals(sinBorrar, listaClientes.size());
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		this.cc.getCliente().borrarCliente(listaClientes.get(1));
	}
	
	@Test
	public void testEditarCliente()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");
		String sinEditar = listaClientes.get(0).getNombre();
		
		this.cc.getVentanaListaDeCliente().getTablaPersonas().changeSelection(this.listaClientes.size()-1, 0, false, false);
		
		this.cc.ventanaEditarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnEditar(), 1, "editarCliente"));
		
		cc.getVentanaCliente().setNombre("Carlitos");
		
		this.cc.guardarCliente(new ActionEvent(this.cc.getVentanaCliente().getBtnGuardar(), 1, "guardarCliente"));
		
		listaClientes = c.obtenerClientes();
		
		assertFalse(listaClientes.get(0).getNombre().equals(sinEditar));
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		
	}
	
	@Test
	public void testEditarSinSeleccion()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");		
		
		this.cc.ventanaEditarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnEditar(), 1, "editarCliente"));
		
		listaClientes = c.obtenerClientes();
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
	}
	
	@Test
	public void testEditarConMasSelecciones()
	{
		constructorTest();
		this.agregarCliente("Carlos", "Urtubey", "12153143", "1123456789", "urtudesm@gmail.com");
		this.agregarCliente("Lucho", "Port", "13153143", "1122456789", "luchitom@gmail.com");
		
		this.cc.getVentanaListaDeCliente().getTablaPersonas().setRowSelectionInterval(0, 1);
		this.cc.ventanaEditarCliente(new ActionEvent(this.cc.getVentanaListaDeCliente().getBtnEditar(), 1, "editarCliente"));
		
		this.cc.getCliente().borrarCliente(listaClientes.get(0));
		this.cc.getCliente().borrarCliente(listaClientes.get(1));
	}
	
}
