package presentacion.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dto.StockAutomovilDTO;
import modelo.Automovil;
import modelo.AutomovilTaller;
import modelo.Autoparte;
import modelo.Cliente;
import modelo.Encuesta;
import modelo.EstadoReserva;
import modelo.Garantia;
import modelo.Mantenimiento;
import modelo.Marca;
import modelo.MedioPago;
import modelo.Modelo;
import modelo.OpcionPago;
import modelo.Pais;
import modelo.Reserva;
import modelo.Sucursal;
import modelo.TipoDocumento;
import modelo.Usuario;
import modelo.Venta;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorAlarmaStock;
import presentacion.controlador.ControladorAutopartes;
import presentacion.controlador.ControladorCliente;
import presentacion.controlador.ControladorEncuesta;
import presentacion.controlador.ControladorGeneral;
import presentacion.controlador.ControladorMantenimiento;
import presentacion.controlador.ControladorMarca;
import presentacion.controlador.ControladorModelo;
import presentacion.controlador.ControladorReporte;
import presentacion.controlador.ControladorReserva;
import presentacion.controlador.ControladorStockAutomoviles;
import presentacion.controlador.ControladorSucursal;
import presentacion.controlador.ControladorUsuario;
import presentacion.controlador.ControladorVenta;
import presentacion.controlador.Notificador;
import presentacion.vista.VentanaAutomovil;


class testAutomovil {
	
	private VentanaAutomovil ventana;
	private ControladorStockAutomoviles controlador;
	private ControladorGeneral controladorGeneral;
	
	private ControladorCliente cc;
	private ControladorUsuario cu;
	private ControladorSucursal cs;
	private ControladorReserva cr;
	private ControladorAutopartes ca;
	private ControladorModelo cmodel;
	private ControladorMarca cmarca;
	
	private ControladorAlarmaStock cast;
	private ControladorVenta cv;
	private ControladorMantenimiento cm;
	private ControladorReporte crep;
	private ControladorEncuesta cenc;
	
	private Notificador notificador;
	
	private Cliente c;
	private Sucursal s;
	private Usuario u;
	private Pais p;
	private TipoDocumento td;
	private Marca m;
	private Modelo mo;
	private Automovil as;
	private Reserva r;
	private EstadoReserva er;
	private AutomovilTaller at;
	private Autoparte au;
	private MedioPago mpag;
	private OpcionPago opag;
	private Garantia g;
	
	private Mantenimiento man;
	private Venta v;
	private Encuesta en;
	
	private List<StockAutomovilDTO> listaAutomoviles;
	
	public void constructorTest()
	{
		ventana = VentanaAutomovil.getInstance();
		
		c = new Cliente(new DAOSQLFactory());
		u = new Usuario(new DAOSQLFactory());
		s = new Sucursal(new DAOSQLFactory());
		p = new Pais(new DAOSQLFactory());
		td = new TipoDocumento(new DAOSQLFactory());
		m = new Marca(new DAOSQLFactory());
		mo = new Modelo(new DAOSQLFactory());
		as = new Automovil(new DAOSQLFactory());
		r = new Reserva(new DAOSQLFactory());
		er = new EstadoReserva(new DAOSQLFactory());
		at = new AutomovilTaller(new DAOSQLFactory());
		au = new Autoparte(new DAOSQLFactory());
		
		man = new Mantenimiento(new DAOSQLFactory());
		v = new Venta(new DAOSQLFactory());
		en = new Encuesta(new DAOSQLFactory());
		mpag = new MedioPago(new DAOSQLFactory());
		opag = new OpcionPago(new DAOSQLFactory());
		g = new Garantia(new DAOSQLFactory());
		
		this.cc = new ControladorCliente(c,p,td);
		this.cu = new ControladorUsuario(u,p,td,s);
		this.cs = new ControladorSucursal(s,p);
		this.cr = new ControladorReserva(r,er,at,p,td,m,mo);
		this.ca = new ControladorAutopartes(au,s,m,mo);
		this.cmodel = new ControladorModelo(mo,m);
		this.cmarca = new ControladorMarca(m);
		
		this.cast = new ControladorAlarmaStock(au,as);
		this.cv = new ControladorVenta(mpag,opag,v,g);
		this.cm = new ControladorMantenimiento(man);
		this.crep = new ControladorReporte();
		this.cenc = new ControladorEncuesta(en);
		this.notificador = new Notificador(r);
		
		
		controlador = new ControladorStockAutomoviles(as,m,mo,s);
		controladorGeneral = new ControladorGeneral(cc,cu,cs,cr,controlador,ca,cmodel,cmarca,notificador,cast,cv,cm,crep,cenc);
		listaAutomoviles = new ArrayList<StockAutomovilDTO>();
		controlador.inicializar(controladorGeneral);
		
	}
	
	public void agregarAutoTest()
	{
		this.controlador.ventanaAgregarAutomovil(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnAgregar(),1,""));
	}
	
	@Test
	void testAgregarAutomovil() 
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("4");
		this.controlador.getVentanaAutomovil().setTxtColor("Amarillo");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar+1);
		
		this.controlador.getAutomovil().borrarAutomovil(this.listaAutomoviles.get(this.listaAutomoviles.size()-1));
	}
	
	@Test
	public void testCampoCantidadPuertasVacio()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("");
		this.controlador.getVentanaAutomovil().setTxtColor("Amarillo");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
		
	}
	

	public void testCampoCantidadPuertasLestras()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("dsadfgh");
		this.controlador.getVentanaAutomovil().setTxtColor("Amarillo");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
	}
	
	
	public void testCampoCantidadPuertasTresCifras()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("123");
		this.controlador.getVentanaAutomovil().setTxtColor("Amarillo");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
	}
	
	
	public void testCampoCantidadPuertasSoloEspacio()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("   ");
		this.controlador.getVentanaAutomovil().setTxtColor("Amarillo");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
	}
	
	
	public void testCampoColorVacio()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("3");
		this.controlador.getVentanaAutomovil().setTxtColor("");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
	}
	
	
	public void testCampoColorConNumero()
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		int sinAgregar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("3");
		this.controlador.getVentanaAutomovil().setTxtColor("2345");
		this.controlador.getVentanaAutomovil().setTxtStock("6");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(), sinAgregar);
	}
	
	public void agregarAuto(String cantPuertas, String color, String stock, String anio)
	{
		this.constructorTest();
		this.agregarAutoTest();
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas(cantPuertas);
		this.controlador.getVentanaAutomovil().setTxtColor(color);
		this.controlador.getVentanaAutomovil().setTxtStock(stock);
		this.controlador.getVentanaAutomovil().setTxtYear(anio);
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
	}
	
	
	public void borrarAutomovil()
	{
		agregarAuto("3","violeta","5","1990");
		
		int sinBorrar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaListaDeAutomoviles().getTablaAutomoviles().changeSelection(this.listaAutomoviles.size()-1, 0, false, false);
		
		this.controlador.borrarAutomovil(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnBorrar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(),sinBorrar-1);
	}
	
	public void testBorrarSinSeleccion()
	{
		agregarAuto("3","violeta","5","1990");
		
		this.controlador.borrarAutomovil(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnBorrar(),1,""));
		
		this.controlador.getAutomovil().borrarAutomovil(this.listaAutomoviles.get(this.listaAutomoviles.size()-1));
	}
	
	
	public void testBorrarMasSelecciones()
	{
		agregarAuto("3","violeta","5","1990");
		agregarAuto("5","verde","5","1998");
		
		int sinBorrar = as.obtenerAutomoviles(2).size();
		
		this.controlador.getVentanaListaDeAutomoviles().getTablaAutomoviles().setRowSelectionInterval(3, 4);
		
		this.controlador.borrarAutomovil(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnBorrar(),1,""));
		
		this.listaAutomoviles = as.obtenerAutomoviles(2);
		
		assertEquals(this.listaAutomoviles.size(),sinBorrar-1);
		
		this.controlador.getAutomovil().borrarAutomovil(this.listaAutomoviles.get(this.listaAutomoviles.size()-1));
		
	}
	
	
	public void testEditarAutomovil()
	{
		agregarAuto("3","violeta","5","1990");
		
		this.controlador.getVentanaListaDeAutomoviles().getTablaAutomoviles().changeSelection(this.listaAutomoviles.size()-1, 0, false, false);
		
		this.controlador.ventanaEditarAuto(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnEditar(),1,""));
		
		this.controlador.getVentanaAutomovil().setTxtCantidadPuertas("10");
		this.controlador.getVentanaAutomovil().setTxtColor("Naranja");
		this.controlador.getVentanaAutomovil().setTxtStock("5");
		this.controlador.getVentanaAutomovil().setTxtYear("1990");
		
		this.controlador.guardarAutomovil(new ActionEvent(this.controlador.getVentanaAutomovil().getBtnGuardar(),1,""));
		
		this.listaAutomoviles = this.as.obtenerAutomoviles(2);
		
		assertTrue(this.listaAutomoviles.get(this.listaAutomoviles.size()-1).getCantidadPuertas().equals("10"));
		assertTrue(this.listaAutomoviles.get(this.listaAutomoviles.size()-1).getColor().equals("Naranja"));
		assertTrue(this.listaAutomoviles.get(this.listaAutomoviles.size()-1).getStock() == 5);
		assertTrue(this.listaAutomoviles.get(this.listaAutomoviles.size()-1).getYear().equals("1990"));
		
		this.controlador.getAutomovil().borrarAutomovil(this.listaAutomoviles.get(this.listaAutomoviles.size()-1));
	}
	
	
	public void testEditarSinSeleccion()
	{
		this.constructorTest();
		this.controlador.ventanaEditarAuto(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnEditar(),1,""));
	}
	
	
	public void testEditarMasSelecciones()
	{
		this.constructorTest();
		
		this.controlador.getVentanaListaDeAutomoviles().getTablaAutomoviles().setRowSelectionInterval(0, 1);
		
		this.controlador.ventanaEditarAuto(new ActionEvent(this.controlador.getVentanaListaDeAutomoviles().getBtnEditar(),1,""));
	}
}
