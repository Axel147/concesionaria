CREATE DATABASE  IF NOT EXISTS `multicar` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `multicar`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: multicar
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autoparte`
--

DROP TABLE IF EXISTS `autoparte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoparte` (
  `idautoparte` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  `modelo` int(11) NOT NULL,
  `year` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT NULL,
  `sucursal` int(11) NOT NULL,
  `stockminimo` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  PRIMARY KEY (`idautoparte`),
  KEY `fk_autoparte_tipoautoparte_idx` (`tipo`),
  KEY `fk_autoparte_modelo_idx` (`modelo`),
  KEY `fk_autoparte_sucursal_idx` (`sucursal`),
  KEY `cf_autoparte_tipoautoparte_idx` (`tipo`),
  KEY `cf_autoparte_modelo_idx` (`modelo`),
  KEY `cf_autoparte_sucursal_idx` (`sucursal`),
  KEY `fk_tipoautoparte_autoparte_idx` (`tipo`),
  KEY `fk_modelo_autoparte_idx` (`modelo`),
  KEY `fk_sucursal_autoparte_idx` (`sucursal`),
  CONSTRAINT `fk_modelo_autoparte` FOREIGN KEY (`modelo`) REFERENCES `modeloauto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sucursal_autoparte` FOREIGN KEY (`sucursal`) REFERENCES `sucursal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipoautoparte_autoparte` FOREIGN KEY (`tipo`) REFERENCES `tipoautoparte` (`idtipoautoparte`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoparte`
--

LOCK TABLES `autoparte` WRITE;
/*!40000 ALTER TABLE `autoparte` DISABLE KEYS */;
/*!40000 ALTER TABLE `autoparte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autosenstock`
--

DROP TABLE IF EXISTS `autosenstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autosenstock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` int(11) NOT NULL,
  `year` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  `cantidadPuertas` varchar(45) NOT NULL,
  `stockminimo` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_autostock_modelo_idx` (`modelo`),
  KEY `fk_autostock_sucursal_idx` (`sucursal`),
  CONSTRAINT `fk_autostock_modelo` FOREIGN KEY (`modelo`) REFERENCES `modeloauto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_autostock_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autosenstock`
--

LOCK TABLES `autosenstock` WRITE;
/*!40000 ALTER TABLE `autosenstock` DISABLE KEYS */;
/*!40000 ALTER TABLE `autosenstock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autosentaller`
--

DROP TABLE IF EXISTS `autosentaller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autosentaller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paispatente` int(11) NOT NULL,
  `patente` varchar(45) NOT NULL,
  `modelo` int(11) NOT NULL,
  `year` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_auto_pais_idx` (`paispatente`),
  KEY `fk_auto_modelo_idx` (`modelo`),
  CONSTRAINT `fk_auto_modelo` FOREIGN KEY (`modelo`) REFERENCES `modeloauto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_auto_pais` FOREIGN KEY (`paispatente`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autosentaller`
--

LOCK TABLES `autosentaller` WRITE;
/*!40000 ALTER TABLE `autosentaller` DISABLE KEYS */;
/*!40000 ALTER TABLE `autosentaller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `paisResidencia` int(11) NOT NULL,
  `tipoDocumento` int(11) NOT NULL,
  `numdocumento` varchar(45) NOT NULL,
  `mail` varchar(45) NOT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_pais_cf_idx` (`paisResidencia`),
  KEY `cliente_tipoDoc_cf_idx` (`tipoDocumento`),
  CONSTRAINT `cliente_pais_cf` FOREIGN KEY (`paisResidencia`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cliente_tipoDoc_cf` FOREIGN KEY (`tipoDocumento`) REFERENCES `tipodocumento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailempresa`
--

DROP TABLE IF EXISTS `emailempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailempresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailempresa`
--

LOCK TABLES `emailempresa` WRITE;
/*!40000 ALTER TABLE `emailempresa` DISABLE KEYS */;
INSERT INTO `emailempresa` VALUES (1,'concesionaria.multicar@gmail.com','concemulti4567zzzzz');
/*!40000 ALTER TABLE `emailempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadoreserva`
--

DROP TABLE IF EXISTS `estadoreserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadoreserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `estado_UNIQUE` (`estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadoreserva`
--

LOCK TABLES `estadoreserva` WRITE;
/*!40000 ALTER TABLE `estadoreserva` DISABLE KEYS */;
INSERT INTO `estadoreserva` VALUES (1,'Activa'),(3,'Cancelada'),(2,'Finalizada');
/*!40000 ALTER TABLE `estadoreserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcaauto`
--

DROP TABLE IF EXISTS `marcaauto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcaauto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marca` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `marca_UNIQUE` (`marca`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcaauto`
--

LOCK TABLES `marcaauto` WRITE;
/*!40000 ALTER TABLE `marcaauto` DISABLE KEYS */;
/*!40000 ALTER TABLE `marcaauto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modeloauto`
--

DROP TABLE IF EXISTS `modeloauto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modeloauto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(45) NOT NULL,
  `marca` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modeloauto_marcaauto_cf_idx` (`marca`),
  CONSTRAINT `modeloauto_marcaauto_cf` FOREIGN KEY (`marca`) REFERENCES `marcaauto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modeloauto`
--

LOCK TABLES `modeloauto` WRITE;
/*!40000 ALTER TABLE `modeloauto` DISABLE KEYS */;
/*!40000 ALTER TABLE `modeloauto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pais_UNIQUE` (`pais`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Argentina'),(4,'Chile'),(3,'Paraguay'),(2,'Uruguay');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva`
--

DROP TABLE IF EXISTS `reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaEmitida` date NOT NULL,
  `fechaTurno` date NOT NULL,
  `horario` varchar(5) NOT NULL,
  `cliente` int(11) NOT NULL,
  `auto` int(11) NOT NULL,
  `notificada` tinyint(4) NOT NULL,
  `estado` int(11) NOT NULL,
  `motivo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reserva_cliente_cf_idx` (`cliente`),
  KEY `reserva_estado_cf_idx` (`estado`),
  KEY `reserva_auto_cf_idx` (`auto`),
  CONSTRAINT `reserva_auto_cf` FOREIGN KEY (`auto`) REFERENCES `autosentaller` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reserva_cliente_cf` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reserva_estado_cf` FOREIGN KEY (`estado`) REFERENCES `estadoreserva` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva`
--

LOCK TABLES `reserva` WRITE;
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` VALUES (1,'2020-10-10','2020-10-15','12:00',1,1,0,1,NULL),(3,'2020-11-25','2020-11-27','01:30',2,6,0,1,''),(4,'2020-11-25','2020-11-29','02:30',3,7,0,3,''),(5,'2020-11-25','2020-11-29','07:00',1,8,0,3,NULL),(6,'2020-11-25','2020-11-30','21:00',2,9,0,3,NULL),(7,'2020-11-25','2020-12-17','02:00',3,10,0,3,'x que quise'),(8,'2020-11-25','2020-12-31','00:00',2,1,0,3,'');
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `calle` varchar(45) NOT NULL,
  `altura` varchar(45) NOT NULL,
  `pais` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  KEY `fk_sucursal_pais_idx` (`pais`),
  KEY `cf_sucursal_pais_idx` (`pais`),
  CONSTRAINT `cf_sucursal_pais` FOREIGN KEY (`pais`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursal`
--

LOCK TABLES `sucursal` WRITE;
/*!40000 ALTER TABLE `sucursal` DISABLE KEYS */;
/*!40000 ALTER TABLE `sucursal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoautoparte`
--

DROP TABLE IF EXISTS `tipoautoparte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoautoparte` (
  `idtipoautoparte` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `tienecolor` tinyint(4) NOT NULL,
  PRIMARY KEY (`idtipoautoparte`),
  UNIQUE KEY `idtipoautoparte_UNIQUE` (`idtipoautoparte`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoautoparte`
--

LOCK TABLES `tipoautoparte` WRITE;
/*!40000 ALTER TABLE `tipoautoparte` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoautoparte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoDocumento` varchar(45) NOT NULL,
  `paisdocumento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipoDocumento_UNIQUE` (`tipoDocumento`),
  KEY `tipodocumento_pais_cf_idx` (`paisdocumento`),
  CONSTRAINT `tipodocumento_pais_cf` FOREIGN KEY (`paisdocumento`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
INSERT INTO `tipodocumento` VALUES (1,'DNI',1),(2,'CU',2),(3,'CP',3),(4,'RUT',4),(5,'PASAPORTE',NULL);
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombretipo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombretipo_UNIQUE` (`nombretipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` VALUES (1,'Administrador'),(2,'Tecnico');
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `sexo` enum('Masculino','Femenino') NOT NULL,
  `paisresidencia` int(11) NOT NULL,
  `tipodoc` int(11) NOT NULL,
  `numdocumento` varchar(45) NOT NULL,
  `nombreusuario` varchar(45) NOT NULL,
  `mail` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `tipousr` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombreusuario_UNIQUE` (`nombreusuario`),
  UNIQUE KEY `numdocumento_UNIQUE` (`numdocumento`),
  KEY `tipouser_idx` (`tipousr`),
  KEY `fk_pais_usuario_idx` (`paisresidencia`),
  KEY `fk_tipodoc_usuario_idx` (`tipodoc`),
  CONSTRAINT `fk_pais_usuario` FOREIGN KEY (`paisresidencia`) REFERENCES `pais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipodoc_usuario` FOREIGN KEY (`tipodoc`) REFERENCES `tipodocumento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tipouser` FOREIGN KEY (`tipousr`) REFERENCES `tipousuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-27  7:39:35
