insert into tipoautoparte values (1,"Motor",false), (2,"Puerta Delantera Derecha",true), (3,"Puerte Trasera Derecha",true), (4,"Puerta Trasera Izquierda",true), (5,"Puerta Delantera Izquierda",true), (6,"Paragolpes Delantero",false), (7,"Paragolpes Trasero",false), (8,"Espejo Retrovisor",false);
insert into marcaauto values (1,"Fiat"), (2,"Renault"), (3,"Peugeot"), (4,"Audi");
insert into modeloauto values (1,"500",1), (2,"UNO",1), (3,"Kangoo II",2), (4,"Partnet",3), (5,"208",3), (6,"308",3), (7,"A4",4);
insert into sucursal values(1,"San Miguel","Av. Balbin","800",1), (2,"Bella Vista","Senador Moron","120",1);
